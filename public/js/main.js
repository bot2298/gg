$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("a.scroll-link").on('click', function (event) {
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function () {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });
});
$(document).ready(function () {
    $(".textarea-container").each(function () {
        $(this).resizable({
            minHeight: 100,
            minWidth: 200,
        });
    });
    $('.password-toggle').on('click', function () {
        let input = $(this).siblings('input');
        if (input.attr('type') === "password") {
            input.attr('type', 'text');
        } else {
            input.attr('type', 'password');
        }
    });

    $(".cabinet-form-title").click(function () {
        $(this).find('.btn-drop-down-form').toggleClass("down");
    })

    function init() {
        $('.dropdown-list').hide();
        $('.custom-dropdown').each(function () {
            let def = $(this).find('.dropdown-list-item.dropdown-default');
            $(this).find('.dropdown-selected').html(def.html());
            $(this).find('input').val(def.data('value'));
        });
        $('.dropdown-block').on('click', function () {
            let list = $(this).find('.dropdown-list');
            list.toggle();
        });
    }

    init();

    $('.dropdown-list-item').on('click', function () {
        let dropdownblock = $(this).parent().parent();
        let input = dropdownblock.siblings('input');
        let selected = dropdownblock.find('.dropdown-selected');
        selected.html($(this).html());
        input.val($(this).data('value'));
    });

//advanced search

    function searchInit() {
        $('.advanced-search-list').hide();
        $('.advanced-search-title').on('click', function () {
            let list = $(this).siblings('.advanced-search-list');
            list.toggle();
        });
        $('.search-section-dropdown .search-section-dropdown-list').hide();
        $('.search-section-dropdown-selected').on('click', function () {
            $(this).siblings('.search-section-dropdown-list').toggle();
        });
        $('.search-section-dropdown').each(function () {
            let selected = $(this).find('.search-section-dropdown-selected');
            let input = $(this).find('input');
            let def = $(this).find('.search-section-dropdown-item.advanced-search-default');
            input.val(def.data('value'));
            selected.html(def.html());
            $(this).find('.search-section-dropdown-item').each(function () {
                $(this).on('click', function () {
                    let dropdownblock = $(this).parent().parent();
                    selected.html($(this).html());
                    input.val($(this).data('value'));
                    dropdownblock.find('.search-section-dropdown-list').hide();
                });
            });
        });
    }

    searchInit();

    $('.advanced-search-title').on('click', function () {
        $('.dropdown-search-list').toggle();
    });


    $('.request-main-content').on('click', function (e) {
        window.location.href = '/' + $(this).data('locale') + '/cabinet/request/' + $(this).data('id');
    });

    $('.rating-main-content').on('click', function () {
        window.location.href = '/' + $(this).data('locale') + '/cabinet/rating/' + $(this).data('id');
    });

    $(document).ready(function () {
        $('.js-example-basic-single').select2({
            dropdownPosition: 'below',
            width: '100%'
        });
    });

    $('#country-select').on('change', function () {
        $.post('/' + $(this).data('locale') + "/request/regions", {country_id: $(this).children("option:selected").val()})
            .done(function (data) {
                if(data){
                    let regions = JSON.parse(data),
                        $select = $('#region-select');
                    if ($select.hasClass('select2-hidden-accessible')) {
                        $select.select2('destroy');
                    }
                    $select.find('option:not(:first-child)').remove(); // keep the default
                    regions.forEach(function (region) {
                        $('#region-select').append($("<option></option>").val(region.id).html(region.name));
                    });
                    $select.prop('disabled', false);
                    if ($('#region-select').data('old')) { // if validation fails or edit
                        $('#region-select').val($('#region-select').data('old')).change();
                    }
                    $select.select2({
                        width: '100%',
                        dropdownPosition: 'below',
                        // minimumResultsForSearch: Infinity,
                    });
                }
            });
    });

    if ($('#country-select').data('old')) { // if validation fails or edit
        $('#country-select').val($('#country-select').data('old')).change();
    }

    $('#region-select').on('change', function () {
        $.post('/' + $(this).data('locale') + "/request/cities", {region_id: $(this).children("option:selected").val()})
            .done(function (data) {
                let cities = JSON.parse(data),
                    $select = $('#city-select');
                $select.select2('destroy');
                $select.find('option:not(:first-child)').remove(); // keep the default
                $select.prop('disabled', !cities.length);
                cities.forEach(function (city) {
                    $('#city-select').append($("<option></option>").val(city.id).html(city.name));
                });

                if ($select.data('old')) { // if validation fails or edit
                    $select.val($select.data('old')).change();
                }

                $select.select2({
                    width: '100%',
                    dropdownPosition: 'below',
                });

                if ($select.data('old-arr')) { // multi select if validation fails or edit
                    let arr = $select.data('old-arr');
                    $select.val(arr);
                    $select.trigger('change');
                }
            });
    });

    $('.request-verified-code').html(function (index, currentcontent) { // code with spaces
        let reformat = currentcontent.replace(/(\d{4})/g, function (match) {
            return match + " ";
        });
        return reformat;
    });

    $("#file-upload").change(function () {
        $('#attachment-file-name').html(this.files[0].name);
    });

    $('#delete-multiple').on('click', function () {
        if (!confirm($(this).data('confirm'))) {
            return;
        }
        let ids = [],
            form = $('#mass-delete-form');
        form.find('.input-id').remove();
        $('.table-row').each(function () {
            let id = $(this).find('.request-main-content').data('id');
            if ($(this).find('input:checked').length) {
                ids.push(id);
            }
        });

        if (!ids.length) {
            return;
        }
        ids.forEach(function (id) {
            $('<input>', {
                type: 'hidden',
                class: 'input-id',
                name: 'id[]',
                value: id
            }).appendTo(form);
        });
        form.submit();
    });

    $('#bookmarks-delete-multiple').on('click', function () {
        if (!confirm($(this).data('confirm'))) {
            return;
        }
        let ids = [],
            form = $('#mass-delete-form');
        form.find('.input-id').remove();
        $('.table-row').each(function () {
            let id = $(this).find('.request-main-content').data('id');
            if ($(this).find('input:checked').length) {
                ids.push(id);
            }
        });
        if (!ids.length) {
            return;
        }
        ids.forEach(function (id) {
            $('<input>', {
                type: 'hidden',
                class: 'input-id',
                name: 'id[]',
                value: id
            }).appendTo(form);
        });
        form.submit();
    });

    $('.application-toggle-bookmark').on('click', function () {
        let $element = $(this);
        $.post('/' + $element.parent().data('locale') + "/cabinet/bookmarks/toggle", {id: $element.parent().data('id')})
            .done(function (data) {
                if (data.status === 1) {
                    $(`[data-id='${$element.parent().data('id')}']`).toggleClass('in-bookmarks');
                }
            });
    });

    $('#cabinet-lang-select').on('select2:selecting', function (e) {
        if(!confirm($(this).data('conform'))) {
            e.preventDefault();
        }
    });

    $('#cabinet-lang-select').on('select2:select', function (e) {
        window.document.location.href=this.options[this.selectedIndex].value;
    });

    $('#cabinet-years-select').select2({
        dropdownPosition: 'below',
        width: '100%'
    });

    $('#country-select-cabinet-general').select2({
        width: '100%',
        dropdownPosition: 'below',
        minimumResultsForSearch: Infinity,
    });

    $('#country-select-cabinet-general').on('change', function () {
        $.post('/' + $(this).data('locale') + "/request/regions", {country_id: $(this).children("option:selected").val()})
            .done(function (data) {
                if(data){
                    let regions = JSON.parse(data),
                        $select = $('#region-select-cabinet-general');
                    if ($select.hasClass('select2-hidden-accessible')) {
                        $select.select2('destroy');
                    }
                    $select.find('option:not(:first-child)').remove(); // keep the default
                    regions.forEach(function (region) {
                        $('#region-select-cabinet-general').append($("<option></option>").val(region.id).html(region.name));
                    });
                    $select.prop('disabled', false);
                    if ($('#region-select-cabinet-general').data('old')) { // if validation fails or edit
                        $('#region-select-cabinet-general').val($('#region-select-cabinet-general').data('old')).change();
                    }
                    $select.select2({
                        width: '100%',
                        dropdownPosition: 'below',
                        minimumResultsForSearch: Infinity,
                    });
                }
            });
    });

    if ($('#country-select-cabinet-general').data('old')) { // if validation fails or edit
        $('#country-select-cabinet-general').val($('#country-select-cabinet-general').data('old')).change();
    }

    $('#region-select-cabinet-general').on('change', function () {
        $.post('/' + $(this).data('locale') + "/request/cities", {region_id: $(this).children("option:selected").val()})
            .done(function (data) {
                let cities = JSON.parse(data),
                    $select = $('#city-select-cabinet-general');
                $select.select2('destroy');
                $select.find('option:not(:first-child)').remove(); // keep the default
                $select.prop('disabled', !cities.length);
                cities.forEach(function (city) {
                    $('#city-select-cabinet-general').append($("<option></option>").val(city.id).html(city.name));
                });

                if ($select.data('old')) { // if validation fails or edit
                    $select.val($select.data('old')).change();
                }

                $select.select2({
                    width: '100%',
                    dropdownPosition: 'below',
                });

                if ($select.data('old-arr')) { // multi select if validation fails or edit
                    let arr = $select.data('old-arr');
                    $select.val(arr);
                    $select.trigger('change');
                }
            });
    });

    if ($('#country-select-cabinet-license').data('old')) { // if validation fails or edit
        $('#country-select-cabinet-license').val($('#country-select-cabinet-license').data('old')).change();
    }

    if ($('#country-select-cabinet-patent_license').data('old')) { // if validation fails or edit
        $('#country-select-cabinet-patent_license').val($('#country-select-cabinet-patent_license').data('old')).change();
    }


    $('#rating-submit').on('click', function () {
        let user_input = $(document).find('#rate-lawyer-modal [name=user]'),
            text_input = $(document).find('#rate-lawyer-modal [name=text]'),
            stars_input = $(document).find('#rate-lawyer-modal [name=rating]');
        $.ajax({
            type: "post",
            url: "/" + $(this).data('locale') + "/tracker/rate",
            data: {
                user: user_input.val(),
                text: text_input.val(),
                rating: stars_input.val()
            },
            dataType: 'json',              // let's set the expected response format
            success: function (data) {
                $('#rate-lawyer-modal').modal('hide');
                if (data.success === true){
                    $('#rate-lawyer-thanks').modal('show');
                } else {
                    $('#rate-lawyer-error').modal('show');
                }
            },
            error: function (err) {
                if (err.status == 422) { // when status code is 422, it's a validation issue
                    console.log(err.responseJSON);
                    $.each(err.responseJSON.errors, function (i, error) {
                        let el = $(document).find('.' + i + '-error');
                        el.html(error[0])
                    });
                } else {
                    $('#rate-lawyer-modal').modal('hide');
                    $('#rate-lawyer-error').modal('show');
                }
            }
        });
    });

    function rateResetInput() {
        $(document).find('#rate-lawyer-modal [name=text]').val('');
        $(document).find('#rate-lawyer-modal [name=rating]').val('');
        $(document).find('#rate-lawyer-modal .modal-validation-error').html('');
        $('.stars-input:radio').prop('checked', false);
    }

    $('#rate-lawyer-modal').on('hidden.bs.modal', function (e) {
        rateResetInput();
    });
    $('.stars-input:radio').change(function () {
        $(document).find('#rate-lawyer-modal [name=rating]').val(this.value)
    });

    $('#report-application-submit').on('click', function () {
        let text_input = $(document).find('#application-report-modal [name=text]');
        $.ajax({
            type: "post",
            url: "/" + $(this).data('locale') + "/cabinet/request/complaint",
            data: {
                application_id: $('#modal-application-id').val(),
                text: text_input.val()
            },
            dataType: 'json',              // let's set the expected response format
            success: function (data) {
                $('#application-report-modal').modal('hide');
                $('#report-success').modal('show');
            },
            error: function (err) {
                if (err.status == 422) { // when status code is 422, it's a validation issue
                    console.log(err.responseJSON);
                    $.each(err.responseJSON.errors, function (i, error) {
                        let el = $(document).find('.' + i + '-error');
                        el.html(error[0])
                    });
                } else if(err.status == 403){ //duplicate
                    $('#application-report-modal').modal('hide');
                    $('#report-duplicate').modal('show');
                } else {
                    $('#application-report-modal').modal('hide');
                    $('#report-error').modal('show');
                }
            }
        });
    });

    function applicationResetInput() {
        $(document).find('#application-report-modal [name=text]').val('');
    }

    $('#report-application-submit').on('hidden.bs.modal', function (e) {
        applicationResetInput();
    });

    $('#report-rating-submit').on('click', function () {
        let text_input = $(document).find('#rating-report-modal [name=text]');

        $.ajax({
            type: "post",
            url: "/" + $(this).data('locale') + "/cabinet/rating/complaint",
            data: {
                rating_id: $('#modal-rating-id').val(),
                text: text_input.val()
            },
            dataType: 'json',              // let's set the expected response format
            success: function (data) {
                $('#rating-report-modal').modal('hide');
                $('#report-success').modal('show');
            },
            error: function (err) {
                if (err.status == 422) { // when status code is 422, it's a validation issue
                    console.log(err.responseJSON);
                    $.each(err.responseJSON.errors, function (i, error) {
                        let el = $(document).find('.' + i + '-error');
                        el.html(error[0])
                    });
                } else if(err.status == 403){ //duplicate
                    $('#rating-report-modal').modal('hide');
                    $('#report-duplicate').modal('show');
                } else {
                    $('#rating-report-modal').modal('hide');
                    $('#report-error').modal('show');
                }
            }
        });
    });

    $('#report-lawyer-submit').on('click', function () {
        let text_input = $(document).find('#lawyer-report-modal [name=text]'),
            fullname = $(document).find('#lawyer-report-modal [name=fullname]'),
            address = $(document).find('#lawyer-report-modal [name=address]'),
            email = $(document).find('#lawyer-report-modal [name=email]'),
            number = $(document).find('#lawyer-report-modal [name=number]'),
            file_input = $(document).find('#lawyer-report-modal [name=attachment]'),
            data = new FormData();
        data.append('lawyer_id', $('#modal-lawyer-id').val());
        data.append('text', text_input.val());
        data.append('fullname', fullname.val());
        data.append('address', address.val());
        data.append('email', email.val());
        data.append('number', number.val());
        data.append('attachment', file_input[0].files[0]);
        $('#lawyer-report-modal .modal-validation-error').html('');
        $.ajax({
            type: "post",
            url: "/" + $(this).data('locale') + "/tracker/complaint",
            data: data,
            dataType: 'json',              // let's set the expected response format
            processData: false,
            contentType: false,
            success: function (data) {
                $('#lawyer-report-modal').modal('hide');
                $('#report-success').modal('show');
            },
            error: function (err) {
                if (err.status == 422) { // when status code is 422, it's a validation issue
                    $.each(err.responseJSON.errors, function (i, error) {
                        let el = $(document).find('.' + i + '-error');
                        el.html(error[0])
                    });
                } else if(err.status == 403){ //duplicate
                    $('#lawyer-report-modal').modal('hide');
                    $('#report-duplicate').modal('show');
                } else {
                    $('#lawyer-report-modal').modal('hide');
                    $('#report-error').modal('show');
                }
            }
        });
    });

    $('.dropdown-menu').on('click', function (event) {
        if ($(event.target).hasClass('select2-selection__rendered')) {
            event.stopPropagation();
            $('#select-lang').on('select2:select', function (e) {
                $("div .dropdown-menu").removeClass('show');
            });
            $('#select-years').on('select2:select', function (e) {
                $("div .dropdown-menu").removeClass('show');
            });
            $('#select-specialization').on('select2:select', function (e) {
                $("div .dropdown-menu").removeClass('show');
            });
            $('#select-rating').on('select2:select', function (e) {
                $("div .dropdown-menu").removeClass('show');
            });
        }
    });

    $('.request-form-textarea').on('input', function () {
        let max = $(this).data('max'),
            length = $(this).val().trim().length;
        $('.textarea-helpers span').html(max - length > 0 ? max - length : 0)
    });
    $('#country-select').trigger('change');
    $('.request-form-textarea').trigger('input'); // if validation fails

    $('.custom-select:not(.select2-hidden-accessible)').select2({
        width: '100%',
        // minimumResultsForSearch: Infinity,
        dropdownPosition: 'below',
    });

    $("#multi-select-cabinet-form").click(function () {
        $('.custom-select:not(.select2-hidden-accessible)').select2({
            width: '100%',
            // minimumResultsForSearch: Infinity,
            dropdownPosition: 'below',
        });
    });

    $('.filters-custom-select').select2({
        width: '100%',
        // minimumResultsForSearch: Infinity,
        dropdownPosition: 'below',
        dropdownCssClass: 'filters-select-dropdown'
    });

    $('.cabinet-request-reply').on('click', function () {
        $(this).toggleClass('active');
        $('#cabinet-reply-block').slideToggle(500);
        if ($(this).hasClass('active')) {
            $('html, body').animate({
                scrollTop: $('#cabinet-reply-block').offset().top
            }, 800);
            $('.cabinet-reply-textarea').focus();
        }
    });

    $('.open-side-navigation').on('click', function () {
        $('.sidebar-navigation-mobile').css("width", "80%");
        $('.sidebar-navigation-mobile').addClass('show');
        $('.sidebar-navigation-mobile .fade-out').css("opacity", "1");
    });

    $('.close-side-navigation').on('click', function() {
        $('.sidebar-navigation-mobile').css("width", "0");
        $('.sidebar-navigation-mobile').removeClass('show');
        $('.sidebar-navigation-mobile .fade-out').css("opacity", "0");
    });

    $('.table-checkmark input').change(function () {
        $(this).parent().parent().parent().toggleClass('selected');
    });

    function watchForHover() {
        // lastTouchTime is used for ignoring emulated mousemove events
        // that are fired after touchstart events. Since they're
        // indistinguishable from real events, we use the fact that they're
        // fired a few milliseconds after touchstart to filter them.
        let lastTouchTime = 0

        function enableHover() {
            if (new Date() - lastTouchTime < 500) return
            document.body.classList.add('hasHover')
        }

        function disableHover() {
            document.body.classList.remove('hasHover')
        }

        function updateLastTouchTime() {
            lastTouchTime = new Date()
        }

        document.addEventListener('touchstart', updateLastTouchTime, true)
        document.addEventListener('touchstart', disableHover, true)
        document.addEventListener('mousemove', enableHover, true)

        enableHover()
    }

    watchForHover()

    $('.request-info-toggle').on('click', function () {
        $('.request-info-container').slideToggle(500);
        $(this).toggleClass('collapsed');
    });


    $('.about-helper').on('click', function () {
        window.location.href = $(this).data('href');
    });

    $('.dossier-single-bookmark .bookmark-add').on('click', function(){
        let parent = $(this).parent(),
            id = parent.data('id');
        $.post( `/dossier/${id}/bookmark/add`, function(response) {
            parent.addClass('in-bookmarks');
            if(response.count > 0){
                $('#header-bookmark-toggle').addClass('active');
            }
        });
    })

    $('.dossier-single-bookmark .bookmark-remove').on('click', function(){
        let parent = $(this).parent(),
            id = parent.data('id');
        $.post( `/dossier/${id}/bookmark/delete`, function(response) {
            parent.removeClass('in-bookmarks');
            if(!response.count > 0){
                $('#header-bookmark-toggle').removeClass('active');
            }
        });
    })

    $('.settings-email-input img').on('click', function() {
        let $input = $('#settings-email');
        $input.attr('readonly', !$input.attr('readonly'));
    })
    $("#inputPhone").mask("+380 (99) 99-99-999");
    $("#personal-code").mask("9999 9999 9999");
    $('#code-input-mask').mask('9999 9999 9999');
    $("#code-input-mask").on('click', function() {
        function setCaretPosition(el, caretPos) {
            if (el !== null) {

                if (el.createTextRange) {
                    var range = el.createTextRange();
                    range.move('character', caretPos);
                    range.select();
                    return true;
                } else {
                    if (el.selectionStart || el.selectionStart === 0) {
                        el.focus();
                        el.setSelectionRange(caretPos, caretPos);
                        return true;
                    } else { // fail city, fortunately this never happens (as far as I've tested) :)
                        el.focus();
                        return false;
                    }
                }
            }
        }
        if($(this).val() === '____ ____ ____'){
            setCaretPosition(this, 0)
        }
    })
});
