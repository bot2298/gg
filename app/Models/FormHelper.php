<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormHelper
{
    public function getDegree()
    {
        return [
            ['name' => 'junior_bachelor'], ['name' => 'bachelor'], ['name' => 'master'], ['name' => 'specialist'],
            ['name' => 'doctor_of_philosophy'], ['name' => 'doctor_of_science'], ['name' => 'other'],
        ];
    }

    public function getField()
    {
        return [
            ['name' => 'legal'], ['name' => 'economic'], ['name' => 'historical'], ['name' => 'biological'],
            ['name' => 'veterinary'], ['name' => 'military'], ['name' => 'geographic'],
            ['name' => 'public_administration'], ['name' => 'culturology'], ['name' => 'medical'],
            ['name' => 'art_history'], ['name' => 'pedagogical'], ['name' => 'political'], ['name' => 'psychological'],
            ['name' => 'agricultural'], ['name' => 'social_communications'], ['name' => 'technical'],
            ['name' => 'pharmaceutical'], ['name' => 'physics_and_mathematics'],
            ['name' => 'physical_education_and_sports'], ['name' => 'philological'], ['name' => 'philosophical'],
            ['name' => 'chemical']
        ];
    }

    public function getAcademicStatusDegree()
    {
        return [
            ['name' => 'professor'], ['name' => 'associate_professor'], ['name' => 'senior_researcher'],
        ];
    }

    public function getActivities()
    {
        return [
            ['name' => 'consultations'], ['name' => 'accompaniment'], ['name' => 'preparation_doc'],
            ['name' => 'representation'], ['name' => 'other']
        ];
    }

    public function getForm()
    {
        return [
            ['name' => 'individually'], ['name' => 'bureau'], ['name' => 'association']
        ];
    }

    public function getExp()
    {
        return [
            ['name' => 'available'], ['name' => 'missing']
        ];
    }
}
