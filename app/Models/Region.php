<?php

namespace App\Models;

use App\Models\Country;
use App\Models\Dossier;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\City;
use phpDocumentor\Reflection\Types\This;

/**
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 * @property \App\Models\Country $country
 * @property array $cities
 *
 */

class Region extends Model
{
    use CrudTrait;

    protected $appends = [
        'active_until'
    ];

    protected $fillable = [
        'name',
        'country_id',
        'cost'
    ];

    public static function citiesOfRegionCount($id)
    {
        return is_null($id) ? 1 : self::find($id)->cities()->get()->count();
    }

    public function country(){
        return $this->belongsTo('App\Models\Country');
    }

    public function cities(){
        return $this->hasMany('App\Models\City');
    }

    public function access(){
        return $this->hasMany('App\Models\Access');
    }

    public function getActiveUntilAttribute(){
        $active_until = $this->access()->where('user_id', auth()->id())->pluck('active_until');
        return count($active_until) ? $active_until[0] : 0;
    }

    /**
     * The dossiers that belong to the region.
     */
    public function dossiers()
    {
        return $this->belongsToMany(Dossier::class, 'dossier_region');
    }
}
