<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PageInfo
 * @package App\Models
 *
 *
 * @property string $locale
 * @property string $title
 * @property string $description
 * @property \App\Models\Page $page
 *
 */

class PageInfo extends Model
{
    use CrudTrait;

    protected $table = 'page_info';
    protected $fillable = [
        'page_id',
        'locale',
        'title',
        'description',
        'h1'
    ];

    public function page(){
        return $this->belongsTo(\App\Models\Page::class);
    }
}
