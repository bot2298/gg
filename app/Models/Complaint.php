<?php

namespace App\Models;

use App\Models\Dossier;
use App\Models\Application;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Complaint
 * @package App
 *
 * @property integer $id
 * @property integer $type
 * @property string $client_email
 * @property Dossier $dossier
 * @property Application $application
 * @property string $response
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 */

class Complaint extends Model
{
    use CrudTrait;

    const TYPE_LAWYER = 10;
    const TYPE_APPLICATION = 20;
    const TYPE_RATING = 30;

    protected $fillable = [
        'type',
        'application_id',
        'rating_id',
        'user_id',
        'content',
        'client_email',
        'response',
        'locale',
        'ip',
        'fullname',
        'address',
        'contact_email',
        'contact_number',
        'attachment'
    ];

    public static function getNewCount($type){
        return Complaint::whereNull('seen_at')
            ->where('type', $type)
            ->count();
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function application(){
        return $this->belongsTo('App\Models\Application');
    }

    public function rating(){
        return $this->belongsTo('App\Models\Rating');
    }

}
