<?php

namespace App\Models;

use App\Carbon;
use App\Models\User;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Raiting
 * @package App
 *
 * @property string $client_email
 * @property User $user
 * @property integer $stars
 * @property string $comment
 * @property Carbon $blocked_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */

class Rating extends Model
{
    use CrudTrait;

    protected $fillable = [
        'user_id',
        'comment',
        'stars',
        'client_email'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function dateCreated(){
        return $this->created_at->format('d.m.Y');
    }

    public function timeCreated(){
        return $this->created_at->format('H:i');
    }

    public static function getCountNew(){
        return Rating::where('user_id', auth()->id())
            ->whereNull('blocked_at')
            ->where('client_email', '<>', 'example@mail.com') // default rating
            ->whereNotIn('id', auth()->user()->getSeenRatings())
            ->count();
    }


}
