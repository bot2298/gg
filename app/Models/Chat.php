<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use CrudTrait;

    protected $fillable = [
        'user_id',
        'application_id',
        'admin'
    ];

    public function messages()
    {
        return $this->hasMany('App\Models\Message');
    }

    public function application()
    {
        return $this->belongsTo('App\Models\Application');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
