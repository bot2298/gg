<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed id
 */
class Experience extends Model
{
    protected $guarded = [];

    protected $casts = [
        'countries' => 'array',
        'regions' => 'array',
        'cities' => 'array',
    ];

    public function dossier()
    {
        return $this->belongsTo('App\Models\Dossier');
    }
}
