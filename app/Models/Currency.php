<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Currency
 * @package App
 *
 * @property string $name
 * @property float $rate
 */

class Currency extends Model
{
    use CrudTrait;

    protected $table = 'currencies';
    protected $fillable = [
        'rate'
    ];

    public function getName(){
        switch($this->name){
            case 'UAH': return '₴';
            case 'USD': return '$';
            case 'EUR': return '€';
            default: return '';
        }
    }

    public static function getAllAndPrepare()
    {
        $result = [];
        foreach (self::all() as $currency) {
            $result[] = $currency->getName();
        }
        return $result;
    }
}
