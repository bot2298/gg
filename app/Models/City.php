<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Region;
use App\Models\Country;

/**
 * @property integer $id
 * @property integer $region_id
 * @property string $name
 * @property Region $region
 *
 */


class City extends Model
{
    use CrudTrait;

    protected $table = 'cities';

    protected $fillable = [
        'region_id',
        'name'
    ];

    public function region(){
        return $this->belongsTo('App\Models\Region');
    }

}
