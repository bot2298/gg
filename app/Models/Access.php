<?php

namespace App\Models;

use App\Models\City;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Access
 * @package App
 *
 * @property App\Models\Region $region
 * @property App\Models\User $user
 * @property array $cities
 * @property Carbon $active_until
 *
 */

class Access extends Model
{
    protected $table = 'access';

    protected $casts = [
        'cities' => 'array',
        'active_until' => 'datetime:d.m.Y',
    ];

    protected $fillable = [
        'region_id',
        'user_id',
        'cities'
    ];

    public function region(){
        return $this->belongsTo('App\Models\Region');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function getCities(){
        if(is_array($this->cities)){
            $city_names = [];
            foreach($this->cities as $city_id){
                if($city = City::find($city_id)){
                    $city_names[] = __('cities.'.$city->name);
                }
            }
            return implode(', ', $city_names);
        }
        return $this->cities;
    }
}
