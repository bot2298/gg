<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Page
 * @package App
 *
 * @property integer $id
 * @property string $location
 * @property string $name
 * @property Collection $info
 */

class Page extends Model
{
    use CrudTrait;

    public function region(){
        return $this->belongsTo('App\Models\Region');
    }

    public function specialization(){
        return $this->belongsTo('App\Models\Specialization');
    }

    public static function findByLocation($location){
        return Page::where('location', $location)->first();
    }

    public function info(){
        return $this->hasMany(\App\Models\PageInfo::class);
    }

    public static function getTitle($location){
        if($page = Page::findByLocation($location)){
            if($info = $page->info->where('locale', app()->getLocale())->first()) {
                if($info->title)
                    return $info->title;
            } elseif ($page->specialization || $page->region) { // category page
                $title  = config('app.name')." | ".__('site.dossier.title_lawyers');
                if($page->region){
                    $title.= __('site.dossier.title_lawyers_region').__('regions.'.$page->region->name);
                }
                if($page->specialization){
                    $title.= __('site.dossier.title_lawyers_spec').__('specializations.'.$page->specialization->name);
                }
                return $title;
            }
        }
        return config('app.name')." | ".__('layout.title.'.$location);
    }

    public static function getH1($location){
        $h1 = null;
        if($page = Page::findByLocation($location)){
            $h1_default = __('site.dossier.title_lawyers');
            if($page->region){
                $h1_default.= __('site.dossier.title_lawyers_region').__('regions.'.$page->region->name);
            }
            if($page->specialization){
                $h1_default.= __('site.dossier.title_lawyers_spec').__('specializations.'.$page->specialization->name);
            }
            $h1 = $h1_default;

            if($info = $page->info->where('locale', app()->getLocale())->first())
            {
                if ($info->h1) {
                    $h1 = $info->h1;
                }
            }
        }

        return $h1;
    }

    public static function getDescription($location){
        if($page = Page::findByLocation($location)){
            if($info = $page->info->where('locale', app()->getLocale())->first())
            {
                return $info->description;
            }
        }
        return null;
    }

}
