<?php

namespace App\Models;

use App\Carbon;
use Backpack\CRUD\CrudTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 * @package App
 *
 * @property array $applications_seen
 * @property array $applications_deleted
 * @property array $ratings_seen
 * @property array $access
 * @property Carbon $blocked_at
 *
 */

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use CrudTrait;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'api_token',
    ];

    protected $appends = [
        'rating'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'blocked_at' => 'datetime',
        'applications_seen' => 'array',
        'applications_deleted' => 'array',
        'ratings_seen' => 'array'
    ];


    public function dossiers()
    {
        return $this->hasMany('App\Models\Dossier');
    }

    public function applications(){
        return $this->hasMany('App\Models\Application');
    }

    public function bookmarks(){
        return $this->hasMany('App\Models\ApplicationBookmark');
    }

    public function getBookmarks(){
        return $this->bookmarks->pluck('application_id');
    }

    public function access(){
        return $this->hasMany('App\Models\Access');
    }

    public function chats()
    {
        return $this->hasMany('App\Models\Chat');
    }

    public function messages(){
        return $this->hasManyThrough('App\Models\Message', 'App\Models\Chat');
    }

    public function ratings(){
        return $this->hasMany('App\Models\Rating');
    }

    public function getActiveRegions(){
        return $this->access->where('active_until', '>', \Illuminate\Support\Carbon::now())->pluck('region_id');
    }

    function getRatingAttribute() {
        return $this->ratings()
            ->whereNull('blocked_at')
            ->pluck('stars');
    }

    public function setApplicationSeen($application_id){
        if (is_null($this->applications_seen)){ // hasnt been set before
            $this->applications_seen = [$application_id];
        }
        else if(!in_array($application_id, $this->applications_seen)) {
            $seen = $this->applications_seen;
            $seen[] = $application_id;
            $this->applications_seen = $seen;
        }

        return $this->save();
    }

    public function isApplicationSeen($application_id){
        if (is_null($this->applications_seen)){
            return false;
        }

        return in_array($application_id, $this->applications_seen);
    }

    public function getSeenApplications(){
        return $this->applications_seen ?? [];
    }

    public function setRatingSeen($rating_id){
        if (is_null($this->ratings_seen)){ // hasnt been set before
            $this->ratings_seen = [$rating_id];
        }
        else if(!in_array($rating_id, $this->ratings_seen)) {
            $seen = $this->ratings_seen;
            $seen[] = $rating_id;
            $this->ratings_seen = $seen;
        }
        return $this->save();
    }

    public function isRatingSeen($rating_id){
        if (is_null($this->ratings_seen)){
            return false;
        }

        return in_array($rating_id, $this->ratings_seen);
    }

    public function getSeenRatings(){
        return $this->ratings_seen ?? [];
    }

    public function getDeletedApplications(){
        return $this->applications_deleted ?? [];
    }

    public function deleteApplication($application_id){
        if (is_null($this->applications_deleted)){ // hasnt been set before
            $this->applications_deleted = [$application_id];
        }
        else if(!in_array($application_id, $this->applications_deleted)) {
            $deleted = $this->applications_deleted;
            $deleted[] = $application_id;
            $this->applications_deleted = $deleted;
        }

        $this->save();

        return back();
    }

    public function deleteApplications($application_ids){
        $deleted = $this->applications_deleted;
        if(is_null($deleted)){
            $deleted = $application_ids;
        } else {
            $deleted = array_unique(array_merge($deleted, $application_ids));
        }
        $this->applications_deleted = $deleted;

        $this->save();

        return back();
    }

    public static function emailExists($email){
        return User::where('email', $email)->first();
    }

    public function sendEmailVerificationNotification(){
        $this->notify(new \App\Notifications\EmailVerify);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\ResetPassword($token));
    }

    public function getUnreadMessages(){
        return $this->messages()->whereNull('seen_at');
    }

    public function getActiveDossiersCount()
    {
        return $this->dossiers()->where('status', Dossier::STATUS_ACTIVE)->count();
    }

    public function getDisabledDossiersCount()
    {
        return $this->dossiers()->where('status', '=', 30)->count();
    }

    public function getDossierLinks(){
        $links = '';
        foreach($this->dossiers as $dossier){
            $links .= '<a href="'.backpack_url('dossier/'.$dossier->id).'" target="_blank">'.$dossier->locale.'</a><br />';
        }
        return $links;
    }

    public function getCabinetName(){
        if($current_lang_dossier = $this->dossiers()->where('locale', app()->getLocale())->first()){
            return $current_lang_dossier->general->fullname;
        } else if ($any_dossier = $this->dossiers()->first()) {
            return $any_dossier->general->fullname;
        }

        return $this->email;
    }

    public function getCabinetPhoto(){
        if($current_lang_dossier = $this->dossiers()->where('locale', app()->getLocale())->first()){
            return Storage::url($current_lang_dossier->general->photo);
        } else if ($any_dossier = $this->dossiers()->first()) {
            return Storage::url($any_dossier->general->photo);
        }

        return asset('img/cabinet/noavatar.png');
    }
}
