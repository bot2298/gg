<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Spec extends Model
{
    protected $guarded = [];

    protected $casts = [
        'specializations' => 'array',
        'activities' => 'array',
        'appeal_cases' => 'array',
        'cassation_cases' => 'array',
        'intl_cases' => 'array',
    ];
    public function dossier()
    {
        return $this->belongsTo('App\Models\Experience');
    }
}
