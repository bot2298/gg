<?php

namespace App\Models;

use App\Models\Application;
use Illuminate\Database\Eloquent\Model;

class ApplicationBookmark extends Model
{
    protected $fillable = [
        'user_id',
        'application_id'
    ];

    public function application(){
        return $this->belongsTo('App\Models\Application');
    }
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public static function getCount(){
        return auth()->user()->bookmarks->where('application.status', Application::STATUS_ACTIVE)->count();
    }
}
