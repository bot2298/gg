<?php

namespace App\Models;

use App\Models\ApplicationBookmark;
use App\Models\Currency;
use App\Notifications\RequestCode;
use App\Notifications\RequestVerify;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

/**
 * @property integer $id
 * @property string $code
 * @property string $token
 * @property integer $region_id
 * @property integer $city_id
 * @property integer $specialization_id
 * @property string $content
 * @property string $attachment
 * @property integer $cost
 * @property Currency $currency
 * @property Carbon $deadline
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property integer $phone_locale
 * @property integer $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */

class Application extends Model
{
    use CrudTrait;

    const STATUS_UNVERIFIED = 0;
    const STATUS_ACTIVE = 10;
    const STATUS_STOPPED = 20;
    const STATUS_BLOCKED = 30;

    const TRACKER_STATUS_PENDING = 1;
    const TRACKER_STATUS_BOOKMARKED = 2;
    const TRACKER_STATUS_RESPONSE = 3;

    const PHONE_LOCALE_UA = 1;

    const DEADLINE_1_HOUR = 5;
    const DEADLINE_3_HOURS = 6;
    const DEADLINE_12_HOURS = 8;
    const DEADLINE_24_HOURS = 10;
    const DEADLINE_3_DAYS = 20;
    const DEADLINE_7_DAYS = 30;
    const DEADLINE_30_DAYS = 40;

    const FILTER_DEFAULT = 'default';
    const FILTER_SEEN = 'seen';
    const FILTER_NOT_SEEN = 'not_seen';
    const FILTER_RESPONSE = 'response';
    const FILTER_NO_RESPONSE = 'no_response';
    const FILTER_ACTUAL = 'actual';
    const FILTER_NOT_ACTUAL = 'not_actual';
    const FILTER_IN_BOOKMARKS = 'in_bookmarks';

    const SORT_DEFAULT = 'default';
    const SORT_CREATED_DESC = 'created_desc';
    const SORT_CREATED_ASC = 'created_asc';
    const SORT_COST_ASC = 'cost_asc';
    const SORT_COST_DESC = 'cost_desc';
    const SORT_DEADLINE_ASC = 'deadline_asc';
    const SORT_DEADLINE_DESC = 'deadline_desc';

    protected $fillable = [
        'region_id',
        'city_id',
        'specialization_id',
        'content',
        'attachment',
        'cost',
        'currency',
        'deadline',
        'name',
        'email',
        'phone',
        'phone_locale',
        'status',
        'user_id',
    ];

    protected $casts = [
        'deadline' => 'datetime'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public static function getVerifiedNotDeleted(){
        #todo cities filter
        return Application::where('status', Application::STATUS_ACTIVE)
            ->whereIn('region_id', auth()->user()->getActiveRegions())
            ->whereNotIn('applications.id', auth()->user()->getDeletedApplications());
    }

    public function sendVerificationLink(){
        Notification::route('mail', $this->email)
            ->route('token', $this->token)
            ->notify(new RequestVerify());
    }

    public function generateCode($length = 12){
        do{
            $code = substr(str_shuffle(str_repeat($x='0123456789', ceil($length/strlen($x)) )),1,$length);
        }
        while (Application::where('code', $code)->count());
        $this->code = $code;
        $this->status = Application::STATUS_ACTIVE;
        return $this->save();
    }

    public function sendCode(){
        Notification::route('mail', $this->email)
            ->route('id', $this->id)
            ->notify(new RequestCode());
    }

    public function calculateDeadline($type){
        switch($type){
            case self::DEADLINE_1_HOUR: return Carbon::now()->addHour();
            case self::DEADLINE_3_HOURS: return Carbon::now()->addHours(3);
            case self::DEADLINE_12_HOURS: return Carbon::now()->addHours(12);
            case self::DEADLINE_24_HOURS: return Carbon::now()->addHours(24);
            case self::DEADLINE_3_DAYS: return Carbon::now()->addDays(3);
            case self::DEADLINE_7_DAYS: return Carbon::now()->addDays(7);
            case self::DEADLINE_30_DAYS: return Carbon::now()->addDays(30);
            default: return Carbon::now();
        }
    }

    public function getDeadline(){
        if($this->deadline->diffInMinutes($this->created_at) <= 1){ // deadline was not selected
            return false;
        }
        else return $this->deadline;
    }

    public function dateCreated(){
        return $this->created_at->format('d.m.Y');
    }

    public function timeCreated(){
        return $this->created_at->format('H:i');
    }

    public function lessThan60Mins(){
        $remaining = $this->deadline->diffInMinutes(Carbon::now());

        return ($this->getDeadLine() > Carbon::now() && $remaining < 60) ? $remaining : 0; //still actual and less than 60 mins
    }

    public static function getCountLessThan60Mins(){
        return Application::getVerifiedNotDeleted()
//            ->whereNotIn('applications.id', auth()->user()->getSeenApplications())
            ->where('deadline', '>=', Carbon::now())
            ->where('deadline', '<', Carbon::now()->addMinutes(60))
            ->count();
    }

    public static function getCountNew(){
        return Application::getVerifiedNotDeleted()
            ->whereNotIn('applications.id', auth()->user()->getSeenApplications())
            ->count();
    }

    public function inBookmarks(){
        return ApplicationBookmark::where([
            'user_id' => auth()->id(),
            'application_id' => $this->id
        ])->first();
    }

    public function isBookmarked(){
        return $this->bookmarks()->first();
    }

    public function getAdminStatus(){
        switch($this->status){
            case self::STATUS_UNVERIFIED: return 'Не верифікований';
            case self::STATUS_ACTIVE: return 'Активний';
            case self::STATUS_STOPPED: return 'Припинений';
            case self::STATUS_BLOCKED: return 'Заблокований';
            default: return 'Невизначений';
        }
    }

    public function getTrackerStatus(){
        if ($this->hasAnyResponse()){
            return Application::TRACKER_STATUS_RESPONSE;
        } else if ($this->isBookmarked()) {
            return Application::TRACKER_STATUS_BOOKMARKED;
        } else {
            return Application::TRACKER_STATUS_PENDING;
        }
    }

    public static function filter($query, Request $request){

        $query->leftJoin('chats', 'applications.id', '=', 'chats.application_id');

        if ($request->has('filter')) {
            switch($request->query('filter')){
                case Application::FILTER_SEEN: $query->whereIn('applications.id', auth()->user()->applications_seen ?? []); break;
                case Application::FILTER_NOT_SEEN: $query->whereNotIn('applications.id', auth()->user()->applications_seen ?? []); break;
                case Application::FILTER_RESPONSE: $query->where('chats.user_id', auth()->id()); break;
                case Application::FILTER_NO_RESPONSE: $query->whereNull('chats.user_id'); break;
                case Application::FILTER_ACTUAL: $query->where('deadline', '>=', Carbon::now()); break;
                case Application::FILTER_NOT_ACTUAL: $query->where('deadline', '<', Carbon::now()); break;
                case Application::FILTER_IN_BOOKMARKS: $query->whereIn('applications.id', auth()->user()->getBookmarks());
                default: break;
            }
        }

        $query->join('currencies', 'applications.currency_id', '=', 'currencies.id');
        $query->select('applications.*', 'currencies.rate', 'chats.user_id', DB::raw('`applications`.`cost` * `currencies`.`rate` as total_cost'));

        if ($request->has('sort')) {
            switch($request->query('sort')){
                case Application::SORT_COST_ASC: $query->orderBy('total_cost', 'asc'); break;
                case Application::SORT_COST_DESC: $query->orderBy('total_cost', 'desc'); break;
                case Application::SORT_CREATED_ASC: $query->oldest(); break;
                case Application::SORT_CREATED_DESC: $query->latest(); break;
                case Application::SORT_DEADLINE_ASC: $query->orderBy('deadline', 'asc'); break;
                case Application::SORT_DEADLINE_DESC: $query->orderBy('deadline', 'desc'); break;
                default: $query = $query->orderBy('applications.created_at', 'desc'); break;
            }
        }
        else $query = $query->latest();

        return $query;
    }

    public function hasResponse(){
        return $this->chats->where('user_id', auth()->id())->first();
    }

    public function hasAnyResponse(){
        return $this->chats->first();
    }

    public function specialization(){
        return $this->belongsTo('App\Models\Specialization');
    }

    public function city(){
        return $this->belongsTo('App\Models\City');
    }

    public function region(){
        return $this->belongsTo('App\Models\Region');
    }

    public function currency(){
        return $this->belongsTo('App\Models\Currency');
    }

    public function chats()
    {
        return $this->hasMany('App\Models\Chat');
    }

    public function bookmarks(){
        return $this->hasMany('App\Models\ApplicationBookmark');
    }
}
