<?php

namespace App\Models;

use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use App\Models\User;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\This;

/**
 * @property mixed license
 * @property mixed|null overview
 * @property mixed|null contacts
 * @property mixed|null education
 * @property mixed|null experience_other
 * @property mixed|null training
 * @property mixed|null publications
 * @property mixed|null recommendations
 * @property mixed|null honors
 * @property mixed|null services
 * @property mixed user_id
 * @property string $qr
 * @property mixed status
 * @property mixed id
 * @property false|mixed|string locale
 * @property mixed experience_id
 * @property mixed spec_id
 */
class Dossier extends Model
{
    use CrudTrait;

    const STATUS_SAVED = 0;
    const STATUS_NEW = 10;
    const STATUS_ACTIVE = 20;
    const STATUS_REJECTED = 30;
    const STATUS_NOT_ACTIVE = 40;

    protected $casts = [
        'license' => 'array',
        'overview' => 'array',
        'contacts' => 'array',
        'education' => 'array',
        'experience_other' => 'array',
        'training' => 'array',
        'publications' => 'array',
        'recommendations' => 'array',
        'honors' => 'array',
        'services' => 'array',
    ];

    protected $fillable = [
        'status',
        'office',
        'locale',
        'seen_counter',
        'qr'
    ];

    protected $appends = [
        'rating',
        'ratingCount'
    ];

    /**
     * @param array $data
     * @param string $keyToCheck
     * @return mixed|null
     */
    public static function checkForNullable(array $data, string $keyToCheck)
    {
        if (!isset($data[$keyToCheck])) {
            return null;
        }
        $result = [];
        foreach ($data[$keyToCheck] as $key => $item) {
            if (is_array($item)) {
                $result[$keyToCheck][$key] = array_filter($item, function ($value) use ($result) {
                    return !is_null($value);
                });
                if (is_array($result[$keyToCheck][$key])) {
                    foreach ($result[$keyToCheck][$key] as $subKey => $subValue) {
                        if (is_array($result[$keyToCheck][$key][$subKey])) {
                            foreach ($result[$keyToCheck][$key][$subKey] as $subSubKey => $subSubValue) {
                                if ($result[$keyToCheck][$key][$subKey][$subSubKey] == null) {
                                    unset($result[$keyToCheck][$key][$subKey][$subSubKey]);
                                }
                            }
                            if ($result[$keyToCheck][$key][$subKey] == []) {
                                unset($result[$keyToCheck][$key][$subKey]);
                            }
                        }
                    }
                }
                if (count($result[$keyToCheck][$key]) == 0) {
                    unset($result[$keyToCheck][$key]);
                }
            } else {
                $result[$keyToCheck][$key] = $item;
            }
            if (is_null($item)) {
                unset($result[$keyToCheck][$key]);
            }
        }
        $result[$keyToCheck] = count($result[$keyToCheck]) > 0 ? $result[$keyToCheck] : null;
        return $result[$keyToCheck];
    }

    public static function getNewCount(){
        return self::where('status', self::STATUS_NEW)->count();
    }

    public static function getCountryName(int $id)
    {
        return Country::find([$id])->first()['name'];
    }

    public static function getCityName(int $id)
    {
        return City::find([$id])->first()['name'];
    }

    public static function getRegionName(int $id)
    {
        return Region::find([$id])->first()['name'];
    }

    function getRatingCountAttribute() {
        return \App\Models\Rating::where('user_id', $this->user_id)
            ->whereNull('blocked_at')
            ->count();
    }

    function getRatingAttribute() {
        $ratings = \App\Models\Rating::where('user_id', $this->user_id)
            ->whereNull('blocked_at')
            ->pluck('stars');
        return $ratings->isNotEmpty() ? round($ratings->sum() / $ratings->count(), 1) : 5;
    }

    public function getAdminStatus(){
        switch ($this->status) {
            case self::STATUS_NEW: return "Нова заявка";
            case self::STATUS_ACTIVE: return "Активне досьє";
            case self::STATUS_REJECTED: return "Відхилена заявка";
            case self::STATUS_SAVED: return "Чернетка";
            case self::STATUS_NOT_ACTIVE: return "Неактивне досьє";
            default: return "Невизначений";
        }
    }

    public function getStatus(){
        switch ($this->status){
            case self::STATUS_SAVED: return ['3' => 'cabinet.navigation.dossier_saved'];
            case self::STATUS_NEW: return ['3' => 'cabinet.navigation.dossier_pending'];
            case self::STATUS_ACTIVE: return ['2' => 'cabinet.navigation.dossier_active'];
            case self::STATUS_NOT_ACTIVE: return ['3' => 'cabinet.navigation.dossier_not_active'];
            case self::STATUS_REJECTED: return ['1' => 'cabinet.navigation.dossier_rejected'];
            default: return ['1' => 'Невизначений'];
        }
    }

    /**
     * @return array
     */
    public static function getExistLanguages()
    {
        $languages = [];
        foreach (auth()->user()->dossiers()->get() as $dossier) {
            $languages[] = $dossier->locale;
        }
        return $languages;
    }

    public static function getUserDossierByLang($lang)
    {
        $dossiers = auth()->user()->dossiers()->get();
        $dossiers->filter(function ($dossier) use ($lang) {
            return $dossier->locale == $lang;
        });
        return $dossiers->first();
    }

    public static function isExistWithLang($lang)
    {
        $languages = self::getExistLanguages();
        return is_int(array_search($lang, $languages));
    }

    public static function firstFreeLang()
    {
        $languages = self::getExistLanguages();
        $langConfig = config('app.locales');
        foreach ($languages as $language) {
            unset($langConfig[$language]);
        }
        return array_key_first($langConfig);
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function region(){
        return $this->belongsTo('App\Models\Region');
    }

    public function experience()
    {
        return $this->hasOne('App\Models\Experience');
    }

    public function spec()
    {
        return $this->hasOne('App\Models\Spec');
    }

    public function general()
    {
        return $this->hasOne('App\Models\General');
    }

    public function city(){
        return $this->belongsTo('App\Models\City');
    }
}
