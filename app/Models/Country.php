<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Region;
use App\Models\City;

/**
 * @property integer $id
 * @property string $name
 * @property array $regions
 * @property array $cities
 * @property integer $active
 */


class Country extends Model
{
    use CrudTrait;

    protected $fillable = [
        'name',
    ];

    public function regions(){
        return $this->hasMany('App\Models\Region');
    }

    public function cities(){
        return $this->hasManyThrough('App\Models\City', 'App\Models\Region');
    }
}
