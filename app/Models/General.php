<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class General extends Model
{
    protected $fillable = [
        'fullname',
        'photo',
        'dossier_id',
        'office'
    ];

    protected $casts = [
        'office' => 'array',
    ];
}
