<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Feedback
 * @package App\Models
 *
 * @property string $name
 * @property string $lastname
 * @property string $email
 * @property string $message
 * @property Carbon $seen_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $place_of_living
 * @property string $postal_address
 * @property string $contact_number
 *
 */

class Feedback extends Model
{
    use CrudTrait;

    protected $table = 'feedback';
    protected $fillable = [
        'name',
        'lastname',
        'email',
        'message',
        'place_of_living',
        'postal_address',
        'contact_number'
    ];

    public static function getNewCount(){
        return self::whereNull('seen_at')->count();
    }

}
