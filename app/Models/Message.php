<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * Fields that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['message', 'owner', 'seen_at'];

    public function chat()
    {
        return $this->belongsTo('App\Models\Chat');
    }

    public function dateCreated(){
        return $this->created_at->format('d.m.Y');
    }

    public function timeCreated(){
        return $this->created_at->format('H:i');
    }
}
