<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddDefaultRating
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param $event
     * @return void
     */
    public function handle($event)
    {
        for($i = 0; $i < 10; $i++){
            $rating = new \App\Models\Rating();
            $rating->stars = 5;
            $rating->comment = 'Рейтинг при реєстрації';
            $rating->user_id = $event->user->id;
            $rating->client_email = 'example@mail.com';
            $rating->save();
        }
    }
}
