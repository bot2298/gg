<?php

namespace App\Listeners;

use App\Events\SocialFirstLogin;
use App\Notifications\PasswordSocial;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPassword
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SocialFirstLogin $event
     * @return void
     */
    public function handle(SocialFirstLogin $event)
    {
        $event->user->notify(new PasswordSocial($event->password));
    }
}
