<?php

namespace App\Listeners;

use App\Events\ApplicationView;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddToSeen
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApplicationView  $event
     * @return void
     */
    public function handle(ApplicationView $event)
    {
        auth()->user()->setApplicationSeen($event->application->id);
    }
}
