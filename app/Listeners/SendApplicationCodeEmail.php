<?php

namespace App\Listeners;

use App\Events\RemindCode;
use App\Mail\RemindCodeMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendApplicationCodeEmail
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RemindCode  $event
     * @return void
     */
    public function handle(RemindCode $event)
    {
        Mail::to($event->applications[0]->email)->send(
            new RemindCodeMail($event->applications)
        );
    }
}
