<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\RatingView;

class RatingAddToSeen
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param RatingView $event
     * @return void
     */
    public function handle(RatingView $event)
    {
        auth()->user()->setRatingSeen($event->rating->id);
    }
}
