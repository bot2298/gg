<?php

namespace App\Http\Resources;

use App\Models\Country;
use App\Models\Region;
use App\Models\Spec;
use Illuminate\Http\Resources\Json\JsonResource;

class DossierResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->user()->first();
        $experience = $this->experience()->first();
        $regions = $user->getActiveRegions();
        $countries = [];
        foreach ($regions->all() as $regionId) {
            $countries = Region::find([$regionId])->first()->country()->pluck('id')->all();
        }
        if (!$regions->isEmpty()) {
            return [
                'id' => $this->id,
                'rating' => $this->getRatingAttribute(),
                'general' => $this->general()->first(),
                'experience' => $experience,
                'countries_id' => $countries,
                'user_id' => $user->id,
                'regions_id' => $regions->all(),
                'locale' => $this->locale,
                'user_dossiers_count' => $user->dossiers()->count(),
                'city' => $this->city ? $this->city->name : null,
                'region' => $this->region->name,
                'country' => $this->region->country->name,
                'spec' => $this->spec,
                'seen_counter' => $this->seen_counter,
                'in_bookmarks' => session()->has('bookmark') && in_array($this->id, session('bookmark'))
            ];
        }
    }
}
