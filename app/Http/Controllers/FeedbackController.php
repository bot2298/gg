<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'email' => 'required|email',
            'name' => 'max:100',
            'lastname' => 'max:100',
            'message' => 'required|max:5000',
            'place_of_living' => 'max:1000',
            'postal_address' => 'max:1000',
            'contact_number' => 'max:1000',
        ]);
        $feedback = new Feedback();
        $feedback->name = $request->post('name');
        $feedback->lastname = $request->post('lastname');
        $feedback->email = $request->post('email');
        $feedback->message = $request->post('message');
        $feedback->place_of_living = $request->post('place_of_living');
        $feedback->postal_address = $request->post('postal_address');
        $feedback->contact_number = $request->post('contact_number');
        $feedback->save();

        return back()->with('notification', [
            'type' => 'success',
            'title' => __('modals.feedback_title'),
            'content' => __('modals.feedback_content')
        ]);
    }
}
