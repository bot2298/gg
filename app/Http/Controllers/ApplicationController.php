<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use App\Models\Country;
use App\Models\Currency;
use App\Events\ApplicationView;
use App\Models\Page;
use App\Models\Region;
use App\Models\City;
use App\Models\Application;
use App\Models\Specialization;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ApplicationController extends Controller
{
    public function __construct()
    {
        $this->middleware('throttle:5')->only('verify');
    }

    public function rules(){
        return [
            'country_id' => 'required|integer',
            'region_id' => 'required|integer',
            'city_id' => 'nullable|integer',
            'specialization_id' => 'nullable|integer',
            'currency_id' => 'required',
            'content' => 'required|max:5000',
            'cost' => 'nullable|integer|min:0|max:100000',
            'deadline_type' => 'nullable|integer',
            'name' => 'required|min:3|max:50',
            'email' => 'required|email',
            'phone' => 'max:100',
            'attachment' => 'mimetypes:image/*,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf|max:15000',
            'agreement' => 'accepted',
        ];
    }

    public function index($locale, $location = "cabinet.request.index", Request $request)
    {
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        $query = Application::getVerifiedNotDeleted();
        $applications = Application::filter($query, $request)->paginate(10);
        return view('site.index', compact(['location', 'title', 'applications']));
    }

    public function show($locale, Application $application, $location = "cabinet.request.single", Request $request)
    {
        if(!in_array($application->region_id, auth()->user()->getActiveRegions()->toArray())){
            return back()->with('notification', [
                'type' => 'success',
                'title' => __('modals.result_error'),
                'content' => __('modals.application_noaccess')
            ]);
        }
        $chat = Chat::query()
            ->where('application_id', '=', $application->id)
            ->where('user_id', '=', auth()->id())
            ->first();
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        event(new ApplicationView($application));
        return view('site.index', compact(['location', 'title', 'application', 'chat']));
    }

    public function requestForm($locale, $location = "site.request.index", Request $request, $js = ['js/request.js'])
    {
        $allowEmailChange = true;
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        $countries = Country::where('active', 1)->get();
        $specializations = Specialization::all();
        $application = new Application();
        $currencies = Currency::all();
        return view('site.index', compact(['location', 'title', 'countries', 'specializations', 'currencies', 'js', 'allowEmailChange']))->withApplication($application);
    }

    public function create(Request $request){
        $application = new Application();
        return $this->store($request, $application);
    }

    public function update(Request $request){
        $application = Application::findOrFail(Cookie::get('application_id'));
        return $this->change($request, $application);
    }

    public function changeEmail($locale, Request $request){
        return $this->showEditForm($locale, $allowEmailChange = true, $needVerification = true, $request);
    }

    public function showEditForm($locale, $allowEmailChange = false, $needVerification = false, Request $request, $location = "site.request.index", $js = ['js/request.js']){
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        $application = Application::find(Cookie::get('application_id'));
        if(is_null($application)){
            return redirect()->route('tracker.index', app()->getLocale());
        }
        $countries = Country::where('active', 1)->get();
        $specializations = Specialization::all();
        $currencies = Currency::all();
        $old_file = $application->attachment;
        return view('site.index', compact(['location', 'title', 'countries', 'specializations', 'currencies', 'js', 'old_file', 'allowEmailChange', 'needVerification']))->withApplication($application)->with('update', true);
    }

    public function store($request, $application){
        $validator = Validator::make($request->except('_token'), $this->rules());
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        if (Application::where('email', $request->email)->where('status', Application::STATUS_ACTIVE)->count() > 3){
            return back()->withInput()->with('notification', [
                'type' => 'error',
                'title' => __('modals.result_error'),
                'content' => __('modals.application_more_than_3')
            ]);
        }
        $attachment = null;
        if ($request->hasFile('attachment')){
            $attachment = $request->file('attachment')->store('request_attachments');
        }

        foreach($request->except(['_token', 'agreement', 'country_id', 'cost', 'deadline_type', 'formatted-phone', 'attachment']) as $k => $v){
            $application->$k = $v;
        }
        $application->attachment = $attachment;
        if($request->cost){
            $application->cost = $request->cost;
        }
        do{
            $token = Str::random(60);
        }
        while (Application::where('token', $token)->first());

        $application->token = $token;
        $application->deadline = $application->calculateDeadline($request->deadline_type);
        $application->save();

        $user = User::firstOrCreate(['email' => $application->email], [
            'api_token' => Str::random(60),
            'password' => Hash::make(Str::random()),
            'email' => $request->email,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        if (!$user->hasRole('Клієнт')){
            $user->assignRole('Клієнт');
        }

        $application->user_id = $user->id;
        $application->save();

        $application->sendVerificationLink();

        Cookie::queue('application_id', $application->id, 60);
//        session()->put('application_id', $application->id);

        return redirect()->route('request.verification', app()->getLocale())->with('email', $application->email)->withInput();
    }

    public function change($request, $application){
        $validator = Validator::make($request->except('_token'), [
            'country_id' => 'required|integer',
            'region_id' => 'required|integer',
            'city_id' => 'nullable|integer',
            'specialization_id' => 'nullable|integer',
            'currency_id' => 'required',
            'content' => 'required|max:5000',
            'cost' => 'nullable|integer|min:0|max:100000',
            'deadline_type' => 'nullable|integer',
            'name' => 'required|min:3|max:50',
            'email' => 'required|email',
            'phone' => 'max:100',
            'attachment' => 'mimetypes:image/*,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf|max:15000',
        ]);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->with('update', true)
                ->withInput();
        }
        if ($request->hasFile('attachment')){
            $application->attachment = $request->file('attachment')->store('request_attachments');
        } elseif($request->old_file) {
            $application_attachment = $request->old_file;
        }

        foreach($request->except(['_token', 'agreement', 'country_id', 'cost', 'deadline_type', 'formatted-phone', 'attachment', 'old_file', 'need_verification']) as $k => $v){
            $application->$k = $v;
        }
        if($request->cost){
            $application->cost = $request->cost;
        }

        $application->deadline = $application->calculateDeadline($request->deadline_type);
        $application->save();

        $user = User::firstOrCreate(['email' => $application->email], [
            'api_token' => Str::random(60),
            'password' => Hash::make(Str::random()),
            'email' => $request->email,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        if (!$user->hasRole('Клієнт')){
            $user->assignRole('Клієнт');
        }

        $application->user_id = $user->id;
        $application->save();
        if($request->need_verification){
            $application->sendVerificationLink();
        }

        Cookie::queue('application_id', $application->id, 60);
        if($request->need_verification){
            return redirect()->route('request.verification', app()->getLocale())->withEmail($application->email);
        }
        return redirect()->route('tracker.application', app()->getLocale())->with('notification', [
            'title' => __('modals.result_success'),
            'type' => 'success',
            'content' => __('modals.request_changed')
        ]);
    }

    public function verification($locale, Request $request, $location = "site.request.verification"){
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        return view('site.index', compact(['location', 'title']));
    }

    public function verify(Request $request){
        if (!$request->hasValidSignature()) {
            abort(401);
        }

        $user_request = Application::where('token', $request->query('token'))->firstOrFail();

        if (!is_null($user_request->code)){ // already verified
            return redirect()->route('index', app()->getLocale())->with('notification', [
                'type' => 'error',
                'title' => __('modals.result_error'),
                'content' => __('modals.application_confirmed')
            ]);
        }
        $user_request->generateCode();
        $user_request->updated_at = $user_request->created_at;
        $user_request->save();

        return redirect()->route('request.verified', app()->getLocale())->with('code', $user_request->code);
    }

    public function verified($locale, Request $request, $location = "site.request.verified"){
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        return view('site.index', compact(['location', 'title']));
    }

    public function sendCode(Request $request){
        /** @var Application $user_request */
        $user_request = Application::where(['code' => $request->input('code')])->firstOrFail();
        $user_request->sendCode();

        return redirect()->route('tracker.application', app()->getLocale());
    }

    public function stop(){
        $application = Application::findOrFail(Cookie::get('application_id'));
        $application->status = Application::STATUS_STOPPED;
        $application->save();
        Cookie::queue(Cookie::forget('application_id'));
//        session()->forget('application_id');
        return redirect()->route('index', app()->getLocale())->with('notification', [
            'type' => 'success',
            'title' => __('modals.result_success'),
            'content' => __('site.tracker.stopped')
        ]);
    }

    public function exit(){
        Cookie::queue(Cookie::forget('application_id'));
        return redirect()->route('tracker.index', app()->getLocale());
    }

    public function delete($locale, Request $request){
        if(is_array($id = $request->input('id'))){
            auth()->user()->deleteApplications($id);
        } else {
            auth()->user()->deleteApplication($id);
        }
        return back();
    }

    public function regions(Request $request){
        if ($id = $request->post('country_id')){
            $regions = Region::select(['name', 'id'])->where('country_id', $id)->get();
            return response()->json($regions->map(function($item){
                $item['name'] = __('regions.'.$item["name"]);
                return $item;
            })->toJson());
        }
    }

    public function cities(Request $request){
        if ($id = $request->post('region_id')){
            $cities = City::select(['id', 'name'])->where('region_id', $id)->get();
            return response()->json($cities->map(function($item){
                $item['name'] = __('cities.'.$item["name"]);
                return $item;
            })->toJson());
        }
    }
}
