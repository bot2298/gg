<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ChangePasswordController extends Controller
{
    public function messages(){
        return [
            'password.current_password' => __('passwords.current_password'),
            'newpassword.confirmed' => __('validation.password_confirmation'),
        ];
    }

    public function rules(){
        return [
            'email' => ['required', Rule::unique('users')->ignore(auth()->id()),'max:255','email'],
            'password' => 'required|current_password',
            'newpassword' => 'nullable|confirmed|min:8',
        ];
    }

    public function index($locale, $location = "cabinet.settings.index", Request $request)
    {
        $title = Page::getTitle($location);
        return view('site.index', compact(['location', 'title']));
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), $this->rules(), $this->messages());

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        /** @var App\Models\User $user */
        $user = auth()->user();
        if($request->newpassword){
            $user->password = Hash::make($request->newpassword);
        }
        $user->email = $request->email;
        $user->save();

        return redirect()->route('cabinet.index', app()->getLocale())->with('notification', [
            'type' => 'success',
            'title' => __('modals.result_success'),
            'content' => __('modals.password_saved')
        ]);
    }
}
