<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Events\RatingView;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Models\Rating;
use Illuminate\Support\Facades\Cookie;

class RatingController extends Controller
{
    public function index($locale, $location = "cabinet.rating.index", Request $request)
    {
        $title = Page::getTitle($location);
        $ratings = auth()->user()->ratings()
            ->whereNull('blocked_at')
            ->where('client_email', '<>', 'example@mail.com') // default rating
            ->latest()
            ->paginate(10);
        return view('site.index', compact(['location', 'title', 'ratings']));
    }

    public function show($locale, Rating $rating, $location = "cabinet.rating.single", Request $request)
    {
        $title = Page::getTitle($location);
        if($rating->user_id !== auth()->id()){
            abort(401);
        }
        event(new RatingView($rating));
        return view('site.index', compact(['location', 'title', 'rating']));
    }



    public function store(Request $request){
        $this->validate($request, [
            'user' => 'required',
            'rating' => 'required|min:1|max:5',
            'text' => 'required'
        ]);

        $application = Application::findOrFail(Cookie::get('application_id'));

        if (Rating::where('user_id', $request->input('user'))->where('client_email', $application->email)->first()) {
            return response()->json([
                'success' => false
            ]);
        }

        $rating = new Rating([
            'user_id' => $request->input('user'),
            'stars' => $request->input('rating'),
            'comment' => $request->input('text'),
            'client_email' => $application->email
        ]);
        return response()->json([
            'success' => $rating->save()
        ]);
    }
}
