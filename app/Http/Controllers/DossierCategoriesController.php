<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\FormHelper;
use App\Models\Page;
use App\Models\Specialization;
use Illuminate\Http\Request;

class DossierCategoriesController extends Controller
{
    public function index($locale, $cat_url, $location = "site.dossier.index", Request $request, $js = ['js/dossier.js']){
       $category = Page::findByLocation($cat_url);
       if (!$category) {
           abort(404);
       }
       $region_id = $category->region ? $category->region->id : null;
       $specialization_name = $category->specialization ? $category->specialization->name : null;
       $title = Page::getTitle($cat_url);
       $description = Page::getDescription($cat_url);
       $h1 = Page::getH1($cat_url);
//       dd($title);
//       dd($description);
        $countries = Country::where('active', 1)->get();
        $specializations = Specialization::all();
        $formHelper = new FormHelper();
        $activities = $formHelper->getActivities();
        return view('site.index', compact(['location', 'title', 'description', 'h1', 'countries', 'specializations', 'activities', 'js', 'region_id', 'specialization_name', 'cat_url']));

    }
}
