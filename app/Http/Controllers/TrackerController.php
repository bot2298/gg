<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Page;
use App\Models\Country;
use App\Models\Dossier;
use App\Models\Message;
use App\Models\Rating;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class TrackerController extends Controller
{
    public function index($locale, $location = "site.tracker.index", Request $request, $js = ['js/tracker.js'])
    {
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        return view('site.index', compact(['location', 'title', 'js']));
    }

    public function remindCode($locale, $location = "site.tracker.remind-code", Request $request, $js = ['js/tracker.js'])
    {
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        return view('site.index', compact(['location', 'title', 'js']));
    }

    public function application($locale, $location = "site.tracker.response-list", Request $request, $js = ['js/tracker.js'])
    {
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
//        $application = $request->session()->has('application_id') ? Application::find($request->session()->get('application_id')) : Application::where('code', '=', $request->code)->first();
        $application = Cookie::get('application_id') ? Application::find(Cookie::get('application_id')) : Application::where('code', '=', $request->code)->first();
        $chats = null;
        $responseListData = [];
        if ($application->chats()->first() != null) {
            $chats = $application->chats()->get();
            foreach ($chats as $chat) {
                $dossier = User::find([$chat->user_id])->first()->dossiers()->first();
                $dossier_general = $dossier->general;
                $responseListData[] = [
                    'chat' => $chat,
                    'api_token' => $chat->application->user->api_token,
                    'dossier' => $dossier_general,
                    'ratingExists' => Rating::where('user_id', $chat->user_id)->where('client_email', $application->email)->count(),
                    'rating' => $dossier->rating,
                    'ratingCount' => $dossier->ratingCount,
                    'city' => $dossier->city ? $dossier->city->name : null,
                    'region' => $dossier->region->name,
                    'country' => $dossier->region->country->name,
                    'dossier_url' => route('dossier.single', ['locale' => app()->getLocale(), 'dossier' => $dossier]),
                    'lastMessage' => $chat->messages()->get()->last()->message,
                    'application' => Application::find($chat->application_id),
                ];
            }
        }
        $responseListData = collect($responseListData);
        $responseListData = $this->paginate($responseListData, $perPage = 5, $page = null, $options = ['path' => $request->url()]);
        return view('site.index', compact(['location', 'title', 'application', 'chats', 'responseListData', 'js']));
    }
}
