<?php

namespace App\Http\Controllers\Auth;

use App\Models\Page;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm($locale, $location = "site.auth.register", Request $request)
    {
        $title = Page::getTitle($location);
        return view('site.index', compact(['location', 'title']));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                function ($attribute, $value, $fail) {
                    $user = \App\Models\User::where('email', $value)->first();
                    if ($user && $user->hasRole('Адвокат')){
                        $fail(__('validation.unique'));
                    }
                },
            ],
            'password' => 'required|string|min:8|confirmed',
        ], [
            'password.confirmed' => __('validation.password_confirmation'),
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = User::firstOrCreate([
            'email' => $data['email'],
        ], [
            'password' => Hash::make($data['password']),
            'api_token' => Str::random(60)
        ]);
        if(!$user->wasRecentlyCreated){ // already existed (client)
            $user->password = Hash::make($data['password']);
            $user->email_verified_at = Carbon::now();
            $user->api_token = Str::random(60);

            $user->save();
        }
        $user->assignRole('Адвокат');
        return $user;
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        #TODO why no auto redirect?
        return redirect(app()->getLocale().'/email/verify');
    }
}
