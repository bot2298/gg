<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    protected function redirectTo()
    {
        return app()->getLocale()."/cabinet";
    }

    public function verify(Request $request)
    {
        $user = \App\Models\User::findOrFail($request->route('id'));
        if ($user->hasVerifiedEmail()) {
            return redirect($this->redirectPath());
        }

        if ($user->markEmailAsVerified()) {
            event(new Verified($user));
        }
        auth()->login($user);
        return redirect($this->redirectPath())->with('notification', [
            'type' => 'success',
            'title' => __('modals.result_success'),
            'content' => __('modals.email_verified'),
        ]);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    /**
     * Show the email verification notice.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show($locale, Request $request, $location = 'site.auth.verify')
    {
        $title = Page::getTitle($location);
        if(auth()->check() && auth()->user()->hasVerifiedEmail()){ // already verified
            redirect($this->redirectPath());
        } elseif (auth()->check()) { // logged but not verified
            return view('site.index', compact(['location', 'title']));
        }
        return redirect()->route('login', app()->getLocale());
    }
}
