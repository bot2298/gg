<?php

namespace App\Http\Controllers\Auth;

use App\Events\SocialFirstLogin;
use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm($locale, $location = "site.auth.login")
    {
        $title = Page::getTitle($location);
        return view('site.index', compact(['location', 'title']));
    }

    protected function redirectTo()
    {
        return app()->getLocale()."/cabinet";
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     */
    public function handleFacebookCallback()
    {
        return $this->socialLogin(Socialite::driver('facebook')->user());
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from Google.
     *
     */
    public function handleGoogleCallback()
    {
        return $this->socialLogin(Socialite::driver('google')->user());
    }

    public function socialLogin($social_user){
        $notification = null;
        if($user = User::emailExists($social_user->email)){
            if (!$user->hasRole('Адвокат')){ // client
//                return redirect()->route('login', app()->getLocale())->with('notification', [
//                    'type' => 'error',
//                    'title' => __('modals.result_error'),
//                    'content' => __('modals.email_already_used')
//                ]);
                $user->assignRole('Адвокат');
                $user->email_verified_at = Carbon::now();
                $user->api_token = Str::random(60);
                $password = Str::random(10);
                $user->password = Hash::make($password);
                $user->save();
                $notification = [
                    'type' => 'success',
                    'title' => __('modals.social_login'),
                    'content' => __('modals.social_login_content')
                ];
                event(new SocialFirstLogin($user, $password));
            }
            auth()->login($user);
        } else {
            $new_user = new User();
            $new_user->email = $social_user->email;
            $new_user->email_verified_at = Carbon::now();
            $new_user->api_token = Str::random(60);
            $new_user->created_at = Carbon::now();
            $password = Str::random(10);
            $new_user->password = Hash::make($password);
            $new_user->save();
            $new_user->assignRole('Адвокат');

            $notification = [
                'type' => 'success',
                'title' => __('modals.social_login'),
                'content' => __('modals.social_login_content')
            ];
            event(new SocialFirstLogin($new_user, $password));
            auth()->login($new_user);
        }

        return redirect()->route('cabinet.index', app()->getLocale())->with('notification', $notification);
    }
}
