<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Page;
use App\Models\Chat;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class CabinetController extends Controller
{
    public function index($locale, $location = "cabinet.home.index", Request $request)
    {
        $title = Page::getTitle($location);
        return view('site.index', compact(['location', 'title']));
    }
    public function mail($locale, $location = "cabinet.mail.index", Request $request, $js = ['js/cabinet-chat.js'])
    {
        $title = Page::getTitle($location);
        $chats = null;
        $responseListData = [];
        if (auth()->user()->chats()->first() != null) {
            $chats = Chat::where('user_id', '=', auth()->id())->get();
            foreach ($chats as $chat) {
                $application = Application::find($chat->application_id);
                $last_message = $chat->messages()->get()->last();
                $responseListData[] = [
                    'chat' => $chat,
                    'lastMessage' => $last_message->message,
                    'application' => $application,
                    'application_link' => route('cabinet.request-single', ['application' => $application, 'locale' => app()->getLocale()]),
                    'date' => $last_message->dateCreated(),
                    'chat_seen' => $last_message->seen_at,
                    'time' => $last_message->timeCreated(),
                    'date_time' => $last_message->created_at,
                    'api_token' => auth()->user()->api_token
                ];
            }
        }
        $responseListData = collect($responseListData);
        $responseListData = $responseListData->sortByDesc('date_time')->values();
        $responseListData = $this->paginate($responseListData, $perPage = 5, $page = null, $options = ['path' => $request->url()]);
        return view('site.index', compact(['location', 'title', 'responseListData', 'js']));
    }
}
