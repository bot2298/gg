<?php

namespace App\Http\Controllers;

use App\Http\Resources\DossierResource;
use App\Models\Application;
use App\Models\Country;
use App\Models\FormHelper;
use App\Models\Page;
use App\Models\Dossier;
use App\Models\Specialization;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class IndexController extends Controller
{
    public function index($locale, $location = "site.home.index", Request $request)
    {
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        return view('site.index', compact(['location', 'title', 'description']));
    }
    public function about($locale, $location = "site.about.index", Request $request)
    {
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        return view('site.index', compact(['location', 'title', 'description']));
    }
    public function bookmarks($locale, $location = "site.bookmarks.index", Request $request, $js = ['js/bookmarks.js'])
    {
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        $dossiers = Dossier::find(session('bookmark'));
        return view('site.index', compact(['location', 'title', 'description', 'dossiers', 'js']));
    }
    public function dossier($locale, $location = "site.dossier.index", Request $request, $js = ['js/dossier.js'])
    {
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        $countries = Country::where('active', 1)->get();
        $specializations = Specialization::all();
        $formHelper = new FormHelper();
        $activities = $formHelper->getActivities();
        return view('site.index', compact(['location', 'title', 'description', 'countries', 'specializations', 'activities', 'js']));
    }
    public function dossierSingle($locale, $id, $location = "site.dossier-single.index", Request $request, $js = ['js/dossier-single.js'])
    {
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        $dossier = Dossier::findOrFail($id);
        if($dossier->status != Dossier::STATUS_ACTIVE && $dossier->user_id != auth()->id()){
            return redirect()->route('dossier', app()->getLocale())->with('notification', [
                'type' => 'error',
                'title' => __('modals.result_error'),
                'content' => __('modals.dossier_notactive')
            ]);
        }
        if (!in_array($id, session('dossier_seen') ?? [])){
            session()->push('dossier_seen', $id);
            session()->save();
            $dossier->seen_counter++;
            $dossier->save();
        }
        return view('site.index', compact(['location', 'title', 'dossier', 'js']));
    }

    public function policy($locale, $location = 'site.policy.index', Request $request){
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        return view('site.index', compact(['location', 'title', 'description']));
    }

    public function faq($locale, $location = "site.faq.index", Request $request)
    {
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        return view('site.index', compact(['location', 'title', 'description']));
    }

    public function home(){
        return redirect(app()->getLocale());
    }

    public function cleanReg(){
        if(auth()->user()){
            auth()->logout();
        }
        return redirect()->route('register', app()->getLocale());
    }

    public function isUserVerified(){
        if(auth()->user() && auth()->user()->email_verified_at){
            return response()->json([
                'success' => 1,
                'url' => route('cabinet.index', app()->getLocale())
            ]);
        }
        return response()->json([
            'success' => 0,
        ]);
    }

    public function isApplicationVerified(){
        $application = Application::find(Cookie::get('application_id'));
        if($application && $application->status === Application::STATUS_ACTIVE){
            return response()->json([
                'success' => 1,
                'url' => route('tracker.application', app()->getLocale())
            ]);
        }
        return response()->json([
            'success' => 0,
        ]);
    }
}
