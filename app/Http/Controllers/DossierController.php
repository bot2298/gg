<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Country;
use App\Models\Currency;
use App\Models\Dossier;
use App\Models\Experience;
use App\Models\FormHelper;
use App\Models\General;
use App\Models\Page;
use App\Models\Region;
use App\Models\Spec;
use App\Models\Specialization;
use http\Client\Response;
use Illuminate\Validation\Rule;
use SimpleSoftwareIO\QrCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use function GuzzleHttp\Promise\all;

class DossierController extends Controller
{

    public function index($locale, $location = "site.cabinet.dossier-index", Request $request, $js = ['js/dossier.js'])
    {
        $title = Page::getTitle($location);
        $dossier = auth()->user()->dossiers()->get();
        return view('site.index', compact(['location', 'title', 'dossier', 'js']));
    }

    public function create($locale, $location = "site.cabinet.dossier-form", Request $request, $js = ['js/dossier.js'])
    {
        $title = Page::getTitle($location);
        $countries = Country::where('active', 1)->get();
        $specializations = Specialization::all();
        $dossier = new Dossier();
        $formHelper = new FormHelper();
        $currencies = Currency::getAllAndPrepare();
        $existing_photo = null;
        if ($any_dossier = auth()->user()->dossiers()->first()) {
            $existing_photo = $any_dossier->general->photo;
        }
        $lang = is_null(app('request')->input('lang')) ? app()->getLocale() : app('request')->input('lang');
        if (Dossier::isExistWithLang($lang)) {
            return redirect()->route('cabinet.dossier.edit', ['locale' => app()->getLocale(), 'id' => Dossier::getUserDossierByLang($lang)->id, 'lang' => Dossier::getUserDossierByLang($lang)->locale])->with('notification', [
                'type' => 'error',
                'title' => __('modals.result_error'),
                'content' => __('modals.lang_exist')
            ]);
        }
        return view('site.index', compact(['location', 'title', 'countries', 'specializations', 'dossier', 'formHelper', 'currencies', 'lang', 'js', 'existing_photo']));
    }

    public function store(Request $request)
    {
        app()->setLocale(substr($request->post('lang'), -2));
        $request->validate([
            'lang' => 'required|string',
            'general.fullname' => 'required|string',
            'general.office.country' => 'required|int',
            'general.office.region' => 'required|int',
            'general.office.city' => [Rule::requiredIf(function () use ($request) {
                return Region::citiesOfRegionCount($request->input('general.office.region')) > 0;
            })],
            'general.office.address.*' => 'required|string',
            'general.office.phones.*.number' => 'required|string',
            'license.*.country' => 'required|string',
            'license.*.institution' => 'required|string',
            'license.*.year' => 'required|string',
            'license.*.number' => 'required|string',
            'file.license.*.' => 'required',
            'file.photo' => [
                'mimes:jpeg,jpg,png,gif',
                'max:10000',
                [Rule::requiredIf(function () use ($request) { // if theres no photo from existing dossier
                    return !$request->existing_photo;
                })],
            ],
            'agreement' => 'accepted',
        ]);
        $data = $request->except('_token');

        $dossier = new Dossier();
        $dossier->locale = substr($data['lang'], -2);
        if (is_int(array_search($dossier->locale, Dossier::getExistLanguages()))) {
            if (is_int(array_search($dossier->locale, Dossier::getExistLanguages()))) {
                return redirect()->route('cabinet.dossier.index', app()->getLocale())->with('notification', [
                    'type' => 'error',
                    'title' => __('modals.result_error'),
                    'content' => __('modals.lang_exist')
                ]);
            }
        }

        request()->has('file.photo') ? $data['general']['photo'] = request()->file('file.photo')->store('avatars') : '';
        $general = new General([
            'fullname' => $data['general']['fullname'],
            'photo' => $data['general']['photo'] ?? $request->existing_photo ?? '',
            'office' => $data['general']['office'],
        ]);
        $general->save();

        $dossier->license = $data['license'];
        $dossier->overview = Dossier::checkForNullable($data, 'overview');
        $dossier->contacts = Dossier::checkForNullable($data, 'contacts');
        $dossier->education = Dossier::checkForNullable($data, 'education');
        $dossier->experience_other = Dossier::checkForNullable($data, 'experience_other');
        $dossier->training = Dossier::checkForNullable($data, 'training');
        $dossier->publications = Dossier::checkForNullable($data, 'publications');
        $dossier->recommendations = Dossier::checkForNullable($data, 'recommendations');
        $dossier->honors = Dossier::checkForNullable($data, 'honors');
        $dossier->services = Dossier::checkForNullable($data, 'services');
        $dossier->user_id = auth()->user()->id;
        if (isset($data['general']['office']['city']) && $data['general']['office']['city'] !== "") {
            $dossier->city_id = $data['general']['office']['city'];
            $dossier->region_id = $data['general']['office']['region'];
        } else {
            $dossier->region_id = $data['general']['office']['region'];
        }
        $notification = [
            'type' => 'success',
            'title' => __('modals.result_success'),
            'content' => __('modals.dossier_saved')
        ];
        $new = false;
        if (isset($data['status'])) {
            $new = true;
            if ($data['status'] != Dossier::STATUS_NEW) {
                abort(400);
            }
            $notification = [
                'type' => 'success',
                'title' => __('modals.result_success'),
                'content' => __('modals.dossier_created')
            ];
            $dossier->status = $data['status'];
        }
        $dossier->save();
        $data['experience'] = Dossier::checkForNullable($data, 'experience');
        if (isset($data['experience'])) {
            if (isset($data['experience']['specs'])) {
                $specData = $data['experience']['specs'];
                unset($data['experience']['specs']);
            }
            $experience = new Experience($data['experience']);
            $experience->save();
            $dossier->experience()->save($experience);
            if (isset($specData)) {
                $spec = new Spec($specData);
                $spec->save();
                $dossier->spec()->save($spec);
            }
        }

        $url = config('app.url') . route('dossier.single', ['locale' => app()->getLocale(), 'dossier' => $dossier->id], false);
        $path = "qr_codes/$dossier->id.png";
        \QrCode::size(500)->format('png')->generate($url, storage_path("app/public/$path"));
        $dossier->qr = $path;
        $dossier->save();
        $dossier->general()->save($general);
        if (isset($data['redirect'])) {
            return redirect()->route('cabinet.dossier.edit', ['locale' => app()->getLocale(), 'dossier' => $dossier->id])
                ->with('url_review', route('dossier.single', ['locale' => app()->getLocale(), 'dossier' => $dossier->id]));
        }
        return redirect()->route($new ? 'cabinet.access' : 'cabinet.dossier.index', app()->getLocale())->with('notification', $notification);
    }

    public function edit($locale, $id, $location = "site.cabinet.dossier-form", Request $request, $js = ['js/dossier.js'])
    {
        $title = Page::getTitle($location);
        if (auth()->user()->dossiers()->get()->contains('id', $id)) {
            $dossier = Dossier::findOrFail($id);
        } else {
            abort(401);
        }
        $countries = Country::where('active', 1)->get();
        $specializations = Specialization::all();
        $formHelper = new FormHelper();
        $currencies = Currency::getAllAndPrepare();
        $lang = is_null(app('request')->input('lang')) ? app()->getLocale() : app('request')->input('lang');
        if (!Dossier::isExistWithLang($lang)) {
            return redirect()->route('cabinet.dossier.create', ['locale' => app()->getLocale(), 'lang' => $lang])->with('notification', [
                'type' => 'error',
                'title' => __('modals.result_error'),
                'content' => __('modals.lang_not_exist')
            ]);
        }
        return view('site.index', compact(['location', 'title', 'countries', 'specializations', 'dossier', 'formHelper', 'currencies', 'lang', 'js']));
    }

    public function update(Request $request, $locale, $id)
    {
        app()->setLocale(substr($request->post('lang'), -2));
        $request->validate([
            'lang' => 'required|string',
            'general.fullname' => 'required|string',
            'general.office.country' => 'required|int',
            'general.office.region' => 'required|int',
            'general.office.city' => [Rule::requiredIf(function () use ($request) {
                return Region::citiesOfRegionCount($request->input('general.office.region')) > 0;
            })],
            'general.office.address.*' => 'required|string',
            'general.office.phones.*.number' => 'required|string',
            'license.*.country' => 'required|string',
            'license.*.institution' => 'required|string',
            'license.*.year' => 'required|string',
            'license.*.number' => 'required|string',
            'license.*.photo' => 'required',
            'file.photo' => 'mimes:jpeg,jpg,png,gif|nullable|max:10000',
            'agreement' => 'accepted',
        ]);
        $data = $request->except('_token');
        $dossier = Dossier::findOrFail($id);

        $dossier->locale = substr($data['lang'], -2);

        $general = General::find([$dossier->general->id])->first();
        request()->has('file.photo') ? $data['general']['photo'] = request()->file('file.photo')->store('avatars') : '';
        $general->update($data['general']);
        $general->save();

        $dossier->license = $data['license'];
        $dossier->overview = Dossier::checkForNullable($data, 'overview');
        $dossier->contacts = Dossier::checkForNullable($data, 'contacts');
        $dossier->education = Dossier::checkForNullable($data, 'education');
        $dossier->experience_other = Dossier::checkForNullable($data, 'experience_other');
        $dossier->training = Dossier::checkForNullable($data, 'training');
        $dossier->publications = Dossier::checkForNullable($data, 'publications');
        $dossier->recommendations = Dossier::checkForNullable($data, 'recommendations');
        $dossier->honors = Dossier::checkForNullable($data, 'honors');
        $dossier->services = Dossier::checkForNullable($data, 'services');
        $dossier->admin_seen_at = null;
        if (isset($data['general']['office']['city']) && $data['general']['office']['city'] !== "") {
            $dossier->city_id = $data['general']['office']['city'];
            $dossier->region_id = $data['general']['office']['region'];
        } else {
            $dossier->region_id = $data['general']['office']['region'];
        }
        $dossier->status = Dossier::STATUS_SAVED;
        $notification = [
            'type' => 'success',
            'title' => __('modals.result_success'),
            'content' => __('modals.dossier_updated')
        ];
        if (isset($data['status'])) {
            if ($data['status'] != Dossier::STATUS_NEW) {
                abort(400);
            }
            $notification = [
                'type' => 'success',
                'title' => __('modals.result_success'),
                'content' => __('modals.dossier_status_updated')
            ];
            $dossier->status = $data['status'];
        }

        $data['experience'] = Dossier::checkForNullable($data, 'experience');
        if (!is_null($dossier->experince) || !is_null($data['experience'])) {
            $specData = $data['experience']['specs'];
            unset($data['experience']['specs']);
        }
        if (!is_null($dossier->experience)) {
            Experience::destroy($dossier->experience->id);
            $experience = new Experience($data['experience']);
            $experience->save();
            $dossier->experience()->save($experience);
        } elseif(!is_null($data['experience'])) {
            $experience = new Experience($data['experience']);
            $experience->save();
            $dossier->experience()->save($experience);
        }

        if (!is_null($dossier->spec)) {
            Spec::destroy($dossier->spec->id);
            $spec = new Spec($specData);
            $spec->save();
            $dossier->spec()->save($spec);
        } elseif(isset($specData)) {
            $spec = new Spec($specData);
            $spec->save();
            $dossier->spec()->save($spec);
        }
        $dossier->save();
        $dossier->general()->save($general);

        if (isset($data['redirect'])) {
            return redirect()->route('cabinet.dossier.edit', ['locale' => app()->getLocale(), 'dossier' => $dossier->id])->with('url_review', route('dossier.single', ['locale' => app()->getLocale(), 'dossier' => $dossier->id]));
        }
        return redirect()->route('cabinet.dossier.index', app()->getLocale())->with('notification', $notification);
    }

    public function destroy(Request $request, $locale, $id)
    {
        if (auth()->user()->dossiers()->get()->contains('id', $id)) {
            Dossier::destroy($id);
            return back()->with('notification', [
                'type' => 'success',
                'title' => __('modals.result_success'),
                'content' => __('modals.dossier_deleted')
            ]);
        }
        abort(401);
    }

    public function activate(Request $request, $locale, $id)
    {
        if (auth()->user()->dossiers()->get()->contains('id', $id)) {
            $dossier = Dossier::find([$id])->first();
            if ($dossier->status != Dossier::STATUS_NOT_ACTIVE) {
                abort(401);
            }
            $dossier->status = Dossier::STATUS_ACTIVE;
            $dossier->save();
            return back()->with('notification', [
                'type' => 'success',
                'title' => __('modals.result_success'),
                'content' => __('modals.dossier_status_updated')
            ]);
        }
        abort(401);
    }

    public function stop(Request $request, $locale, $id)
    {
        if (auth()->user()->dossiers()->get()->contains('id', $id)) {
            $dossier = Dossier::find([$id])->first();
            if ($dossier->status != Dossier::STATUS_ACTIVE) {
                abort(401);
            }
            $dossier->status = Dossier::STATUS_NOT_ACTIVE;
            $dossier->save();
            return back()->with('notification', [
                'type' => 'success',
                'title' => __('modals.result_success'),
                'content' => __('modals.dossier_status_updated')
            ]);
        }
        abort(401);
    }

    public function addToBookmark(Request $request, $id)
    {
        if ($request->session()->exists('bookmark')) {
            if (!array_search($id, session('bookmark'))) {
                $request->session()->push('bookmark', $id);
                $request->session()->save();
                return response()->json([
                    'success' => true,
                    'count' => count(session('bookmark'))
                ]);
            }
        }
        $request->session()->push('bookmark', $id);
        $request->session()->save();
        return response()->json([
            'success' => true,
            'count' => count(session('bookmark'))
        ]);
    }

    public function deleteFromBookmark(Request $request, $id)
    {
        if ($request->session()->exists('bookmark')) {
            $sessionDossierKey = array_search($id, session('bookmark'));
            $request->session()->forget('bookmark.' . $sessionDossierKey);
            $request->session()->save();
            return response()->json([
                'success' => true,
                'count' => count(session('bookmark'))
            ], 200);
        }
    }
}
