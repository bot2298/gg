<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Complaint;
use App\Models\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class ComplaintController extends Controller
{
    public function applicationTypeStore(Request $request){
        $this->validate($request, [
            'text' => 'required'
        ]);

        if (Complaint::where('type', Complaint::TYPE_APPLICATION)
            ->where('application_id', $request->post('application_id'))
            ->where('ip', $request->ip())
            ->first()){
            return response()->json([], 403);
        }

        $complaint = new Complaint([
            'type' => Complaint::TYPE_APPLICATION,
            'application_id' => $request->post('application_id'),
            'user_id' => auth()->id(),
            'content' => $request->input('text'),
            'locale' => app()->getLocale(),
            'ip' => $request->ip()
        ]);

        return response()->json([
            'success' => $complaint->save()
        ]);
    }

    public function ratingTypeStore(Request $request){
        $this->validate($request, [
            'text' => 'required'
        ]);

        $rating = Rating::findOrFail($request->post('rating_id'));
        if($rating->user_id !== auth()->id()){
            abort(401);
        }

        if (Complaint::where('type', Complaint::TYPE_RATING)
            ->where('rating_id', $request->post('rating_id'))
            ->where('ip', $request->ip())
            ->first()){
            return response()->json([], 403);
        }

        $complaint = new Complaint([
            'type' => Complaint::TYPE_RATING,
            'rating_id' => $request->post('rating_id'),
            'user_id' => auth()->id(),
            'content' => $request->input('text'),
            'locale' => app()->getLocale(),
            'ip' => $request->ip()
        ]);

        return response()->json([
            'success' => $complaint->save()
        ]);
    }

    public function lawyerTypeStore(Request $request){
        $this->validate($request, [
            'text' => 'required|max:1000',
            'fullname' => 'required|max:100',
            'address' => 'required|max:100',
            'email' => 'required|email|max:100',
            'number' => 'required|max:100',
            'attachment' => [
                'required',
                'mimetypes:image/*,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf',
                'max:5000'
            ]
        ]);
        if(Cookie::has('application_id')){
            $application = Application::findOrFail(Cookie::get('application_id'));
        }

        if (Complaint::where('type', Complaint::TYPE_LAWYER)
            ->where('user_id', $request->post('lawyer_id'))
            ->where('ip', $request->ip())
            ->first()){
            return response()->json([], 403);
        }

        $attachment = $request->file('attachment')->store('complaint_attachments');

        $complaint = new Complaint([
            'type' => Complaint::TYPE_LAWYER,
            'client_email' => $application->email ?? 'Гість',
            'user_id' => $request->post('lawyer_id'),
            'content' => $request->post('text'),
            'fullname' => $request->post('fullname'),
            'address' => $request->post('address'),
            'contact_email' => $request->post('email'),
            'contact_number' => $request->post('number'),
            'attachment' => $attachment,
            'locale' => app()->getLocale(),
            'ip' => $request->ip()
        ]);

        return response()->json([
            'success' => $complaint->save()
        ]);
    }
}
