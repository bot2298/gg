<?php

namespace App\Http\Controllers\Admin;

use App\Models\Dossier;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CardRequest as StoreRequest;
use App\Http\Requests\CardRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\Storage;

/**
 * Class CardCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CardCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Dossier');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/card');
        $this->crud->setEntityNameStrings('візитка', 'візитки');
        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');
        $this->crud->removeButton('update');
        $this->crud->allowAccess('show');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->addFilter([ // dropdown filter
            'name' => 'locale',
            'type' => 'dropdown',
            'label'=> 'Мова'
        ],
            config('app.locales'),
            function($value) { // if the filter is active
            $this->crud->addClause('where', 'locale', $value);
        });
        $this->crud->enableExportButtons();
        $this->crud->addClause('where', 'status', Dossier::STATUS_ACTIVE);
        $this->crud->addColumns([
            [
                'name' => 'general.fullname',
                'label' => 'Ім\'я'
            ],
//            [
//                'name' => 'qr',
//                'type' => 'image',
//                'prefix' => 'storage/',
//                'label' => 'QR'
//            ],
            [
                'name' => 'address_full',
                'label' => 'Адреса',
                'type' => 'closure',
                'function' => function($entry){
                    $address_full = __('countries.'.$entry->region->country->name, [], $entry->locale).", ".__('regions.'.$entry->region->name, [], $entry->locale);
                    if ($entry->city){
                        $address_full.= ", ".__("cities.".$entry->city->name, [], $entry->locale);
                    }
                    $address_full.= "<br>\r\n";
                    if(isset($entry->general->office['address'])){
                        foreach($entry->general->office['address'] as $address){
                            $address_full.= $address."<br>\r\n";
                        }
                    }
                    return $address_full;
                }
            ],
            [
                'name' => 'phones_all',
                'label' => 'Телефони',
                'type' => 'closure',
                'function' => function($entry){
                    $result = '';
                    if(isset($entry->general->office['phones'])){
                        foreach($entry->general->office['phones'] as $phone){
                            $result .= $phone['number']."<br>\r\n";
                        }
                    }
                    return $result;
                }
            ],
            [
                'name' => 'qr_url',
                'label' => 'Посилання на QR код',
                'type' => 'closure',
                'function' => function($entry){
                    return Storage::url($entry->qr);
                }
            ],
        ]);

        // add asterisk for fields that are required in CardRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function show($id)
    {
        $content = parent::show($id);
        $this->crud->removeColumns([
            'locale',
            'status',
            'seen_counter',
            'qr'
        ]);

        return $content;
    }
}
