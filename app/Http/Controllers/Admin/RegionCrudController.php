<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\RegionRequest as StoreRequest;
use App\Http\Requests\RegionRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class RegionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class RegionCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Region');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/region');
        $this->crud->setEntityNameStrings('регіон', 'регіони');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->addFilter([ // select2 filter
            'name' => 'country_id',
            'type' => 'select2',
            'label'=> 'Країна'
        ], function() {
            return \App\Models\Country::all()
                ->mapWithKeys(function($item, $Key){
                    return [$item->id => __('countries.'.$item->name)];
                })
                ->toArray();
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'country_id', $value);
        });


        $this->crud->setColumns([
            [
                'name' => 'name_trans',
                'type' => 'closure',
                'label' => 'Назва (укр)',
                'function' => function ($entry){
                    return __('regions.'.$entry->name);
                }
            ],
            [
                'name' => 'name',
                'label' => 'Ключ для перекладів'
            ],
            [
                'name' => 'country.name',
                'label' => 'Країна (укр)',
                'type' => 'closure',
                'function' => function($entry){
                    return __('countries.'.$entry->country->name);
                }
            ],
            [
                'name' => 'cost',
                'label' => 'Вартість'
            ]
        ]);

        $this->crud->addFields([
            [
                'name' => 'name',
                'label' => 'Ключ для перекладів'
            ],
            [
                'name' => 'cost',
                'label' => 'Вартість',
                'default' => 0
            ],
            [
                'label' => "Країна",
                'name' => 'country_id',
                'type' => 'select_from_array',
                'options' => Country::all()->mapWithKeys(function($item, $key){
                    return [$item->id => __("countries.".$item->name)];
                })
            ]
        ]);

        // add asterisk for fields that are required in RegionRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
