<?php

namespace App\Http\Controllers\Admin;

use App\Models\Application;
use App\Models\Chat;
use App\Models\Dossier;
use App\Notifications\MessageFromAdmin;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\DossierRequest as StoreRequest;
use App\Http\Requests\DossierRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Carbon;

/**
 * Class DossierCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class DossierCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Dossier');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/dossier');
        $this->crud->setEntityNameStrings('досьє', 'досьє');
        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */


        // todo fix
//        $this->crud->orderBy('updated_at', 'DESC');

        $this->crud->setColumns([
            [
                'name' => 'fullname',
                'label' => 'Адвокат',
                'type' => 'closure',
                'function' => static function($entry){
                    return $entry->general->fullname;
                }
            ],
            [
                'name' => 'status',
                'label' => 'Статус',
                'type' => 'closure',
                'function' => function($entry){
                    return $entry->getAdminStatus();
                },
                'orderable' => false,
            ],
            [
                'name' => 'created_at',
                'label' => 'Створена',
                'orderable' => false,
            ],
            [
                'name' => 'updated_at',
                'label' => 'Оновлена',
                'orderable' => false,
            ],
            [
                'name' => 'admin_seen_at',
                'label' => 'Переглянута менеджером',
                'orderable' => false,
            ]
        ]);

        $this->crud->addFields([
            [
                'name' => 'status',
                'label' => "Статус",
                'type' => 'select_from_array',
                'options' => [
                    Dossier::STATUS_SAVED => 'Чернетка',
                    Dossier::STATUS_NEW => 'Нова',
                    Dossier::STATUS_ACTIVE => 'Активне досьє',
                    DOSSIER::STATUS_NOT_ACTIVE => 'Неактивне досьє',
                    Dossier::STATUS_REJECTED => 'Відхилена',
                ],
            ]
        ]);

        $this->crud->addFilter([ // dropdown filter
            'name' => 'status',
            'type' => 'dropdown',
            'label'=> 'Статус'
        ], [
            Dossier::STATUS_SAVED => 'Чернетка',
            Dossier::STATUS_NEW => 'Нова',
            Dossier::STATUS_ACTIVE => 'Активне досьє',
            DOSSIER::STATUS_NOT_ACTIVE => 'Неактивне досьє',
            Dossier::STATUS_REJECTED => 'Відхилена',
        ], function($value) { // if the filter is active
            $this->crud->addClause('where', 'status', $value);
        });


        $this->crud->addFilter([ // dropdown filter
            'name' => 'with_access',
            'type' => 'dropdown',
            'label'=> 'Доступи'
        ], [
            '1' => 'Тільки з активними доступами',
        ],
            function($value) { // if the filter is active
                if((bool)$value){
                    $this->crud->addClause('whereHas', 'user.access', function($query){
                        return $query->where('active_until', '>=', Carbon::now());
                    });
                }
            });

        $this->crud->addButtonFromView('line', 'approve-dossier', 'approve-dossier', 'end');
        $this->crud->addButtonFromView('line', 'block-dossier', 'block-dossier', 'end');
//        $this->crud->enableDetailsRow();
//        $this->crud->allowAccess('details_row');
        $this->crud->allowAccess('show');

        // add asterisk for fields that are required in DossierRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function showDetailsRow($id){
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;

        return view('crud::dossier-details', $this->data);
    }

    public function cabinetNotify($user, $notification){
        $admin_chat = Chat::firstOrCreate(['user_id' => $user, 'admin' => 1]);
        $admin_chat->messages()->create([
            'owner' => 'admin',
            'message' => $notification,
        ]);
        $admin_chat->save();

        $admin_chat->user->notify(new MessageFromAdmin($admin_chat, app()->getLocale()));
    }

    public function block(Dossier $dossier){
        $dossier->status = Dossier::STATUS_REJECTED;
        $dossier->save();
        $this->cabinetNotify($dossier->user_id, __('mail.dossier_rejected'));
    }

    public function approve(Dossier $dossier){
        $dossier->status = Dossier::STATUS_ACTIVE;
        $dossier->save();
        $this->cabinetNotify($dossier->user_id, __('mail.dossier_approved'));
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function show($id)
    {
        $dossier = Dossier::findOrFail($id);
        if (is_null($dossier->admin_seen_at)){
            $dossier->admin_seen_at = Carbon::now();
            $dossier->save();
        }
        $content = parent::show($id);

//        $this->crud->removeButton('approve');
//        $this->crud->removeButton('block');

        $this->crud->removeColumns([
            'qr',
            'locale',
            'seen_counter',
        ]);

        # https://backpackforlaravel.com/docs/3.6/crud-columns
        $this->crud->addColumns([
            [
                'name' => 'locale_val',
                'label' => 'Мова',
                'type' => 'closure',
                'function' => function($entry){
                    return config('app.locales')[$entry->locale];
                }
            ],
            [
                'name' => 'seen_counter',
                'label' => 'Переглянуто разів'
            ],
            [
                'name' => 'qr',
                'label' => 'QR',
                'type' => 'image',
                'prefix' => 'storage/'
            ],
            [
                'name' => 'general.fullname',
                'label' => 'Ім\'я і прізвище'
            ],
            [
                'name' => 'general.photo',
                'prefix' => 'storage/',
                'label' => 'Фото',
                'type' => 'image',
            ],
            [
                'label' => 'Місто',
                'name' => 'city_full',
                'type' => 'closure',
                'function' => function($entry){
                    return __("countries.".$entry->region->country->name).", ".__("regions.".$entry->region->name).(isset($entry->city) ? ", ".__("cities.".$entry->city->name) : null);
                }
            ],
            [
                'name' => 'general.office.address',
                'label' => 'Адреса',
                'type' => 'array',
            ],
            [
                'name' => 'phones',
                'label' => 'Телефони',
                'type' => 'closure',
                'function' => function ($entry){
                    $phones = $entry->general->office['phones'];
                    $result = '';
                    if($phones){
                        foreach ($phones as $phone) {
                            $result.= $phone['number'].'<br />';
                            $result.= implode(', ',$phone['messengers'] ?? []).'<br /><br />';
                        }
                    }
                    return $result;
                }
            ],
            [
                'label' => 'Адвокатська діяльність',
                'name' => 'license_full',
                'type' => 'closure',
                'function' => function($entry){
                    $result = '';
                    if($license = $entry->license){
                        foreach($license as $item){
                            $result.= '<b>Країна отримання</b>: '.($item['country'] ?? '')."<br />";
                            $result.= '<b>Орган, що видав</b>: '.($item['institution'] ?? '')."<br /><br />";
                            $result.= '<b>Рік отримання</b>: '.($item['year'] ?? '')."<br /><br />";
                            $result.= '<b>Номер свідоцтва</b>: '.($item['number'] ?? '')."<br /><br />";
                            $result.= "<br />";
                            $photo = $item['photo'] ?? '';
                            if ($photo) {
                                $result.= '<b>Фото документа</b>: ';

                                foreach ($photo as $image) {
                                    $image = $image['path'];
                                    $result .= "<img style='width: 32px;' src='$image' alt=''>";
                                }
                            }
                            $result.= "<br />";
                        }
                        $result.= '<br /><br />';
                    }
                    return $result;
                }
            ],
            [
                'label' => 'Загальні відомості',
                'name' => 'overview_full',
                'type' => 'closure',
                'function' => function ($entry){
                    $result = '';
                    if($overview = $entry->overview){
                        $result.= '<b>Вільне володіння мовами</b>: ';
                        $result.= isset($overview['languages']) ? $overview['languages'] : '';
                        $result.= '<br />';
                        $result.= '<b>Девіз</b>: ';
                        $result.= isset($overview['motto']) ? $overview['motto'] : '';
                        $result.= '<br />';
                        $result.= '<b>Сильні якості</b>: ';
                        $result.= isset($overview['strong_sides']) ? $overview['strong_sides'] : '';
                        $result.= '<br />';
                        $result.= '<b>Хоббі</b>: ';
                        $result.= isset($overview['hobbies']) ? $overview['hobbies'] : '';
                        $result.= '<br />';
                        $result.= '<b>Сайт</b>: ';
                        $site = $overview['site'] ?? '';
                        $result.= isset($overview['site']) ? "<a href='$site'>$site</a>" : '';
                        $result.= '<br />';

                        $result.= '<b>Facebook/підписники</b>: ';
                        $result.= ($overview['facebook'] ?? '')." / ".($overview['facebook_followers'] ?? 'N/A');
                        $result.= '<br />';
                        $result.= '<b>Instagram/підписники</b>: ';
                        $result.= ($overview['instagram'] ?? '')." / ".($overview['instagram_followers'] ?? 'N/A');
                        $result.= '<br />';
                        $result.= '<b>Linkedin/підписники</b>: ';
                        $result.= ($overview['linkedin'] ?? '')." / ".($overview['linkedin_followers'] ?? 'N/A');
                        $result.= '<br />';
                        $result.= '<b>Twitter/підписники</b>: ';
                        $result.= ($overview['twitter'] ?? '')." / ".($overview['twitter_followers'] ?? 'N/A');
                        $result.= '<br />';
                        $result.= '<b>Youtube/підписники</b>: ';
                        $result.= ($overview['youtube'] ?? '')." / ".($overview['youtube_followers'] ?? 'N/A');
                        $result.= '<br />';
                    }
                    return $result;
                }
            ],
            [
                'label' => 'Контактні дані',
                'name' => 'contacts_full',
                'type' => 'closure',
                'function' => function ($entry){
                    $result = '';
                    if($contacts = $entry->contacts){
                        $result.= '<b>E-mail</b>: ';
                        $result.= '<br />';
                        $emails = $contacts['emails'] ?? null;
                        if ($emails){
                            foreach ($emails as $email){
                                $result .= $email.'<br />';
                            }
                        }
                        $result.= '<br />';

                        $result.= '<i>Робочий час</i>: ';
                        $result.= '<br />';
                        if ($working_hours = $contacts['working_hours']){
                            if (isset($contacts['working_hours']['mo'])){
                                $result.= '<b>Понеділок</b>: ';
                                if (isset($contacts['working_hours']['mo']['start'])){
                                    $result .= $contacts['working_hours']['mo']['start']."-";
                                }
                                if (isset($contacts['working_hours']['mo']['end'])){
                                    $result .= $contacts['working_hours']['mo']['end'].'<br />';
                                }
                            }
                            if (isset($contacts['working_hours']['tu'])){
                                $result.= '<b>Вівторок</b>: ';
                                if (isset($contacts['working_hours']['tu']['start'])){
                                    $result .= $contacts['working_hours']['tu']['start']."-";
                                }
                                if (isset($contacts['working_hours']['tu']['end'])){
                                    $result .= $contacts['working_hours']['tu']['end'].'<br />';
                                }
                            }
                            if (isset($contacts['working_hours']['we'])){
                                $result.= '<b>Середа</b>: ';
                                if (isset($contacts['working_hours']['we']['start'])){
                                    $result .= $contacts['working_hours']['we']['start']."-";
                                }
                                if (isset($contacts['working_hours']['we']['end'])){
                                    $result .= $contacts['working_hours']['we']['end'].'<br />';
                                }
                            }
                            if (isset($contacts['working_hours']['th'])){
                                $result.= '<b>Четвер</b>: ';
                                if (isset($contacts['working_hours']['th']['start'])){
                                    $result .= $contacts['working_hours']['th']['start']."-";
                                }
                                if (isset($contacts['working_hours']['th']['end'])){
                                    $result .= $contacts['working_hours']['th']['end'].'<br />';
                                }
                            }
                            if (isset($contacts['working_hours']['fr'])){
                                $result.= '<b>П\'ятниця</b>: ';
                                if (isset($contacts['working_hours']['fr']['start'])){
                                    $result .= $contacts['working_hours']['fr']['start']."-";
                                }
                                if (isset($contacts['working_hours']['fr']['end'])){
                                    $result .= $contacts['working_hours']['fr']['end'].'<br />';
                                }
                            }
                            if (isset($contacts['working_hours']['sa'])){
                                $result.= '<b>Субота</b>: ';
                                if (isset($contacts['working_hours']['sa']['start'])){
                                    $result .= $contacts['working_hours']['sa']['start']."-";
                                }
                                if (isset($contacts['working_hours']['sa']['end'])){
                                    $result .= $contacts['working_hours']['sa']['end'].'<br />';
                                }
                            }
                            if (isset($contacts['working_hours']['su'])){
                                $result.= '<b>Неділя</b>: ';
                                if (isset($contacts['working_hours']['su']['start'])){
                                    $result .= $contacts['working_hours']['su']['start']."-";
                                }
                                if (isset($contacts['working_hours']['su']['end'])){
                                    $result .= $contacts['working_hours']['su']['end'].'<br />';
                                }
                            }
                        }
                        $result.= '<br />';
                    }
                    return $result;
                }
            ],
            [
                'label' => 'Освіта',
                'name' => 'education_full',
                'type' => 'closure',
                'function' => function ($entry){
                    $result = '';
                    if($education = $entry->education){
                        if (isset($education['higher'])){
                            $result.= '<i>Вища освіта</i>:<br />';

                            foreach($entry->education['higher'] as $higher){
                                $result.= '<b>З</b>: '.($higher['from'] ?? '')."<br />";
                                $result.= '<b>По</b>: '.($higher['to'] ?? '')."<br />";
                                $result.= '<b>Країна</b>: '.($higher['country'] ?? '')."<br />";
                                $result.= '<b>ВНЗ</b>: '.($higher['university'] ?? '')."<br />";
                                $result.= '<b>Ступінь</b>: ';
                                if(isset($higher['degree'])){
                                   $result.= $higher['degree'];
                                }
                                $result.= "<br />";
                                $result.= '<b>Галузь</b>: ';
                                if(isset($higher['field'])){
                                    $result.= $higher['field'];
                                }
                                $result.= "<br />";
                                $result.= '<b>Спеціальність</b>: '.($higher['specialty'] ?? '')."<br />";
                                $photo = $higher['photo'] ?? '';
                                if ($photo) {
                                    $result.= '<b>Фото документа</b>: ';
                                    foreach ($photo as $image) {
                                        $image = $image['path'];
                                        $result .= "<img style='width: 32px;' src='$image' alt=''>";
                                    }
                                    $result.= "<br />";
                                }
                            }

                            $result.= '<br /><br />';
                        }

                        if (isset($education['academic_status'])){
                            $result.= '<i>Вчене звання</i>:<br />';

                            foreach($entry->education['academic_status'] as $academic_status){
                                $result.= '<b>З</b>: '.($academic_status['from'] ?? '')."<br />";
                                $result.= '<b>По</b>: '.($academic_status['to'] ?? '')."<br />";
                                $result.= '<b>ВНЗ</b>: '.($academic_status['university'] ?? '')."<br />";
                                $result.= '<b>Ступінь</b>: ';
                                if(isset($academic_status['degree'])){
                                    $result.= $academic_status['degree'];
                                }
                                $result.= "<br />";
                                $photo = $academic_status['photo'] ?? '';
                                if (!empty($photo)) {
                                    $result.= '<b>Фото документа</b>: ';
                                    foreach ($photo as $image) {
                                        $image = $image['path'];
                                        $result .= "<img style='width: 32px;' src='$image' alt=''>";
                                    }
                                    $result.= "<br />";
                                }
                            }
                            $result.= '<br /><br />';
                        }

                        if (isset($education['teaching'])){
                            $result.= '<i>Викладацька діяльність</i>:<br />';

                            foreach($entry->education['teaching'] as $teaching){
                                $result.= '<b>З</b>: '.($teaching['from'] ?? '')."<br />";
                                $result.= '<b>По</b>: '.($teaching['to'] ?? '')."<br />";
                                $result.= '<b>ВНЗ</b>: '.($teaching['university'] ?? '')."<br />";
                                $result.= '<b>Тематика</b>: '.($teaching['topic'] ?? '')."<br /><br />";
                            }
                            $result.= '<br /><br />';
                        }
                    }
                    return $result;
                }
            ],
            [
                'label' => 'Адвокатська діяльність',
                'name' => 'experience_full',
                'type' => 'closure',
                'function' => function($entry){
                    $result = '';
                    if($experience = $entry->experience){
                        if ($years = $experience->years)
                        $result.= '<b>Стаж</b>: '.$years." (роки)<br />";
                        if ($countries = $experience->countries){
                            $result.= '<b>Країни</b>: '.implode(', ', $countries)."<br />";
                        }
                        if ($regions = $experience->regions){
                            $result.= '<b>Регіони</b>: '.implode(', ', $regions)."<br />";
                        }
                        if ($cities = $experience->cities){
                            $result.= '<b>Міста</b>: '.implode(', ', $cities)."<br />";
                        }
                        $result.='<br />';
                    }
                    if(isset($entry->spec) && $spec = $entry->spec) {
                        $result.= '<i>Спеціалізація</i>:<br />';
                        if ($specializations = $spec->specializations){
                            $result.= '<b>Спеціалізація</b>: '.implode(', ', $specializations)."<br />";
                        }
                        if ($activities = $spec->activities){
                            $result.= '<b>Вид діяльності</b>: '.implode(', ', $activities)."<br />";
                        }
                        if ($form = $spec->form){
                            $result.= '<b>Форма здійснення діяльності</b>: '.$form."<br />";
                        }
                        if ($appeal_exp = $spec->appeal_exp){
                            $result.= '<b>Досвід представництва у суді апеляційної інстанції</b>: '.$appeal_exp."<br />";
                        }
                        if ($appeal_cases = $spec->appeal_cases){
                            $result.= '<b>Важлива справа, у якій Вами взято участь у апеляційній інстанції і яка може
                                охарактеризувати Вашу професійну компетентність (найменування суду, період розгляду
                                справи, стисла суть справи)</b>:<br /> '
                                .implode(', ' . "<br />", $appeal_cases)."<br />";
                        }
                        if ($appeal_exp = $spec->cassation_exp){
                            $result.= '<b>Досвід представництва у суді касаційної інстанції</b>: '.$appeal_exp."<br />";
                        }
                        if ($cassation_cases = $spec->cassation_cases){
                            $result.= '<b>Важлива справа, у якій Вами взято участь у касаційній інстанції і яка може
                                охарактеризувати Вашу професійну компетентність (найменування суду, період розгляду
                                справи, стисла суть справи)</b>: <br />'
                                .implode(', ' . "<br />", $cassation_cases)."<br />";
                        }
                        if ($intl_exp = $spec->intl_exp){
                            $result.= '<b>Досвід представництва у міжнародних установах (органах, організаціях тощо)
                                з розв’язання спорів</b>: '.$intl_exp."<br /> ";
                        }
                        if ($intl_cases = $spec->intl_cases){
                            $result.= '<b>Важлива справа, у якій Вами взято участь у міжнародній установі і яка може
                                охарактеризувати Вашу професійну компетентність (найменування суду, період розгляду
                                справи, стисла суть справи)</b>: <br />'
                                .implode(', ' . "<br />", $intl_cases)."<br />";
                        }
                        $result.='<br />';
                    }

                    return $result;
                }
            ],
            [
                'label' => 'Інша адвокатська діяльність',
                'name' => 'experience_other_full',
                'type' => 'closure',
                'function' => function($entry){
                    $result = '';
                    if($experience_other = $entry->experience_other){
                        if (isset($experience_other['experience'])){
                            foreach($experience_other['experience'] as $exp){
                                $result.= '<b>З</b>: '.($exp['from'] ?? '')."<br />";
                                $result.= '<b>По</b>: '.($exp['to'] ?? '')."<br />";
                                $result.= '<b>ВНЗ</b>: '.($exp['place_of_work'] ?? '')."<br />";
                                $result.= '<b>Країна</b>: '.($exp['position'] ?? '')."<br /><br />";
                            }
                            $result.= '<br /><br />';
                        }
                        $result.= '<i>Свідоцтво патентного повіреного</i><br />';
                        if (isset($experience_other['patent_license'])){
                            $result.= '<b>Країна</b>: '.($experience_other['country'] ?? '')."<br />";
                            $result.= '<b>Орган, що видав</b>: '.($exp['to'] ?? '')."<br />";
                            $result.= '<b>Рік отримання</b>: '.($exp['place_of_work'] ?? '')."<br />";
                            $result.= '<b>Номер свідоцтва</b>: '.($exp['position'] ?? '')."<br /><br />";
                            $result.= "<br />";
                            $photo = $exp['photo'] ?? '';
                            if (!empty($photo)) {
                                $result.= '<b>Фото документа</b>: ';
                                foreach ($photo as $image) {
                                    $image = $image['path'];
                                $result.= "<img style='width: 32px;' src='$image' alt=''>";
                                }
                                $result.= "<br />";
                            }
                        }
                    }

                    return $result;
                }
            ],
            [
                'label' => 'Підвищення кваліфікації',
                'name' => 'training_full',
                'type' => 'closure',
                'function' => function($entry){
                    $result = '';
                    if($training = $entry->training){
                        foreach($training as $item){
                            $result.= '<b>З</b>: '.($item['from'] ?? '')."<br />";
                            $result.= '<b>По</b>: '.($item['to'] ?? '')."<br />";
                            $result.= '<b>Країна</b>: '.($item['country'] ?? '')."<br />";
                            $result.= '<b>Навчальний заклад</b>: '.($item['university'] ?? '')."<br /><br />";
                            $result.= '<b>Тема</b>: '.($item['topic'] ?? '')."<br /><br />";
                            $result.= "<br />";
                            $photo = $item['photo'] ?? '';
                            if (!empty($photo)) {
                                $result.= '<b>Фото документа</b>: ';
                                foreach ($photo as $image) {
                                    $image = $image['path'];
                                    $result .= "<img style='width: 32px;' src='$image' alt=''>";
                                }
                                $result.= "<br />";
                            }
                        }
                        $result.= '<br /><br />';
                    }

                    return $result;
                }
            ],
            [
                'label' => 'Публікації',
                'name' => 'publications_full',
                'type' => 'closure',
                'function' => function($entry){
                    $result = '';
                    if($publications = $entry->publications){
                        foreach($publications as $item){
                            $result.= '<b>Видання</b>: '.($item['edition'] ?? '')."<br />";
                            $result.= '<b>Тема</b>: '.($item['topic'] ?? '')."<br /><br />";
                            $result.= '<b>Посилання на публікацію</b>: '.($item['url'] ?? '')."<br /><br />";
                            $result.= "<br />";
                            $photo = $item['photo'] ?? '';
                            if (!empty($photo)) {
                                $result.= '<b>Фото документа</b>: ';
                                foreach ($photo as $image) {
                                    $image = $image['path'];
                                    $result .= "<img style='width: 32px;' src='$image' alt=''>";
                                }
                                $result.= "<br />";
                            }
                        }
                        $result.= '<br /><br />';
                    }

                    return $result;
                }
            ],
            [
                'label' => 'Рекомендації',
                'name' => 'recommendations_full',
                'type' => 'closure',
                'function' => function($entry){
                    $result = '';
                    if($recommendations = $entry->recommendations){
                        foreach($recommendations as $item){
                            $result.= '<b>Прізвище, ім’я</b>: '.($item['fullname'] ?? '')."<br />";
                            $result.= '<b>Посада</b>: '.($item['position'] ?? '')."<br /><br />";
                            $result.= '<b>Місце роботи</b>: '.($item['place_of_work'] ?? '')."<br /><br />";
                            $result.= "<br />";
                            $photo = $item['photo'] ?? '';
                            if (!empty($photo)) {
                                $result.= '<b>Фото документа</b>: ';
                                foreach ($photo as $image)
                                    $image = $image['path'];
                                $result.= "<img style='width: 32px;' src='$image' alt=''>";
                            }
                        }
                        $result.= '<br /><br />';
                    }
                    return $result;
                }
            ],
            [
                'label' => 'Відзнаки',
                'name' => 'honors_full',
                'type' => 'closure',
                'function' => function($entry){
                    $result = '';
                    if($honors = $entry->honors){
                        foreach($honors as $item){
                            $result.= '<b>Рік</b>: '.($item['year'] ?? '')."<br />";
                            $result.= '<b>Найменування</b>: '.($item['name'] ?? '')."<br /><br />";
                            $result.= "<br />";
                        }
                        $result.= '<br /><br />';
                    }
                    return $result;
                }
            ],
            [
                'label' => 'Вартість послуг',
                'name' => 'services_full',
                'type' => 'closure',
                'function' => function($entry){
                    $result = '';
                    if($services = $entry->services){
                        foreach($services as $item){
                            $result.= '<b>Найменування</b>: '.($item['name'] ?? '')."<br />";
                            $result.= '<b>Вартість</b>: '.($item['cost'] ?? '')."<br /><br />";
                            $result.= '<b>Валюта</b>: '.($item['currency'] ?? '')."<br /><br />";
                            $result.= "<br />";
                        }
                        $result.= '<br /><br />';
                    }
                    return $result;
                }
            ]
        ]);

        return $content;
    }
}
