<?php

namespace App\Http\Controllers\Admin;

use App\Models\Complaint;
use App\Models\Feedback;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Notifications\ComplaintResponse;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\LawyerComplaintRequest as StoreRequest;
use App\Http\Requests\LawyerComplaintRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

/**
 * Class LawyerComplaintCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class LawyerComplaintCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Complaint');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/lawyer-complaint');
        $this->crud->setEntityNameStrings('скарга', 'скарги на адвокатів');
        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');
        $this->crud->removeButton('update');
        $this->crud->addButtonFromView('line', 'respond', 'respond', 'beginning');
        $this->crud->allowAccess('show');
        $this->crud->addButtonFromView('line', 'block', 'block', 'end');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addClause('where', 'type', '=', Complaint::TYPE_LAWYER);
        $this->crud->orderBy('created_at', 'desc');

        $this->crud->setColumns([
            [
                'name' => 'user.email',
                'label' => 'Адвокат'
            ],
            [
                'name' => 'content',
                'type' => 'textarea',
                'label' => 'Зміст скарги'
            ],
            [
                'name' => 'dossiers',
                'label' => 'Досьє адвоката',
                'type' => 'closure',
                'function' => function($entry){
                    return $entry->user->getDossierLinks();
                }
            ],
            [
                'name' => 'locale',
                'label' => 'Мова'
            ],
            [
                'name' => 'client_email',
                'label' => 'Пошта клієнта'
            ],
            [
                'name' => 'response',
                'type' => 'textarea',
                'label' => 'Відповідь на скаргу'
            ],
            [
                'name' => 'created_at',
                'label' => 'Скарга створена'
            ],
            [
                'name' => 'seen_at',
                'label' => 'Скарга прочитана'
            ]
        ]);

        $this->crud->addFields([
            [
                'name' => 'response',
                'type' => 'textarea',
                'label' => 'Відповідь'
            ]
        ]);

        // add asterisk for fields that are required in LawyerComplaintRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);

        Notification::route('mail', $this->crud->entry->client_email)
            ->notify(new ComplaintResponse($this->crud->entry));
        $this->seen($this->crud->entry);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function seen($complaint){
        if (is_null($complaint->seen_at)){
            $complaint->seen_at = Carbon::now();
        }
        return $complaint->save();
    }

    public function show($id)
    {
        $complaint = Complaint::findOrFail($id);
        $this->seen($complaint);
        $content = parent::show($id);

        $this->crud->addColumns([
            [
                'name' => 'fullname',
                'label' => 'Ім\'я та прізвище'
            ],
            [
                'name' => 'address',
                'label' => 'Контактна поштова адреса'
            ],
            [
                'name' => 'contact_email',
                'label' => 'Контактна електронна адреса'
            ],
            [
                'name' => 'contact_number',
                'label' => 'Номер засобу зв\'язку'
            ],
            [
                'name' => 'attachment_link',
                'label' => 'Додаток',
                'type' => 'closure',
                'function' => function ($entry){
                    $link = Storage::url($entry->attachment);
                    return "<a href='$link'>Завантажити</a>";
                }
            ]
        ]);

        $this->crud->removeButton('block');

        $this->crud->removeColumn('type');
        $this->crud->removeColumn('application_id');
        $this->crud->removeColumn('user_id');
        $this->crud->removeColumn('rating_id');
        $this->crud->removeColumn('attachment');

        return $content;
    }
}
