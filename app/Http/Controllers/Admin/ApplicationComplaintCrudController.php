<?php

namespace App\Http\Controllers\Admin;

use App\Models\Complaint;
use App\Notifications\ComplaintResponse;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ApplicationComplaintRequest as StoreRequest;
use App\Http\Requests\ApplicationComplaintRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Notification;

/**
 * Class ApplicationComplaintCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ApplicationComplaintCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Complaint');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/application-complaint');
        $this->crud->setEntityNameStrings('скарга', 'скарги на запити');
        $this->crud->removeButton('create');
        $this->crud->removeButton('update');
        $this->crud->addButtonFromView('line', 'respond', 'respond', 'beginning');
        $this->crud->allowAccess('show');
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addClause('where', 'type', '=', Complaint::TYPE_APPLICATION);

        $this->crud->orderBy('created_at', 'desc');
        $this->crud->allowAccess('show');
        $this->crud->removeButton('delete');

        $this->crud->setColumns([
            [
                'name' => 'user.email',
                'label' => 'Адвокат'
            ],
            [
                'name' => 'content',
                'label' => 'Зміст скарги',
                'type' => 'textarea'
            ],
            [
                'name' => 'response',
                'label' => 'Відповідь на скаргу',
                'type' => 'textarea'
            ],
            [
                'name' => 'locale',
                'label' => 'Мова скарги'
            ],
            [
                'name' => 'created_at',
                'label' => 'Скарга створена'
            ],
            [
                'name' => 'seen_at',
                'label' => 'Скарга прочитана'
            ],
            [
                'name' => 'application_link',
                'label' => 'Запит',
                'type' => 'closure',
                'function' => function($entry){
                    return '<a href="'.backpack_url('application/'.$entry->application->id).'" target="_blank">'.$entry->application->code.'</a><br />';
                }
            ],
            [
                'name' => 'application.status',
                'label' => 'Статус запиту',
                'type' => 'closure',
                'function' => function($entry){
                    return $entry->application->getAdminStatus();
                }
            ],
            [
                'name' => 'application.code',
                'label' => 'Код запиту'
            ],
            [
                'name' => 'application.content',
                'label' => 'Текст запиту'
            ],
            [
                'name' => 'application.name',
                'label' => 'Ім\'я клієнта'
            ],
            [
                'name' => 'application.email',
                'label' => 'Email клієнта'
            ],
            [
                'name' => 'application.phone',
                'label' => 'Телефон клієнта'
            ],
            [
                'name' => 'application.created_at',
                'label' => 'Запит створено'
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'response',
                'type' => 'textarea',
                'label' => 'Відповідь'
            ]
        ]);

        $this->crud->addButtonFromView('line', 'respond', 'respond', 'beginning');
        $this->crud->addButtonFromView('line', 'block', 'block-application', 'end');

        // add asterisk for fields that are required in ApplicationComplaintRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);

        $this->crud->entry->user->notify(new ComplaintResponse($this->crud->entry));
        $this->seen($this->crud->entry);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function seen($complaint){
        if (is_null($complaint->seen_at)){
            $complaint->seen_at = Carbon::now();
        }
        return $complaint->save();
    }

    public function show($id)
    {
        $complaint = Complaint::findOrFail($id);
        $this->seen($complaint);
        $content = parent::show($id);

        $this->crud->removeButton('block');

        $this->crud->removeColumns([
            'type',
            'user_id',
            'rating_id',
            'client_email',
            'application_id'
        ]);

        return $content;
    }
}
