<?php

namespace App\Http\Controllers\Admin;

use App\Models\Feedback;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\FeedbackRequest as StoreRequest;
use App\Http\Requests\FeedbackRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Carbon;

/**
 * Class FeedbackCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class FeedbackCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Feedback');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/feedback');
        $this->crud->setEntityNameStrings('фідбек', 'фідбек');
        $this->crud->removeButton('create');
        $this->crud->removeButton('update');
        $this->crud->removeButton('delete');
        $this->crud->allowAccess('show');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->orderBy('created_at', 'desc');
        $this->crud->setColumns([
            [
                'name' => 'name',
                'label' => 'Ім\'я',
            ],
            [
                'name' => 'lastname',
                'label' => 'Прізвище'
            ],
            [
                'name' => 'email',
                'label' => 'E-mail'
            ],
            [
                'name' => 'message',
                'label' => 'Повідомлення',
                'type' => 'textarea'
            ],
            [
                'name' => 'created_at',
                'label' => 'Створено'
            ],
            [
                'name' => 'seen_at',
                'label' => 'Прочитано'
            ],
        ]);

        // add asterisk for fields that are required in FeedbackRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function show($id)
    {
        $feedback = Feedback::findOrFail($id);
        if (is_null($feedback->seen_at)){
            $feedback->seen_at = Carbon::now();
            $feedback->save();
        }
        $this->crud->addColumns([
            [
                'name' => 'place_of_living',
                'label' => 'Місце проживання',
            ],
            [
                'name' => 'postal_address',
                'label' => 'Поштова адреса',
            ],
            [
                'name' => 'contact_number',
                'label' => 'Контактний телефон',
            ]

        ]);
        $content = parent::show($id);

        $this->crud->removeAllButtons();

        return $content;
    }
}
