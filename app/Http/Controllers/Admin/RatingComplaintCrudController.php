<?php

namespace App\Http\Controllers\Admin;

use App\Models\Complaint;
use App\Notifications\ComplaintResponse;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\RatingComplaintRequest as StoreRequest;
use App\Http\Requests\RatingComplaintRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Notification;

/**
 * Class RatingComplaintCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class RatingComplaintCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Complaint');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/rating-complaint');
        $this->crud->setEntityNameStrings('скарга', 'скарги на оцінки та коментарі');
        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');
        $this->crud->removeButton('update');
        $this->crud->addButtonFromView('line', 'respond', 'respond', 'beginning');
        $this->crud->allowAccess('show');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->addClause('where', 'type', '=', Complaint::TYPE_RATING);
        $this->crud->orderBy('created_at', 'desc');

        $this->crud->setColumns([
            [
                'name' => 'user.email',
                'label' => 'Адвокат'
            ],
            [
                'name' => 'content',
                'label' => 'Зміст скарги',
                'type' => 'textarea'
            ],
            [
                'name' => 'response',
                'label' => 'Відповідь на скаргу',
                'type' => 'textarea'
            ],
            [
                'name' => 'locale',
                'label' => 'Мова скарги'
            ],
            [
                'name' => 'rating.blocked_at',
                'label' => 'Коментар заблоковано'
            ],
            [
                'name' => 'created_at',
                'label' => 'Скарга створена'
            ],
            [
                'name' => 'seen_at',
                'label' => 'Скарга прочитана'
            ],
            [
                'name' => 'rating.client_email',
                'label' => 'Email клієнта'
            ],
            [
                'name' => 'rating.stars',
                'label' => 'Оцінка'
            ],
            [
                'name' => 'rating.comment',
                'label' => 'Коментар',
                'type' => 'textarea'
            ],
            [
                'name' => 'rating.created_at',
                'label' => 'Коментар створено'
            ]
        ]);

        $this->crud->addFields([
            [
                'name' => 'response',
                'type' => 'textarea',
                'label' => 'Відповідь'
            ]
        ]);

        $this->crud->allowAccess('show');
        $this->crud->addButtonFromView('line', 'block', 'block-rating', 'end');

        // add asterisk for fields that are required in RatingComplaintRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);

        $this->seen($this->crud->entry);
        $this->crud->entry->user->notify(new ComplaintResponse($this->crud->entry));
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function seen($complaint){
        if (is_null($complaint->seen_at)){
            $complaint->seen_at = Carbon::now();
        }
        return $complaint->save();
    }

    public function show($id)
    {
        $complaint = Complaint::findOrFail($id);
        $this->seen($complaint);
        $content = parent::show($id);

        $this->crud->removeButton('block');
        $this->crud->removeColumns([
           'application_id',
           'rating_id',
           'user_id',
           'type',
            'client_email'
        ]);

        return $content;
    }

}
