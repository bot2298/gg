<?php

namespace App\Http\Controllers\Admin;

use App\Models\Rating;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\RatingRequest as StoreRequest;
use App\Http\Requests\RatingRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Carbon;

/**
 * Class RatingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class RatingCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Rating');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/rating');
        $this->crud->setEntityNameStrings('оцінка', 'оцінки та коментарі');
        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');
        $this->crud->removeButton('update');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->orderBy('created_at', 'desc');
        $this->crud->allowAccess('show');
        $this->crud->setColumns([
            [
                'name' => 'client_email',
                'label' => 'Клієнт'
            ],
            [
                'name' => 'user.email',
                'label' => 'Адвокат',
            ],
            [
                'name' => 'stars',
                'label' => 'Оцінка'
            ],
            [
                'name' => 'comment',
                'label' => 'Коментар',
                'type' => 'textarea'
            ],
            [
                'name' => 'blocked_at',
                'label' => 'Заблокований'
            ],
            [
                'name' => 'created_at',
                'label' => 'Створений'
            ],
            [
                'name' => 'updated_at',
                'type' => 'text',
                'label' => 'Оновлений'
            ]
        ]);

        $this->crud->addFields([
            [
                'name' => 'comment',
                'label' => 'Коментар',
                'type' => 'textarea'
            ],
        ]);

        $this->crud->addButtonFromView('line', 'block', 'block-rating', 'end');

        // add asterisk for fields that are required in RatingRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function toggleBlock(Rating $rating){
        $this->crud->hasAccessOrFail('create');
        $this->crud->setOperation('Block');

        $rating->blocked_at = $rating->blocked_at ? null : Carbon::now();
        $rating->save();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumn('user_id');
        $this->crud->removeAllButtons();

        return $content;
    }
}
