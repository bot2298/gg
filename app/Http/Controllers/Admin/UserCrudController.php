<?php

namespace App\Http\Controllers\Admin;

use App\Models\Dossier;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\UserRequest as StoreRequest;
use App\Http\Requests\UserRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Carbon;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class UserCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\User');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/user');
        $this->crud->setEntityNameStrings('профіль', 'профілі');

        $this->crud->removeButton('create');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->addClause('whereHas', 'roles', function($query){
            $query->where('roles.name', 'Адвокат');
        });
        $this->crud->orderBy('created_at', 'desc');
        $this->crud->setColumns([
            [
                'name' => 'email',
                'label' => 'E-mail',
            ],
            [
                'name' => 'email_verified_at',
                'label' => 'Верифікований',
            ],
            [
                'name' => 'blocked_at',
                'label' => 'Заблокований'
            ],
            [
                'name' => 'created_at',
                'label' => 'Створений'
            ],
            [
                'name' => 'updated_at',
                'label' => 'Оновлений'
            ]
        ]);

        $this->crud->addFields([
            [
                'name' => 'email',
                'label' => 'E-mail'
            ]
        ]);

        // add asterisk for fields that are required in UserRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->addButtonFromView('line', 'block', 'block', 'end');
    }

    public function toggleBlock(User $user){
        $this->crud->hasAccessOrFail('create');
        $this->crud->setOperation('Block');

        if($user->blocked_at){
            //unblock
            $user->blocked_at = null;
            $user->dossiers()->each(function($dossier){
                $dossier->status = Dossier::STATUS_SAVED;
                $dossier->save();
            });
        } else {
            //block
            $user->blocked_at = Carbon::now();
            $user->dossiers()->each(function($dossier){
                $dossier->status = Dossier::STATUS_REJECTED;
                $dossier->save();
            });
        }

        $user->save();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
