<?php

namespace App\Http\Controllers\Admin;

use App\Models\Application;
use App\Models\Country;
use App\Models\Dossier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ApplicationStatsController extends Controller
{
    public function index(Request $request){
        $countries = Country::all();
        $oldest = Application::oldest()->first() ? Application::oldest()->first()->created_at->format("d/m/Y") : Carbon::now()->format("d/m/Y");
        $from = $request->query('from') ?? $oldest;
        $to = $request->query('to') ?? Carbon::now()->format("d/m/Y");
        $monthgroup = $this->getQuery($request, $from, $to)
            ->select([DB::raw('YEAR(`applications`.`created_at`) as `year`'), DB::raw('MONTH(`applications`.`created_at`) as `month`'), DB::raw('COUNT(*) as cnt')])
            ->groupBy(['year', 'month'])
            ->orderBy('year', 'ASC')
            ->orderBy('month', 'ASC')
            ->get()
            ->mapWithKeys(function($item, $key){
                return [$item->month."-".$item->year => $item->cnt];
            });
        $daygroup = $this->getQuery($request, $from, $to)
            ->select([DB::raw('YEAR(`applications`.`created_at`) as `year`'), DB::raw('MONTH(`applications`.`created_at`) as `month`'), DB::raw('DAY(`applications`.`created_at`) as `day`'), DB::raw('COUNT(*) as cnt')])
            ->groupBy(['year', 'month', 'day'])
            ->orderBy('year', 'ASC')
            ->orderBy('month', 'ASC')
            ->orderBy('day', 'ASC')
            ->get()
            ->mapWithKeys(function($item, $key){
                return [$item->day."-".$item->month."-".$item->year => $item->cnt];
            });
        return view('vendor.backpack.application_stats', compact('countries', 'from', 'to', 'monthgroup', 'daygroup'));
    }

    public function getQuery(Request $request, $from, $to){
        $country = $request->query('country');
        $region = $request->query('region');
        $city = $request->query('city');
        $statuses = $request->query('statuses');

        $query = DB::table('applications')
            ->leftJoin('cities', 'applications.city_id', '=', 'cities.id')
            ->join('regions', 'applications.region_id', '=', 'regions.id')
            ->join('countries', 'regions.country_id', '=', 'countries.id')
            ->when($from, function ($query, $from) {
                return $query->where('applications.created_at', '>=', Carbon::createFromFormat("d/m/Y", $from)->setTime(0,0,0));
            })
            ->when($to, function ($query, $to) {
                return $query->where('applications.created_at', '<=', Carbon::createFromFormat("d/m/Y", $to)->setTime(23, 59, 59));
            })
            ->when($country, function ($query, $country) {
                return $query->where('countries.id', $country);
            })
            ->when($region, function ($query, $region) {
                return $query->where('applications.region_id', $region);
            })
            ->when($city, function ($query, $city) {
                return $query->where('applications.city_id', $city);
            })
            ->when($statuses, function ($query, $statuses) {
                return $query->whereIn('applications.status', explode(',', $statuses));
            });
        return $query;
    }
}
