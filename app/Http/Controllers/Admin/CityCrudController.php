<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use App\Models\Region;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CityRequest as StoreRequest;
use App\Http\Requests\CityRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class CityCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CityCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\City');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/city');
        $this->crud->setEntityNameStrings('місто', 'міста');
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->setColumns([
            [
                'name' => 'name_trans',
                'type' => 'closure',
                'label' => 'Назва (укр)',
                'function' => function ($entry){
                    return __('cities.'.$entry->name);
                }
            ],
            [
                'name' => 'name',
                'label' => 'Ключ для перекладів'
            ],
            [
                'name' => 'country_trans',
                'label' => 'Країна (укр)',
                'type' => 'closure',
                'function' => function($entry){
                    return __('countries.'.$entry->region->country->name);
                }
            ],
            [
                'name' => 'region_trans',
                'label' => 'Регіон',
                'type' => 'closure',
                'function' => function($entry){
                    return __('regions.'.$entry->region->name);
                }
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Ключ для перекладів',
            ],
            [
                'label' => "Регіон",
                'name' => 'region_id',
                'type' => 'select2_from_array',
                'options' => Region::all()->mapWithKeys(function($item, $key){
                    return [$item->id => __("regions.".$item->name)];
                })
            ],
        ]);

        $this->crud->addFilter([ // select2 filter
            'name' => 'country',
            'type' => 'select2',
            'label'=> 'Країна'
        ], function() {
            return \App\Models\Country::all()
                ->mapWithKeys(function($item, $Key){
                    return [$item->id => __('countries.'.$item->name)];
                })
                ->toArray();
        }, function($value) { // if the filter is active
            $this->crud->addClause('whereHas', 'region', function($query) use ($value){
                $query->where('country_id', $value);
            });
        });


        $this->crud->addFilter([ // select2 filter
            'name' => 'region_id',
            'type' => 'select2',
            'label'=> 'Регіон'
        ], function() {
            return Region::all()->mapWithKeys(function($item, $key){
                return [$item->id => __("regions.".$item->name)];
            })->toArray();
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'region_id', $value);
        });

        // add asterisk for fields that are required in CityRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
