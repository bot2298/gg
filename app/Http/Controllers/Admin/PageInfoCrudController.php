<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PageinfoRequest as StoreRequest;
use App\Http\Requests\PageinfoRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use \App\Models\Page;

/**
 * Class PageinfoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PageInfoCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\PageInfo');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/pageinfo');
        $this->crud->setEntityNameStrings('мета', 'мета');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->setColumns([
            [
                'name' => 'page',
                'label' => 'Сторінка або URL',
                'type' => 'closure',
                'function' => function($entry){
                    return __(($entry->page->specialization || $entry->page->region ? '' : 'layout.title.').$entry->page->location);
                }
            ],
            [
                'name' => 'locale_name',
                'label' => 'Мова',
                'type' => 'closure',
                'function' => function ($entry){
                    return config('app.locales')[$entry->locale];
                }
            ],
            [
                'name' => 'title',
                'label' => 'Title',
            ],
            [
                'name' => 'description',
                'label' => 'Description'
            ],
            [
                'name' => 'h1',
                'label' => 'H1'
            ],
            [
                'name' => 'updated_at',
                'label' => 'Оновлено',
            ]
        ]);
        $this->crud->addFields([
            [
                'label' => 'Сторінка',
                'name' => 'page_id',
                'type' => 'select2_from_array',
                'options' => Page::all()->mapWithKeys(function($item, $key){
                    return [$item->id => __(($item->specialization || $item->region ? '' : 'layout.title.').$item->location)];
                })
            ],
            [
                'label' => 'Мова',
                'name' => 'locale',
                'type' => 'select_from_array',
                'options' => config('app.locales'),
            ],
            [
                'label' => 'Title',
                'name' => 'title',
            ],
            [
                'label' => 'Description',
                'name' => 'description',
            ],
            [
                'label' => 'H1',
                'name' => 'h1'
            ]

        ]);

        // add asterisk for fields that are required in PageinfoRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
