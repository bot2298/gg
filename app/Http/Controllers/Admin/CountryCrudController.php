<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CountryRequest as StoreRequest;
use App\Http\Requests\CountryRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class CountryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CountryCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Country');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/country');
        $this->crud->setEntityNameStrings('країна', 'країни');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->addButtonFromView('line', 'toggle-active', 'country-active', 'end');
        $this->crud->setColumns([
            [
                'name' => 'name_trans',
                'type' => 'closure',
                'label' => 'Назва (укр)',
                'function' => function ($entry){
                    return __('countries.'.$entry->name);
                }
            ],
            [
                'name' => 'name',
                'label' => 'Ключ для перекладів'
            ],
            [
                'name' => 'active',
                'type' => 'boolean',
                'label' => 'Активна'
            ]
        ]);
        $this->crud->addField([
            'name' => 'name',
            'label' => 'Ключ для перекладів'
        ]);

        // add asterisk for fields that are required in CountryRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function toggleStatus(\App\Models\Country $country){
        $country->active = !$country->active;
        return response()->json(['success' => $country->save()], 200);
    }
}
