<?php

namespace App\Http\Controllers\Admin;

use App\Models\Chat;
use App\Models\Country;
use App\Models\Message;
use App\Models\User;
use App\Notifications\MessageFromAdmin;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ChatRequest as StoreRequest;
use App\Http\Requests\ChatRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class ChatCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ChatCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Chat');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/chat');
        $this->crud->setEntityNameStrings('повідомлення', 'повідомлення');
        $this->crud->removeButton('update');
        $this->crud->allowAccess('show');
        $this->crud->addClause('where', 'admin', 1);

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->addButtonFromView('top', 'mailing', 'mailing');
        $this->crud->setColumns([
            [
                'name' => 'user.email',
                'label' => 'Кому',
            ],
            [
                'name' => 'message_last_time',
                'label' => 'Останнє повідомлення',
                'type' => 'closure',
                'function' => function($entry){
                    if ($last = $entry->messages()->latest()->first()){
                        return $last->created_at;
                    }
                    return '';
                }
            ],
            [
                'name' => 'message_last',
                'label' => 'Останнє повідомлення',
                'type' => 'closure',
                'function' => function($entry){
                    if ($last = $entry->messages()->latest()->first()){
                        return $last->message;
                    }
                    return '';
                }
            ],
            [
                'name' => 'seen_at',
                'label' => 'Прочитано',
                'type' => 'closure',
                'function' => function ($entry) {
                    return $entry->messages()->latest()->first()->seen_at;
                }
            ],
            [
                'name' => 'messages_count',
                'label' => 'К-ть повідомлень',
                'type' => 'closure',
                'function' => function($entry){
                    return $entry->messages()->count();
                }
            ],
        ]);

        $this->crud->addFields([
            [
                'label' => "Кому",
                'type' => 'select2',
                'name' => 'user_id', // the db column for the foreign key
                'entity' => 'user', // the method that defines the relationship in your Model
                'attribute' => 'email', // foreign key attribute that is shown to user
                'model' => "App\Models\User", // foreign key model
                'options' => function ($query){
                    return $query->whereHas('roles', function($role_query){
                        $role_query->where('roles.name', 'Адвокат');
                    })->get();
                }
            ],
            [
                'label' => 'Повідомлення',
                'type' => 'textarea',
                'name' => 'message',
            ],
            [
                'label' => 'Мова',
                'name' => 'locale',
                'type' => 'select_from_array',
                'options' => config('app.locales'),
            ],
        ]);

        // add asterisk for fields that are required in ChatRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        $this->crud->hasAccessOrFail('create');
        $this->crud->setOperation('create');

        // your additional operations before save here

        // fallback to global request instance
        if (is_null($request)) {
            $request = \Request::instance();
        }

        $admin_chat = Chat::firstOrCreate(['user_id' => $request->user_id, 'admin' => 1]);
        $admin_chat->messages()->create([
            'owner' => 'admin',
            'message' => $request->message,
        ]);
        $admin_chat->save();

        $admin_chat->user->notify(new MessageFromAdmin($admin_chat, $request->post('locale')));

        $this->data['entry'] = $this->crud->entry = $admin_chat;

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $this->setSaveAction();

        return $this->performSaveAction($admin_chat->getKey());
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function mailingForm(Request $request){
        $countries = Country::all();
        return view('vendor.backpack.crud.mailing', compact('countries'));
    }

    public function mailing(Request $request){
        $request->validate([
            'country_id' => 'required',
            'message' => 'required',
            'locale' => 'required'
        ]);
        DB::table('users')
            ->join('dossiers', 'users.id', '=', 'dossiers.user_id')
            ->join('cities', 'dossiers.city_id', '=', 'cities.id')
            ->join('regions', 'cities.region_id', '=', 'regions.id')
            ->join('countries', 'regions.country_id', '=', 'countries.id')
            ->where(function($query) use ($request){
                   $query->where('countries.id', $request->country_id)
                   ->when($request->region_id, function($query, $region_id){
                       $query->where('regions.id', $region_id);
                   })
                   ->when($request->city_id, function($query, $city_id){
                       $query->where('dossiers.city_id', $city_id);
                   });
            })
            ->select('users.id')
            ->groupBy('users.id')
            ->orderBy('users.id')
            ->each(function($user) use ($request){
                $admin_chat = Chat::firstOrCreate(['user_id' => $user->id, 'admin' => 1]);
                $admin_chat->messages()->create([
                    'owner' => 'admin',
                    'message' => $request->message,
                ]);
                $admin_chat->save();

                $admin_chat->user->notify(new MessageFromAdmin($admin_chat, $request->post('locale')));
            });

        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        return redirect(backpack_url('chat'));
    }

    public function show($id)
    {
        $content = parent::show($id);
        $this->crud->removeAllButtons();
        $this->crud->removeColumns([
            'application_id',
            'user_id',
            'admin',
            'action'
        ]);
        $this->crud->addColumn([
            'name' => 'messages',
            'type' => 'closure',
            'label' => 'Повідомлення',
            'function' => function ($entry){
                $result = '';
                foreach($entry->messages()->latest()->get() as $message){
                    $result.= $message->message.'<br />'.$message->created_at.'<br /><br />';
                }
                return $result;
            }

        ]);
        return $content;
    }
}
