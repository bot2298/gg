<?php

namespace App\Http\Controllers\Admin;

use App\Models\Region;
use App\Models\Specialization;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PageRequest as StoreRequest;
use App\Http\Requests\PageRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class PageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PageCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Page');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/page');
        $this->crud->setEntityNameStrings('сторінка', 'сторінки');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->setColumns([
            [
                'name' => 'name_trans',
                'label' => 'Назва або URL',
                'type' => 'closure',
                'function' => function($entry) {
                    return __(($entry->specialization || $entry->region ? '' : 'layout.title.').$entry->location);
                }
            ],
            [
                'name' => 'location',
                'label' => 'Ключ',
            ],
            [
                'name' => 'region_trans',
                'label' => 'Регіон',
                'type' => 'closure',
                'function' => function($entry){
                    if($entry->region){
                        return __('regions.'.$entry->region->name);
                    }
                    return '';
                }
            ],
            [
                'name' => 'spec_trans',
                'label' => 'Спеціалізація',
                'type' => 'closure',
                'function' => function($entry){
                    if($entry->specialization){
                        return __('specializations.'.$entry->specialization->name);
                    }
                    return '';
                }

            ]
        ]);

        $this->crud->addFields([
            [
                'name' => 'location',
                'label' => 'Ключ'
            ],
//            [
//                'label' => 'Регіон',
//                'name' => 'regions_id',
//                'type' => 'select2_from_array',
//                'options' => Region::all()->mapWithKeys(function($item, $key){
//                    return [$item->id => __("regions.".$item->name)];
//                })
//            ],
//            [
//                'label' => 'Спеціалізація',
//                'name' => 'specialization_id',
//                'type' => 'select2_from_array',
//                'options' => Specialization::all()->mapWithKeys(function($item, $key){
//                    return [$item->id => __("specializations.".$item->name)];
//                })
//            ],
        ]);

        // add asterisk for fields that are required in PageRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
