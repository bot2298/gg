<?php

namespace App\Http\Controllers\Admin;

use App\Models\Application;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ApplicationRequest as StoreRequest;
use App\Http\Requests\ApplicationRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;

/**
 * Class ApplicationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ApplicationCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Application');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/application');
        $this->crud->setEntityNameStrings('запит', 'запити');

        $this->crud->enableDetailsRow();
        $this->crud->allowAccess('details_row');
        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->allowAccess('show');
        $this->crud->orderBy('created_at', 'desc');
        $this->crud->setColumns([
            [
                'name' => 'code',
                'label' => 'Код'
            ],
            [
                'name' => 'status',
                'label' => 'Статус',
                'type' => 'closure',
                'function' => function($entry){
                    return $entry->getAdminStatus();
                }
            ],
            [
                'name' => 'country_trans',
                'label' => 'Країна',
                'type' => 'closure',
                'function' => function($entry){
                    return __('countries.'.$entry->region->country->name);
                }
            ],
            [
                'name' => 'region_trans',
                'label' => 'Регіон',
                'type' => 'closure',
                'function' => function($entry){
                    return __('regions.'.$entry->region->name);
                }

            ],
            [
                'name' => 'city_trans',
                'label' => 'Місто',
                'type' => 'closure',
                'function' => function($entry){
                    return $entry->city ? __('cities.'.$entry->city->name) : '';
                }
            ],
            [
                'name' => 'specialization_trans',
                'label' => 'Спеціалізація',
                'type' => 'closure',
                'function' => function($entry){
                    return $entry->specialization ? __('specializations.'.$entry->specialization->name) : '';
                }
            ],
            [
                'name' => 'content',
                'type' => 'textarea',
                'label' => 'Текст'
            ],
            [
                'name' => 'attachment_link',
                'label' => 'Додаток',
                'type' => 'closure',
                'function' => function($entry){
                    if ($entry->attachment){
                        $link = Storage::url($entry->attachment);
                        return "<a target='_blank' href='$link'>Завантажити</a>";
                    }
                    return '';
                }
            ],
            [
                'name' => 'name',
                'label' => 'Ім\'я'
            ],
            [
                'name' => 'email',
                'label' => 'E-mail'
            ],
            [
                'name' => 'phone',
                'label' => 'Телефон'
            ],
            [
                'name' => 'cost',
                'label' => 'Вартість'
            ],
            [
                'name' => 'currency',
                'label' => 'Валюта',
                'type' => 'closure',
                'function' => function($entry) {
                    return $entry->currency->getName();
                }
            ],
            [
                'name' => 'created_at',
                'label' => 'Створений'
            ],
            [
                'name' => 'deadline',
                'label' => 'Актуальний до'
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'status',
                'label' => "Статус",
                'type' => 'select_from_array',
                'options' => [
                    Application::STATUS_UNVERIFIED => 'Неверифікований',
                    Application::STATUS_ACTIVE => 'Активний',
                    Application::STATUS_STOPPED => 'Припинений',
                    Application::STATUS_BLOCKED => 'Заблокований'
                ],
            ]
        ]);

        $this->crud->addButtonFromView('line', 'block', 'block-application', 'end');
        $this->crud->addFilter([ // dropdown filter
            'name' => 'status',
            'type' => 'dropdown',
            'label'=> 'Статус'
        ], [
            Application::STATUS_UNVERIFIED => 'Неверифікований',
            Application::STATUS_ACTIVE => 'Активний',
            Application::STATUS_STOPPED => 'Припинений',
            Application::STATUS_BLOCKED => 'Заблокований',
        ], function($value) { // if the filter is active
             $this->crud->addClause('where', 'status', $value);
        });

        $this->crud->addFilter([ // select2 filter
            'name' => 'region_id',
            'type' => 'select2',
            'label'=> 'Регіон'
        ], function() {
            return \App\Models\Region::all()
                ->mapWithKeys(function($item, $Key){
                    return [$item->id => __('regions.'.$item->name)];
                })
                ->toArray();
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'region_id', $value);
        });

        $this->crud->addFilter([ // select2 filter
            'name' => 'specialization_id',
            'type' => 'select2',
            'label'=> 'Спеціалізація'
        ], function() {
            return \App\Models\Specialization::all()
                ->mapWithKeys(function($item, $Key){
                    return [$item->id => __('specializations.'.$item->name)];
                })
                ->toArray();
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'specialization_id', $value);
        });

        // add asterisk for fields that are required in ApplicationRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function toggleBlock(Application $application){
        $this->crud->hasAccessOrFail('create');
        $this->crud->setOperation('Block');

        $application->status = $application->status !== Application::STATUS_BLOCKED ? Application::STATUS_BLOCKED : Application::STATUS_ACTIVE;
        $application->save();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function show($id)
    {
        $content = parent::show($id);
        $this->crud->removeButton('block');
        $this->crud->removeColumns([
            'city_id',
            'region_id',
            'specialization_id',
            'phone_locale',
            'attachment'
        ]);

        return $content;
    }
}
