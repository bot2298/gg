<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use App\Models\Dossier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LawyersStatsController extends Controller
{
    public function index(Request $request){
        $countries = Country::all();
        $oldest = Dossier::oldest()->first() ? Dossier::oldest()->first()->created_at->format("d/m/Y") : Carbon::now()->format("d/m/Y");
        $from = $request->query('from') ?? $oldest;
        $to = $request->query('to') ?? Carbon::now()->format("d/m/Y");
        $monthgroup = $this->getQuery($request, $from, $to)
            ->select([DB::raw('YEAR(`dossiers`.`created_at`) as `year`'), DB::raw('MONTH(`dossiers`.`created_at`) as `month`'), DB::raw('COUNT(*) as cnt')])
            ->groupBy(['year', 'month'])
            ->orderBy('year', 'ASC')
            ->orderBy('month', 'ASC')
            ->get()
            ->mapWithKeys(function($item, $key){
                return [$item->month."-".$item->year => $item->cnt];
            });
        $daygroup = $this->getQuery($request, $from, $to)
            ->select([DB::raw('YEAR(`dossiers`.`created_at`) as `year`'), DB::raw('MONTH(`dossiers`.`created_at`) as `month`'), DB::raw('DAY(`dossiers`.`created_at`) as `day`'), DB::raw('COUNT(*) as cnt')])
            ->groupBy(['year', 'month', 'day'])
            ->orderBy('year', 'ASC')
            ->orderBy('month', 'ASC')
            ->orderBy('day', 'ASC')
            ->get()
            ->mapWithKeys(function($item, $key){
                return [$item->day."-".$item->month."-".$item->year => $item->cnt];
            });
        return view('vendor.backpack.lawyers_stats', compact('countries', 'from', 'to', 'monthgroup', 'daygroup'));
    }

    public function getQuery(Request $request, $from, $to){
        $country = $request->query('country');
        $region = $request->query('region');
        $city = $request->query('city');
        $statuses = $request->query('statuses');

        $query = DB::table('dossiers')
            ->join('cities', 'dossiers.city_id', '=', 'cities.id')
            ->join('regions', 'cities.region_id', '=', 'regions.id')
            ->join('countries', 'regions.country_id', '=', 'countries.id')
            ->when($from, function ($query, $from) {
                return $query->where('dossiers.created_at', '>=', Carbon::createFromFormat("d/m/Y", $from)->setTime(0,0,0));
            })
            ->when($to, function ($query, $to) {
                return $query->where('dossiers.created_at', '<=', Carbon::createFromFormat("d/m/Y", $to)->setTime(23, 59, 59));
            })
            ->when($country, function ($query, $country) {
                return $query->where('countries.id', $country);
            })
            ->when($region, function ($query, $region) {
                return $query->where('regions.id', $region);
            })
            ->when($city, function ($query, $city) {
                return $query->where('cities.id', $city);
            })
            ->when($statuses, function ($query, $statuses) {
                return $query->whereIn('dossiers.status', explode(',', $statuses));
            });
        return $query;
    }

}
