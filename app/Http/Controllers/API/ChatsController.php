<?php

namespace App\Http\Controllers\API;

use App\Models\Chat;
use App\Models\Message;
use App\Notifications\MessageFromClient;
use App\Notifications\MessageFromLawyer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\MessageSent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class ChatsController extends Controller
{
    /**
     * Fetch all messages
     *
     * @param $id
     * @return Message
     */

    public function checkAccess($chat, $user_id){
        if($chat->application){
            if(!($user_id != $chat->user_id || $user_id != $chat->application->user_id)){
                abort(401);
            }
        }
        else if($user_id != $chat->user_id) {
            abort(401);
        }
    }

    public function fetchMessages($id)
    {
        $api_user = Auth::guard('api')->id();
        $chat = Chat::findOrFail([$id])->first();

        $this->checkAccess($chat, $api_user);

        return $chat->messages()->get();
    }

    /**
     * Persist message to database
     *
     * @param Request $request
     * @return void
     */
    public function sendMessage(Request $request)
    {
        $api_user = Auth::guard('api')->id();
        $chat = Chat::findOrFail([$request->input('chat_id')])->first();

        $this->checkAccess($chat, $api_user);

        if ($chat !== null) {
            $chat->messages()->create($request->all());
            if ($request->input('owner') == 'application') {
                $chat->save();
            }

            if($request->input('owner') == 'application'){
                $chat->user->notify(new MessageFromClient($chat));
            } else {
                Notification::route('mail', $chat->application->email)
                    ->notify(new MessageFromLawyer($chat));
            }
        }
    }

    public function updateSeenStatus($id)
    {
        $api_user = Auth::guard('api')->id();
        $chat = Chat::findOrFail([$id])->first();

        $this->checkAccess($chat, $api_user);

        $chat->messages()->whereNull('seen_at')->update(['seen_at' => \Illuminate\Support\Carbon::now()]);
    }

    public function startChat(Request $request)
    {
        $request->validate([
            'reply-message' => 'string|required'
        ]);
        $chat = new Chat([
            'user_id' => $request->input('user'),
            'application_id' => $request->input('application'),
        ]);
        $chat->save();
        $chat->messages()->create([
            'message' => $request->input('reply-message'),
            'owner' => 'user',
            'seen_at' => Carbon::now()
        ]);

        Notification::route('mail', $chat->application->email)
            ->notify(new MessageFromLawyer($chat));

        return redirect()->route('cabinet.request', app()->getLocale())->with('notification', [
            'type' => 'success',
            'title' => __('modals.result_success'),
            'content' =>  __('modals.application_response_message')
        ]);
    }
}
