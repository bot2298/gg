<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\DossierResource;
use App\Models\Dossier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DossierController extends Controller
{
    public function getAll(Request $request)
    {
        $validated = $request->validate([
            'q' => 'nullable|string',
            'activities' => 'nullable|array',
            'years' => 'nullable|string',
            'specialization' => 'nullable|string',
            'sort_by' => 'string',
            'regions_id' => 'nullable',
            'countries_id' => 'nullable',
            'locale' => 'nullable',
            'rating' => 'int|nullable',
            'per_page' => 'int'
        ]);
        $preparedRules = explode('_', $validated['sort_by']);
        $sortRules['sort'] = $preparedRules[0];
        $sortRules['option'] = $preparedRules[1];

        $dossiers = DossierResource::collection(Dossier::where('status', Dossier::STATUS_ACTIVE)->get())->toArray($request);
        $dossiers = collect($dossiers)->filter(function ($value) { return !is_null($value); });

        if ($sortRules['option'] == 'desc') {
            $dossiers = $dossiers->sortByDesc($sortRules['sort'])->values();
        } else {
            $dossiers = $dossiers->sortBy($sortRules['sort'])->values();
        }

        if ($validated['activities'] != []) {
            foreach ($validated['activities'] as $activity) {
                $dossiers = $dossiers->filter( function($dossier) use ($activity) {
                    $result = isset($dossier['spec']['activities']) ? array_search($activity, $dossier['spec']['activities']) : false;
                    return is_int($result);
                });
            }
        }

        if (isset($validated['specialization'])) {
            $specialization = $validated['specialization'];
            $dossiers = $dossiers->filter( function($dossier) use ($specialization) {
                $result = isset($dossier['spec']['specializations']) ? array_search($specialization, $dossier['spec']['specializations']) : false;
                return is_int($result);
            });
        }

        if (isset($validated['years'])) {
            $preparedRules = explode('_', $validated['years']);
            $filterRules['from'] = $preparedRules[0];
            $filterRules['to'] = $preparedRules[1];
            $dossiers = $dossiers->filter( function($dossier) use ($filterRules) {
                return isset($dossier['experience']['years']) ? $dossier['experience']['years'] > $filterRules['from'] && $dossier['experience']['years'] < $filterRules['to'] : false;
            });
        }

        if (isset($validated['rating'])) {
            $rating = $validated['rating'];
            $dossiers = $dossiers->filter( function($dossier) use ($rating) {
                if ($rating <= 4) {
                    return $dossier['rating'] ? $dossier['rating'] > $rating : false;
                }
                return $dossier['rating'] ? $dossier['rating'] == $rating : false;
            });
        }

        if ($validated['q'] != '') {
            $needle = $validated['q'];
            $dossiers = $dossiers->filter(function ($item) use ($needle) {
                return false !== stristr($item['general']['fullname'], $needle);
            });
        }

        if (isset($validated['regions_id'])) {
            $needle = $validated['regions_id'];
            $dossiers = $dossiers->filter( function($dossier) use ($needle) {
                $result = array_search($needle, $dossier['regions_id']);
                return is_int($result);
            });
        }

        if (isset($validated['countries_id'])) {
            $needle = $validated['countries_id'];
            $dossiers = $dossiers->filter( function($dossier) use ($needle) {
                $result = array_search($needle, $dossier['countries_id']);
                return is_int($result);
            });
        }

        if (isset($validated['locale'])) {
            $dossiers = $dossiers->where('locale', $validated['locale']);
        } else {
            $dossiers = $dossiers->filter( function($dossier) {
                return $dossier['locale'] == app()->getLocale() || $dossier['user_dossiers_count'] === 1;
            });
        }

        $dossiers = $dossiers->unique('user_id');

        $dossiers = $this->paginate($dossiers, $perPage = $validated['per_page'], $page = null, $options = ['path' => $request->url()]);
        return response()->json($dossiers);
    }
}
