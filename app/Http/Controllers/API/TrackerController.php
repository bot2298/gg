<?php

namespace App\Http\Controllers\API;

use App\Models\Application;
use App\Events\AccessRequested;
use App\Events\RemindCode;
use App\Notifications\RequestCode;
use App\Notifications\TrackerAccess;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\Rule;

class TrackerController extends Controller
{
    public function getApplication(Request $request)
    {
        app()->setLocale($request->get('locale'));
        $request->validate([
            'code' => [
                'required',
                'string',
                'size:12',
                Rule::exists('applications')->where(function ($query) use ($request) {
                    $query->where('status', Application::STATUS_ACTIVE);
                }),
            ],
        ]);
        $application = Application::where('code', '=', $request->code)->first();
        return response()->json($application->email, 200);
    }

    public function sendAccessMail(Request $request)
    {
        app()->setLocale($request->get('locale'));
        $request->validate([
            'code' => [
                'required',
                'string',
                'size:12',
                Rule::exists('applications')->where(function ($query) use ($request) {
                    $query->where('code', $request->code)->where('status', Application::STATUS_ACTIVE);
                }),
            ],
        ]);
        $application = Application::where('code', '=', $request->code)->first();
        Notification::route('mail', $application->email)
            ->notify(new TrackerAccess($application));
        return response(null, 200);
    }

    public function remindCodeMail(Request $request)
    {
        app()->setLocale($request->get('locale'));
        $request->validate([
            'email' => 'email|required|string|exists:applications,email'
        ]);
        $applications = Application::where('email', '=', $request->email)->where('status', Application::STATUS_ACTIVE)->get();
        event(new RemindCode($applications));
        return response(null, 200);
    }
}
