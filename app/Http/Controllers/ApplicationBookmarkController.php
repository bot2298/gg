<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\ApplicationBookmark;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ApplicationBookmarkController extends Controller
{
    public function index($locale, $location = "cabinet.bookmarks.index", Request $request)
    {
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        $query = Application::whereIn('applications.id', auth()->user()->getBookmarks())
        ->where('status', Application::STATUS_ACTIVE);

        $bookmarks = Application::filter($query, $request)->paginate(10);

        return view('site.index', compact(['location', 'title', 'bookmarks']));
    }

    public function toggle(Request $request){
        if($bookmark = ApplicationBookmark::where([
            'user_id' => auth()->id(),
            'application_id' => $request->post('id')
        ])->first()){
            $bookmark->delete();
            return response()->json([
                'status' => 1
            ]);
        } else {
            $bookmark = new ApplicationBookmark([
                'user_id' => auth()->id(),
                'application_id' => $request->post('id')
            ]);
            return response()->json([
                'status' => (int)$bookmark->save()
            ]);
        }
    }

    public function remove(Request $request){
        if(is_array($request->post('id'))){
            ApplicationBookmark::where('user_id', auth()->id())
                ->whereIn('application_id', $request->post('id'))
                ->delete();
        } else {
            ApplicationBookmark::where([
                'user_id' => auth()->id(),
                'application_id' => $request->post('id')
            ])->delete();
        }

        return back();
    }
}
