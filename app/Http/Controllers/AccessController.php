<?php

namespace App\Http\Controllers;

use App\Models\Access;
use App\Models\Country;
use App\Models\Dossier;
use App\Models\Page;
use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class AccessController extends Controller
{
    public function showForm($locale, $location = "cabinet.settings.access", Request $request, $js = ['js/access.js'])
    {
//        if(!auth()->user()->dossiers()->where('status', Dossier::STATUS_ACTIVE)->count()){
//            return redirect()->route('cabinet.dossier.index', app()->getLocale())->with('notification', [
//                'type' => 'success',
//                'title' => __('modals.access_no_dossier'),
//                'content' => __('modals.access_no_dossier_content')
//            ]);
//        }
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        $countries = Country::where('active', 1)->get();
        $access = new Access();
        return view('site.index', compact(['location', 'title', 'countries', 'access', 'js']));
    }
    public function showEditForm($locale, Access $access, $location = "cabinet.settings.access", Request $request)
    {
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        if($access->user_id != auth()->id()){
            abort(401);
        }
        $countries = Country::where('active', 1)->get();
        return view('site.index', compact(['location', 'title', 'countries', 'access']));
    }

    public function create(Request $request){
        if(auth()->user()->access()->where('active_until', '>=', Carbon::now())->count() >= 2){
            return response()->json([], 403);
        }
        $request->validate([
            'region_id' => 'array|required',
        ]);
        //todo calculate price with discounts and navigate to merchant
        foreach ($request->region_id as $region) {
            $access = Access::firstOrNew([
                'user_id' => auth()->id(),
                'region_id' => $region['region_id'],
            ]);

            $until = $access->active_until && $access->active_until->gt(Carbon::now()) ? $access->active_until : Carbon::now();
            $access->active_until = $until->addDays(30);
            $access->save();
        }
        return route('cabinet.access-history', app()->getLocale());
    }

    public function regions(Request $request){
        if ($id = $request->post('country_id')){
            $regions = Region::select(['name', 'id', 'cost'])->where('country_id', $id)->get();

            return response()->json($regions->map(function($item){
                if($item['active_until']){
                    $active = Carbon::createFromTimeString($item['active_until']);
                    if($active->diffInDays(Carbon::now()) > 7){
                        return null;
                    } else $item['active'] = $active->format('d.m.Y');
                } else {
                    $item['active'] = null;
                }
                $item['name'] = __('regions.'.$item["name"]);
                return $item;
            }));
        }
    }

//    public function update($locale, Access $access, Request $request){
//        if($access->user_id != auth()->id()){
//            abort(401);
//        }
//        return $this->store($request, $access);
//    }

    public function history($locale, $location = "cabinet.settings.access-history", Request $request)
    {
        $title = Page::getTitle($location);
        $description = Page::getDescription($location);
        $access = auth()->user()->access->all();
        return view('site.index', compact(['location', 'title', 'access']));
    }

    public function toggleActive(Request $request){
        $access = Access::findOrFail($request->post('id'));
        if($access->user_id !== auth()->id()){
            abort(401);
        }
        $access->active = !$access->active;
        $access->save();

        return back();
    }

    public function toggleActiveNew(Request $request){
        $access = Access::findOrFail($request->post('id'));
        if($access->user_id !== auth()->id()){
            abort(401);
        }
        if ($access->active_until->gt(Carbon::now())){
            $access->active_until = Carbon::now()->subMinutes(1); // expired date
            $access->save();

            return back();
        }

        return redirect()->route('cabinet.access', app()->getLocale());
    }

//    public function delete(Request $request){
//        $access = Access::findOrFail($request->post('id'));
//        if($access->user_id !== auth()->id()){
//            abort(401);
//        }
//
//        $access->delete();
//
//        return back();
//    }
}
