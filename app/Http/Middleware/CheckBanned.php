<?php

namespace App\Http\Middleware;

use Closure;

class CheckBanned
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() && auth()->user()->blocked_at){
            auth()->logout();
            return redirect()->route('login', app()->getLocale())->withBlocked('Ваш акаунт заблоковано');
        }

        return $next($request);
    }
}
