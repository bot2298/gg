<?php

namespace App\Http\Middleware;

use App\Models\Application;
use Closure;
use Illuminate\Support\Facades\Cookie;

class CheckApplicationAccess
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->hasValidSignature() && $request->has('token') && $request->has('code')) {
            $application = Application::query()
                ->where('token', '=', $request->token)
                ->where('code', '=', $request->code)
                ->first();
            if ($application) {
                if (in_array($application->status, [Application::STATUS_UNVERIFIED, Application::STATUS_STOPPED, Application::STATUS_BLOCKED])){
                    return redirect()->route('tracker.index', app()->getLocale())->with('notification', [
                        'type' => 'success',
                        'title' => __('modals.result_error'),
                        'content' => __('modals.application_notactive')
                    ]);
                }
                Cookie::queue('application_id', $application->id, 60);
//                $request->session()->put('application_id', $application->id);
                return $next($request);
            }
            return redirect()->route('tracker.index', app()->getLocale());
        }
        if (Cookie::get('application_id') && $application = Application::find(Cookie::get('application_id'))) {
            if (in_array($application->status, [Application::STATUS_UNVERIFIED, Application::STATUS_STOPPED, Application::STATUS_BLOCKED])){
                Cookie::queue(Cookie::forget('application_id'));
//                session()->forget('application_id');
                return redirect()->route('tracker.index', app()->getLocale())->with('notification', [
                    'type' => 'success',
                    'title' => __('modals.result_error'),
                    'content' => __('modals.application_notactive')
                ]);
            }
            return $next($request);
        }
        if(!$request->hasValidSignature() && $request->has('token') && $request->has('code')){
            abort(401);
        }
        return redirect()->route('tracker.index', app()->getLocale());
    }
}
