<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\URL;

class MessageFromLawyer extends Notification
{
    use Queueable;

    public $chat;

    /**
     * Create a new notification instance.
     *
     * @param $chat
     */
    public function __construct($chat)
    {
        $this->chat = $chat;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(__('mail.from_lawyer_subject'))
            ->line(__('mail.from_lawyer_content'))
            ->line(__('mail.from_lawyer_press'))
            ->action(__('mail.from_lawyer_button'), URL::signedRoute('tracker.application', ['locale' => app()->getLocale(), 'code' => $this->chat->application->code, 'token' => $this->chat->application->token]))
            ->line(__('mail.from_lawyer_reminder'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
