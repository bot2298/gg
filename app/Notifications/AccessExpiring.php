<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AccessExpiring extends Notification
{
    use Queueable;

    public $access;

    /**
     * Create a new notification instance.
     *
     * @param $access
     */
    public function __construct($access)
    {
        $this->access = $access;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject(__('mail.access_expire_subject'))
                    ->line(__('mail.access_expire_line', [
                        'region' => __('regions.'.$this->access->region->name, [], app()->getLocale()),
                        'until' => $this->access->active_until->format('d.m.Y H:i:s')
                    ]))
                    ->line(__('mail.access_expire_line2'))
                    ->action(__('mail.access_expire_action'), route('cabinet.access', app()->getLocale()));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
