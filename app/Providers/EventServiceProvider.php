<?php

namespace App\Providers;

use App\Events\AccessRequested;
use App\Events\ApplicationView;
use App\Events\RemindCode;
use App\Events\RatingView;
use App\Events\SocialFirstLogin;
use App\Listeners\AddDefaultRating;
use App\Listeners\AddToSeen;
use App\Listeners\RatingAddToSeen;
use App\Listeners\SendApplicationCodeEmail;
use App\Listeners\SendMailAccessLink;
use App\Listeners\SendPassword;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
            AddDefaultRating::class,
        ],
        ApplicationView::class => [
            AddToSeen::class
        ],
        RatingView::class => [
            RatingAddToSeen::class
        ],
        RemindCode::class => [
            SendApplicationCodeEmail::class
        ],
        SocialFirstLogin::class => [
            SendPassword::class,
            AddDefaultRating::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
