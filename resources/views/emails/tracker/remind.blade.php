@component('mail::message')
# {{ __('mail.greeting') }} {{$applications[0]->name}}!
{{ __('mail.request_list') }}:
@foreach($applications as $key => $application)
@if($application->code)
{{$key+1}}. {{ __('mail.request_code') }}: {{ $application->code }}<br>
{{ __('mail.request_about') }}:<br>
@php
    if (strlen($application->content) > 100 ){
        echo substr($application->content, 0, 100) . '...';
    } else {
        echo $application->content;
    }
@endphp
@component('mail::button', ['url' => URL::signedRoute('tracker.application', ['locale' => app()->getLocale(), 'code' => $application->code, 'token' => $application->token])])
{{ __('mail.link_go') }}
@endcomponent

@endif
@endforeach
{{ __('mail.with_regards') }}<br>
{{ config('app.name') }}
@endcomponent
