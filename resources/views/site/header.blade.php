<header class="header-container">
    <nav class="navbar navbar-expand-xl container">
        <a class="navbar-brand" href="{{ route('index', app()->getLocale()) }}">
            <div class="logo-block mr-auto mr-xl-5">
                <div class="logo-img"></div>
                <a class="logo-title" href="{{ route('index', app()->getLocale()) }}">
                    <div>
                        <span>Googllaw</span>
                        <div class="logo-sub">Lawyers service</div>
                    </div>
                </a>
            </div>
        </a>
        <div id="dossier-lol" style="position:absolute; top: 50%; right: 0; transform: translateY(-50%); display:none; font-size: 48px; font-weight: 700;">{{ __('site.dossier_pdf_header') }}</div>
        <button data-html2canvas-ignore="true" class="navbar-toggler custom-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            {{-- <span class="navbar-toggler-icon"></span> --}}
            <div class="menu-toggler">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        @if (Request::segment(2) == 'cabinet')
            <div class="open-side-navigation d-flex d-lg-none"><span>&#10219;</span></div>
        @endif
        <div data-html2canvas-ignore="true" class="navbar-collapse{{auth()->check()?' justify-content-end':null}} main-menu" >
            <ul class="navbar-nav nav-fill{{auth()->check()?null:' w-100'}}">                
                <li class="hide-menu__wrapper align-items-center px-3">
                    <div class="logo-block mr-auto mr-xl-5">
                        <div class="logo-img"></div>
                        <a class="logo-title" href="{{ route('index', app()->getLocale()) }}">
                            <div>
                                <span>Googllaw</span>
                                <div class="logo-sub">Lawyers service</div>
                            </div>
                        </a>
                    </div>
                    <div  class="hide-menu d-flex">
                        <span ></span>
                        <span ></span>
                        <span ></span>
                    </div>
                </li>
                <li class="nav-item lang-select-container dropdown-toggle my-auto mx-auto mx-xl-0 mr-0 mr-xl-auto d-flex ">
                    <button id="lang-select" class="nav-link d-inline menu-item dropdown-toggle">
                        {{ strtoupper(app()->getLocale()) }}
                    </button>
                    <div class="lang-select-dropdown dropdown-menu{{ auth()->check() ? null : ' guest-check'}}">
                        @foreach (config('app.locales') as $locale => $name)
                            @php
                                $params = \Illuminate\Support\Facades\Route::current()->parameters;
                                $params["locale"] = $locale;
                            @endphp
                            <a class="dropdown-item" href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(),  $params) }}" @if (app()->getLocale() == $locale) style="color: #EC9C23; font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($locale) }}</a>
                        @endforeach
                    </div>
                    <div class="nav-item my-auto mx-auto mx-xl-0 mr-0 mr-xl-auto{{auth()->check() ? ' d-none':null}}">
                        <a id="header-bookmark-toggle" class="{{ session()->has('bookmark') && count(session('bookmark')) ? 'active' : null }}" href="{{ route('bookmarks', app()->getLocale()) }}">
                            <img style="width: 24px; vertical-align: text-bottom;" src="{{ asset('img/bookmark_white.svg') }}" alt="">
                            <img style="width: 24px; vertical-align: text-bottom;" src="{{ asset('img/bookmark_orange.svg') }}" alt="">
                        </a>
                    </div>
                </li>
                {{-- <li class="nav-item my-auto mx-auto mx-xl-0 mr-0 mr-xl-auto{{auth()->check() ? ' d-none':null}}">
                    <a id="header-bookmark-toggle" class="{{ session()->has('bookmark') && count(session('bookmark')) ? 'active' : null }}" href="{{ route('bookmarks', app()->getLocale()) }}">
                        <img style="width: 24px; vertical-align: text-bottom;" src="{{ asset('img/bookmark_white.svg') }}" alt="">
                        <img style="width: 24px; vertical-align: text-bottom;" src="{{ asset('img/bookmark_orange.svg') }}" alt="">
                    </a>
                </li> --}}
                <li class="nav-item my-auto mx-auto mx-xl-0 mr-0 mr-xl-auto{{auth()->check()?' ml-xl-4':null}}">
                    <a class="nav-link menu-item{{ Route::is('about')?' active':'' }}" href="{{ route('about', app()->getLocale()) }}">
                        @lang('layout.about')
                    </a>
                </li>
                <li class="nav-item position-relative my-auto mx-auto mx-xl-0 mr-0 mr-xl-auto{{auth()->check()?' ml-xl-4':null}}">
                    {{-- {{ route('dossier', app()->getLocale()) }} --}}
                    <button class="nav-link nav-link-dossier dropdown-toggle menu-item{{ Request::segment(2) === 'dossier' && Request::segment(3) !== 'view' ?' active':'' }}" >
                        @lang('layout.dossier')
                    </button>
                    <div id="dossier-region-dropdown" style="" class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <div class="row justify-content-start">
                            <div class="col-12 col-md-6 text-center text-xl-left">
                                <a href="{{ route('dossier', app()->getLocale()) }}" class="dossier-cat-link mb-2 mb-xl-0">{{ __('layout.all_dossier') }}</a>
                            </div>
                            @foreach(\App\Models\Page::whereNotNull('region_id')->whereNull('specialization_id')->get() as $cat)
                                <div class="col-12 col-md-6 text-center text-xl-left">
                                    <a class="dossier-cat-link mb-2 mb-xl-0" href="{{ route('dossier.category', ['locale' => app()->getLocale(), 'cat' => $cat->location]) }}">{{ __('regions.'.$cat->region->name) }}</a>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </li>
                <li class="nav-item my-auto mx-auto mx-xl-0 mr-0 mr-xl-auto{{auth()->check() ? ' d-none':null}}">
                    <a class="nav-link menu-item{{ Route::is('request')?' active':'' }}" href="{{ route('request', app()->getLocale()) }}">
                        @lang('layout.request')
                    </a>
                </li>
                <li class="nav-item my-auto mx-auto mx-xl-0 mr-0 mr-xl-auto{{auth()->check() ? ' d-none':null}}">
                    <a class="nav-link menu-item{{ Route::is('tracker.application')?' active':'' }}" href="{{ route('tracker.application', app()->getLocale()) }}">
                        @lang('layout.tracker')
                    </a>
                </li>
                <li class="nav-item my-auto mx-auto mx-xl-0 mr-0 mr-xl-auto menu-faq">
                    <a class="nav-link menu-item{{ Route::is('tracker.application')?' active':'' }}" href="#">
                        @lang('layout.faq')
                    </a>
                </li>
                <form id="logout-form3" action="{{ route('logout', app()->getLocale()) }}" method="POST" style="display: none;">
                    @csrf
                </form>
                <style>
                    #logout-link{
                        /*position: absolute;*/
                        /*top: 105%;*/
                        /*left: 50%;*/
                        /*transform: translateX(-50%);*/
                        color: #FFF;
                        font-size: 16px;
                    }
                    #logout-link::before{
                        content: url(/img/logout.svg);
                        display: inline-block;
                        vertical-align: middle;
                        width: 16px;
                        margin-right: 3px;
                    }
                    #logout-link:hover{
                        text-decoration: none;
                        color: #EC9C23;
                    }

                    .menu-toggler,
                    .hide-menu{
                        width: 27px;
                        height: 17px;
                        
                        justify-content: space-between;
                    }

                    .menu-toggler span,
                    .hide-menu span{
                        width: 1px;
                        height: 100%;
                        background: #EC9C23;
                        display: block;
                    }

                    .hide-menu{
                        margin: 0 16px 0 auto ;
                    }
                    .hide-menu span{
                        background: #fff;
                    }

                    .hide-menu__wrapper,
                    .menu-toggler,
                    .menu-faq{
                        display: none;
                    }

                    .dropdown-toggle{
                        background: transparent;
                        border: none;
                    }

                    @media screen and (max-width: 1200px){
                        .main-menu{
                            position: fixed;
                            left: -414px;
                            width: 414px;
                            height: 100vh;
                            top: 0;
                            display: flex;
                            flex-direction: column;
                            transition: all 0.5s;
                            background: #262424;
                            z-index: 2;
                            padding-top: 30px;
                        }

                        .main-menu.active{
                            left: 0;
                        }

                        .header-container .menu-item{
                            padding-left: 10px;
                            padding-right: 10px;
                        }

                        #lang-select{
                            flex: 1;
                        }
                        
                        
                        .menu-toggler,
                        .menu-faq,
                        .hide-menu__wrapper{
                            display: flex;
                        }

                        .lang-select-container{
                            width: 100%;
                            padding: 0 20px;
                            flex-direction: row-reverse;
                        }

                        .navbar-nav .dropdown-menu{
                            left: auto;
                            right: 0;
                            width: 50vw;
                        }

                        .navbar-nav .dropdown-menu .dropdown-item{
                            text-align: center;
                        }

                        #cabinet-has-logout{
                            width: 100%;
                            max-width: 100%;
                            padding: 0 15px;
                        }
                        #cabinet-has-logout .nav-link{
                            width: 100%;
                            max-width: 100%;
                            /* margin: 0 15px; */
                            background: rgba(255,255,255, 0.1);
                            border: none;
                        }
                    }
                </style>
                <li id="cabinet-has-logout" class="nav-item my-auto outlined mx-auto mx-xl-0 mr-0 mr-xl-auto{{auth()->check()?' ml-xl-4 inverse':null}}">
                    <a class="nav-link{{ auth()->check() ?' active':'' }}" href="{{ route('login', app()->getLocale()) }}">
                        @lang('layout.cabinet')
                    </a>
                </li>
                @auth
                <li class="nav-item my-auto mx-auto mx-xl-0 mr-0 mr-xl-auto{{auth()->check()?' ml-xl-4':null}}">
                    <a class="d-none d-xl-inline-block" id="logout-link" onclick="document.getElementById('logout-form3').submit()" href="javascript:;">Вийти</a>
                </li>
                    <li class="d-xl-none nav-item my-auto mx-auto mx-xl-0 mr-0 mr-xl-auto">
                        <a onclick="event.preventDefault(); document.getElementById('logout-form3').submit();" class="nav-link menu-item" href="javascript:;">
                            @lang('cabinet.navigation.logout')
                        </a>
                    </li>
                @endauth
            </ul>
        </div>
    </nav>
</header>
<script defer>
    document.addEventListener("DOMContentLoaded", function(){
        const toggler = document.querySelector('.menu-toggler'),
              menu = document.querySelector('.main-menu'),
              hideMenu = document.querySelector('.hide-menu'),
              dropdownToggle = document.querySelectorAll('.dropdown-toggle');

        toggler.addEventListener('click', function(){
            menu.classList.toggle('active');
        })

        hideMenu.addEventListener('click', function(){
            menu.classList.toggle('active');
        })

        dropdownToggle.forEach(function(item){
            item.addEventListener('click', function(){
                item.nextElementSibling.classList.toggle('show');
            })
        })
    })


</script>
