<section class="cabinet-container need-footer-separator">
    <div class="container">
        <div class="row">
            @include('../../cabinet.sidebar')
            <div class="col-12 col-lg-8 col-xl-9 cabinet-delimiter pr-0" id="app" v-cloak>
                <form name="stop"
                      action="{{ $dossier->exists ? route('cabinet.dossier.update', ['locale' => app()->getLocale(), 'dossier' => $dossier->id]) : route('cabinet.dossier.store', app()->getLocale()) }}"
                      method="post" enctype="multipart/form-data">
                    @if($dossier->exists)
                        @method('PUT')
                    @endif
                    @csrf
                    <div class="dossier-cabinet-form-container row">
                        <div class="row justify-content-between col-12 pr-0">
                            <div class="cabinet-form-field display-aling-center col-md-4 col-12 mt-4">
                                {{ __('cabinet.form.dossier_lang', [], $lang) }}
                            </div>
                            <div class="col-md-7 col-12 mt-md-4 mt-3">
                                <select data-minimum-results-for-search="Infinity"
                                        data-conform="{{ __('cabinet.form.lang_confirm', [], $lang) }}"
                                        id="cabinet-lang-select" class="pl-2 cabinet-form-lang-select custom-select"
                                        name="lang">
                                    <option
                                        {{ app('request')->input('lang') == 'uk' ? 'selected' : '' }} value="?lang=uk">
                                        Українська
                                    </option>
                                    <option
                                        {{ app('request')->input('lang') == 'en' ? 'selected' : '' }} value="?lang=en">
                                        English
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="cabinet-form-title mt-4 w-100">
                            <span> {{ __('cabinet.form.title_general', [], $lang) }}<span
                                    style="font-size: 12px; font-weight: normal; color: #FFFFFF; margin-left: 10px;">{{ __('cabinet.form.required_field', [], $lang) }}</span></span>
                        </div>
                        <div class="row justify-content-between col-12 pr-0">
                            @component('site.blocks.cabinet.dossier-field')
                                @slot('name', __('cabinet.form.fullname', [], $lang))
                                @slot('style', 'display-aling-center')
                            @endcomponent
                            @component('site.blocks.cabinet.text-input')
                                @slot('placeholder', __('cabinet.form.fullname_placeholder', [], $lang))
                                @slot('name', 'general[fullname]')
                                @slot('errorName', 'general.fullname')
                                @slot('type', 'input')
                                @slot('old', old('general.fullname', $dossier->general ? $dossier->general->fullname : ''))
                            @endcomponent
                            @component('site.blocks.cabinet.dossier-field')
                                @slot('name', __('cabinet.form.photo', [], $lang))
                            @endcomponent
                            <cabinet-avatar
                                :data="{{ json_encode(['photo' => $existing_photo ?? $dossier->general->photo ?? null]) }}"></cabinet-avatar>
                            <div class="col-md-7 col-12 offset-md-5">
                                @error('file.photo')
                                <span class="cabinet-validation-error">{{ $message }}</span>
                                @enderror
                            </div>
                            @component('site.blocks.cabinet.sub-title-required')
                                @slot('name', __('cabinet.form.sub_title_office', [], $lang))
                            @endcomponent
                                <div class="col-12"><span style="padding-left: 20px; font-size: 16px">{{ __('cabinet.form.main_address') }}</span></div>
                            @component('site.blocks.cabinet.dossier-field')
                                @slot('name', __('cabinet.form.country', [], $lang))
                                @slot('style', 'display-aling-center')
                            @endcomponent
                            @component('site.blocks.cabinet.select')
                                @slot('name', 'general[office][country]')
                                @slot('idAttribute', 'country-select')
                                @slot('style', 'custom-select')
                                @slot('selectedFirst', 'true')
                                @slot('placeholder', __('forms.placeholder_choose_country', [], app('request')->input('lang')))
                                @slot('errorName', 'general.office.country')
                                @slot('data', $countries)
                                @slot('data_local', 'true')
                                @slot('langKey', 'countries.')
                                @slot('data_old', old('general.office.country', isset($dossier->general->office['country']) ? $dossier->general->office['country'] : ''))
                            @endcomponent
                            @component('site.blocks.cabinet.dossier-field')
                                @slot('name', __('cabinet.form.region', [], $lang))
                                @slot('style', 'display-aling-center')
                            @endcomponent
                            @component('site.blocks.cabinet.select')
                                @slot('name', 'general[office][region]')
                                @slot('errorName', 'general.office.region')
                                @slot('style', 'custom-select')
                                @slot('placeholder', __('forms.placeholder_choose_region', [], app('request')->input('lang')))
                                @slot('idAttribute', 'region-select')
                                @slot('disabled', 'true')
                                @slot('data_local', 'true')
                                @slot('data_old', old('general.office.region', isset($dossier->general->office['region']) ? $dossier->general->office['region'] : ''))
                            @endcomponent
                            @component('site.blocks.cabinet.dossier-field')
                                @slot('name', __('cabinet.form.city', [], $lang))
                                @slot('style', 'display-aling-center')
                            @endcomponent
                            @component('site.blocks.cabinet.select')
                                @slot('name', 'general[office][city]')
                                @slot('errorName', 'general.office.city')
                                @slot('disabled', 'true')
                                @slot('placeholder', __('forms.placeholder_choose', [], app('request')->input('lang')))
                                @slot('style', 'custom-select')
                                @slot('idAttribute', 'city-select')
                                @slot('data_local', 'true')
                                @slot('data_old', old('general.office.city', isset($dossier->general->office['city']) ? $dossier->general->office['city']: ''))
                            @endcomponent
                            @component('site.blocks.cabinet.dossier-field')
                                @slot('name', __('cabinet.form.office_address', [], $lang))
                            @endcomponent
                            <div class="pr-0 col-md-7 col-12">
                                <cabinet-text-input class="w-100"
                                                    :data="{{ json_encode(['placeholder' => __('forms.placeholder_enter', [], $lang), 'name' => 'general[office][address]', 'values' => old('general.office.address', $dossier->general ? $dossier->general->office['address'] : '')])}}"></cabinet-text-input>
                                @error('general.office.address.0')
                                <span class="cabinet-validation-error">{{ $message }}</span>
                                @enderror
                            </div>
                            @component('site.blocks.cabinet.dossier-field')
                                @slot('name', __('cabinet.form.phone_number'))
                            @endcomponent
                            <div class="pr-0 col-md-7 col-12">
                                <cabinet-phone class="w-100"
                                               :data="{{ json_encode(['placeholder' => __('forms.placeholder_enter', [], $lang), 'name' => 'general[office][phones]', 'values' => old('general.office.phones', $dossier->general ? $dossier->general->office['phones'] : ''), 'error' => ['status' => $errors->has('general.office.phones.0.number'), 'message' =>  $errors->first('general.office.phones.0.number')]])}}"></cabinet-phone>
                            </div>
                        </div>
                        <div class="cabinet-form-title mt-4 w-100">
                            <span>{{ __('cabinet.form.title_licence', [], $lang) }}<span
                                    style="font-size: 12px; font-weight: normal; color: #FFFFFF; margin-left: 10px;">{{ __('cabinet.form.required_field', [], $lang) }}</span></span>
                        </div>
                        <div class="row justify-content-between col-12 pr-0">
                            @component('site.blocks.cabinet.sub-title-required')
                                @slot('name', __('cabinet.form.sub_title_licence', [], $lang))
                            @endcomponent
                            <cabinet-license :data="{{ json_encode(['countries' => $countries, 'values' => old('license', isset($dossier->license) ? $dossier->license : ''),
                                                    'errorCountry' => $errors->first('license.0.country'),
                                                    'errorInstitution' => $errors->first('license.0.institution'),
                                                    'errorYear' => $errors->first('license.0.year'),
                                                    'errorNumber' => $errors->first('license.0.number'),
                                                    'errorPhoto' => $errors->first('license.0.photo'),
                                                    ]) }}"></cabinet-license>
                            <div class="mt-5 col-12 text-center">
                                <div
                                    class="cabinet-form-title-recommended">{{ __('cabinet.form.title_recommended', [], $lang) }}
                                </div>
                                <div
                                    class="cabinet-form-title-recommended_sub mt-1">{{ __('cabinet.form.title_recommended_sub', [], $lang) }}
                                </div>
                            </div>
                        </div>
                        <div class="cabinet-form-title cursor-pointer mt-4 w-100 row col-12 display-aling-center"
                             v-on:click="showOverview = !showOverview">
                            <span class="col-sm-7 col-10">{{ __('cabinet.form.title_overview', [], $lang) }}</span>
                            <button class="btn-drop-down-form btn-drop-down-form-rotate"
                                    type="button"></button>
                        </div>
                        <transition name="fade">
                            <div v-show="showOverview" class="row justify-content-between col-12 pr-0">
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.overview_languages', [], $lang))
                                @endcomponent
                                @component('site.blocks.cabinet.text-input')
                                    @slot('type', 'input')
                                    @slot('name', 'overview[languages]')
                                    @slot('old', old('overview.languages', isset($dossier->overview['languages']) ? $dossier->overview['languages'] : ''))
                                @endcomponent
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.overview_motto', [], $lang))
                                @endcomponent
                                @component('site.blocks.cabinet.text-input')
                                    @slot('type', 'input')
                                    @slot('name', 'overview[motto]')
                                    @slot('old', old('overview.motto', isset($dossier->overview['motto']) ? $dossier->overview['motto'] : ''))
                                @endcomponent
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.overview_strong_sides', [], $lang))
                                @endcomponent
                                @component('site.blocks.cabinet.text-input')
                                    @slot('type', 'input')
                                    @slot('name', 'overview[strong_sides]')
                                    @slot('old', old('overview.strong_sides', isset($dossier->overview['strong_sides']) ? $dossier->overview['strong_sides'] : ''))
                                @endcomponent
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.overview_hobbies', [], $lang))
                                @endcomponent
                                @component('site.blocks.cabinet.text-input')
                                    @slot('type', 'input')
                                    @slot('name', 'overview[hobbies]')
                                    @slot('old', old('overview.hobbies', isset($dossier->overview['hobbies']) ? $dossier->overview['hobbies'] : ''))
                                @endcomponent
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.overview_site', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12 mt-md-4 mt-3">
                                    <cabinet-input-site
                                        :data="{{ json_encode(['value' => old('overview.site', isset($dossier->overview['site']) ? $dossier->overview['site'] : ''), 'lang' => $lang]) }}"></cabinet-input-site>
                                </div>
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.overview_facebook_followers', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12 mt-md-4 mt-3">
                                    <div class="cabinet-container-text-socials row ml-0 justify-content-between">
                                        <cabinet-input-soclink
                                            :data="{{ json_encode(['value' => old('overview.facebook', isset($dossier->overview['facebook']) ? $dossier->overview['facebook'] : ''), 'lang' => $lang, 'name' => 'overview[facebook]']) }}"></cabinet-input-soclink>
                                        <input class="col-6 mt-2 cabinet-form-text-double-input"
                                               name="overview[facebook_followers]" type="input"
                                               value="{{ old('overview.facebook_followers', isset($dossier->overview['facebook_followers']) ? $dossier->overview['facebook_followers'] : '') }}"
                                               placeholder="{{ __('forms.placeholder_enter', [], $lang)}}">
                                    </div>
                                </div>
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.overview_instagram_followers', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12 mt-md-4 mt-3">
                                    <div class="cabinet-container-text-socials row ml-0 justify-content-between">
                                        <cabinet-input-soclink
                                            :data="{{ json_encode(['value' => old('overview.instagram', isset($dossier->overview['instagram']) ? $dossier->overview['facebook'] : ''), 'lang' => $lang, 'name' => 'overview[instagram]']) }}"></cabinet-input-soclink>
                                        <input class="col-6 mt-2 cabinet-form-text-double-input"
                                               name="overview[instagram_followers]" type="input"
                                               value="{{ old('overview.instagram_followers', isset($dossier->overview['instagram_followers']) ? $dossier->overview['instagram_followers'] : '') }}"
                                               placeholder="{{ __('forms.placeholder_enter', [], $lang)}}">
                                    </div>
                                </div>
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.overview_linkedin_followers', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12 mt-md-4 mt-3">
                                    <div class="cabinet-container-text-socials row ml-0 justify-content-between">
                                        <cabinet-input-soclink
                                            :data="{{ json_encode(['value' => old('overview.linkedin', isset($dossier->overview['linkedin']) ? $dossier->overview['linkedin'] : ''), 'lang' => $lang, 'name' => 'overview[linkedin]']) }}"></cabinet-input-soclink>
                                        <input class="col-6 mt-2 cabinet-form-text-double-input"
                                               name="overview[linkedin_followers]" type="input"
                                               value="{{ old('overview.linkedin_followers', isset($dossier->overview['linkedin_followers']) ? $dossier->overview['linkedin_followers'] : '') }}"
                                               placeholder="{{ __('forms.placeholder_enter', [], $lang)}}">
                                    </div>
                                </div>
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.overview_twitter_followers', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12 mt-md-4 mt-3">
                                    <div class="cabinet-container-text-socials row ml-0 justify-content-between">
                                        <cabinet-input-soclink
                                            :data="{{ json_encode(['value' => old('overview.twitter', isset($dossier->overview['twitter']) ? $dossier->overview['twitter'] : ''), 'lang' => $lang, 'name' => 'overview[twitter]']) }}"></cabinet-input-soclink>
                                        <input class="col-6 mt-2 cabinet-form-text-double-input"
                                               name="overview[twitter_followers]" type="input"
                                               value="{{ old('overview.twitter_followers', isset($dossier->overview['twitter_followers']) ? $dossier->overview['twitter_followers'] : '') }}"
                                               placeholder="{{ __('forms.placeholder_enter', [], $lang)}}">
                                    </div>
                                </div>
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.overview_youtube_followers', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12 mt-md-4 mt-3">
                                    <div class="cabinet-container-text-socials row ml-0 justify-content-between">
                                        <cabinet-input-soclink
                                            :data="{{ json_encode(['value' => old('overview.youtube', isset($dossier->overview['youtube']) ? $dossier->overview['youtube'] : ''), 'lang' => $lang, 'name' => 'overview[youtube]']) }}"></cabinet-input-soclink>
                                        <input class="col-6 mt-2 cabinet-form-text-double-input"
                                               name="overview[youtube_followers]" type="input"
                                               value="{{ old('overview.youtube_followers', isset($dossier->overview['youtube_followers']) ? $dossier->overview['youtube_followers'] : '') }}"
                                               placeholder="{{ __('forms.placeholder_enter', [], $lang)}}">
                                    </div>
                                </div>
                            </div>
                        </transition>
                        <div class="cabinet-form-title cursor-pointer mt-4 w-100 row col-12 display-aling-center"
                             v-on:click="showContacts = !showContacts">
                            <span class="col-sm-7 col-10">{{ __('cabinet.form.title_contact', [], $lang) }}</span>
                            <button class="btn-drop-down-form btn-drop-down-form-rotate"
                                    type="button"></button>
                        </div>
                        <transition name="fade">
                            <div v-show="showContacts" class="row justify-content-between col-12 pr-0">
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', 'Email')
                                @endcomponent
                                <div class="col-md-7 col-12">
                                    <cabinet-text-input
                                        :data="{{ json_encode(['placeholder' => __('forms.placeholder_enter', [], $lang), 'name' => 'contacts[emails]', 'values' => old('contacts.emails', isset($dossier->contacts['emails']) ? $dossier->contacts['emails'] : ''), 'email' => true])}}"></cabinet-text-input>
                                </div>
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.workinghours', [], $lang))
                                @endcomponent
                                <div class="dossier-form-hours col-xl-7 col-12 mt-4">
                                    @component('site.blocks.cabinet.workinghours-select')
                                        @slot('day', __('cabinet.form.day_mo', [], $lang))
                                        @slot('lang', $lang)
                                        @slot('firstName', 'contacts[working_hours][mo][start]')
                                        @slot('secondName', 'contacts[working_hours][mo][end]')
                                        @slot('firstOld', old('contacts.working_hours.mo.start', $dossier->contacts && isset($dossier->contacts['working_hours']['mo']['start']) ? $dossier->contacts['working_hours']['mo']['start'] : ''))
                                        @slot('secondOld', old('contacts.working_hours.mo.end', $dossier->contacts && isset($dossier->contacts['working_hours']['mo']['end']) ? $dossier->contacts['working_hours']['mo']['end'] : ''))
                                    @endcomponent
                                    @component('site.blocks.cabinet.workinghours-select')
                                        @slot('day', __('cabinet.form.day_tu', [], $lang))
                                        @slot('lang', $lang)
                                        @slot('firstName', 'contacts[working_hours][tu][start]')
                                        @slot('secondName', 'contacts[working_hours][tu][end]')
                                        @slot('firstOld', old('contacts.working_hours.tu.start', $dossier->contacts && isset($dossier->contacts['working_hours']['tu']['start']) ? $dossier->contacts['working_hours']['tu']['start'] : ''))
                                        @slot('secondOld', old('contacts.working_hours.tu.end', $dossier->contacts && isset($dossier->contacts['working_hours']['tu']['end']) ? $dossier->contacts['working_hours']['tu']['end'] : ''))
                                    @endcomponent
                                    @component('site.blocks.cabinet.workinghours-select')
                                        @slot('day', __('cabinet.form.day_we', [], $lang))
                                        @slot('lang', $lang)
                                        @slot('firstName', 'contacts[working_hours][we][start]')
                                        @slot('secondName', 'contacts[working_hours][we][end]')
                                        @slot('firstOld', old('contacts.working_hours.we.start', $dossier->contacts && isset($dossier->contacts['working_hours']['we']['start']) ? $dossier->contacts['working_hours']['we']['start'] : ''))
                                        @slot('secondOld', old('contacts.working_hours.we.end', $dossier->contacts && isset($dossier->contacts['working_hours']['we']['end']) ? $dossier->contacts['working_hours']['we']['end'] : ''))
                                    @endcomponent
                                    @component('site.blocks.cabinet.workinghours-select')
                                        @slot('day', __('cabinet.form.day_th', [], $lang))
                                        @slot('lang', $lang)
                                        @slot('firstName', 'contacts[working_hours][th][start]')
                                        @slot('secondName', 'contacts[working_hours][th][end]')
                                        @slot('firstOld', old('contacts.working_hours.th.start', $dossier->contacts && isset($dossier->contacts['working_hours']['th']['start']) ? $dossier->contacts['working_hours']['th']['start'] : ''))
                                        @slot('secondOld', old('contacts.working_hours.th.end', $dossier->contacts && isset($dossier->contacts['working_hours']['th']['end']) ? $dossier->contacts['working_hours']['th']['end'] : ''))
                                    @endcomponent
                                    @component('site.blocks.cabinet.workinghours-select')
                                        @slot('day', __('cabinet.form.day_fr', [], $lang))
                                        @slot('firstName', 'contacts[working_hours][fr][start]')
                                        @slot('lang', $lang)
                                        @slot('secondName', 'contacts[working_hours][fr][end]')
                                        @slot('firstOld', old('contacts.working_hours.fr.start', $dossier->contacts && isset($dossier->contacts['working_hours']['fr']['start']) ? $dossier->contacts['working_hours']['fr']['start'] : ''))
                                        @slot('secondOld', old('contacts.working_hours.fr.end', $dossier->contacts && isset($dossier->contacts['working_hours']['fr']['end']) ? $dossier->contacts['working_hours']['fr']['end'] : ''))
                                    @endcomponent
                                    @component('site.blocks.cabinet.workinghours-select')
                                        @slot('day', __('cabinet.form.day_sa', [], $lang))
                                        @slot('lang', $lang)
                                        @slot('firstName', 'contacts[working_hours][sa][start]')
                                        @slot('secondName', 'contacts[working_hours][sa][end]')
                                        @slot('firstOld', old('contacts.working_hours.sa.start', $dossier->contacts && isset($dossier->contacts['working_hours']['sa']['start']) ? $dossier->contacts['working_hours']['sa']['start'] : ''))
                                        @slot('secondOld', old('contacts.working_hours.sa.end', $dossier->contacts && isset($dossier->contacts['working_hours']['sa']['end']) ? $dossier->contacts['working_hours']['sa']['end'] : ''))
                                    @endcomponent
                                    @component('site.blocks.cabinet.workinghours-select')
                                        @slot('day', __('cabinet.form.day_su', [], $lang))
                                        @slot('lang', $lang)
                                        @slot('firstName', 'contacts[working_hours][su][start]')
                                        @slot('secondName', 'contacts[working_hours][su][end]')
                                        @slot('firstOld', old('contacts.working_hours.su.start', $dossier->contacts && isset($dossier->contacts['working_hours']['su']['start']) ? $dossier->contacts['working_hours']['su']['start'] : ''))
                                        @slot('secondOld', old('contacts.working_hours.su.end', $dossier->contacts && isset($dossier->contacts['working_hours']['su']['end']) ? $dossier->contacts['working_hours']['su']['end'] : ''))
                                    @endcomponent
                                </div>
                            </div>
                        </transition>
                        <div class="cabinet-form-title cursor-pointer mt-4 w-100 row col-12 display-aling-center"
                             v-on:click="showEducation = !showEducation">
                            <span class="col-sm-7 col-10">{{ __('cabinet.form.title_education', [], $lang) }}</span>
                            <button class="btn-drop-down-form btn-drop-down-form-rotate"
                                    type="button"></button>
                        </div>
                        <transition name="fade">
                            <div v-show="showEducation" class="col-12 pr-0">
                                @component('site.blocks.cabinet.sub-title')
                                    @slot('name', __('cabinet.form.sub_title_education_higher', [], $lang))
                                @endcomponent
                                <cabinet-education-higher
                                    :data="{{ json_encode(['values' => old('education.higher', isset($dossier->education['higher']) ? $dossier->education['higher'] : ''), 'degreeOptionData' => $formHelper->getDegree(), 'fieldOptionData' => $formHelper->getField()]) }}"></cabinet-education-higher>
                                @component('site.blocks.cabinet.sub-title')
                                    @slot('name', __('cabinet.form.sub_title_education_academic_status', [], $lang))
                                @endcomponent
                                <cabinet-academic-status
                                    :data="{{ json_encode(['values' => old('education.academic_status', isset($dossier->education['academic_status']) ? $dossier->education['academic_status'] : ''), 'degreeOptionData' => $formHelper->getAcademicStatusDegree()]) }}"></cabinet-academic-status>
                                @component('site.blocks.cabinet.sub-title')
                                    @slot('name', __('cabinet.form.sub_title_education_teaching', [], $lang))
                                @endcomponent
                                <cabinet-teaching
                                    :data="{{ json_encode(['values' => old('education.teaching',  isset($dossier->education['teaching']) ? $dossier->education['teaching'] : '')]) }}"></cabinet-teaching>
                            </div>
                        </transition>
                        <div class="cabinet-form-title mt-4 w-100 row col-12 display-aling-center cursor-pointer"
                             v-on:click="showExperience = !showExperience">
                            <span class="col-sm-7 col-10">{{ __('cabinet.form.title_experience', [], $lang) }}</span>
                            <button class="btn-drop-down-form btn-drop-down-form-rotate"
                                    type="button"></button>
                        </div>
                        <transition name="fade">
                            <div v-show="showExperience" class="row justify-content-between col-12 pr-0">
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.experience_year', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12 mt-md-4 mt-3">
                                    <select id="cabinet-years-select" class="pl-2 cabinet-form-select"
                                            name="experience[years]">
                                        <option selected
                                                disabled>{{ __('forms.placeholder_choose', [], $lang) }}</option>
                                        <option>{{  __('cabinet.form.none', [], $lang) }}</option>
                                        @for($i = 1; $i <= 50; $i++)
                                            <option
                                                {{ $i == old('experience.years',  isset($dossier->experience) ? $dossier->experience['years'] : '') ? 'selected' : '' }} value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.experience_country', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12">
                                    <cabinet-select
                                        :data="{{ json_encode(['name' => 'experience[countries]', 'optionData' => $countries, 'values' => old('experience.countries', isset($dossier->experience['countries']) ? $dossier->experience['countries'] : ''), 'langKey' => 'countries.'])}}"></cabinet-select>
                                </div>
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.experience_region', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12">
                                    <cabinet-text-input
                                        :data="{{ json_encode(['placeholder' => __('forms.placeholder_enter', [], $lang), 'name' => 'experience[regions]', 'values' => old('experience.regions', isset($dossier->experience['regions']) ? $dossier->experience['regions'] : '')])}}"></cabinet-text-input>
                                </div>
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.experience_city', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12">
                                    <cabinet-text-input
                                        :data="{{ json_encode(['placeholder' => __('forms.placeholder_enter', [], $lang), 'name' => 'experience[cities]', 'values' => old('experience.cities', isset($dossier->experience['cities']) ? $dossier->experience['cities'] : '')])}}"></cabinet-text-input>
                                </div>
                                @component('site.blocks.cabinet.sub-title-required')
                                    @slot('name', __('cabinet.form.experience_specializations', [], $lang))
                                @endcomponent
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.experience_specializations', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12">
                                    <cabinet-select
                                        :data="{{ json_encode(['name' => 'experience[specs][specializations]', 'optionData' => $specializations, 'values' => old('experience.specs.specializations', isset($dossier->spec['specializations']) ? $dossier->spec['specializations'] : ''), 'langKey' => 'specializations.']) }}"></cabinet-select>
                                </div>
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.experience_activities', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12">
                                    <cabinet-select
                                        :data="{{ json_encode(['name' => 'experience[specs][activities]', 'optionData' => $formHelper->getActivities(), 'values' => old('experience.specs.activities', isset($dossier->spec['activities']) ? $dossier->spec['activities'] : ''), 'langKey' => 'cabinet.form.'])}}"></cabinet-select>
                                </div>
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.experience_form', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12 mt-md-4 mt-3">
                                    <cabinet-single-select :data="{{ json_encode([
                                                                            'selects' => $formHelper->getForm(),
                                                                            'selected' => old('experience.specs.form', isset($dossier->spec['form']) ?? ''),
                                                                            'lang' => $lang,
                                                                            'langKey' => 'cabinet.form.',
                                                                            'class' => 'cabinet-form-component-select'
                                                                        ]) }}"></cabinet-single-select>
                                </div>
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.experience_appeal_exp', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12 mt-md-4 mt-3">
                                    <cabinet-spec-exp
                                        :data="{{ json_encode([
                                                                        'value' => old('experience.specs.appeal_exp',
                                                                        isset($dossier->spec['appeal_exp'])
                                                                         ? $dossier->spec['appeal_exp'] : ''),
                                                                         'lang' => $lang,
                                                                         'name' => 'experience[specs][appeal_exp]'
                                                                         ]) }}"
                                        @changed="setAppealExpCaseShow"
                                    ></cabinet-spec-exp>
                                </div>
                                <div v-show="appeal_exp" class="cabinet-form-field col-md-4 col-12 mt-4">
                                    {{ __('cabinet.form.experience_appeal_cases', [], $lang) }}
                                </div>
                                <div v-show="appeal_exp" class="col-md-7 col-12">
                                    <cabinet-textarea
                                        :data="{{ json_encode(['name' => 'experience[specs][appeal_cases]', 'placeholder' => __('cabinet.form.textarea_enter_text'), 'values' => old('experience.specs.appeal_cases', isset($dossier->spec['appeal_cases']) ? $dossier->spec['appeal_cases'] : '')])}}"></cabinet-textarea>
                                </div>
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name',  __('cabinet.form.experience_cassation_exp', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12 mt-md-4 mt-3">
                                    <cabinet-spec-exp
                                        :data="{{ json_encode([
                                                                        'value' => old('experience.specs.cassation_exp',
                                                                        isset($dossier->spec['cassation_exp'])
                                                                         ? $dossier->spec['cassation_exp'] : ''),
                                                                         'lang' => $lang,
                                                                         'name' => 'experience[specs][cassation_exp]'
                                                                         ]) }}"
                                        @changed="setCassationExpCaseShow"
                                    ></cabinet-spec-exp>
                                </div>
                                <div v-show="cassation_exp"
                                     class="cabinet-form-field col-md-4 col-12 mt-4">
                                    {{ __('cabinet.form.experience_cassation_cases', [], $lang) }}
                                </div>
                                <div v-show="cassation_exp" class="col-md-7 col-12">
                                    <cabinet-textarea
                                        :data="{{ json_encode(['name' => 'experience[specs][cassation_cases]', 'placeholder' => __('cabinet.form.textarea_enter_text'), 'values' => old('experience.specs.cassation_cases', isset($dossier->spec['cassation_cases']) ? $dossier->spec['cassation_cases'] : '')])}}"></cabinet-textarea>
                                </div>
                                @component('site.blocks.cabinet.dossier-field')
                                    @slot('name', __('cabinet.form.experience_intl_exp', [], $lang))
                                @endcomponent
                                <div class="col-md-7 col-12 mt-md-4 mt-3">
                                    <cabinet-spec-exp
                                        :data="{{ json_encode([
                                                                        'value' => old('experience.specs.intl_exp',
                                                                        isset($dossier->spec['intl_exp'])
                                                                         ? $dossier->spec['intl_exp'] : ''),
                                                                         'lang' => $lang,
                                                                         'name' => 'experience[specs][intl_exp]'
                                                                         ]) }}"
                                        @changed="setIntlExpCaseShow"
                                    ></cabinet-spec-exp>
                                </div>
                                <div v-show="intl_exp" class="cabinet-form-field col-md-4 col-12 mt-4">
                                    {{ __('cabinet.form.experience_intl_cases', [], $lang) }}
                                </div>
                                <div v-show="intl_exp" class="col-md-7 col-12">
                                    <cabinet-textarea
                                        :data="{{ json_encode(['name' => 'experience[specs][intl_cases]',  'placeholder' => __('cabinet.form.textarea_enter_text', [], $lang), 'values' => old('experience.specs.intl_cases', isset($dossier->spec['intl_cases']) ? $dossier->spec['intl_cases'] : '')])}}"></cabinet-textarea>
                                </div>
                            </div>
                        </transition>
                        <div class="cabinet-form-title mt-4 w-100 row col-12 display-aling-center cursor-pointer"
                             v-on:click="showExperienceOther = !showExperienceOther">
                            <span
                                class="col-sm-7 col-10">{{ __('cabinet.form.title_other_experience', [], $lang) }}</span>
                            <button class="btn-drop-down-form btn-drop-down-form-rotate"
                                    type="button"></button>
                        </div>
                        <transition name="fade">
                            <div v-show="showExperienceOther" class="col-12 pr-0">
                                <cabinet-experience-other
                                    :data="{{ json_encode(['values' => old('experience_other.experience',  isset($dossier->experience_other['experience']) ? $dossier->experience_other['experience'] : '')]) }}"></cabinet-experience-other>
                                @component('site.blocks.cabinet.sub-title')
                                    @slot('name', __('cabinet.form.sub_title_patent_license', [], $lang))
                                @endcomponent
                                <div class="row justify-content-between col-12 pr-0 pl-0">
                                    @component('site.blocks.cabinet.dossier-field')
                                        @slot('name', __('cabinet.form.licence_country', [], $lang))
                                    @endcomponent
                                        <div class="col-md-7 col-12 mt-md-4 mt-3">
                                            <cabinet-single-select :data="{{ json_encode([
                                                                            'selects' => $countries,
                                                                            'selected' => old('experience_other.patent_license.country', isset($dossier->experience_other['patent_license']['country']) ?? ''),
                                                                            'lang' => $lang,
                                                                            'class' => 'cabinet-form-component-select',
                                                                            'langKey' => 'countries.'
                                                                        ]) }}"></cabinet-single-select>
                                        </div>
                                    @component('site.blocks.cabinet.dossier-field')
                                        @slot('name', __('cabinet.form.patent_license_institution', [], $lang))
                                    @endcomponent
                                    @component('site.blocks.cabinet.text-input')
                                        @slot('type', 'input')
                                        @slot('name', 'experience_other[patent_license][institution]')
                                        @slot('old', old('experience_other.patent_license.institution', isset($dossier->experience_other['patent_license']['institution']) ? $dossier->experience_other['patent_license']['institution'] : ''))
                                    @endcomponent
                                    @component('site.blocks.cabinet.dossier-field')
                                        @slot('name', __('cabinet.form.licence_year', [], $lang))
                                    @endcomponent
                                    @component('site.blocks.cabinet.text-input')
                                        @slot('type', 'number')
                                        @slot('name', 'experience_other[patent_license][year]')
                                        @slot('old', old('experience_other.patent_license.institution', isset($dossier->experience_other['patent_license']['year']) ? $dossier->experience_other['patent_license']['year'] : ''))
                                    @endcomponent
                                    @component('site.blocks.cabinet.dossier-field')
                                        @slot('name', __('cabinet.form.patent_license_number', [], $lang))
                                    @endcomponent
                                    @component('site.blocks.cabinet.text-input')
                                        @slot('type', 'input')
                                        @slot('name', 'experience_other[patent_license][number]]')
                                        @slot('old', old('experience_other.patent_license.institution', isset($dossier->experience_other['patent_license']['number']) ? $dossier->experience_other['patent_license']['number'] : ''))
                                    @endcomponent
                                    @component('site.blocks.cabinet.dossier-field')
                                        @slot('name', __('cabinet.form.doc_photo', [], $lang))
                                    @endcomponent
                                    <cabinet-document
                                        :data="{{ json_encode(['name' => 'file[experience_other][patent_license]', 'photo' => isset($dossier->experience_other['patent_license']['photo']) ? $dossier->experience_other['patent_license']['photo'] : false])}}"></cabinet-document>
                                </div>
                            </div>
                        </transition>
                        <div class="cabinet-form-title mt-4 w-100 row col-12 display-aling-center cursor-pointer"
                             v-on:click="showTraining = !showTraining">
                            <span class="col-sm-7 col-10">{{ __('cabinet.form.title_training', [], $lang) }}</span>
                            <button class="btn-drop-down-form btn-drop-down-form-rotate" type="button"></button>
                        </div>
                        <transition name="fade">
                            <div v-show="showTraining" class="col-12 pr-0">
                                <cabinet-training
                                    :data="{{ json_encode(['values' => old('training',  isset($dossier->training) ? $dossier->training : ''), 'countries' => $countries]) }}"></cabinet-training>
                            </div>
                        </transition>
                        <div class="cabinet-form-title mt-4 w-100 row col-12 display-aling-center cursor-pointer"
                             v-on:click="showPublications = !showPublications">
                            <span class="col-sm-7 col-10">{{ __('cabinet.form.title_publications', [], $lang) }}</span>
                            <button class="btn-drop-down-form btn-drop-down-form-rotate"
                                    type="button"></button>
                        </div>
                        <transition name="fade">
                            <div v-show="showPublications" class="col-12 pr-0">
                                <cabinet-publications
                                    :data="{{ json_encode(['values' => old('publications',  isset($dossier->publications) ? $dossier->publications : '')]) }}"></cabinet-publications>
                            </div>
                        </transition>
                        <div class="cabinet-form-title mt-4 w-100 row col-12 display-aling-center cursor-pointer"
                             v-on:click="showRecommendations = !showRecommendations">
                            <span
                                class="col-sm-7 col-10">{{ __('cabinet.form.title_recommendations', [], $lang) }}</span>
                            <button class="btn-drop-down-form btn-drop-down-form-rotate"
                                    type="button"></button>
                        </div>
                        <transition name="fade">
                            <div v-show="showRecommendations" class="col-12 pr-0">
                                <cabinet-recommendations
                                    :data="{{ json_encode(['values' => old('recommendations',  isset($dossier->recommendations) ? $dossier->recommendations : '')]) }}"></cabinet-recommendations>
                            </div>
                        </transition>
                        <div class="cabinet-form-title mt-4 w-100 row col-12 display-aling-center cursor-pointer"
                             v-on:click="showHonors = !showHonors">
                            <span class="col-sm-7 col-10">{{ __('cabinet.form.title_honors', [], $lang) }}</span>
                            <button class="btn-drop-down-form btn-drop-down-form-rotate"
                                    type="button"></button>
                        </div>
                        <transition name="fade">
                            <div v-show="showHonors" class="col-12 pr-0">
                                <cabinet-honors class="w-100"
                                                :data="{{ json_encode(['values' => old('honors',  isset($dossier->honors) ? $dossier->honors : '')]) }}"></cabinet-honors>
                            </div>
                        </transition>
                        <div class="cabinet-form-title mt-4 w-100 row col-12 display-aling-center cursor-pointer"
                             v-on:click="showServices = !showServices">
                            <span class="col-sm-7 col-10">{{ __('cabinet.form.title_services', [], $lang) }}</span>
                            <button class="btn-drop-down-form btn-drop-down-form-rotate"
                                    type="button"></button>
                        </div>
                        <transition name="fade">
                            <div v-show="showServices" class="col-12 pr-0">
                                <cabinet-services
                                    :data="{{ json_encode(['values' => old('services',  isset($dossier->services) ? $dossier->services : ''), 'currencies' => $currencies]) }}"></cabinet-services>
                            </div>
                        </transition>
                        <div class="request-agreement mt-4 mb-5 ml-md-4 ml-0 pl-3 pr-0">
                            <label class="checkbox-container">
                                @lang('site.request.agreement', ['link' => route('policy', app()->getLocale())], $lang)
                                <input type="checkbox" class="@error('agreement') is-invalid @enderror"
                                       name="agreement">
                                <span class="checkmark"></span>
                            </label>
                            <span></span>
                            @error('agreement')
                            <span class="cabinet-validation-error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="row justify-content-center col-12 pl-5 pr-5">
                            <div class="offset-md-6-md col-12 pr-0 pl-0 mb-1 mb-md-4 text-md-left text-center">
                                <button onclick="if (!confirm(('{{ addslashes(__('cabinet.form.all_filled')) }}'))) event.preventDefault(); event.stopPropagation();" class="cabinet-text-decoration-line" name="redirect" type="submit" value="1">
                                    @lang('cabinet.form.preview_link', [], $lang)
                                </button>
                            </div>
                            {{--                            <div class="col-md-6 col-12 text-md-right mb-1 p-0 text-center">--}}
                            {{--                                <a class="cabinet-text-decoration-line" href="">Створити досьє іншою мовою</a>--}}
                            {{--                            </div>--}}
                        </div>
                        <div class="row justify-content-center col-12">
                            <div class="row col-10 justify-content-around">
                                <button onclick="if (!confirm(('{{ addslashes(__('cabinet.form.all_filled')) }}'))) event.preventDefault(); event.stopPropagation();" class="cabinet-form-submit col-md-5 col-xl-4 col-12 mt-4" type="submit">
                                    @lang('cabinet.form.save_submit', [], $lang)
                                </button>
                                <button  class="cabinet-form-submit col-md-5 col-xl-4 col-12 mt-4" name="status"
                                        value="10" type="submit">
                                    @lang('cabinet.form.publicate_submit', [], $lang)
                                </button>
                            </div>
                        </div>
                        @if (session()->has('url_review'))
                            <cabinet-redirect-preview
                                :url="{{ json_encode(session('url_review')) }}"></cabinet-redirect-preview>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
    window.addEventListener('load', function() {
        let invalid = $('.is-invalid');
        if(invalid.length){
            $('html, body').animate({
                scrollTop: $('.is-invalid').offset().top
            }, 800);
        }
    })
</script>
@include('site.modals.notification-component')
{{--<script>--}}
{{--    import SelectSingleComponent from "../../../js/components/cabinet/SelectSingleComponent";--}}

{{--    export default {--}}
{{--        components: {SelectSingleComponent}--}}
{{--    }--}}
{{--</script>--}}
