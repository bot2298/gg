<section class="cabinet-container need-footer-separator">
    <div class="container">
        <div class="row">
            @include('../../cabinet.sidebar')
            <div class="col-12 col-lg-8 col-xl-9 cabinet-delimiter cabinet-dossier-index-container pr-0" id="app" v-cloak>
                @php($lang = \App\Models\Dossier::firstFreeLang())
                <div class="row">
                    @if($lang || count(\App\Models\Dossier::all()) < 1)
                        <div class="col-12 mb-5">
                            <a href="{{ route('cabinet.dossier.create', ['locale' => app()->getLocale(), 'lang' => $lang ? $lang : app()->getLocale()]) }}">
                                <button class="dossier-cabinet-add-dossier">
                                    {{ auth()->user()->dossiers->count() ? __('cabinet.form_index.create_another_lang'): __('cabinet.form_index.create_new') }}
                                </button>
                            </a>
                        </div>
                    @endif
                    @foreach($dossier as $item)
                        <div class="col-12 col-md-6 col-xl-4 mb-4 text-center">
                            @if($item->status != \App\Models\Dossier::STATUS_NEW)
                                <div class="dossier-item-cabinet-index-card-more-container d-inline-block float-right">
                                    <a class="pl-1 dossier-item-cabinet-index-more-actions" type="button"
                                       id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false">
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        {{--                                    <form--}}
                                        {{--                                        action="{{ route('cabinet.dossier.destroy', ['locale' => app()->getLocale(), 'id' => $item->id]) }}"--}}
                                        {{--                                        name="delete" method="post">--}}
                                        {{--                                        @csrf--}}
                                        {{--                                        @method('DELETE')--}}
                                        {{--                                        <a href="#" class="dropdown-item"--}}
                                        {{--                                           onclick="if(confirm('{{ __('cabinet.form_index.delete_confirm') }}')) document.forms['delete'].submit();">@lang('cabinet.form_index.delete')</a>--}}
                                        {{--                                    </form>--}}
                                        <a target="_blank" class="dropdown-item"
                                           href="{{ route('dossier.single', ['locale' => app()->getLocale(), 'id' => $item->id]) }}">@lang('cabinet.form_index.view')</a>
                                    </div>
                                </div>
                            @endif
                            <div class="dossier-item-cabinet-index-card mx-auto w-100">
                            <span
                                class="float-left dossier-status-{{key($item->getStatus()) }}">{{ __($item->getStatus()[key($item->getStatus())]) }}</span>
                                <div style="width: 230px; height: 230px; display: flex; justify-content: center; align-items: center; clear: both; margin: 0 auto;">
                                    <img class="dossier-item-cabinet-index-card-img"
                                         src="{{ Storage::url($item->general->photo) }}" alt="">
                                </div>
                                <div class="dossier-item-cabinet-index-card-name">{{ $item->general->fullname }}</div>
                                <div class="dossier-item-cabinet-index-card-leng">{{ __('cabinet.form.dossier_lang') }}:
                                    {{ config('app.locales')[$item->locale] }}
                                </div>
                                <div class="row justify-content-around mt-2 mb-2">
                                    @if($item->status !== \App\Models\Dossier::STATUS_NEW)
                                        <div class="col-12 mb-2">
                                            <a href="{{ route('cabinet.dossier.edit', ['locale' => app()->getLocale(), 'id' => $item->id, 'lang' => $item->locale]) }}"
                                               class="dossier-item-cabinet-index-card-button">
                                                @lang('cabinet.form_index.edit')
                                            </a>
                                        </div>
                                    @endif
                                    @if($item->getStatus()[key($item->getStatus())] == 'cabinet.navigation.dossier_not_active')
                                        <div class="col-12 mb-2">
                                            <form
                                                action="{{ route('cabinet.dossier.activate', ['locale' => app()->getLocale(), 'id' => $item->id]) }}"
                                                name="activate" method="post">
                                                @csrf
                                                @method('PATCH')
                                                <a href="#" class="dossier-item-cabinet-index-card-button"
                                                   onclick="document.forms['activate'].submit();">@lang('cabinet.form_index.activate')</a>
                                            </form>
                                        </div>
                                    @elseif($item->getStatus()[key($item->getStatus())] == 'cabinet.navigation.dossier_active')
                                        <div class="col-12">
                                            <form
                                                action="{{ route('cabinet.dossier.stop', ['locale' => app()->getLocale(), 'id' => $item->id]) }}"
                                                name="stop" method="post">
                                                @csrf
                                                @method('PATCH')
                                                <a href="#" class="dossier-item-cabinet-index-card-button"
                                                   onclick="document.forms['stop'].submit();">@lang('cabinet.form_index.stop')</a>
                                            </form>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@include('site.modals.notification-component')
