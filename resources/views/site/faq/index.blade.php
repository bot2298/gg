<section class="header-info-container faq-header-info background-fluid mb-3">
    <div class="container">
        <div class="row">
            <div class="faq-heading">{{ __('site.faq.heading')  }}</div>
        </div>
    </div>
</section>
<div class="container">
    {{ Breadcrumbs::render('faq') }}
</div>

<section class="need-footer-separator">
    <div class="faq-container container">
        <div class="accordion faq-card-item active mb-5" id="faq-cards-1">
            <div class="faq-card__item">
                <div class="faq-card__header collapsed" id="card-num-1-0" data-toggle="collapse" data-target="#card-collapse-1-0" aria-expanded="false" aria-controls="card-collapse-1-0">
                    <h4>{{ __('site.faq.item1') }}</h4>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="24" height="24">
                            <rect width="24" height="24" transform="matrix(1 0 0 -1 0 24)" fill="url(#pattern0)"/>
                        </mask>
                        <g mask="url(#mask0)">
                            <rect width="24" height="24" transform="matrix(1 0 0 -1 0 24)" fill="#262424"/>
                        </g>
                        <defs>
                            <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                <use xlink:href="#image0" transform="scale(0.0104167)"/>
                            </pattern>
                            <image id="image0" width="96" height="96" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAF1ElEQVR4Xu2bTYgcVRDH/9U9PbOrBz/iyQ+8CpKjmKBgIAezJpKFFfGkIEYPgiQHCZ72ag5iCKh4VPwgi6K4gQRzED0I6kXw4MUcFERBD0KMZnpmuqQ/dt2dzExXv6mZ171Tc8lh61W99/91db9/vw7Bfl4VIK/VrTgMgOeLwAAYAM8KeC5vHWAAPCvgubx1gAHwrIDn8tYBBsCzAp7LWwcYAM8KeC5vHWAAPCvgubx1gAHwrIDn8tYBBsCzAp7LWwcYAM8KeC5vHWAAPCvgubx1gAHwrIDn8qIOYOZlcPwRKDhDFH3lec61Ls8cHwDzOqj9BBFdK5tsKQBmDsD9DTCvAeiC+WlqdTbKEi/i37nfXQXRBwCWQXwB1F4losEkLcoBDOLXAZzckSRN+BKF7TcXUeRxa+Z+/CwIbwNo7Yg5S2H7lDMAHsSp8CmAET8+g6D9ChHxooPgQe80wK+O0eEUhe2z4zQa2wHcv/44KPgEQDhB4HcQRCeIqLeIEJg5RNJL7wTPT1h/AuYnqdX5eFTMSADM8QNI8AWAm0uFJdoEtZ4ion9KY/dQADMvgfvvFc/GspVdR0CHiaKvhwPHAOjej4QuArinLHP2d8a3CKNjRPSHKL7hQcx8Gwa9z0B4WLYU/g0BPUbU/l4EINOUr92JJLoEYL+sCH5EMFghWv5ZGN/IMEddjhAt/yK+BW0FapJupNpDk2bWvzOUb0OZO+D++8J7XTrlvxDQKlH05V4Q/f+LsXcQCW8C2Cdal/DZWAogvx1lT/s3ALwgKp4btmeo1TkvjK912C6DJZupeHcoArB9FUze7w5PbU8YtjEGawKGav6oEoCsG0Y7vknXxTkE0cmmGTZmJiTxOkDrsoseThdcZQA5hO5xEH2YvfOQ/d5FED3XFMMmNFg7V+78jswJQP5ciA8gwQX5QwmXQdEaEV2VMfMTxcw3gePzYDomnEG66Tju+pbYGUAOQX9bJlz0TMKKbfcmCA/JCow3WLLxmP6/qToYkysIkkeJlq5IJzmPOOZ/70USpsbzPmG91HiONVjCHNMDKLapatZcOnHNuLyTg0sA3y3Kq/jqZapb0M7JcnXDdhUJrVEUXRYtekZBzL1HkPCnAG4VlRAaLFEuKNyChiA0yrDN0mB5AbBVtOSAYnhuDNDLFEavSSetEceD+EUA5wAEsnzVDJYsp3IH7OqGmhq2eRks7wCyh3PNDFthsN4CcEIokLPBEubX2QVNKlYXwzZvg1UbAPk2tbJh+w5hdFTrhI2Zby9OsOZmsGoFIIdQ+YQtNWxHiJZ+ki5mVJwvgyWds5oPkBR0OGH7HQGtjDpLldXzZ7Ak80tj5gog74TKJ2x/F4btc+misjq93iEEmcG6RTRO2WCJavoAUECoatji4pNI0QlbYbDS1+VLQiHEJ1jCfOKwuXfALq9Q7YRNZNjqYrCkBLwCyL3CyG8qJ81/5Alb3QxWYwBoGLY6GqxGAcifC24nbAAG4HgDTEeFi57qBEtYQxzm/Ra065nA3f1IgosA3yVaAeObLI7woCge9CuCZIWo84MsfvZRtQKQd0JlwyZVSeUES1pMGlc7AMU2teIJW8lyFU+wpMJK42oJoIBQ9ZPI0Wv2ZLAaD6CAUNWwDa/bm8HaEwC2FlHxhK0YNpsTLKmw0rja3oKGF1DBsDl9IigVTDuuMQCyW1L5CdvMT7AWGkD+XBhr2GplsKSgGtUB28+EGz6kmv4TQalg2nGNBJB3wvanhFEdP3WUgmosgGKbekf6LxH9KV1w3eIaDaBuYrrMxwC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY45j9Q7Gl/1FXl4gAAAABJRU5ErkJggg=="/>
                        </defs>
                    </svg>
                </div>
                <div id="card-collapse-1-0" class="faq-card__body collapse" aria-labelledby="card-num-1-0" data-parent="#faq-cards-1" style="">
                    <div class="faq-body-container">
                        @lang('site.faq.item1_content')
                    </div>
                </div>
            </div>

            <div class="faq-card__item">
                <div class="faq-card__header collapsed" id="card-num-1-1" data-toggle="collapse" data-target="#card-collapse-1-1" aria-expanded="false" aria-controls="card-collapse-1-1">
                    <h4>{{ __('site.faq.item2') }}</h4>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="24" height="24">
                            <rect width="24" height="24" transform="matrix(1 0 0 -1 0 24)" fill="url(#pattern0)"/>
                        </mask>
                        <g mask="url(#mask0)">
                            <rect width="24" height="24" transform="matrix(1 0 0 -1 0 24)" fill="#262424"/>
                        </g>
                        <defs>
                            <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                <use xlink:href="#image0" transform="scale(0.0104167)"/>
                            </pattern>
                            <image id="image0" width="96" height="96" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAF1ElEQVR4Xu2bTYgcVRDH/9U9PbOrBz/iyQ+8CpKjmKBgIAezJpKFFfGkIEYPgiQHCZ72ag5iCKh4VPwgi6K4gQRzED0I6kXw4MUcFERBD0KMZnpmuqQ/dt2dzExXv6mZ171Tc8lh61W99/91db9/vw7Bfl4VIK/VrTgMgOeLwAAYAM8KeC5vHWAAPCvgubx1gAHwrIDn8tYBBsCzAp7LWwcYAM8KeC5vHWAAPCvgubx1gAHwrIDn8tYBBsCzAp7LWwcYAM8KeC5vHWAAPCvgubx1gAHwrIDn8qIOYOZlcPwRKDhDFH3lec61Ls8cHwDzOqj9BBFdK5tsKQBmDsD9DTCvAeiC+WlqdTbKEi/i37nfXQXRBwCWQXwB1F4losEkLcoBDOLXAZzckSRN+BKF7TcXUeRxa+Z+/CwIbwNo7Yg5S2H7lDMAHsSp8CmAET8+g6D9ChHxooPgQe80wK+O0eEUhe2z4zQa2wHcv/44KPgEQDhB4HcQRCeIqLeIEJg5RNJL7wTPT1h/AuYnqdX5eFTMSADM8QNI8AWAm0uFJdoEtZ4ion9KY/dQADMvgfvvFc/GspVdR0CHiaKvhwPHAOjej4QuArinLHP2d8a3CKNjRPSHKL7hQcx8Gwa9z0B4WLYU/g0BPUbU/l4EINOUr92JJLoEYL+sCH5EMFghWv5ZGN/IMEddjhAt/yK+BW0FapJupNpDk2bWvzOUb0OZO+D++8J7XTrlvxDQKlH05V4Q/f+LsXcQCW8C2Cdal/DZWAogvx1lT/s3ALwgKp4btmeo1TkvjK912C6DJZupeHcoArB9FUze7w5PbU8YtjEGawKGav6oEoCsG0Y7vknXxTkE0cmmGTZmJiTxOkDrsoseThdcZQA5hO5xEH2YvfOQ/d5FED3XFMMmNFg7V+78jswJQP5ciA8gwQX5QwmXQdEaEV2VMfMTxcw3gePzYDomnEG66Tju+pbYGUAOQX9bJlz0TMKKbfcmCA/JCow3WLLxmP6/qToYkysIkkeJlq5IJzmPOOZ/70USpsbzPmG91HiONVjCHNMDKLapatZcOnHNuLyTg0sA3y3Kq/jqZapb0M7JcnXDdhUJrVEUXRYtekZBzL1HkPCnAG4VlRAaLFEuKNyChiA0yrDN0mB5AbBVtOSAYnhuDNDLFEavSSetEceD+EUA5wAEsnzVDJYsp3IH7OqGmhq2eRks7wCyh3PNDFthsN4CcEIokLPBEubX2QVNKlYXwzZvg1UbAPk2tbJh+w5hdFTrhI2Zby9OsOZmsGoFIIdQ+YQtNWxHiJZ+ki5mVJwvgyWds5oPkBR0OGH7HQGtjDpLldXzZ7Ak80tj5gog74TKJ2x/F4btc+misjq93iEEmcG6RTRO2WCJavoAUECoatji4pNI0QlbYbDS1+VLQiHEJ1jCfOKwuXfALq9Q7YRNZNjqYrCkBLwCyL3CyG8qJ81/5Alb3QxWYwBoGLY6GqxGAcifC24nbAAG4HgDTEeFi57qBEtYQxzm/Ra065nA3f1IgosA3yVaAeObLI7woCge9CuCZIWo84MsfvZRtQKQd0JlwyZVSeUES1pMGlc7AMU2teIJW8lyFU+wpMJK42oJoIBQ9ZPI0Wv2ZLAaD6CAUNWwDa/bm8HaEwC2FlHxhK0YNpsTLKmw0rja3oKGF1DBsDl9IigVTDuuMQCyW1L5CdvMT7AWGkD+XBhr2GplsKSgGtUB28+EGz6kmv4TQalg2nGNBJB3wvanhFEdP3WUgmosgGKbekf6LxH9KV1w3eIaDaBuYrrMxwC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY45j9Q7Gl/1FXl4gAAAABJRU5ErkJggg=="/>
                        </defs>
                    </svg>
                </div>
                <div id="card-collapse-1-1" class="faq-card__body collapse" aria-labelledby="card-num-1-1" data-parent="#faq-cards-1" style="">
                    <div class="faq-body-container">
                        @lang('site.faq.item1_content')
                    </div>
                </div>
            </div>

            <div class="faq-card__item">
                <div class="faq-card__header" id="card-num-1-2" data-toggle="collapse" data-target="#card-collapse-1-2" aria-expanded="false" aria-controls="card-collapse-1-2">
                    <h4>{{ __('site.faq.item2') }}</h4>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="24" height="24">
                            <rect width="24" height="24" transform="matrix(1 0 0 -1 0 24)" fill="url(#pattern0)"/>
                        </mask>
                        <g mask="url(#mask0)">
                            <rect width="24" height="24" transform="matrix(1 0 0 -1 0 24)" fill="#262424"/>
                        </g>
                        <defs>
                            <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                <use xlink:href="#image0" transform="scale(0.0104167)"/>
                            </pattern>
                            <image id="image0" width="96" height="96" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAF1ElEQVR4Xu2bTYgcVRDH/9U9PbOrBz/iyQ+8CpKjmKBgIAezJpKFFfGkIEYPgiQHCZ72ag5iCKh4VPwgi6K4gQRzED0I6kXw4MUcFERBD0KMZnpmuqQ/dt2dzExXv6mZ171Tc8lh61W99/91db9/vw7Bfl4VIK/VrTgMgOeLwAAYAM8KeC5vHWAAPCvgubx1gAHwrIDn8tYBBsCzAp7LWwcYAM8KeC5vHWAAPCvgubx1gAHwrIDn8tYBBsCzAp7LWwcYAM8KeC5vHWAAPCvgubx1gAHwrIDn8qIOYOZlcPwRKDhDFH3lec61Ls8cHwDzOqj9BBFdK5tsKQBmDsD9DTCvAeiC+WlqdTbKEi/i37nfXQXRBwCWQXwB1F4losEkLcoBDOLXAZzckSRN+BKF7TcXUeRxa+Z+/CwIbwNo7Yg5S2H7lDMAHsSp8CmAET8+g6D9ChHxooPgQe80wK+O0eEUhe2z4zQa2wHcv/44KPgEQDhB4HcQRCeIqLeIEJg5RNJL7wTPT1h/AuYnqdX5eFTMSADM8QNI8AWAm0uFJdoEtZ4ion9KY/dQADMvgfvvFc/GspVdR0CHiaKvhwPHAOjej4QuArinLHP2d8a3CKNjRPSHKL7hQcx8Gwa9z0B4WLYU/g0BPUbU/l4EINOUr92JJLoEYL+sCH5EMFghWv5ZGN/IMEddjhAt/yK+BW0FapJupNpDk2bWvzOUb0OZO+D++8J7XTrlvxDQKlH05V4Q/f+LsXcQCW8C2Cdal/DZWAogvx1lT/s3ALwgKp4btmeo1TkvjK912C6DJZupeHcoArB9FUze7w5PbU8YtjEGawKGav6oEoCsG0Y7vknXxTkE0cmmGTZmJiTxOkDrsoseThdcZQA5hO5xEH2YvfOQ/d5FED3XFMMmNFg7V+78jswJQP5ciA8gwQX5QwmXQdEaEV2VMfMTxcw3gePzYDomnEG66Tju+pbYGUAOQX9bJlz0TMKKbfcmCA/JCow3WLLxmP6/qToYkysIkkeJlq5IJzmPOOZ/70USpsbzPmG91HiONVjCHNMDKLapatZcOnHNuLyTg0sA3y3Kq/jqZapb0M7JcnXDdhUJrVEUXRYtekZBzL1HkPCnAG4VlRAaLFEuKNyChiA0yrDN0mB5AbBVtOSAYnhuDNDLFEavSSetEceD+EUA5wAEsnzVDJYsp3IH7OqGmhq2eRks7wCyh3PNDFthsN4CcEIokLPBEubX2QVNKlYXwzZvg1UbAPk2tbJh+w5hdFTrhI2Zby9OsOZmsGoFIIdQ+YQtNWxHiJZ+ki5mVJwvgyWds5oPkBR0OGH7HQGtjDpLldXzZ7Ak80tj5gog74TKJ2x/F4btc+misjq93iEEmcG6RTRO2WCJavoAUECoatji4pNI0QlbYbDS1+VLQiHEJ1jCfOKwuXfALq9Q7YRNZNjqYrCkBLwCyL3CyG8qJ81/5Alb3QxWYwBoGLY6GqxGAcifC24nbAAG4HgDTEeFi57qBEtYQxzm/Ra065nA3f1IgosA3yVaAeObLI7woCge9CuCZIWo84MsfvZRtQKQd0JlwyZVSeUES1pMGlc7AMU2teIJW8lyFU+wpMJK42oJoIBQ9ZPI0Wv2ZLAaD6CAUNWwDa/bm8HaEwC2FlHxhK0YNpsTLKmw0rja3oKGF1DBsDl9IigVTDuuMQCyW1L5CdvMT7AWGkD+XBhr2GplsKSgGtUB28+EGz6kmv4TQalg2nGNBJB3wvanhFEdP3WUgmosgGKbekf6LxH9KV1w3eIaDaBuYrrMxwC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY45j9Q7Gl/1FXl4gAAAABJRU5ErkJggg=="/>
                        </defs>
                    </svg>
                </div>
                <div id="card-collapse-1-2" class="faq-card__body collapse " aria-labelledby="card-num-1-2" data-parent="#faq-cards-1">
                    <div class="faq-body-container">
                        @lang('site.faq.item1_content')
                    </div>
                </div>
            </div>

            <div class="faq-card__item">
                <div class="faq-card__header" id="card-num-1-3" data-toggle="collapse" data-target="#card-collapse-1-3" aria-expanded="false" aria-controls="card-collapse-1-3">
                    <h4>{{ __('site.faq.item2') }}</h4>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="24" height="24">
                            <rect width="24" height="24" transform="matrix(1 0 0 -1 0 24)" fill="url(#pattern0)"/>
                        </mask>
                        <g mask="url(#mask0)">
                            <rect width="24" height="24" transform="matrix(1 0 0 -1 0 24)" fill="#262424"/>
                        </g>
                        <defs>
                            <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                <use xlink:href="#image0" transform="scale(0.0104167)"/>
                            </pattern>
                            <image id="image0" width="96" height="96" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAF1ElEQVR4Xu2bTYgcVRDH/9U9PbOrBz/iyQ+8CpKjmKBgIAezJpKFFfGkIEYPgiQHCZ72ag5iCKh4VPwgi6K4gQRzED0I6kXw4MUcFERBD0KMZnpmuqQ/dt2dzExXv6mZ171Tc8lh61W99/91db9/vw7Bfl4VIK/VrTgMgOeLwAAYAM8KeC5vHWAAPCvgubx1gAHwrIDn8tYBBsCzAp7LWwcYAM8KeC5vHWAAPCvgubx1gAHwrIDn8tYBBsCzAp7LWwcYAM8KeC5vHWAAPCvgubx1gAHwrIDn8qIOYOZlcPwRKDhDFH3lec61Ls8cHwDzOqj9BBFdK5tsKQBmDsD9DTCvAeiC+WlqdTbKEi/i37nfXQXRBwCWQXwB1F4losEkLcoBDOLXAZzckSRN+BKF7TcXUeRxa+Z+/CwIbwNo7Yg5S2H7lDMAHsSp8CmAET8+g6D9ChHxooPgQe80wK+O0eEUhe2z4zQa2wHcv/44KPgEQDhB4HcQRCeIqLeIEJg5RNJL7wTPT1h/AuYnqdX5eFTMSADM8QNI8AWAm0uFJdoEtZ4ion9KY/dQADMvgfvvFc/GspVdR0CHiaKvhwPHAOjej4QuArinLHP2d8a3CKNjRPSHKL7hQcx8Gwa9z0B4WLYU/g0BPUbU/l4EINOUr92JJLoEYL+sCH5EMFghWv5ZGN/IMEddjhAt/yK+BW0FapJupNpDk2bWvzOUb0OZO+D++8J7XTrlvxDQKlH05V4Q/f+LsXcQCW8C2Cdal/DZWAogvx1lT/s3ALwgKp4btmeo1TkvjK912C6DJZupeHcoArB9FUze7w5PbU8YtjEGawKGav6oEoCsG0Y7vknXxTkE0cmmGTZmJiTxOkDrsoseThdcZQA5hO5xEH2YvfOQ/d5FED3XFMMmNFg7V+78jswJQP5ciA8gwQX5QwmXQdEaEV2VMfMTxcw3gePzYDomnEG66Tju+pbYGUAOQX9bJlz0TMKKbfcmCA/JCow3WLLxmP6/qToYkysIkkeJlq5IJzmPOOZ/70USpsbzPmG91HiONVjCHNMDKLapatZcOnHNuLyTg0sA3y3Kq/jqZapb0M7JcnXDdhUJrVEUXRYtekZBzL1HkPCnAG4VlRAaLFEuKNyChiA0yrDN0mB5AbBVtOSAYnhuDNDLFEavSSetEceD+EUA5wAEsnzVDJYsp3IH7OqGmhq2eRks7wCyh3PNDFthsN4CcEIokLPBEubX2QVNKlYXwzZvg1UbAPk2tbJh+w5hdFTrhI2Zby9OsOZmsGoFIIdQ+YQtNWxHiJZ+ki5mVJwvgyWds5oPkBR0OGH7HQGtjDpLldXzZ7Ak80tj5gog74TKJ2x/F4btc+misjq93iEEmcG6RTRO2WCJavoAUECoatji4pNI0QlbYbDS1+VLQiHEJ1jCfOKwuXfALq9Q7YRNZNjqYrCkBLwCyL3CyG8qJ81/5Alb3QxWYwBoGLY6GqxGAcifC24nbAAG4HgDTEeFi57qBEtYQxzm/Ra065nA3f1IgosA3yVaAeObLI7woCge9CuCZIWo84MsfvZRtQKQd0JlwyZVSeUES1pMGlc7AMU2teIJW8lyFU+wpMJK42oJoIBQ9ZPI0Wv2ZLAaD6CAUNWwDa/bm8HaEwC2FlHxhK0YNpsTLKmw0rja3oKGF1DBsDl9IigVTDuuMQCyW1L5CdvMT7AWGkD+XBhr2GplsKSgGtUB28+EGz6kmv4TQalg2nGNBJB3wvanhFEdP3WUgmosgGKbekf6LxH9KV1w3eIaDaBuYrrMxwC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY45j9Q7Gl/1FXl4gAAAABJRU5ErkJggg=="/>
                        </defs>
                    </svg>
                </div>
                <div id="card-collapse-1-3" class="faq-card__body collapse " aria-labelledby="card-num-1-3" data-parent="#faq-cards-1">
                    <div class="faq-body-container">
                        @lang('site.faq.item1_content')
                    </div>
                </div>
            </div>

            <div class="faq-card__item">
                <div class="faq-card__header" id="card-num-1-4" data-toggle="collapse" data-target="#card-collapse-1-4" aria-expanded="false" aria-controls="card-collapse-1-4">
                    <h4>{{ __('site.faq.item2') }}</h4>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="24" height="24">
                            <rect width="24" height="24" transform="matrix(1 0 0 -1 0 24)" fill="url(#pattern0)"/>
                        </mask>
                        <g mask="url(#mask0)">
                            <rect width="24" height="24" transform="matrix(1 0 0 -1 0 24)" fill="#262424"/>
                        </g>
                        <defs>
                            <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                <use xlink:href="#image0" transform="scale(0.0104167)"/>
                            </pattern>
                            <image id="image0" width="96" height="96" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAF1ElEQVR4Xu2bTYgcVRDH/9U9PbOrBz/iyQ+8CpKjmKBgIAezJpKFFfGkIEYPgiQHCZ72ag5iCKh4VPwgi6K4gQRzED0I6kXw4MUcFERBD0KMZnpmuqQ/dt2dzExXv6mZ171Tc8lh61W99/91db9/vw7Bfl4VIK/VrTgMgOeLwAAYAM8KeC5vHWAAPCvgubx1gAHwrIDn8tYBBsCzAp7LWwcYAM8KeC5vHWAAPCvgubx1gAHwrIDn8tYBBsCzAp7LWwcYAM8KeC5vHWAAPCvgubx1gAHwrIDn8qIOYOZlcPwRKDhDFH3lec61Ls8cHwDzOqj9BBFdK5tsKQBmDsD9DTCvAeiC+WlqdTbKEi/i37nfXQXRBwCWQXwB1F4losEkLcoBDOLXAZzckSRN+BKF7TcXUeRxa+Z+/CwIbwNo7Yg5S2H7lDMAHsSp8CmAET8+g6D9ChHxooPgQe80wK+O0eEUhe2z4zQa2wHcv/44KPgEQDhB4HcQRCeIqLeIEJg5RNJL7wTPT1h/AuYnqdX5eFTMSADM8QNI8AWAm0uFJdoEtZ4ion9KY/dQADMvgfvvFc/GspVdR0CHiaKvhwPHAOjej4QuArinLHP2d8a3CKNjRPSHKL7hQcx8Gwa9z0B4WLYU/g0BPUbU/l4EINOUr92JJLoEYL+sCH5EMFghWv5ZGN/IMEddjhAt/yK+BW0FapJupNpDk2bWvzOUb0OZO+D++8J7XTrlvxDQKlH05V4Q/f+LsXcQCW8C2Cdal/DZWAogvx1lT/s3ALwgKp4btmeo1TkvjK912C6DJZupeHcoArB9FUze7w5PbU8YtjEGawKGav6oEoCsG0Y7vknXxTkE0cmmGTZmJiTxOkDrsoseThdcZQA5hO5xEH2YvfOQ/d5FED3XFMMmNFg7V+78jswJQP5ciA8gwQX5QwmXQdEaEV2VMfMTxcw3gePzYDomnEG66Tju+pbYGUAOQX9bJlz0TMKKbfcmCA/JCow3WLLxmP6/qToYkysIkkeJlq5IJzmPOOZ/70USpsbzPmG91HiONVjCHNMDKLapatZcOnHNuLyTg0sA3y3Kq/jqZapb0M7JcnXDdhUJrVEUXRYtekZBzL1HkPCnAG4VlRAaLFEuKNyChiA0yrDN0mB5AbBVtOSAYnhuDNDLFEavSSetEceD+EUA5wAEsnzVDJYsp3IH7OqGmhq2eRks7wCyh3PNDFthsN4CcEIokLPBEubX2QVNKlYXwzZvg1UbAPk2tbJh+w5hdFTrhI2Zby9OsOZmsGoFIIdQ+YQtNWxHiJZ+ki5mVJwvgyWds5oPkBR0OGH7HQGtjDpLldXzZ7Ak80tj5gog74TKJ2x/F4btc+misjq93iEEmcG6RTRO2WCJavoAUECoatji4pNI0QlbYbDS1+VLQiHEJ1jCfOKwuXfALq9Q7YRNZNjqYrCkBLwCyL3CyG8qJ81/5Alb3QxWYwBoGLY6GqxGAcifC24nbAAG4HgDTEeFi57qBEtYQxzm/Ra065nA3f1IgosA3yVaAeObLI7woCge9CuCZIWo84MsfvZRtQKQd0JlwyZVSeUES1pMGlc7AMU2teIJW8lyFU+wpMJK42oJoIBQ9ZPI0Wv2ZLAaD6CAUNWwDa/bm8HaEwC2FlHxhK0YNpsTLKmw0rja3oKGF1DBsDl9IigVTDuuMQCyW1L5CdvMT7AWGkD+XBhr2GplsKSgGtUB28+EGz6kmv4TQalg2nGNBJB3wvanhFEdP3WUgmosgGKbekf6LxH9KV1w3eIaDaBuYrrMxwC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY45j9Q7Gl/1FXl4gAAAABJRU5ErkJggg=="/>
                        </defs>
                    </svg>
                </div>
                <div id="card-collapse-1-4" class="faq-card__body collapse " aria-labelledby="card-num-1-4" data-parent="#faq-cards-1">
                    <div class="faq-body-container">
                        @lang('site.faq.item1_content')
                    </div>
                </div>
            </div>

            <div class="faq-card__item">
                <div class="faq-card__header" id="card-num-1-5" data-toggle="collapse" data-target="#card-collapse-1-5" aria-expanded="false" aria-controls="card-collapse-1-5">
                    <h4>{{ __('site.faq.item2') }}</h4>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="24" height="24">
                            <rect width="24" height="24" transform="matrix(1 0 0 -1 0 24)" fill="url(#pattern0)"/>
                        </mask>
                        <g mask="url(#mask0)">
                            <rect width="24" height="24" transform="matrix(1 0 0 -1 0 24)" fill="#262424"/>
                        </g>
                        <defs>
                            <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                <use xlink:href="#image0" transform="scale(0.0104167)"/>
                            </pattern>
                            <image id="image0" width="96" height="96" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAF1ElEQVR4Xu2bTYgcVRDH/9U9PbOrBz/iyQ+8CpKjmKBgIAezJpKFFfGkIEYPgiQHCZ72ag5iCKh4VPwgi6K4gQRzED0I6kXw4MUcFERBD0KMZnpmuqQ/dt2dzExXv6mZ171Tc8lh61W99/91db9/vw7Bfl4VIK/VrTgMgOeLwAAYAM8KeC5vHWAAPCvgubx1gAHwrIDn8tYBBsCzAp7LWwcYAM8KeC5vHWAAPCvgubx1gAHwrIDn8tYBBsCzAp7LWwcYAM8KeC5vHWAAPCvgubx1gAHwrIDn8qIOYOZlcPwRKDhDFH3lec61Ls8cHwDzOqj9BBFdK5tsKQBmDsD9DTCvAeiC+WlqdTbKEi/i37nfXQXRBwCWQXwB1F4losEkLcoBDOLXAZzckSRN+BKF7TcXUeRxa+Z+/CwIbwNo7Yg5S2H7lDMAHsSp8CmAET8+g6D9ChHxooPgQe80wK+O0eEUhe2z4zQa2wHcv/44KPgEQDhB4HcQRCeIqLeIEJg5RNJL7wTPT1h/AuYnqdX5eFTMSADM8QNI8AWAm0uFJdoEtZ4ion9KY/dQADMvgfvvFc/GspVdR0CHiaKvhwPHAOjej4QuArinLHP2d8a3CKNjRPSHKL7hQcx8Gwa9z0B4WLYU/g0BPUbU/l4EINOUr92JJLoEYL+sCH5EMFghWv5ZGN/IMEddjhAt/yK+BW0FapJupNpDk2bWvzOUb0OZO+D++8J7XTrlvxDQKlH05V4Q/f+LsXcQCW8C2Cdal/DZWAogvx1lT/s3ALwgKp4btmeo1TkvjK912C6DJZupeHcoArB9FUze7w5PbU8YtjEGawKGav6oEoCsG0Y7vknXxTkE0cmmGTZmJiTxOkDrsoseThdcZQA5hO5xEH2YvfOQ/d5FED3XFMMmNFg7V+78jswJQP5ciA8gwQX5QwmXQdEaEV2VMfMTxcw3gePzYDomnEG66Tju+pbYGUAOQX9bJlz0TMKKbfcmCA/JCow3WLLxmP6/qToYkysIkkeJlq5IJzmPOOZ/70USpsbzPmG91HiONVjCHNMDKLapatZcOnHNuLyTg0sA3y3Kq/jqZapb0M7JcnXDdhUJrVEUXRYtekZBzL1HkPCnAG4VlRAaLFEuKNyChiA0yrDN0mB5AbBVtOSAYnhuDNDLFEavSSetEceD+EUA5wAEsnzVDJYsp3IH7OqGmhq2eRks7wCyh3PNDFthsN4CcEIokLPBEubX2QVNKlYXwzZvg1UbAPk2tbJh+w5hdFTrhI2Zby9OsOZmsGoFIIdQ+YQtNWxHiJZ+ki5mVJwvgyWds5oPkBR0OGH7HQGtjDpLldXzZ7Ak80tj5gog74TKJ2x/F4btc+misjq93iEEmcG6RTRO2WCJavoAUECoatji4pNI0QlbYbDS1+VLQiHEJ1jCfOKwuXfALq9Q7YRNZNjqYrCkBLwCyL3CyG8qJ81/5Alb3QxWYwBoGLY6GqxGAcifC24nbAAG4HgDTEeFi57qBEtYQxzm/Ra065nA3f1IgosA3yVaAeObLI7woCge9CuCZIWo84MsfvZRtQKQd0JlwyZVSeUES1pMGlc7AMU2teIJW8lyFU+wpMJK42oJoIBQ9ZPI0Wv2ZLAaD6CAUNWwDa/bm8HaEwC2FlHxhK0YNpsTLKmw0rja3oKGF1DBsDl9IigVTDuuMQCyW1L5CdvMT7AWGkD+XBhr2GplsKSgGtUB28+EGz6kmv4TQalg2nGNBJB3wvanhFEdP3WUgmosgGKbekf6LxH9KV1w3eIaDaBuYrrMxwC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY45j9Q7Gl/1FXl4gAAAABJRU5ErkJggg=="/>
                        </defs>
                    </svg>
                </div>
                <div id="card-collapse-1-5" class="faq-card__body collapse " aria-labelledby="card-num-1-5" data-parent="#faq-cards-1">
                    <div class="faq-body-container">
                        @lang('site.faq.item1_content')
                    </div>
                </div>
            </div>

            <div class="faq-card__item">
                <div class="faq-card__header" id="card-num-1-6" data-toggle="collapse" data-target="#card-collapse-1-6" aria-expanded="false" aria-controls="card-collapse-1-6">
                    <h4>{{ __('site.faq.item2') }}</h4>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="24" height="24">
                            <rect width="24" height="24" transform="matrix(1 0 0 -1 0 24)" fill="url(#pattern0)"/>
                        </mask>
                        <g mask="url(#mask0)">
                            <rect width="24" height="24" transform="matrix(1 0 0 -1 0 24)" fill="#262424"/>
                        </g>
                        <defs>
                            <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                <use xlink:href="#image0" transform="scale(0.0104167)"/>
                            </pattern>
                            <image id="image0" width="96" height="96" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAF1ElEQVR4Xu2bTYgcVRDH/9U9PbOrBz/iyQ+8CpKjmKBgIAezJpKFFfGkIEYPgiQHCZ72ag5iCKh4VPwgi6K4gQRzED0I6kXw4MUcFERBD0KMZnpmuqQ/dt2dzExXv6mZ171Tc8lh61W99/91db9/vw7Bfl4VIK/VrTgMgOeLwAAYAM8KeC5vHWAAPCvgubx1gAHwrIDn8tYBBsCzAp7LWwcYAM8KeC5vHWAAPCvgubx1gAHwrIDn8tYBBsCzAp7LWwcYAM8KeC5vHWAAPCvgubx1gAHwrIDn8qIOYOZlcPwRKDhDFH3lec61Ls8cHwDzOqj9BBFdK5tsKQBmDsD9DTCvAeiC+WlqdTbKEi/i37nfXQXRBwCWQXwB1F4losEkLcoBDOLXAZzckSRN+BKF7TcXUeRxa+Z+/CwIbwNo7Yg5S2H7lDMAHsSp8CmAET8+g6D9ChHxooPgQe80wK+O0eEUhe2z4zQa2wHcv/44KPgEQDhB4HcQRCeIqLeIEJg5RNJL7wTPT1h/AuYnqdX5eFTMSADM8QNI8AWAm0uFJdoEtZ4ion9KY/dQADMvgfvvFc/GspVdR0CHiaKvhwPHAOjej4QuArinLHP2d8a3CKNjRPSHKL7hQcx8Gwa9z0B4WLYU/g0BPUbU/l4EINOUr92JJLoEYL+sCH5EMFghWv5ZGN/IMEddjhAt/yK+BW0FapJupNpDk2bWvzOUb0OZO+D++8J7XTrlvxDQKlH05V4Q/f+LsXcQCW8C2Cdal/DZWAogvx1lT/s3ALwgKp4btmeo1TkvjK912C6DJZupeHcoArB9FUze7w5PbU8YtjEGawKGav6oEoCsG0Y7vknXxTkE0cmmGTZmJiTxOkDrsoseThdcZQA5hO5xEH2YvfOQ/d5FED3XFMMmNFg7V+78jswJQP5ciA8gwQX5QwmXQdEaEV2VMfMTxcw3gePzYDomnEG66Tju+pbYGUAOQX9bJlz0TMKKbfcmCA/JCow3WLLxmP6/qToYkysIkkeJlq5IJzmPOOZ/70USpsbzPmG91HiONVjCHNMDKLapatZcOnHNuLyTg0sA3y3Kq/jqZapb0M7JcnXDdhUJrVEUXRYtekZBzL1HkPCnAG4VlRAaLFEuKNyChiA0yrDN0mB5AbBVtOSAYnhuDNDLFEavSSetEceD+EUA5wAEsnzVDJYsp3IH7OqGmhq2eRks7wCyh3PNDFthsN4CcEIokLPBEubX2QVNKlYXwzZvg1UbAPk2tbJh+w5hdFTrhI2Zby9OsOZmsGoFIIdQ+YQtNWxHiJZ+ki5mVJwvgyWds5oPkBR0OGH7HQGtjDpLldXzZ7Ak80tj5gog74TKJ2x/F4btc+misjq93iEEmcG6RTRO2WCJavoAUECoatji4pNI0QlbYbDS1+VLQiHEJ1jCfOKwuXfALq9Q7YRNZNjqYrCkBLwCyL3CyG8qJ81/5Alb3QxWYwBoGLY6GqxGAcifC24nbAAG4HgDTEeFi57qBEtYQxzm/Ra065nA3f1IgosA3yVaAeObLI7woCge9CuCZIWo84MsfvZRtQKQd0JlwyZVSeUES1pMGlc7AMU2teIJW8lyFU+wpMJK42oJoIBQ9ZPI0Wv2ZLAaD6CAUNWwDa/bm8HaEwC2FlHxhK0YNpsTLKmw0rja3oKGF1DBsDl9IigVTDuuMQCyW1L5CdvMT7AWGkD+XBhr2GplsKSgGtUB28+EGz6kmv4TQalg2nGNBJB3wvanhFEdP3WUgmosgGKbekf6LxH9KV1w3eIaDaBuYrrMxwC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY4xgAoiumSygC4qKY45j9Q7Gl/1FXl4gAAAABJRU5ErkJggg=="/>
                        </defs>
                    </svg>
                </div>
                <div id="card-collapse-1-6" class="faq-card__body collapse " aria-labelledby="card-num-1-6" data-parent="#faq-cards-1">
                    <div class="faq-body-container">
                        @lang('site.faq.item1_content')
                    </div>
                </div>
            </div>
        </div>

        <div class="faq-feedback-header mb-5">
            {{ __('site.faq.feedback_header') }}
        </div>
        <form action="{{ route('feedback', app()->getLocale()) }}" method="POST" class="w-100">
            @csrf
            <div class="row">
                <div class="col-12 col-md-6 mb-4">
                    <input name="name" class="faq-input" type="text" placeholder="{{ __('site.faq.placeholders.name') }}">
                </div>
                <div class="col-12 col-md-6 mb-4">
                    <input name="lastname" class="faq-input" type="text" placeholder="{{ __('site.faq.placeholders.lastname') }}">
                </div>
                <div class="col-12 col-md-6 mb-4">
                    <input name="place_of_living" class="faq-input" type="text" placeholder="{{ __('site.faq.placeholders.place_of_living') }}">
                </div>
                <div class="col-12 col-md-6 mb-4">
                    <input name="postal_address" class="faq-input" type="text" placeholder="{{ __('site.faq.placeholders.postal_address') }}">
                </div>
                <div class="col-12 col-md-6 mb-4">
                    <input name="contact_number" class="faq-input" type="tel" placeholder="{{ __('site.faq.placeholders.number') }}">
                </div>
                <div class="col-12 col-md-6 mb-4">
                    <input name="email" class="faq-input input-outlined" type="email" placeholder="{{ __('site.faq.placeholders.email') }}">
                    @error('email')
                    <div class="cabinet-validation-error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-12 mb-4">
                    <textarea name="message" class="faq-input faq-textarea input-outlined" rows="6" placeholder="{{ __('site.faq.placeholders.textarea') }}"></textarea>
                    @error('message')
                    <div class="cabinet-validation-error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-12 text-center mb-3">
                    <button type="submit" class="request-form-submit">{{ __('site.faq.submit') }}</button>
                </div>
            </div>
        </form>
    </div>
</section>
@include('site.modals.notification-component')
