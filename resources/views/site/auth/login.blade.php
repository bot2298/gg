<section class="auth-header-info background-fluid footer-to-bottom mb-3">
    <div class="container">
        <div class="auth-header-caption text-center mb-5">
            @lang('auth.login_header')
        </div>

        <div class="row">
            <form method="POST" action="{{ route('login', app()->getLocale()) }}" class="login-form form-inline w-100">
                @csrf
                <div class="offset-lg-2 col-12 col-sm-6 col-md-5 col-lg-3 mb-2 mb-sm-0 text-center text-sm-right">
                    <input tabindex="1" name="email" class="login-input-email @error('email') auth-is-invalid @enderror" type="email" placeholder="Email" value="{{ old('email') }}">
                    <div class="text-center text-sm-left mt-1">
                        <a href="{{ route('password.request', app()->getLocale()) }}" class="forgot-password">@lang('auth.forgot_password')</a>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-5 col-lg-3 text-center text-sm-left mb-auto">
                    <div class="login-input-password-container d-inline-block position-relative">
                        <input tabindex="2" name="password" class="login-input-password @error('email') auth-is-invalid @enderror" type="password" placeholder="@lang('forms.placeholder_password')" required autocomplete="current-password">
                        <span class="password-toggle"></span>
                    </div>
                </div>
                <div class="col-12 col-md-2 col-lg-2 mt-4 mt-md-0 text-center text-md-left mb-auto">
                    <button class="login-button" type="submit">
                        @lang('auth.login_action')
                    </button>
                </div>
                @if ($errors->any())
                    <div class="col-12 text-center mt-2">
                        <div style="font-size: 18px;" class="cabinet-validation-error">{{ $errors->first() }}</div>
                    </div>
                @endif
            </form>
        </div>
        @if(session('blocked'))
            <div style="color: #E74C3C;" class="text-center">{{ session('blocked') }}</div>
        @endisset
        <div class="mt-3 mt-md-0 text-center">
            <a href="{{ route('register', app()->getLocale()) }}" class="register-small-button mt-3">
                @lang('auth.login_register')
            </a>
        </div>
        <div class="row mt-3 mt-md-5">
            <div class="col-12 col-sm-6 text-center text-sm-right mb-3 mb-sm-0">
                <a href="{{ route('login.google') }}" class="login-social-link login-google">
                    @lang('auth.login_google')
                </a>
            </div>
            <div class="col-12 col-sm-6 text-center text-sm-left">
                <a href="{{ route('login.facebook') }}" class="login-social-link login-facebook">
                    @lang('auth.login_facebook')
                </a>
            </div>
        </div>
    </div>
</section>
@include('site.modals.notification-component')
