<section class="auth-header-info background-fluid footer-to-bottom mb-3">
    <div class="container">
        <div class="auth-header-caption text-center mb-5">
            @lang('auth.register_header')
        </div>
        <div class="row">
            <form method="POST" action="{{ route('register', app()->getLocale()) }}" class="login-form form-inline w-100">
                @csrf
                <div class="col-12 col-md-6 col-xl-3 mb-4 mb-xl-0 text-center">
                    <input name="email" class="login-input-email @error('email') auth-is-invalid @enderror" type="email" placeholder="Email" required autocomplete="email" autofocus value="{{ old('email') }}">
                </div>
                <div class="col-12 col-md-6 col-xl-3 mb-4 mb-xl-0 text-center">
                    <div class="login-input-password-container d-inline-block position-relative">
                        <input name="password" class="login-input-password @error('password') auth-is-invalid @enderror" type="password" placeholder="@lang('forms.placeholder_password')" required autocomplete="new-password">
                        <span class="password-toggle"></span>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-3 mb-4 mb-xl-0 text-center">
                    <div class="login-input-password-container d-inline-block position-relative">
                        <input name="password_confirmation" class="login-input-password @error('password') auth-is-invalid @enderror" type="password" placeholder="@lang('forms.placeholder_password_confirmation')" required autocomplete="new-password">
                        <span class="password-toggle"></span>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-3 mb-4 mb-xl-0 text-center">
                    <button class="login-button" type="submit">
                        @lang('auth.login_register')
                    </button>
                </div>
                <div class="col-12 text-center mt-3">
                    <span style="font-size: 18px; color: #FFF">{{ __('site.password_8_chars') }}</span>
                </div>

                @if ($errors->any())
                    <div class="col-12 text-center mb-2">
                        @foreach ($errors->all() as $error)
                        <div style="font-size: 18px;" class="cabinet-validation-error">{{ $error }}</div>
                        @endforeach
                    </div>
                @endif
            </form>
        </div>
        <div class="row mt-4">
            <div class="col-12 col-sm-6 text-center text-sm-right mb-3 mb-sm-0">
                <a href="{{ route('login.google') }}" class="login-social-link login-google">
                    @lang('auth.login_google')
                </a>
            </div>
            <div class="col-12 col-sm-6 text-center text-sm-left">
                <a href="{{ route('login.facebook') }}" class="login-social-link login-facebook">
                    @lang('auth.login_facebook')
                </a>
            </div>
        </div>
    </div>
</section>
