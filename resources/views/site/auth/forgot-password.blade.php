<section class="auth-header-info background-fluid footer-to-bottom mb-3">
    <div class="container">
        <div class="auth-header-caption text-center mb-5">
            @lang('auth.restore_password')
        </div>
        <div class="row">
            <form method="POST" action="{{ route('password.email', app()->getLocale()) }}" class="login-form form-inline w-100">
                @csrf
                <div class="col-12 col-sm-6 mb-4 mb-sm-0 text-center text-sm-right">
                    <input name="email" class="login-input-email @error('email') auth-is-invalid @enderror" type="email" placeholder="Email" value="{{ old('email') }}">
                </div>
                <div class="col-12 col-sm-6 mt-md-0 text-center text-md-left mb-auto">
                    <button class="password-request-button" type="submit">
                        @lang('auth.restore_password_action')
                    </button>
                </div>
                <div class="col-12 text-center mt-2">
                    <div style="font-size: 18px;" class="cabinet-validation-error">{{ $errors->first() }}</div>
                </div>
                @if (session('status'))
                    <div class="col-12 text-center mt-4">
                        <div style="color: #EC9C23;">
                            {{ session('status') }}
                        </div>
                    </div>
                @endif
            </form>
        </div>
    </div>
</section>
