<section class="auth-header-info background-fluid footer-to-bottom mb-3">
    <div class="container">
        <div class="auth-header-caption text-center mb-5">
            @lang('auth.change_password')
        </div>
        <div class="row">
            <form method="POST" action="{{ route('password.update', app()->getLocale()) }}" class="login-form form-inline w-100 justify-content-center">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="d-none col-12 col-sm-6 col-xl-3 mb-4 mb-xl-0 text-center text-sm-right text-xl-center">
                    <input name="email" class="login-input-email @error('email') auth-is-invalid @enderror" value="{{ $email ?? old('email') }}" type="email" placeholder="Email">
                </div>
                <div class="col-12 col-sm-6 col-lg-4 col-xl-3 mb-4 mb-xl-0 text-center text-sm-left text-xl-center">
                    <div class="login-input-password-container d-inline-block position-relative">
                        <input name="password" class="login-input-password @error('password') auth-is-invalid @enderror" type="password" placeholder="@lang('forms.placeholder_newpassword')" required autocomplete="new-password">
                        <span class="password-toggle"></span>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-4 col-xl-3 mb-4 mb-xl-0 text-center text-sm-right text-xl-center">
                    <div class="login-input-password-container d-inline-block position-relative">
                        <input name="password_confirmation" class="login-input-password @error('password') auth-is-invalid @enderror" type="password" placeholder="@lang('forms.placeholder_newpassword_confirmation')" required autocomplete="new-password">
                        <span class="password-toggle"></span>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-4 col-xl-3 mb-4 mb-xl-0 text-center text-sm-left text-xl-center">
                    <button class="password-request-button" type="submit">
                        @lang('auth.restore_password_action_final')
                    </button>
                </div>
                @if ($errors->any())
                    <div class="col-12 text-center mt-2">
                        <div style="font-size: 18px;" class="cabinet-validation-error">{{ $errors->first() }}</div>
                    </div>
                @endif
                @if (session('status'))
                    <div class="col-12 text-center mt-4">
                        <div style="color: #EC9C23;">
                            {{ session('status') }}
                        </div>
                    </div>
                @endif
            </form>
        </div>
    </div>
</section>
