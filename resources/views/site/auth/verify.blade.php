<section class="auth-header-info background-fluid footer-to-bottom mb-3">
    <div class="container">
        <div class="auth-header-caption text-center mb-5">
            @lang('auth.email_confirm_header')
        </div>
        <div style="font-weight: 600;" class="text-center mb-5">
            @lang('auth.email_confirm_info', ['email' => auth()->user() ? auth()->user()->email : ''])
        </div>
        <div class="text-center">
            {{ __('site.request.change_email') }}
        </div>
        <div class="text-center mb-5">
            <form id="register-clean-form" action="{{ route('register.clean') }}" method="POST" style="display: none;">
                @csrf
            </form>
            <a onclick="document.getElementById('register-clean-form').submit()" class="request-change-email" href="javascript:;">{{ __('site.request.change_email_action') }}</a>
        </div>
        <div class="col-12 col-xl-8 offset-0 offset-xl-2 text-center mb-3">
            {{ __('auth.email_check_spam') }}
        </div>
        <div class="text-center mb-3">
            @lang('auth.email_confirm_resend', ['link' => route('verification.resend', app()->getLocale())])
        </div>
        @if (session('resent'))
            <div class="text-center">
                <div style="color: #EC9C23;">
                    @lang('auth.email_confirm_resent')
                </div>
            </div>
        @endif
    </div>
    <script>
        window.addEventListener('load', (event) => {
            sendRequest();
            function sendRequest(){
                $.ajax({
                    url: "{{ route('verify.check') }}",
                    type: "POST",
                    success:
                        function(data){
                            if(data.success === 1){
                                window.location.href = data.url;
                            }
                            setTimeout(sendRequest, 5000);
                        },
                });
            };
        });
    </script>
</section>
