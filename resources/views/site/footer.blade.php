<footer class="footer-container" data-html2canvas-ignore="true">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-6 col-xl-2 order-4 order-sm-3 order-xl-1 text-center footer-desktop__qr">
                <img class="mx-auto"  src="{{ asset('img/qr.svg') }}" alt="">
                Googllaw © 2020
            </div>
            <div class="col-xl-8 order-1 order-xl-2 my-auto text-center">
                <div class="row justify-content-center">
                    <div class="col-sm-6 col-lg-3 py-2 my-auto mobile-hidden">
                        <a class="footer-link" href="{{ route('about', app()->getLocale()) }}">@lang('layout.about')</a>
                    </div>
                    <div class="col-sm-6 col-lg-3 py-2 my-auto">
                        <a class="footer-link" href="{{ route('request', app()->getLocale()) }}">@lang('layout.request')</a>
                    </div>
                    <div class="col-sm-6 col-lg-3 py-2 my-auto">
                        <a class="footer-link" href="{{ route('policy', app()->getLocale()) }}">@lang('layout.policy')</a>
                    </div>
                    <div class="col-sm-6 col-lg-3 py-2 my-auto">
                        <a class="footer-link" href="{{ route('tracker.application', app()->getLocale()) }}">@lang('layout.offer')</a>
                    </div>
                    <div class="col-sm-6 col-lg-3 py-2 my-auto">
                        <a class="footer-link" href="{{ route('dossier', app()->getLocale()) }}">@lang('layout.dossier')</a>
                    </div>
                    <div class="col-sm-6 col-lg-3 py-2 my-auto">
                        <a class="footer-link" href="{{ route('tracker.application', app()->getLocale()) }}">@lang('layout.tracker')</a>
                    </div>
                    <div class="col-sm-6 col-lg-3 py-2 my-auto">
                        <a class="footer-link" href="{{ route('faq', app()->getLocale()) }}">@lang('layout.faq')</a>
                    </div>
                    <div class="col-sm-6 col-lg-3 py-2 my-auto mobile-hidden">
                        <a class="footer-cabinet-link outlined{{ auth()->check() ?' active':'' }}" href="{{ route('login', app()->getLocale()) }}">@lang('layout.cabinet')</a>
                    </div>
                </div>
                <div class="row">
                    <div class="footer-block col-12 col-sm-3 my-auto">
                        <div></div>
                        <div></div>
                    </div>
                    <div class="footer-block col-12 col-sm-3 my-auto">
                        <div></div>
                        <div></div>
                    </div>
                    <div class="footer-block col-12 col-sm-3 my-auto">
                        <div></div>
                        <div></div>
                    </div>
                    <div class="footer-block col-12 col-sm-3 my-auto">
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-2 order-3 order-sm-4 mb-3 m-sm-auto text-center flex-row-reverse flex-lg-column d-flex">
                <div class="col-6 col-xl-2 text-center footer-mobile__qr flex-column">
                    <img class="mx-auto"  src="{{ asset('img/qr.svg') }}" alt="">
                    Googllaw © 2020
                </div>
                <div class="d-flex flex-column col-6">
                    <div>@lang('layout.share')</div>
                    <div class="mt-2 d-flex">
                        <a target="_blank" href="#"><img class="footer-social-img mr-2" src="{{ asset('img/footer/facebook.svg') }}" alt="Facebook"></a>
                        <a target="_blank" href="#"><img class="footer-social-img mr-2" src="{{ asset('img/footer/linkedin.svg') }}" alt="Linkedin"></a>
                        <a target="_blank" href="#"><img class="footer-social-img" src="{{ asset('img/footer/telegram.svg') }}" alt="Telegram"></a>
                    </div>
                    <div class="mt-2 d-flex">
                        <a target="_blank" href="#"><img class="footer-social-img mr-2" src="{{ asset('img/footer/twitter.svg') }}" alt="Twitter"></a>
                        <a target="_blank" href="#"><img class="footer-social-img mr-2" src="{{ asset('img/footer/viber.svg') }}" alt="Viber"></a>
                        <a target="_blank" href="https://instagram.com/googllawyer"><img class="footer-social-img" src="{{ asset('img/footer/instagram.svg') }}" alt="Instagram"></a>
                    </div>
                </div>

            </div>
        </div>
        <style>
            .footer-mobile__qr{
                display: none;
            }

            .footer-mobile__qr img,
            .footer-desktop__qr img{
                width: 150px;
                display: block;
            }

            @media screen and (max-width: 992px){
                .footer-mobile__qr{
                    display: flex;
                }

                .footer-desktop__qr{
                    display: none;
                }
                .footer-mobile__qr img{
                    width: 85px;
                }
                .mobile-hidden{
                    display: none;
                }
            }
            @media screen and (max-width: 992px){
                .footer-mobile__qr img{
                    width: 110px;
                }
            }

        <style>
    </div>
</footer>
