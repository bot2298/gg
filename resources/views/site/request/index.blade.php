<section class="header-info-container dossier-header-info background-fluid mb-3 need-footer-separator">
    <div class="container">
        <div class="row mb-3">
            <div class="dossier-header-caption-container col-12 col-md-4 text-center text-md-left" style="height: 200px; display: flex; align-items: center">
                <span style="font-size: 44px; display: block; width: 350px; line-height: 1.1;" class="dossier-header-caption">{{ __('site.request.main_caption') }}</span>
            </div>
            <div class="col-12 col-md-8">
                <div class="row h-100">
                    <div class="col-12 col-lg-4 mb-3 mb-lg-0 text-center text-lg-left">
                        <img style="width: 100px;" class="dossier-header-img mb-4 mr-3" src="{{ asset('img/dossier/01.png') }}" alt="">
                        <span class="dossier-header-step">{{ __('site.request.caption1') }}</span>
                    </div>
                    <div class="col-12 col-lg-4 mt-auto mb-3 mb-lg-0 text-center text-lg-left">
                        <img style="width: 100px;" class="dossier-header-img mb-4 mr-3" src="{{ asset('img/dossier/02.png') }}" alt="">
                        <span class="dossier-header-step">{{ __('site.request.caption2') }}</span>
                    </div>
                    <div class="col-12 col-lg-4 mb-3 mb-lg-0 text-center text-lg-left">
                        <img style="width: 100px;" class="dossier-header-img mb-4 mr-3" src="{{ asset('img/dossier/03.png') }}" alt="">
                        <span class="dossier-header-step">{{ __('site.request.caption3') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    {{ Breadcrumbs::render('request') }}
</div>
<div class="request-form need-footer-separator">
    <div class="container">
        <div class="row">
            {{-- header --}}
            <div class="col-12 request-page-heading text-center mb-5 mt-3">
                @lang('site.request.header')
            </div>
            <form class="row" action="{{ $application->exists ? route('request.update', app()->getLocale()) : route ('request.create', app()->getLocale()) }}" method="POST" enctype="multipart/form-data">
                @csrf
                {{-- country --}}
{{--                @component('site.blocks.request.field')--}}
{{--                    @slot('name', __('forms.lawyer_country'))--}}
{{--                    @slot('required', 1)--}}
{{--                @endcomponent--}}
                <div class="d-none col-lg-6 col-12 mb-5">
                    <select data-minimum-results-for-search="Infinity" id="country-select" data-locale="{{app()->getLocale()}}" class="request-form-select custom-select w-100 input-outlined @error('country_id') is-invalid @enderror" name="country_id" data-placeholder="@lang('forms.placeholder_choose')" data-old="{{ old('country_id', ($application->region->country->id ?? '')) }}">
                        <option></option>
                        @foreach ($countries as $country)
                            <option @if($loop->first) selected @endif value="{{ $country->id }}">{{ __('countries.'.$country->name) }}</option>
                        @endforeach
                    </select>
                    @error('country_id')
                    <span class="cabinet-validation-error">{{ $message }}</span>
                    @enderror
                </div>
                {{-- region --}}
                @component('site.blocks.request.field')
                    @slot('name', __('forms.lawyer_region'))
                    @slot('required', 1)
                @endcomponent
                <div class="col-lg-6 col-12 mb-5">
                    <select id="region-select" data-locale="{{app()->getLocale()}}" class="request-form-select w-100 custom-select input-outlined @error('region_id') is-invalid @enderror" name="region_id" data-placeholder="@lang('forms.placeholder_choose')" data-old="{{ old('region_id', $application->region_id) }}" disabled>
                        <option></option>
                    </select>
                    @error('region_id')
                    <span class="cabinet-validation-error">{{ $message }}</span>
                    @enderror
                </div>
                {{-- city --}}
                @component('site.blocks.request.field')
                    @slot('name', __('forms.lawyer_city'))
                @endcomponent
                <div class="col-lg-6 col-12 mb-5">
                    <select id="city-select" data-locale="{{app()->getLocale()}}" class="request-form-select custom-select w-100 @error('city') is-invalid @enderror" name="city_id" data-placeholder="@lang('forms.placeholder_enter')" data-old="{{ old('city_id', $application->city_id) }}" disabled>
                        <option></option>
                    </select>
                    @error('city_id')
                    <span class="cabinet-validation-error">{{ $message }}</span>
                    @enderror
                </div>
                {{-- spec --}}
                @component('site.blocks.request.field')
                    @slot('name', __('forms.lawyer_specialization'))
                @endcomponent
                <div class="col-lg-6 col-12 mb-5">
                    <select class="request-form-select custom-select w-100" name="specialization_id" data-placeholder="@lang('forms.placeholder_choose')">
                        <option></option>
                        @foreach ($specializations as $specialization)
                            <option value="{{ $specialization->id }}" {{ old('specialization_id', $application->specialization_id) == $specialization->id?'selected':null }}>{{ __('specializations.'.$specialization->name) }}</option>
                        @endforeach
                    </select>
                    @error('specialization_id')
                    <span class="cabinet-validation-error">{{ $message }}</span>
                    @enderror
                </div>
                {{-- plot --}}
                @component('site.blocks.request.field')
                    @slot('name', __('site.request.content'))
                    @slot('required', 1)
                @endcomponent
                <div class="col-lg-6 col-12 mb-5">
                    <div class="textarea-container input-outlined @error('content') is-invalid @enderror">
                        <textarea data-max="5000" class="request-form-textarea w-100" name="content" id="content" placeholder="{{ __('forms.placeholder_enter_text') }}">{{ old('content', $application->content) }}</textarea>
                        <div class="textarea-helpers">
                            @lang('site.request.chars_left')
                        </div>
                        <div class="textarea-attach">
                            <label for="file-upload" class="custom-file-upload mb-0">
                                <img class="w-100" src="{{ asset('img/request/attach.png') }}" alt="@lang('site.request.attach_alt')">
                            </label>
                            <input class="d-none @error('attachment') is-invalid @enderror" name="attachment" id="file-upload" type="file" accept="application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/*,application/pdf"/>
                        </div>
                    </div>
                    <div id="attachment-file-name">
                        @if($application->attachment)
                            <input name="old_file" type="hidden" value="{{ $application->attachment }}">
                            <a class="cabinet-request-attachment" target="_blank" href="{{ Storage::url($application->attachment) }}">{{ __('site.request.attachment_download') }}</a>
                        @endif
                    </div>
                    @error('content')
                    <span class="cabinet-validation-error">{{ $message }}</span>
                    @enderror
                    @error('attachment')
                    <span class="cabinet-validation-error">{{ $message }}</span>
                    @enderror
                </div>
                {{-- cost --}}
                @component('site.blocks.request.field')
                    @slot('name', __('site.request.cost'))
                @endcomponent
                <div class="col-lg-6 col-12 mb-5">
                    <div class="double-input @error('cost') is-invalid @enderror">
                        <input style="vertical-align: bottom;" class="request-form-text" name="cost" autocomplete="off" type="number" min="0" oninput="validity.valid||(value='');" placeholder="@lang('forms.placeholder_enter')" value="{{ old('cost', $application->cost) }}"><!--
                    --><select style="padding-left: 10px; width: 65px;" class="request-form-select @error('currency_id') is-invalid @enderror" name="currency_id">
                            @foreach ($currencies as $currency)
                                <option value="{{ $currency->id }}" {{ old('currency_id', $application->currency ? $application->currency->id : null ) == $currency->id?'selected':null }}>{{ $currency->getName() }}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('cost')
                    <span class="cabinet-validation-error">{{ $message }}</span>
                    @enderror
                    @error('currency_id')
                    <span class="cabinet-validation-error">{{ $message }}</span>
                    @enderror
                </div>
                {{-- dedline --}}
                @component('site.blocks.request.field')
                    @slot('name', __('site.request.deadline'))
                @endcomponent
                <div class="col-lg-6 col-12 mb-5">
                    <select data-minimum-results-for-search="Infinity" data-dropdown-css-class="show-all-deadlines" class="request-form-select custom-select w-100 @error('deadline_type') is-invalid @enderror" name="deadline_type" data-placeholder="@lang('forms.placeholder_choose')"><<!--
                -->
                        <option></option>
                        <option value="{{ \App\Models\Application::DEADLINE_1_HOUR }}" {{ old('deadline_type', $application->deadline_type) == \App\Models\Application::DEADLINE_1_HOUR?'selected':null }}>@lang('site.request.deadline1hour')</option>
                        <option value="{{ \App\Models\Application::DEADLINE_3_HOURS }}" {{ old('deadline_type', $application->deadline_type) == \App\Models\Application::DEADLINE_3_HOURS?'selected':null }}>@lang('site.request.deadline3hours')</option>
                        <option value="{{ \App\Models\Application::DEADLINE_12_HOURS }}" {{ old('deadline_type', $application->deadline_type) == \App\Models\Application::DEADLINE_12_HOURS?'selected':null }}>@lang('site.request.deadline12hours')</option>
                        <option value="{{ \App\Models\Application::DEADLINE_24_HOURS }}" {{ old('deadline_type', $application->deadline_type) == \App\Models\Application::DEADLINE_24_HOURS?'selected':null }}>@lang('site.request.deadline24hours')</option>
                        <option value="{{ \App\Models\Application::DEADLINE_3_DAYS }}" {{ old('deadline_type', $application->deadline_type) == \App\Models\Application::DEADLINE_3_DAYS?'selected':null }}>@lang('site.request.deadline3days')</option>
                        <option value="{{ \App\Models\Application::DEADLINE_7_DAYS }}" {{ old('deadline_type', $application->deadline_type) == \App\Models\Application::DEADLINE_7_DAYS?'selected':null }}>@lang('site.request.deadline7days')</option>
                        <option value="{{ \App\Models\Application::DEADLINE_30_DAYS }}" {{ old('deadline_type', $application->deadline_type) == \App\Models\Application::DEADLINE_30_DAYS?'selected':null }}>@lang('site.request.deadline30days')</option>
                    </select>
                    @error('deadline_type')
                    <span class="cabinet-validation-error">{{ $message }}</span>
                    @enderror
                </div>
                {{-- login --}}
                @component('site.blocks.request.field')
                    @slot('name', __('site.request.name'))
                    @slot('required', 1)
                @endcomponent
                @component('site.blocks.request.text-input')
                    @slot('type', 'text')
                    @slot('name', 'name')
                    @slot('placeholder', __('forms.placeholder_enter'))
                    @slot('style', ' w-100 input-outlined')
                    @slot('old', $application->name)
                @endcomponent
                {{-- email --}}
                @component('site.blocks.request.field')
                    @slot('name', __('site.request.email'))
                    @slot('required', 1)
                @endcomponent
                @component('site.blocks.request.text-input')
                    @slot('type', 'email')
                    @slot('name', 'email')
                    @slot('placeholder', __('forms.placeholder_enter'))
                    @slot('style', ' w-100 input-outlined')
                    @slot('allowChange', $allowEmailChange)
                    @slot('old', $application->email)
                @endcomponent
                {{-- phone --}}
                @component('site.blocks.request.field')
                    @slot('name', __('site.request.phone'))
                @endcomponent
                <div class="col-lg-6 col-12">
{{--                    <div class="double-input">--}}
{{--                        <input style="vertical-align: bottom;" id="inputPhone" class="request-form-text" name="phone" type="phone" value="{{ old('phone', $application->phone) }}"><!----}}
{{--                    --><select style="padding-left: 10px; width: 75px" class="request-form-select" name="phone_locale">--}}
{{--                            <option value="1">UA</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
                    <div>
                        <input name="formatted-phone" type="tel" id="intl-phone" value="{{ old('phone', $application->phone) }}">
                    </div>
                    <div class="request-phone-caption mt-2 @isset($update) mb-3 @endisset">@lang('site.request.callme')</div>
                    @error('phone')
                    <span class="cabinet-validation-error">{{ $message }}</span>
                    @enderror
                    @error('phone_locale')
                    <span class="cabinet-validation-error">{{ $message }}</span>
                    @enderror
                </div>
                @unless(isset($update))
                <div class="col-12 mb-5 mt-5 request-info">
                    <img src="{{ asset('img/request/notice.png') }}" alt="">
                    <span>@lang('site.request.info')</span>
                </div>
                <div class="col-12 request-agreement mb-5">
                    <label class="checkbox-container">
                        @lang('site.request.agreement', ['link' => route('policy', app()->getLocale())])
                        <input class="@error('agreement') is-invalid @enderror" type="checkbox" name="agreement">
                        <span class="checkmark"></span>
                    </label>
                    <span></span>
                    @error('agreement')
                    <span class="cabinet-validation-error">{{ $message }}</span>
                    @enderror
                </div>
                @endunless
                @if(isset($needVerification) && $needVerification)
                    <input name="need_verification" type="hidden" value="1">
                @endif
                <div class="col-12 text-center">
                    <button type="submit" class="request-form-submit">
                        @if(isset($update))
                            {{ __('site.request.update') }}
                        @else
                            @lang('site.request.send')
                        @endif
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    window.addEventListener('load', function() {
        let invalid = $('.is-invalid');
        if (invalid.length){
            $('html, body').animate({
                scrollTop: $('.is-invalid').offset().top
            }, 800);
        }
    })
</script>
@include('site.modals.notification-component')
