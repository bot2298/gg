<section class="auth-header-info background-fluid footer-to-bottom mb-3">
    <div class="container">
        <div class="auth-header-caption text-center mb-5">
            {{ __('site.request.verify_header') }}
        </div>
        <div style="font-weight: 600;" class="text-center mb-5">
            {{ __('site.request.verify_caption') }}
            <a class="site-link" href="mailto:{{ session('email') }}">{{ session('email') }}</a>.
        </div>
        <div class="text-center">
            {{ __('site.request.change_email') }}
        </div>
        <div class="text-center mb-5">
            <a class="request-change-email" href="{{ route('request.change-email', app()->getLocale()) }}">{{ __('site.request.change_email_action') }}</a>
        </div>
        <div class="col-12 col-xl-8 offset-0 offset-xl-2 text-center">
            {{ __('site.request.check_spam') }}
        </div>
    </div>
</section>
<script>
    window.addEventListener('load', (event) => {
        sendRequest();
        function sendRequest(){
            $.ajax({
                url: "{{ route('application.verify.check') }}",
                type: "POST",
                success:
                    function(data){
                        if(data.success === 1){
                            window.location.href = data.url;
                        }
                        setTimeout(sendRequest, 5000);
                    },
            });
        };
    });
</script>
