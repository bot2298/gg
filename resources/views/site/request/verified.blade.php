<section class="auth-header-info background-fluid footer-to-bottom mb-3">
    <div class="container">
        <div class="request-verified-header text-center mb-3">{{ __('site.request.your_code') }}</div>
        <div class="request-verified-code text-center mb-3">{{ session('code') }}</div>
        <div style="font-size: 24px;" class="request-verified-info text-center mb-2">
            {{ __('site.tracker.info') }}
        </div>
        <div class="row">
            <div class="col-12 text-center mb-3">
                <span style="font-size: 20px;" class="request-code-send">{{ __('site.request.send_email') }}</span>
            </div>
            <div class="col-6 text-right mb-3">
                <form id="send-code" action="{{ route('request.send-code', app()->getLocale()) }}" method="POST" style="display: none;">
                    @csrf
                    <input name="code" type="hidden" value="{{ session('code') }}">
                </form>
                <button onclick="event.preventDefault(); document.getElementById('send-code').submit();" class="request-code-button">{{ __('site.request.yes') }}</button>
            </div>
            <div class="col-6 text-left mb-3">
                <button class="request-code-button" onclick="window.location = '/{{ app()->getLocale() }}/tracker/request'">{{ __('site.request.no') }}</button>
            </div>
        </div>
    </div>
</section>
