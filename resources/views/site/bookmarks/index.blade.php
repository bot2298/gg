<div class="container">
    {{ Breadcrumbs::render('bookmarks') }}
</div>
<section class="dossier-list-container need-footer-separator footer-to-bottom">
    <div style="align-self: start;" class="container" id="app">
        <div class="row justify-content-center">
            @if($dossiers && $dossiers->isNotEmpty())
                @foreach($dossiers as $dossier)
                    @component('site.bookmarks.item')
                        @slot('img', $dossier->general->photo)
                        @slot('name', $dossier->general->fullname)
                        @slot('starsCount', $dossier->rating)
                        @slot('viewsCount', $dossier->seen_counter)
                        @slot('address', $dossier->general->office['address'])
                        @slot('id', $dossier->id)
                    @endcomponent
                @endforeach
{{--                <div class="col-12 text-center">--}}
{{--                    <a href="#" class="dossier-more">--}}
{{--                        {{ __('site.dossier.show_more') }}--}}
{{--                    </a>--}}
{{--                </div>--}}
            @else
                <div style="font-size: 24px;" class="col-12 text-center about-page-heading mb-3">{{ __('cabinet.request.bookmarks_empty') }}</div>
            @endif
        </div>
    </div>
</section>
