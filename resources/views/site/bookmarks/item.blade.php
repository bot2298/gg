<div class="col-12 col-md-6 col-lg-4 col-xl-3 mb-4 text-center pr-0">
    <div class="dossier-item mx-auto">
        <div class="dossier-single-img-wrapper">
            <img class="dossier-item-img" src="{{ Storage::url($img) }}" alt="">
        </div>
        <div class="dossier-item-name">{{ $name }}</div>
        <div class="dossier-item-stats">
            <div class="dossier-item-stats-a">
                {{ $starsCount }}
            </div>
            <div class="dossier-item-stats-b">
                {{ $viewsCount }}
            </div>
        </div>
        <div class="dossier-item-address">
            <div>{{ $address[0] }}</div>
        </div>
        <a href="{{ route('dossier.single', ['locale' => app()->getLocale(), 'dossier' => $id]) }}" class="dossier-item-link">
            {{ __('cabinet.form_index.view') }}
        </a>
        <dossiers-bookmark :data="{{ json_encode(['id' => $id, 'in_bookmarks' => session()->has('bookmark') && in_array($id, session('bookmark'))]) }}"></dossiers-bookmark>
    </div>
</div>
