<section id="index-header" class="header-info-container background-fluid">
    <div class="container">
        <div class="row">
            <div class="order-2 order-lg-1 col-lg-7 header-info-caption my-auto text-center">
                <div class="text-center text-md-center">
                    @lang('site.home.caption')
                    <p>@lang('site.home.caption_more')</p>
                </div>
                <div>
                    <a class="scroll-link" id="scroll" href="#scrollto">
                        <svg width="68" height="170" viewBox="0 0 68 170" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path class="scroll-arrow1" d="M13.4117 82.3665L6.7088 76.3391L1.51777e-07 82.3718L6.70291 88.3992L13.4117 82.3665Z" fill="#EC9C23"/>
                            <path class="scroll-arrow1" d="M67.1457 82.3453L60.4428 76.3179L53.734 82.3506L60.4369 88.378L67.1457 82.3453Z" fill="#EC9C23"/>
                            <path class="scroll-arrow1" d="M26.823 94.4555L20.1201 88.4282L13.4113 94.4608L20.1142 100.488L26.823 94.4555Z" fill="#EC9C23"/>
                            <path class="scroll-arrow1" d="M53.7227 94.4449L47.0198 88.4175L40.311 94.4502L47.0139 100.478L53.7227 94.4449Z" fill="#EC9C23"/>
                            <path class="scroll-arrow1" d="M40.3953 106.754L33.6924 100.727L26.9836 106.76L33.6865 112.787L40.3953 106.754Z" fill="#EC9C23"/>

                            <path class="scroll-arrow2" d="M13.6344 136.159L6.93146 130.132L0.222656 136.165L6.92557 142.192L13.6344 136.159Z" fill="#EC9C23"/>
                            <path class="scroll-arrow2" d="M67.3684 136.138L60.6655 130.111L53.9567 136.143L60.6596 142.171L67.3684 136.138Z" fill="#EC9C23"/>
                            <path class="scroll-arrow2" d="M27.0457 148.248L20.3428 142.221L13.634 148.254L20.3369 154.281L27.0457 148.248Z" fill="#EC9C23"/>
                            <path class="scroll-arrow2" d="M53.9454 148.238L47.2425 142.21L40.5337 148.243L47.2366 154.27L53.9454 148.238Z" fill="#EC9C23"/>
                            <path class="scroll-arrow2" d="M40.4895 160.337L33.7866 154.31L27.0778 160.343L33.7807 166.37L40.4895 160.337Z" fill="#EC9C23"/>

                            <path class="scroll-arrow3" d="M13.6344 33.8086L6.93146 27.7812L0.222656 33.8139L6.92557 39.8412L13.6344 33.8086Z" fill="#EC9C23"/>
                            <path class="scroll-arrow3" d="M67.3683 33.7874L60.6654 27.76L53.9567 33.7927L60.6596 39.8201L67.3683 33.7874Z" fill="#EC9C23"/>
                            <path class="scroll-arrow3" d="M27.0457 45.8976L20.3428 39.8702L13.634 45.9029L20.3369 51.9303L27.0457 45.8976Z" fill="#EC9C23"/>
                            <path class="scroll-arrow3" d="M53.9454 45.887L47.2425 39.8597L40.5337 45.8923L47.2366 51.9196L53.9454 45.887Z" fill="#EC9C23"/>
                            <path class="scroll-arrow3" d="M40.4895 57.9866L33.7866 51.9592L27.0778 57.9919L33.7807 64.0193L40.4895 57.9866Z" fill="#EC9C23"/>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="order-1 order-lg-2 col-lg-5 text-center my-auto">
                <img class="w-100" src="{{ asset('img/people.png') }}" alt="">
            </div>
        </div>
    </div>
</section>
<section class="about-container">
    <div id="scrollto" class="container about-box">
        <div class="row">
            <div class="d-none col-md-2 d-md-block text-center">
                <svg class="about-rect first" width="147" height="206" viewBox="0 0 147 206" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect x="130.846" y="49.8847" width="74" height="73" transform="rotate(80.3214 130.846 49.8847)" stroke="#EC9C23" stroke-width="5"/>
                    <rect x="75.0236" y="2.5" width="72.5236" height="72.5236" transform="rotate(90 75.0236 2.5)" stroke="#EC9C23" stroke-width="5"/>
                    <rect x="14.0818" y="132.046" width="74" height="73" transform="rotate(-15.6516 14.0818 132.046)" stroke="#EC9C23" stroke-width="5"/>
                </svg>
            </div>
            <div class="col-12 col-md-8">
                <div class="about-text">
                    @lang('site.home.about')
                </div>
            </div>
            <div class="d-none col-md-2 d-md-block text-center">
                <svg class="about-rect second" width="102" height="232" viewBox="0 0 102 232" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect x="35.5487" y="3.45638" width="59.1572" height="59.1572" transform="rotate(32.8534 35.5487 3.45638)" stroke="#EC9C23" stroke-width="5"/>
                    <rect x="17.2196" y="113.695" width="59.1572" height="59.1572" transform="rotate(-31.4811 17.2196 113.695)" stroke="#EC9C23" stroke-width="5"/>
                    <rect x="39.5" y="170.5" width="59" height="59" stroke="#EC9C23" stroke-width="5"/>
                </svg>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 about-block mb-4 mb-lg-0">
                <div data-href="{{ route('request', app()->getLocale()) }}" class="about-helper">
                    <p>@lang('site.home.need_help')</p>
                    <a href="{{ route('request', app()->getLocale()) }}" class="about-action">
                        <span>@lang('site.home.need_help_action')</span>
                        <img src="{{ asset('img/arrow-right.png') }}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-6 about-block">
                <div data-href="{{ route('dossier', app()->getLocale()) }}" class="about-helper">
                    <p>@lang('site.home.no_need_help')</p>
                    <a href="{{ route('dossier', app()->getLocale()) }}" class="about-action">
                        <span>@lang('site.home.no_need_help_action')</span>
                        <img src="{{ asset('img/arrow-right.png') }}" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

{{--<section class="stats-container">--}}
{{--    <div class="stats-sent container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-4">--}}
{{--                <span>@lang('site.home.sent')</span>--}}
{{--            </div>--}}
{{--            <div class="col-6 rects my-auto">--}}
{{--                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg"> <rect width="25" height="25" fill="#EC9C23"/> </svg>--}}
{{--                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg"> <rect width="25" height="25" fill="#EC9C23"/> </svg>--}}
{{--                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg"> <rect width="25" height="25" fill="#EC9C23"/> </svg>--}}
{{--                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg"> <rect width="25" height="25" fill="#EC9C23"/> </svg>--}}
{{--            </div>--}}
{{--            <div class="col-2 count my-auto text-right">--}}
{{--                500--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="stats-online container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-4">--}}
{{--                <span>@lang('site.home.lawyers_count')</span>--}}
{{--            </div>--}}
{{--            <div class="col-6 rects my-auto text-right">--}}
{{--                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg"> <rect width="25" height="25" fill="#EC9C23"/> </svg>--}}
{{--                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg"> <rect width="25" height="25" fill="#EC9C23"/> </svg>--}}
{{--                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg"> <rect width="25" height="25" fill="#EC9C23"/> </svg>--}}
{{--                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg"> <rect width="25" height="25" fill="#EC9C23"/> </svg>--}}
{{--            </div>--}}
{{--            <div class="col-2 count my-auto text-right">--}}
{{--                1000--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
@include('site.modals.notification-component')
