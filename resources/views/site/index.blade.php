@extends('layouts.site')
@if(isset($title))
@section('title')
    {{$title}}
@endsection
@endif
@section('header')
    @include('site.header')
@endsection
@section('content')
    @include($location)
@endsection
@section('footer')
    @include('site.footer')
@endsection
@section('scripts')
    @isset($js)
        @foreach($js as $script)
            <script src="{{ asset($script) }}"></script>
        @endforeach
    @endisset
@endsection
