<div class="container">
    {{ Breadcrumbs::render('policy') }}
</div>

<div class="policy-container need-footer-separator">
    <div style="color: black" class="container">
        {{-- header --}}
        <div class="col-12 request-page-heading text-center mb-5 mt-3">
            @lang('site.policy.heading')
        </div>
        <div class="policy-block">
            <h2 class="policy-block-header">{{ __('site.policy.content1.header') }}</h2>
            <div class="policy-block-subheader">{{ __('site.policy.content1.subcontent1.header') }}</div>
            <p>{{ __('site.policy.content1.subcontent1.content_p1') }}</p>
            <p>{{ __('site.policy.content1.subcontent1.content_p2') }}</p>
            <p>{{ __('site.policy.content1.subcontent1.content_p3') }}</p>
            <div class="policy-block-subheader">{{ __('site.policy.content1.subcontent2.header') }}</div>
            <p>{{ __('site.policy.content1.subcontent2.content_p1') }}</p>
            <div class="policy-block-subheader">{{ __('site.policy.content1.subcontent3.header') }}</div>
            <p>{{ __('site.policy.content1.subcontent3.content_p1') }}</p>
            <div class="policy-block-subheader">{{ __('site.policy.content1.subcontent4.header') }}</div>
            <p>{{ __('site.policy.content1.subcontent4.content_p1') }}</p>
            <div class="policy-block-subheader">{{ __('site.policy.content1.subcontent5.header') }}</div>
            <p>{{ __('site.policy.content1.subcontent5.content_p1') }}</p>
            <div class="policy-block-subheader">{{ __('site.policy.content1.subcontent6.header') }}</div>
            <p>{{ __('site.policy.content1.subcontent6.content_p1') }}</p>
            <div>{{ __('site.policy.content1.subcontent6.list.header') }}</div>
            <ul class="policy-block-list">
                <li>{{ __('site.policy.content1.subcontent6.list.item1') }}</li>
                <li>{{ __('site.policy.content1.subcontent6.list.item2') }}</li>
                <li>{{ __('site.policy.content1.subcontent6.list.item3') }}</li>
                <li>{{ __('site.policy.content1.subcontent6.list.item4') }}</li>
                <li>{{ __('site.policy.content1.subcontent6.list.item5') }}</li>
                <li>{{ __('site.policy.content1.subcontent6.list.item6') }}</li>
                <li>{{ __('site.policy.content1.subcontent6.list.item7') }}</li>
            </ul>
            <p>{{ __('site.policy.content1.subcontent7.p') }}</p>
        </div>
        <div class="policy-block">
            <h2 class="policy-block-header">{{ __('site.policy.content2.header') }}</h2>
            <div>{{ __('site.policy.content2.p1') }}</div>
            <div class="policy-block-subheader">{{ __('site.policy.content2.subcontent1.header') }}</div>
            <p>{{ __('site.policy.content2.subcontent1.p1') }}</p>
            <div class="policy-block-subheader">{{ __('site.policy.content2.subcontent2.header') }}</div>
            <p>{{ __('site.policy.content2.subcontent2.p1') }}</p>
            <div class="policy-block-subheader">{{ __('site.policy.content2.subcontent3.header') }}</div>
            <p>{{ __('site.policy.content2.subcontent3.p1') }}</p>
        </div>
        <div class="policy-block">
            <h2 class="policy-block-header">{{ __('site.policy.content3.header') }}</h2>
            <div class="policy-block-subheader">{{ __('site.policy.content3.subcontent1.header') }}</div>
            <p>{{ __('site.policy.content3.subcontent1.p1') }}</p>
            <p>{{ __('site.policy.content3.subcontent1.p2') }}</p>
            <p>{{ __('site.policy.content3.subcontent1.p3') }}</p>
            <div class="policy-block-subheader">{{ __('site.policy.content3.subcontent2.header') }}</div>
            <p>{{ __('site.policy.content3.subcontent2.p1') }}</p>
            <div class="policy-block-subheader">{{ __('site.policy.content3.subcontent3.header') }}</div>
            <p>{{ __('site.policy.content3.subcontent3.p1') }}</p>
            <div>{{ __('site.policy.content3.subcontent3.list.header') }}</div>
            <ul class="policy-block-list">
                <li>{{ __('site.policy.content3.subcontent3.list.item1') }}</li>
                <li>{{ __('site.policy.content3.subcontent3.list.item2') }}</li>
                <li>{{ __('site.policy.content3.subcontent3.list.item3') }}</li>
            </ul>
        </div>
        <div class="policy-block">
            <h2 class="policy-block-header">{{ __('site.policy.content4.header') }}</h2>
            <p>{{ __('site.policy.content4.p1') }}</p>
            <p>{{ __('site.policy.content4.p2') }}</p>
            <p>{{ __('site.policy.content4.p3') }}</p>
        </div>
        <div class="policy-block">
            <h2 class="policy-block-header">{{ __('site.policy.content5.header') }}</h2>
            <div>{{ __('site.policy.content5.list.header') }}</div>
            <ul class="policy-block-list">
                <li>{{ __('site.policy.content5.list.item2') }}</li>
                <li>{{ __('site.policy.content5.list.item2') }}</li>
                <li>{{ __('site.policy.content5.list.item3') }}</li>
                <li>{{ __('site.policy.content5.list.item4') }}</li>
            </ul>
            <p>{{ __('site.policy.content5.p1') }}</p>
            <p>{{ __('site.policy.content5.p2') }}</p>
        </div>
        <div class="policy-block">
            <h2 class="policy-block-header">{{ __('site.policy.content6.header') }}</h2>
            <p>{{ __('site.policy.content6.p1') }}</p>
            <p>{{ __('site.policy.content6.p2') }}</p>
        </div>
        <div class="policy-block">
            <h2 class="policy-block-header">{{ __('site.policy.content7.header') }}</h2>
            <p>{{ __('site.policy.content7.p1') }}</p>
        </div>
        <div class="policy-block">
            <h2 class="policy-block-header">{{ __('site.policy.content8.header') }}</h2>
            <p>{{ __('site.policy.content8.p1') }}</p>
        </div>
        <div class="policy-block">
            <h2 class="policy-block-header">{{ __('site.policy.content9.header') }}</h2>
            <p>{{ __('site.policy.content9.p1') }}</p>
        </div>
    </div>
</div>
