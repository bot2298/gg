<section class="header-info-container about-header-info background-fluid mb-3">
    <div class="container">
        <div class="about-header-caption">@lang('site.about.header_goal')</div>
    </div>
</section>
<div class="container">
    {{ Breadcrumbs::render('about') }}
</div>
<section class="about-page">
    <div class="about-page-block">
        <div class="container">
            <h2 class="about-page-heading text-center mb-3">
                @lang('site.about.goal')
            </h2>
            <div class="row about-2cols-steps">
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/01.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto">
                    @lang('site.about.goal1')
                </div>
                <div class="col-6 about-2cols-step text-right my-auto">
                    @lang('site.about.goal2')
                </div>
                <div class="col-6 about-2cols-step">
                    <img src="{{ asset('img/about/02.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/03.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto">
                    @lang('site.about.goal3')
                </div>
                <div class="col-6 about-2cols-step text-right my-auto">
                    @lang('site.about.goal4')
                </div>
                <div class="col-6 about-2cols-step">
                    <img src="{{ asset('img/about/04.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/05.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto">
                    @lang('site.about.goal5')
                </div>
                <div class="about-2cols-line"></div>
            </div>
        </div>
    </div>
    <div class="about-page-block about-page-background">
        <div class="container">
            <h2 class="about-page-heading heading-white text-center mb-5">
                @lang('site.about.sections')
            </h2>
            <div class="row mb-4">
                <div class="col-12 col-sm-6 col-lg-3 text-center text-sm-right text-lg-center text-xl-left mb-3 mb-lg-0">
                    <img class="mr-3" src="{{ asset('img/about/01.png') }}" alt="">
                    <div class="feature-step about-page-text about-text-white">
                        @lang('site.about.section1')
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3 text-center text-sm-left text-lg-center mb-3 mb-lg-0">
                    <img class="mr-3" src="{{ asset('img/about/02.png') }}" alt="">
                    <div class="feature-step about-page-text about-text-white">
                        @lang('site.about.section2')
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3 text-center text-sm-right text-lg-center mb-3 mb-lg-0">
                    <img class="mr-3" src="{{ asset('img/about/03.png') }}" alt="">
                    <div class="feature-step about-page-text about-text-white">
                        @lang('site.about.section3')
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3 text-center text-sm-left text-lg-center text-xl-right mb-3 mb-lg-0">
                    <img class="mr-3" src="{{ asset('img/about/04.png') }}" alt="">
                    <div class="feature-step about-page-text about-text-white">
                        @lang('site.about.section4')
                    </div>
                </div>
            </div>
            <div class="about-page-text about-text-white">
                <img class="feature-step-img" src="{{ asset('img/request/notice.png') }}" alt="">
                @lang('site.about.sections_caption')
            </div>
        </div>
    </div>
    <div class="about-page-block">
        <div class="container">
            <h2 class="about-page-heading text-center mb-5">
                @lang('site.about.dossier')
            </h2>
            <div class="about-page-text mb-3">
                @lang('site.about.dossier_about')
            </div>
            <div class="about-page-text text-center">
                @lang('site.about.dossier_list')
            </div>
            <div class="row about-2cols-steps">
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/01.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto">
                    @lang('site.about.dossier_list1')
                </div>
                <div class="col-6 about-2cols-step text-right my-auto">
                    @lang('site.about.dossier_list2')
                </div>
                <div class="col-6 about-2cols-step">
                    <img src="{{ asset('img/about/02.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/03.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto">
                    @lang('site.about.dossier_list3')
                </div>
                <div class="col-6 about-2cols-step text-right my-auto">
                    @lang('site.about.dossier_list4')
                </div>
                <div class="col-6 about-2cols-step">
                    <img src="{{ asset('img/about/04.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/05.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto">
                    @lang('site.about.dossier_list5')
                </div>
                <div class="about-2cols-line"></div>
            </div>
            <ul class="about-page-ul">
                <li>
                    <span class="about-page-text">@lang('site.about.dossier_marker1')</span>
                </li>
                <li>
                    <span class="about-page-text">@lang('site.about.dossier_marker2')</span>
                </li>
                <li>
                    <span class="about-page-text">@lang('site.about.dossier_marker3')</span>
                </li>
            </ul>
        </div>
    </div>
    <div class="about-page-block about-page-background">
        <div class="container">
            <h2 class="about-page-heading heading-white text-center mb-5">
                @lang('site.about.request')
            </h2>
            <ul class="about-page-ul">
                <li>
                    <span class="about-page-text about-text-white">@lang('site.about.request_marker1')</span>
                </li>
                <li>
                    <span class="about-page-text about-text-white">@lang('site.about.request_marker2')</span>
                </li>
                <li>
                    <span class="about-page-text about-text-white">@lang('site.about.request_marker3')</span>
                </li>
                <li>
                    <span class="about-page-text about-text-white">@lang('site.about.request_marker4')</span>
                </li>
                <li>
                    <span class="about-page-text about-text-white">@lang('site.about.request_marker5')</span>
                </li>
                <li>
                    <span class="about-page-text about-text-white">@lang('site.about.request_marker6')</span>
                </li>
                <li>
                    <span class="about-page-text about-text-white">@lang('site.about.request_marker7')</span>
                </li>
                <li>
                    <span class="about-page-text about-text-white">@lang('site.about.request_marker8')</span>
                </li>
            </ul>
        </div>
    </div>
    <div class="about-page-block">
        <div class="container">
            <h2 class="about-page-heading text-center mb-5">
                @lang('site.about.tracker')
            </h2>
            <div class="about-page-text text-center mb-3">
                @lang('site.about.tracker_about')
            </div>
            <div class="about-page-text text-center">
                @lang('site.about.tracker_list')
            </div>
            <div class="row about-2cols-steps">
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/01.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto">
                    @lang('site.about.tracker_list1')
                </div>
                <div class="col-6 about-2cols-step text-right my-auto">
                    @lang('site.about.tracker_list2')
                </div>
                <div class="col-6 about-2cols-step">
                    <img src="{{ asset('img/about/02.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/03.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto">
                    @lang('site.about.tracker_list3')
                </div>
                <div class="col-6 about-2cols-step text-right my-auto">
                    @lang('site.about.tracker_list4')
                </div>
                <div class="col-6 about-2cols-step">
                    <img src="{{ asset('img/about/04.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/05.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto">
                    @lang('site.about.tracker_list5')
                </div>
                <div class="about-2cols-line"></div>
            </div>
            <ul class="about-page-ul">
                <li>
                    <span class="about-page-text">@lang('site.about.tracker_marker1')</span>
                </li>
                <li>
                    <span class="about-page-text">@lang('site.about.tracker_marker2')</span>
                </li>
                <li>
                    <span class="about-page-text">@lang('site.about.tracker_marker3')</span>
                </li>
            </ul>
        </div>
    </div>
    <div class="about-page-block about-page-background">
        <div class="container">
            <h2 class="about-page-heading heading-white text-center mb-5">
                @lang('site.about.cabinet')
            </h2>
            <div class="about-page-text about-text-white mb-3">
                @lang('site.about.cabinet_about')
            </div>
            <div class="about-page-text about-text-white text-center mb-3">
                @lang('site.about.cabinet_list')
            </div>
            <div class="row about-2cols-steps">
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/01.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto about-text-white">
                    @lang('site.about.cabinet_list1')
                </div>
                <div class="col-6 about-2cols-step text-right my-auto about-text-white">
                    @lang('site.about.cabinet_list2')
                </div>
                <div class="col-6 about-2cols-step">
                    <img src="{{ asset('img/about/02.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/03.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto about-text-white">
                    @lang('site.about.cabinet_list3')
                </div>
                <div class="col-6 about-2cols-step text-right my-auto about-text-white">
                    @lang('site.about.cabinet_list4')
                </div>
                <div class="col-6 about-2cols-step">
                    <img src="{{ asset('img/about/04.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/05.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto about-text-white">
                    @lang('site.about.cabinet_list5')
                </div>
                <div class="about-2cols-line"></div>
            </div>
            {{-- style="padding-left: 60px" --}}
            <div >
                <div style="font-weight: 600">@lang('site.about.cabinet_services')</div>
                <ul class="about-page-ul">
                    <li>
                        <span class="about-page-text about-text-white">@lang('site.about.cabinet_service1')</span>
                    </li>
                    <li>
                        <span class="about-page-text about-text-white">@lang('site.about.cabinet_service2')</span>
                    </li>
                </ul>
            </div>
            <ul class="about-page-ul">
                <li>
                    <span class="about-page-text about-text-white">@lang('site.about.cabinet_marker1')</span>
                </li>
                <li>
                    <span class="about-page-text about-text-white">@lang('site.about.cabinet_marker2')</span>
                </li>
                <li>
                    <span class="about-page-text about-text-white">@lang('site.about.cabinet_marker3')</span>
                </li>
                <li>
                    <span class="about-page-text about-text-white">@lang('site.about.cabinet_marker4')</span>
                </li>
                <li>
                    <span class="about-page-text about-text-white">@lang('site.about.cabinet_marker5')</span>
                </li>
                <li>
                    <span class="about-page-text about-text-white">@lang('site.about.cabinet_marker6')</span>
                </li>
                <li>
                    <span class="about-page-text about-text-white">@lang('site.about.cabinet_marker7')</span>
                </li>
            </ul>
        </div>
    </div>
    <div class="about-page-block">
        <div class="container">
            <h2 class="about-page-heading text-center mb-5">
                @lang('site.about.self')
            </h2>
            <div class="row mb-3">
                <div class="col-12 col-md-4 mb-3 mb-md-0 text-center">
                    <img class="feature-step-img mr-3" src="{{ asset('img/about/01.png') }}" alt="">
                    <div style="width: 200px;" class="feature-step about-step-text">
                        @lang('site.about.self_step1')
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-3 mb-md-0 text-center">
                    <img class="feature-step-img mr-3" src="{{ asset('img/about/02.png') }}" alt="">
                    <div style="width: 200px;" class="feature-step about-step-text">
                        @lang('site.about.self_step2')
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-3 mb-md-0 text-center">
                    <img class="feature-step-img mr-3" src="{{ asset('img/about/03.png') }}" alt="">
                    <div style="width: 200px;" class="feature-step about-step-text">
                        @lang('site.about.self_step3')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about-page-block about-page-background">
        <div class="container">
            <h2 class="about-page-heading heading-white text-center mb-3">
                @lang('site.about.help')
            </h2>
            <div class="row about-2cols-steps">
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/01.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto about-text-white">
                    @lang('site.about.help_step1')
                </div>
                <div class="col-6 about-2cols-step text-right my-auto about-text-white">
                    @lang('site.about.help_step2')
                </div>
                <div class="col-6 about-2cols-step">
                    <img src="{{ asset('img/about/02.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/03.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto about-text-white">
                    @lang('site.about.help_step3')
                </div>
                <div class="about-2cols-line"></div>
            </div>
        </div>
    </div>
    <div class="about-page-block">
        <div class="container">
            <h2 class="about-page-heading text-center mb-3">
                @lang('site.about.lawyer')
            </h2>
            <div class="row about-2cols-steps">
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/01.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto">
                    @lang('site.about.lawyer_step1')
                </div>
                <div class="col-6 about-2cols-step text-right my-auto">
                    @lang('site.about.lawyer_step2')
                </div>
                <div class="col-6 about-2cols-step">
                    <img src="{{ asset('img/about/02.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step text-right">
                    <img src="{{ asset('img/about/03.png') }}" alt="">
                </div>
                <div class="col-6 about-2cols-step my-auto">
                    @lang('site.about.lawyer_step3')
                </div>
                <div class="about-2cols-line"></div>
            </div>
        </div>
    </div>
    <div class="about-page-block about-page-background">
        <div class="container">
            <div class="mb-4 text-center">
                <span class="about-page-text about-text-white">@lang('site.about.footer1')</span>
            </div>
            <div class="mb-4 text-center">
                <span class="about-page-text about-text-white">@lang('site.about.footer2')</span>
            </div>
            <div class="text-center">
                <span class="about-page-text about-text-white">@lang('site.about.footer3')</span>
            </div>
        </div>
    </div>
</section>
