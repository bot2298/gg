<div class="container" data-html2canvas-ignore="true">
    {{ Breadcrumbs::render('dossier-single') }}
</div>
<div class="dossier-single-item need-footer-separator">
    <div class="container" id="app">
        <div class="row justify-content-center justify-content-lg-between mb-3" data-html2canvas-ignore="true">
            @php
                $all_dossiers = $dossier->user->dossiers()->where('status', \App\Models\Dossier::STATUS_ACTIVE)->get();
            @endphp
            <div
                class="text-left pl-5 mb-3 mb-lg-0 {{ $all_dossiers->count() > 1 ? ' col-12 col-lg-4': 'col-12 col-md-6' }}">
                <a class="dossier-navigation row display-aling-center"
                   href="{{ url()->current() == url()->previous() ? route('dossier', app()->getLocale()) : url()->previous() }}">
                    <img style="transform: rotate(180deg);height: 37px;" src="/img/tracker/next.png" alt="">
                    <span class="dossier-single-back pl-1">{{ __('site.dossier_single.back') }}</span>
                </a>
            </div>
            <div
                class="col-12 col-md-6 col-lg-4 my-auto{{ $all_dossiers->count() > 1 ? ' text-center' : ' text-center text-md-right' }}">
                <div data-id="{{$dossier->id}}"
                     class="dossier-single-bookmark {{ session()->exists('bookmark') && in_array($dossier->id, session('bookmark')) ? 'in-bookmarks' : null }}">
                    <a class="bookmark-add dossier-navigation" href="javascript:;">
                        {{ __('site.dossier_single.to_bookmark') }}
                    </a>
                    <a class="bookmark-remove dossier-navigation" href="javascript:;">
                        {{ __('site.dossier_single.from_bookmark') }}
                    </a>
                </div>

            </div>
            @if($all_dossiers->count() > 1)
                <div class="col-12 col-md-6 col-lg-4 my-auto text-center text-md-right">
                    <div class="dropdown">
                        <a href="javascript:;" class="dossier-navigation" id="dropdownMenuLink" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <span class="dossier-single-lang">{{ __('cabinet.form.dossier_lang') }}</span>
                        </a>
                        <div style="min-width: unset" class="dropdown-menu dropdown-menu-right"
                             aria-labelledby="dropdownMenuLink">
                            @foreach ($all_dossiers as $one_dossier)
                                <a class="dossier-lang-option dropdown-item{{ $dossier->locale == $one_dossier->locale ? ' active': null}}"
                                   href="{{ route('dossier.single', ['locale' => app()->getLocale(), 'dossier' => $one_dossier]) }}">{{ config('app.locales')[$one_dossier->locale] }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif

            @if(isset($dossier->overview['motto']))
                <div
                    class="col-12 dossier-single-motto{{ $all_dossiers->count() > 1 ? ' col-sm-4 offset-sm-8 text-right': ' text-center text-lg-right col-lg-4 offset-lg-8 ' }}">
                    <div class="mb-1 mt-5">"{{ $dossier->overview['motto'] }}"</div>
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-xl-4 mt-5 mt-xl-0 col-12 mb-5 text-left dossier-single-info">
                <div class="row">
                    <div class="col-md-6 col-xl-12 col-12">
                        <div class="d-block d-md-none tracker-response-location text-center mb-4">
                            <div class="dossier-single-lawyer-name">{{ $dossier->general->fullname}}</div>
                        </div>
                        <img class="mb-3 w-100" src="{{ Storage::url($dossier->general->photo) }}" alt="">
                        <div class="d-none d-md-block tracker-response-location text-center mb-1">
                            <div class="dossier-single-lawyer-name">{{ $dossier->general->fullname}}</div>
                        </div>
                        @if($dossier->ratingCount)
                            <div class="tracker-response-stats text-center mb-1">
                                <img src="/img/dossier/star.png" alt="">
                                {{ $dossier->rating }}
                                {{--                                <img src="/img/tracker/quote.png" alt="">--}}
                                {{--                                {{ $dossier->ratingCount }}--}}
                            </div>
                        @endif
                        @if($dossier->overview)
                            <div class="dossier-single-self-container-lg d-md-block d-xl-none d-none">
                                @if(isset($dossier->overview['languages']))
                                    <div
                                        class="single-info-header">{{ __('cabinet.form.overview_languages', [], $dossier->locale) }}</div>
                                    <div class="law-text-gray mb-3">{{ $dossier->overview['languages'] }}</div>
                                @endif
                                @if(isset($dossier->overview['strong_sides']))
                                    <div
                                        class="single-info-header">{{ __('cabinet.form.overview_strong_sides', [], $dossier->locale) }}</div>
                                    <div class="mb-3">{{ $dossier->overview['strong_sides'] }}</div>
                                @endif
                                @if(isset($dossier->overview['hobbies']))
                                    <div
                                        class="single-info-header">{{ __('cabinet.form.overview_hobbies', [], $dossier->locale) }}</div>
                                    <div class="mb-3">{{ $dossier->overview['hobbies'] }}</div>
                                @endif
                                @if(isset($dossier->overview['motto']))
                                    <div
                                        class="single-info-header">{{ __('cabinet.form.overview_motto', [], $dossier->locale) }}</div>
                                    <div class="mb-3">{{ $dossier->overview['motto']  }}</div>
                                @endif
                            </div>
                        @endif
                        {{--                        <div class="d-block d-sm-none">--}}
                        {{--                            @if(isset($dossier->qr))--}}
                        {{--                                <div class="text-center mt-5">--}}
                        {{--                                    <img style="width: 70%" src="{{ Storage::url($dossier->qr) }}" alt="">--}}
                        {{--                                </div>--}}
                        {{--                            @endif--}}
                        {{--                            <div class="row dossier-single-actions text-center mt-5">--}}
                        {{--                                <div class="col-6"><a onclick="exportPdf()" href="javascript:;">{{ __('site.dossier_single.download') }}</a></div>--}}
                        {{--                                <div class="col-6"><a href="javascript:;" onclick="$('#lawyer-report-modal').modal('show'); $('#modal-lawyer-id').val({{ $dossier->user_id }})">{{ __('site.dossier_single.report') }}</a></div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>
                    <div class="col-md-6 col-xl-12 col-12 text-left pr-0">
                        <div
                            class="single-info-header">{{ __('cabinet.form.office_address', [], $dossier->locale) }}</div>
                        <div
                            class="law-text-gray">@lang( 'countries.' . \App\Models\Dossier::getCountryName($dossier->general->office['country']),[], $dossier->locale)</div>
                        <div
                            class="law-text-gray">@lang( 'regions.' . \App\Models\Dossier::getRegionName($dossier->general->office['region']), [], $dossier->locale)</div>
                        @if(isset($dossier->general->office['city']) && $dossier->general->office['city'] != null)
                            <div
                                class="law-text-gray">@lang( 'cities.' . \App\Models\Dossier::getCityName($dossier->general->office['city']), [], $dossier->locale)</div>
                        @endif
                        @if($dossier->general->office['address'])
                            @foreach($dossier->general->office['address'] as $address)
                                <div class="law-text-gray mb-3">{{ $address }}</div>
                            @endforeach
                        @endif
                        @if($dossier->contacts)
                            @if(isset($dossier->contacts['working_hours']))
                                <div
                                    class="single-info-header">{{ __('cabinet.form.workinghours', [], $dossier->locale) }}</div>
                                @if(isset($dossier->contacts['working_hours']['mo']['start']) && isset($dossier->contacts['working_hours']['mo']['end']))
                                    <div class="">{{ __('cabinet.form.day_mo', [], $dossier->locale) }} {{ $dossier->contacts['working_hours']['mo']['start'] }}
                                        -{{ $dossier->contacts['working_hours']['mo']['end'] }}</div>
                                @endif
                                @if(isset($dossier->contacts['working_hours']['tu']['start']) && isset($dossier->contacts['working_hours']['tu']['end']))
                                    <div class="">{{ __('cabinet.form.day_tu', [], $dossier->locale) }} {{ $dossier->contacts['working_hours']['tu']['start'] }}
                                        -{{ $dossier->contacts['working_hours']['tu']['end'] }}</div>
                                @endif
                                @if(isset($dossier->contacts['working_hours']['we']['start']) && isset($dossier->contacts['working_hours']['we']['end']))
                                    <div class="">{{ __('cabinet.form.day_we', [], $dossier->locale) }} {{ $dossier->contacts['working_hours']['we']['start'] }}
                                        -{{ $dossier->contacts['working_hours']['we']['end'] }}</div>
                                @endif
                                @if(isset($dossier->contacts['working_hours']['th']['start']) && isset($dossier->contacts['working_hours']['th']['end']))
                                    <div class="">{{ __('cabinet.form.day_th', [], $dossier->locale) }} {{ $dossier->contacts['working_hours']['th']['start'] }}
                                        -{{ $dossier->contacts['working_hours']['th']['end'] }}</div>
                                @endif
                                @if(isset($dossier->contacts['working_hours']['fr']['start']) && isset($dossier->contacts['working_hours']['fr']['end']))
                                    <div class="">{{ __('cabinet.form.day_fr', [], $dossier->locale) }} {{ $dossier->contacts['working_hours']['fr']['start'] }}
                                        -{{ $dossier->contacts['working_hours']['fr']['end'] }}</div>
                                @endif
                                @if(isset($dossier->contacts['working_hours']['sa']['start']) && isset($dossier->contacts['working_hours']['sa']['end']))
                                    <div class="">{{ __('cabinet.form.day_sa', [], $dossier->locale) }} {{ $dossier->contacts['working_hours']['sa']['start'] }}
                                        -{{ $dossier->contacts['working_hours']['sa']['end'] }}</div>
                                @endif
                                @if(isset($dossier->contacts['working_hours']['su']['start']) && isset($dossier->contacts['working_hours']['su']['end']))
                                    <div class="">{{ __('cabinet.form.day_su', [], $dossier->locale) }} {{ $dossier->contacts['working_hours']['su']['start'] }}
                                        -{{ $dossier->contacts['working_hours']['su']['end'] }}</div>
                                @endif
                            @endif
                        @endif
                        @if($dossier->general->office['phones'][0])
                            <div class="mb-2">
                                <div
                                    class="single-info-header mt-2">{{ __('cabinet.form.phone_number', [], $dossier->locale) }}</div>
                                @foreach($dossier->general->office['phones'] as $phone)
                                    <div class="d-flex flex-wrap">
                                        <div class="law-text-gray mr-2">{{ $phone['number'] }}</div>
                                        <div
                                            class="dossier-single-socials">
                                            @isset($phone['messengers']['viber']) <a href="#"><img
                                                    class="dossier-single-socials-img mr-1"
                                                    src="{{ asset('img/messengers/viber.svg') }}"
                                                    alt=""></a> @endisset
                                            @isset($phone['messengers']['whatsapp']) <a href="#"><img
                                                    class="dossier-single-socials-img mr-1"
                                                    src="{{ asset('img/messengers/whatsapp.svg') }}"
                                                    alt=""></a>@endisset
                                            @isset($phone['messengers']['telegram']) <a href="#"><img
                                                    class="dossier-single-socials-img mr-1"
                                                    src="{{ asset('img/messengers/telegram.svg') }}"
                                                    alt=""></a>@endisset
                                            @isset($phone['messengers']['skype']) <a href="#"><img
                                                    class="dossier-single-socials-img"
                                                    src="{{ asset('img/messengers/skype.svg') }}"
                                                    alt=""></a>@endisset
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        @if(isset($dossier->contacts['emails']))
                            @if($dossier->contacts['emails'] !== [])
                                <div class="single-info-header color-black">E-mail</div>
                                @foreach($dossier->contacts['emails'] as $email)
                                    <div class="{{ $loop->last ? 'mb-3': null }}"><a class="site-link"
                                                                                     href="mailto:{{ $email }}">{{ $email }}</a>
                                    </div>
                                @endforeach
                            @endif
                        @endif
                        @if($dossier->overview)
                            @if(isset($dossier->overview['site']))
                                <div
                                    class="single-info-header color-black">{{ __('cabinet.form.overview_site', [], $dossier->locale) }}</div>
                                <div class="law-text-gray mb-3">
                                    <a target="_blank" class="site-link"
                                       href="{{ $dossier->overview['site'] }}">{{ $dossier->overview['site'] }}</a>
                                </div>
                            @endif
                            @if(isset($dossier->overview['facebook']) || isset($dossier->overview['instagram']) || isset($dossier->overview['linkedin']) || isset($dossier->overview['twitter']) || isset($dossier->overview['youtube']))
                                <div class="dossier-single-socials-big">
                                    @if(isset($dossier->overview['facebook']))
                                        <div class="text-left d-flex mb-2">
                                            <img class="dossier-single-socials-img mr-2"
                                                 src="{{ asset('img/socials/facebook.svg') }}" alt="">
                                            <div class="d-flex flex-column justify-content-between">
                                            <span>
                                                <a target="_blank" class="site-link"
                                                   href="{{ $dossier->overview['facebook'] }}">{{ $dossier->overview['facebook'] }}</a>
                                            </span>
                                                @if(isset($dossier->overview['facebook_followers']))
                                                    <span
                                                        style="display: block">{{ __('site.dossier_single.followers') }}: {{ $dossier->overview['facebook_followers'] }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                    @if(isset($dossier->overview['youtube']))
                                        <div class="text-left d-flex mb-2">
                                            <img class="dossier-single-socials-img mr-2"
                                                 src="{{ asset('img/socials/youtube.svg') }}" alt="">
                                            <div class="d-flex flex-column justify-content-between">
                                                <a target="_blank" class="site-link"
                                                   href="{{ $dossier->overview['youtube'] }}">{{ $dossier->overview['youtube'] }}</a>
                                                @if(isset($dossier->overview['youtube_followers']))
                                                    <span
                                                        style="display: block">{{ __('site.dossier_single.followers') }}: {{ $dossier->overview['youtube_followers'] }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                    @if(isset($dossier->overview['linkedin']))
                                        <div class="text-left d-flex mb-2">
                                            <img class="dossier-single-socials-img mr-2"
                                                 src="{{ asset('img/socials/linkedin.svg') }}" alt="">
                                            <div class="d-flex flex-column justify-content-between">
                                                <a target="_blank" class="site-link"
                                                   href="{{ $dossier->overview['linkedin'] }}">{{ $dossier->overview['linkedin'] }}</a>
                                                @if(isset($dossier->overview['linkedin_followers']))
                                                    <span
                                                        style="display: block">{{ __('site.dossier_single.followers') }}: {{ $dossier->overview['linkedin_followers'] }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                    @if(isset($dossier->overview['instagram']))
                                        <div class="text-left d-flex mb-2">
                                            <img class="dossier-single-socials-img mr-2"
                                                 src="{{ asset('img/socials/instagram.svg') }}" alt="">
                                            <div class="d-flex flex-column justify-content-between">
                                                <a target="_blank" class="site-link"
                                                   href="{{ $dossier->overview['instagram'] }}">{{ $dossier->overview['instagram'] }}</a>
                                                @if(isset($dossier->overview['linkedin_followers']))
                                                    <span
                                                        style="display: block">{{ __('site.dossier_single.followers') }}: {{ $dossier->overview['instagram_followers'] }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                    @if(isset($dossier->overview['twitter']))
                                        <div class="text-left d-flex mb-2">
                                            <img class="dossier-single-socials-img mr-2"
                                                 src="{{ asset('img/socials/twitter.svg') }}" alt="">
                                            <div class="d-flex flex-column justify-content-between">
                                                <a target="_blank" class="site-link"
                                                   href="{{ $dossier->overview['twitter'] }}">{{ $dossier->overview['twitter'] }}</a>
                                                @if(isset($dossier->overview['twitter_followers']))
                                                    <span
                                                        style="display: block">{{ __('site.dossier_single.followers') }}: {{ $dossier->overview['twitter_followers'] }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endif
                            <div class="dossier-single-self-container d-xl-block d-none">
                                @if(isset($dossier->overview['languages']))
                                    <div
                                        class="single-info-header">{{ __('cabinet.form.overview_languages', [], $dossier->locale) }}</div>
                                    <div class="law-text-gray mb-3">{{ $dossier->overview['languages'] }}</div>
                                @endif
                                @if(isset($dossier->overview['strong_sides']))
                                    <div
                                        class="single-info-header">{{ __('cabinet.form.overview_strong_sides', [], $dossier->locale) }}</div>
                                    <div class="mb-3">{{ $dossier->overview['strong_sides'] }}</div>
                                @endif
                                @if(isset($dossier->overview['hobbies']))
                                    <div
                                        class="single-info-header">{{ __('cabinet.form.overview_hobbies', [], $dossier->locale) }}</div>
                                    <div class="mb-5">{{ $dossier->overview['hobbies'] }}</div>
                                @endif
                            </div>
                        @endif
                        <div>
                            @if($dossier->qr)
                                <div class="text-left mt-5">
                                    <img style="width: 70%" src="{{ Storage::url($dossier->qr) }}" alt="">
                                </div>
                            @endif
                            <div style="width: 100%" class="row dossier-single-actions mt-5"
                                 data-html2canvas-ignore="true">
                                <div class="col-12 col-sm-6"><a onclick="exportPdf()"
                                                                href="javascript:;">{{ __('site.dossier_single.download') }}</a>
                                </div>
                                <div class="col-12 col-sm-6 mt-3 mt-sm-0"><a href="javascript:;"
                                                                             onclick="$('#lawyer-report-modal').modal('show'); $('#modal-lawyer-id').val({{ $dossier->user_id }})">{{ __('site.dossier_single.report') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-12 dossier-single-exp">
                <div class="experience-block">
                    <div style="width: 100%;" class="exp-header dossier-single-title-separator mb-3">
                        <img style="left: -26px;" class="experience-block-icon d-none d-lg-flex"
                             src="/img/dossier-single/section-icon1.png"
                             alt="">
                        {{ __('cabinet.form.title_experience', [], $dossier->locale) }}
                    </div>
                    <div class="exp-section-header mb-3">
                        {{ __('cabinet.form.sub_title_patent_license', [], $dossier->locale) }}
                    </div>
                    @foreach($dossier->license as $license)
                        <div class="row">
                            @if(isset($license['country']))
                                @component('site.blocks.dossier-single-field')
                                    @slot('name', __('cabinet.form.licence_country', [], $dossier->locale))
                                    @slot('value', $license['country']))
                                @endcomponent
                            @endif
                            @if(isset($license['institution']))
                                @component('site.blocks.dossier-single-field')
                                    @slot('name', __('cabinet.form.patent_license_institution', [], $dossier->locale))
                                    @slot('value', $license['institution'])
                                @endcomponent
                            @endif
                            @if(isset($license['year']))
                                @component('site.blocks.dossier-single-field')
                                    @slot('name', __('cabinet.form.licence_year', [], $dossier->locale))
                                    @slot('value', $license['year'])
                                @endcomponent
                            @endif
                            @if(isset($license['number']))
                                @component('site.blocks.dossier-single-field')
                                    @slot('name', __('cabinet.form.licence_number', [], $dossier->locale))
                                    @slot('value', $license['number'])
                                @endcomponent
                            @endif
                            @if(isset($license['photo']))
                                @component('site.blocks.dossier-single-image-field')
                                    @slot('value', $license['photo'])
                                    @slot('locale', $dossier->locale)
                                @endcomponent
                            @endif
                            {{--                        @component('site.blocks.dossier-single-field')--}}
                            {{--                            @slot('name', 'Стаж')--}}
                            {{--                            @slot('value', '11 років')--}}
                            {{--                        @endcomponent--}}
                            {{--                        @component('site.blocks.dossier-single-field')--}}
                            {{--                            @slot('name', 'Країна здійснення')--}}
                            {{--                            @slot('value', 'Україна')--}}
                            {{--                        @endcomponent--}}
                            {{--                        @component('site.blocks.dossier-single-field')--}}
                            {{--                            @slot('name', 'Регіон здійснення')--}}
                            {{--                            @slot('value', 'Львівський')--}}
                            {{--                        @endcomponent--}}
                            {{--                        @component('site.blocks.dossier-single-field')--}}
                            {{--                            @slot('name', 'Населений пункт (місто)')--}}
                            {{--                            @slot('value', 'Львів')--}}
                            {{--                        @endcomponent--}}
                        </div>
                    @endforeach
                        @if(isset($dossier->spec))
                            @if($dossier->spec->appeal_cases[0] || $dossier->spec->cassation_cases[0] || $dossier->spec->intl_cases[0])
                                <div class="exp-section-header mb-3">
                                    {{ __('cabinet.form.experience_specializations', [], $dossier->locale) }}
                                </div>
                                <div class="row">
                                    @if(isset($dossier->spec->appeal_cases[0]))
                                        @component('site.blocks.dossier-single-field')
                                            @slot('name', __('cabinet.form.experience_appeal_exp', [], $dossier->locale))
                                            @slot('value', $dossier->spec->appeal_cases[0])
                                        @endcomponent
                                    @endif
                                    @if(isset($dossier->spec->cassation_cases[0]))
                                        @component('site.blocks.dossier-single-field')
                                            @slot('name', __('cabinet.form.experience_cassation_cases', [], $dossier->locale))
                                            @slot('value', $dossier->spec->cassation_cases[0])
                                        @endcomponent
                                    @endif
                                    @if(isset($dossier->spec->intl_cases[0]))
                                        @component('site.blocks.dossier-single-field')
                                            @slot('name', __('cabinet.form.experience_intl_cases', [], $dossier->locale))
                                            @slot('value', $dossier->spec->intl_cases[0])
                                        @endcomponent
                                    @endif
                                </div>
                            @endif
                        @endif
                </div>
                @if($dossier->education)
                    @if(!is_null($dossier->education['higher']) || !is_null($dossier->education['academic_status']) || !is_null($dossier->education['teaching']))
                        <div class="experience-block">
                            <div style="width: 100%;" class="exp-header dossier-single-title-separator mb-3">
                                <img style="left: -26px;" class="experience-block-icon d-none d-lg-flex"
                                     src="{{ asset('img/dossier-single/section-icon2.png') }}"
                                     alt="">
                                Освіта
                            </div>
                            @if(isset($dossier->education['higher'][0]))
                                <div style="vertical-align: super; width: 89%"
                                     class="d-inline-block"></div>
                                <div class="exp-section-header mb-3">
                                    {{ __('cabinet.form.sub_title_education_higher', [], $dossier->locale) }}
                                </div>
                                @foreach($dossier->education['higher'] as $higher)
                                    <div class="row">
                                        @if(isset($higher['from']) && isset($higher['to']))
                                            @component('site.blocks.dossier-single-field')
                                                @slot('name', __('cabinet.form.period', [], $dossier->locale))
                                                @slot('value', __('cabinet.form.from', [], $dossier->locale) . ' ' . $higher['from'] . ' ' . __('cabinet.form.to', [], $dossier->locale) . ' ' . $higher['to'])
                                            @endcomponent
                                        @endif
                                        @if(isset($higher['country']))
                                            @component('site.blocks.dossier-single-field')
                                                @slot('name', __('cabinet.form.country', [], $dossier->locale))
                                                @slot('value', $higher['country'])
                                            @endcomponent
                                        @endif
                                        @if(isset($higher['university']))
                                            @component('site.blocks.dossier-single-field')
                                                @slot('name', __('cabinet.form.university', [], $dossier->locale))
                                                @slot('value', $higher['university'])
                                            @endcomponent
                                        @endif
                                        @if(isset($higher['degree']))
                                            @component('site.blocks.dossier-single-field')
                                                @slot('name', __('cabinet.form.degree', [], $dossier->locale))
                                                @slot('value', $higher['degree'])
                                            @endcomponent
                                        @endif
                                        @if(isset($higher['photo']))
                                                @component('site.blocks.dossier-single-image-field')
                                                    @slot('value', $higher['photo'])
                                                    @slot('locale', $dossier->locale)
                                                @endcomponent
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                            @if(isset($dossier->education['academic_status'][0]))
                                <div class="exp-section-header mb-3">
                                    {{ __('cabinet.form.sub_title_education_academic_status', [], $dossier->locale) }}
                                </div>
                                @foreach($dossier->education['academic_status'] as $academic_status)
                                    <div class="row">
                                        @if(isset($academic_status['from']) && isset($academic_status['to']))
                                            @component('site.blocks.dossier-single-field')
                                                @slot('name', __('cabinet.form.period', [], $dossier->locale))
                                                @slot('value', __('cabinet.form.from', [], $dossier->locale) . ' ' . $academic_status['from'] . ' ' . __('cabinet.form.to', [], $dossier->locale) . ' ' . $academic_status['to'])
                                            @endcomponent
                                        @endif
                                        @if($academic_status['university'])
                                            @component('site.blocks.dossier-single-field')
                                                @slot('name', __('cabinet.form.university', [], $dossier->locale))
                                                @slot('value', $academic_status['university'])
                                            @endcomponent
                                        @endif
                                        @if(isset($academic_status['degree']))
                                            @component('site.blocks.dossier-single-field')
                                                @slot('name', __('cabinet.form.degree_rank', [], $dossier->locale))
                                                @slot('value',  $academic_status['degree'])
                                            @endcomponent
                                        @endif
                                        @if(isset($academic_status['photo']))
                                                @component('site.blocks.dossier-single-image-field')
                                                    @slot('value', $academic_status['photo'])
                                                    @slot('locale', $dossier->locale)
                                                @endcomponent
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                            @if(isset($dossier->education['teaching'][0]))
                                <div class="exp-section-header mb-3">
                                    {{ __('cabinet.form.sub_title_education_teaching', [], $dossier->locale) }}
                                </div>
                                @foreach($dossier->education['teaching'] as $teaching)
                                    <div class="row">
                                        @if(isset($teaching['from']) && isset($teaching['to']))
                                            @component('site.blocks.dossier-single-field')
                                                @slot('name', __('cabinet.form.period', [], $dossier->locale))
                                                @slot('value', __('cabinet.form.from', [], $dossier->locale) . ' ' . $teaching['from'] . ' ' . __('cabinet.form.to', [], $dossier->locale) . ' ' . $teaching['to'])
                                            @endcomponent
                                        @endif
                                        @if(isset($teaching['university']))
                                            @component('site.blocks.dossier-single-field')
                                                @slot('name', __('cabinet.form.university', [], $dossier->locale))
                                                @slot('value', $teaching['university'])
                                            @endcomponent
                                        @endif
                                        @if(isset($teaching['topic']))
                                            @component('site.blocks.dossier-single-field')
                                                @slot('name', __('cabinet.form.subject', [], $dossier->locale))
                                                @slot('value', $teaching['topic'])
                                            @endcomponent
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    @endif
                @endif
                @if(isset($dossier->experience_other))
                    @if(isset($dossier->experience_other['experience'][0]))
                        <div class="experience-block">
                            <div style="width: 100%;" class="exp-header dossier-single-title-separator mb-3">
                                <img style="left: -26px;" class="experience-block-icon d-none d-lg-flex"
                                     src="{{ asset('img/dossier-single/section-icon3.png') }}"
                                     alt="">
                                {{ __('cabinet.form.title_other_experience', [], $dossier->locale) }}
                            </div>
                            @foreach($dossier->experience_other['experience'] as $experience)
                                <div class="row">
                                    @if(isset($experience['from']) && isset($experience['to']))
                                        @component('site.blocks.dossier-single-field')
                                            @slot('name', __('cabinet.form.period', [], $dossier->locale))
                                            @slot('value', __('cabinet.form.from', [], $dossier->locale) . ' ' . $experience['from'] . ' ' . __('cabinet.form.to', [], $dossier->locale) . ' ' . $experience['to'])
                                        @endcomponent
                                    @endif
                                    @if(isset($experience['position']))
                                        @component('site.blocks.dossier-single-field')
                                            @slot('name', __('cabinet.form.position', [], $dossier->locale))
                                            @slot('value', $experience['position'])
                                        @endcomponent
                                    @endif
                                    @if(isset($experience['place_of_work']))
                                        @component('site.blocks.dossier-single-field')
                                            @slot('name', __('cabinet.form.place_of_work', [], $dossier->locale))
                                            @slot('value', $experience['place_of_work'])
                                        @endcomponent
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    @endif
                @endif
                @if($dossier->publications)
                    <div class="experience-block">
                        <div style="width: 100%;" class="exp-header dossier-single-title-separator mb-3">
                            <img style="left: -26px;" class="experience-block-icon d-none d-lg-flex"
                                 src="{{ asset('img/dossier-single/section-icon4.png') }}"
                                 alt="">
                            {{ __('cabinet.form.title_publications', [], $dossier->locale) }}
                        </div>
                        <div style="vertical-align: super; width: 84%" class="d-inline-block"></div>
                        @foreach($dossier->publications as $publication)
                            <div class="row">
                                @if(isset($publication['edition']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name', __('cabinet.form.edition', [], $dossier->locale))
                                        @slot('value', $publication['edition'])
                                    @endcomponent
                                @endif
                                @if(isset($publication['topic']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name', __('cabinet.form.topic', [], $dossier->locale))
                                        @slot('value', $publication['topic'])
                                    @endcomponent
                                @endif
                                @if(isset($publication['url']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name', __('cabinet.form.link_to_public', [], $dossier->locale))
                                        @slot('url', $publication['url'])
                                    @endcomponent
                                @endif
                                @if(isset($publication['photo']))
                                        @component('site.blocks.dossier-single-image-field')
                                            @slot('value', $publication['photo'])
                                            @slot('locale', $dossier->locale)
                                        @endcomponent
                                @endif
                            </div>
                        @endforeach
                    </div>
                @endif
                @if($dossier->training)
                    <div class="experience-block">
                        <div style="width: 100%;" class="exp-header dossier-single-title-separator mb-3">
                            <img style="left: -26px;" class="experience-block-icon d-none d-lg-flex"
                                 src="{{ asset('img/dossier-single/section-icon4.png') }}"
                                 alt="">
                            {{ __('cabinet.form.title_training', [], $dossier->locale) }}
                        </div>
                        <div style="vertical-align: super; width: 63%" class="d-inline-block"></div>
                        @foreach($dossier->training as $training)
                            <div class="row">
                                @if(isset($training['from']) && isset($training['to']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name', __('cabinet.form.period', [], $dossier->locale))
                                        @slot('value', __('cabinet.form.from', [], $dossier->locale) . ' ' . $training['from'] . ' ' . __('cabinet.form.to', [], $dossier->locale) . ' ' . $training['to'])
                                    @endcomponent
                                @endif
                                @if(isset($training['country']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name', __('cabinet.form.country', [], $dossier->locale))
                                        @slot('value', $training['country'])
                                    @endcomponent
                                @endif
                                @if(isset($training['university']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name', __('cabinet.form.university', [], $dossier->locale))
                                        @slot('value', $training['university'])
                                    @endcomponent
                                @endif
                                @if(isset($training['topic']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name', __('cabinet.form.topic', [], $dossier->locale))
                                        @slot('value', $training['topic'])
                                    @endcomponent
                                @endif
                                @if(isset($training['photo']))
                                        @component('site.blocks.dossier-single-image-field')
                                            @slot('value', $training['photo'])
                                            @slot('locale', $dossier->locale)
                                        @endcomponent
                                @endif
                            </div>
                        @endforeach
                    </div>
                @endif
                @if($dossier->recommendations)
                    <div class="experience-block">
                        <div style="width: 100%;" class="exp-header dossier-single-title-separator mb-3">
                            <img style="left: -26px;" class="experience-block-icon d-none d-lg-flex"
                                 src="{{ asset('img/dossier-single/section-icon4.png') }}"
                                 alt="">
                            {{ __('cabinet.form.title_recommendations', [], $dossier->locale) }}
                        </div>
                        <div style="vertical-align: super; width: 79%" class="d-inline-block"></div>
                        @foreach($dossier->recommendations as $recommendations)
                            <div class="row">
                                @if(isset($recommendations['fullname']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name', __('cabinet.form.fullname', [], $dossier->locale))
                                        @slot('value', $recommendations['fullname'])
                                    @endcomponent
                                @endif
                                @if(isset($recommendations['position']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name', __('cabinet.form.position', [], $dossier->locale))
                                        @slot('value', $recommendations['position'])
                                    @endcomponent
                                @endif
                                @if(isset($recommendations['place_of_work']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name', __('cabinet.form.place_of_work', [], $dossier->locale))
                                        @slot('value', $recommendations['place_of_work'])
                                    @endcomponent
                                @endif
                                @if(isset($recommendations['document']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name', __('cabinet.form.recomm_doc', [], $dossier->locale))
                                        @slot('img', $recommendations['document'])
                                    @endcomponent
                                @endif
                            </div>
                        @endforeach
                    </div>
                @endif
                @if($dossier->honors)
                    <div class="experience-block">
                        <div style="width: 100%;" class="exp-header dossier-single-title-separator mb-3">
                            <img style="left: -26px;" class="experience-block-icon d-none d-lg-flex"
                                 src="{{ asset('img/dossier-single/section-icon4.png') }}"
                                 alt="">
                            {{ __('cabinet.form.title_honors', [], $dossier->locale) }}
                        </div>
                        <div style="vertical-align: super; width: 85%" class=" d-inline-block"></div>
                        @foreach($dossier->honors as $honors)
                            <div class="row">
                                @if(isset($honors['year']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name',  __('cabinet.form.year_honors', [], $dossier->locale))
                                        @slot('value', $honors['year'])
                                    @endcomponent
                                @endif
                                @if(isset($honors['name']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name', __('cabinet.form.name', [], $dossier->locale))
                                        @slot('value', $honors['name'])
                                    @endcomponent
                                @endif
                            </div>
                        @endforeach
                    </div>
                @endif
                @if(isset($dossier->services[0]) && isset($dossier->services[0]['name']))
                    <div class="experience-block">
                        <div style="width: 100%;" class="exp-header dossier-single-title-separator mb-3">
                            <img style="left: -26px;" class="experience-block-icon d-none d-lg-flex"
                                 src="{{ asset('img/dossier-single/section-icon4.png') }}"
                                 alt="">
                            {{ __('cabinet.form.title_services', [], $dossier->locale) }}
                        </div>
                        <div style="vertical-align: super; width: 76%" class="d-inline-block"></div>
                        @foreach($dossier->services as $services)
                            <div class="row">
                                @if(isset($services['name']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name', __('cabinet.form.name', [], $dossier->locale))
                                        @slot('value', $services['name'])
                                    @endcomponent
                                @endif
                                @if(isset($services['cost']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name', __('cabinet.form.price', [], $dossier->locale))
                                        @slot('value', $services['cost'])
                                    @endcomponent
                                @endif
                                @if(isset($services['currency']) && isset($services['cost']))
                                    @component('site.blocks.dossier-single-field')
                                        @slot('name', __('cabinet.form.currency', [], $dossier->locale))
                                        @slot('value', $services['currency'])
                                    @endcomponent
                                @endif
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@include('site.modals.report-lawyer')
@include('site.modals.report-result')
<script>
    var exportPdf = function () {
        var HTML_Width = $("body").width();
        var HTML_Height = $("body").height();
        var top_left_margin = 15;
        var PDF_Width = HTML_Width + (top_left_margin * 2);
        var PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
        var canvas_image_width = HTML_Width;
        var canvas_image_height = HTML_Height;
        var dossier_lol = $('#dossier-lol');
        var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;

        dossier_lol.show();
        html2canvas($("body")[0], {
            allowTaint: true,
            // width: HTML_Width,
            // height: HTML_Height
        }).then(function (canvas) {
            canvas.getContext('2d');

            console.log(canvas.height + "  " + canvas.width);


            var imgData = canvas.toDataURL("image/jpeg", 1.0);
            var pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
            pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);


            for (var i = 1; i <= totalPDFPages; i++) {
                pdf.addPage(PDF_Width, PDF_Height);
                pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height * i) + (top_left_margin * 4), canvas_image_width, canvas_image_height);
            }

            pdf.save("{{ 'dossier-'.$dossier->id."-".$dossier->locale }}.pdf");
        });
        dossier_lol.hide();
    }
</script>
