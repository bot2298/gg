<div class="modal fade site-modal" id="report-success" tabindex="-1" role="dialog" aria-labelledby="report-success" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('modals.result_success')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>@lang('modals.result_success_content')</div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade site-modal" id="report-error" tabindex="-1" role="dialog" aria-labelledby="report-error" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('modals.result_error')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>@lang('modals.result_error_content')</div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade site-modal" id="report-duplicate" tabindex="-1" role="dialog" aria-labelledby="report-duplicate" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('modals.result_error')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>{{ __('modals.report_duplicate') }}</div>
            </div>
        </div>
    </div>
</div>
