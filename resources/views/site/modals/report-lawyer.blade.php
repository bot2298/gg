<div class="modal fade site-modal" id="lawyer-report-modal" tabindex="-1" role="dialog" aria-labelledby="lawyer-report-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('modals.lawyer_complaint_title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <style>
                .lawyer-modal-input{
                    color: #000;
                    background: #FBFBFB;
                    width: 100%;
                    border-radius: 0;
                    height: 48px;
                    border: 1px solid #EC9C23;
                    padding-left: 10px;
                    padding-right: 10px;
                }
            </style>
            <div class="modal-body">
                <form method="POST" enctype="multipart/form-data" action="{{ route('cabinet.request.complaint', app()->getLocale()) }}">
                    @csrf
                    @php $application = \App\Models\Application::find(Cookie::get('application_id')) @endphp
                    <div class="mb-2">
                        <label for="fullname-input" class="col-form-label d-block">Ім'я та прізвище</label>
                        <input class="lawyer-modal-input" type="text" name="fullname" id="fullname-input" value="{{ $application->name ?? null }}" required>
                        <span class="modal-validation-error fullname-error"></span>
                    </div>
                    <div class="mb-2">
                        <label for="address-input" class="col-form-label d-block">Контактна поштова адреса</label>
                        <input class="lawyer-modal-input" type="text" name="address" id="address-input" required>
                        <span class="modal-validation-error address-error"></span>
                    </div>
                    <div class="mb-2">
                        <label for="contact-email-input" class="col-form-label d-block">Контактна електронна адреса</label>
                        <input class="lawyer-modal-input" type="email" name="email" id="contact-email-input" value="{{ $application->email ?? null }}" required>
                        <span class="modal-validation-error email-error"></span>
                    </div>
                    <div class="mb-2">
                        <label for="contact-number-input" class="col-form-label d-block">Номер засобу зв'язку</label>
                        <input class="lawyer-modal-input" type="tel" name="number" id="contact-number-input" value="{{ $application->phone ?? null }}" required>
                        <span class="modal-validation-error number-error"></span>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">@lang('modals.lawyer_complaint_comment')</label>
                        <textarea name="text" rows="3" class="form-control lawyer-modal-input" id="message-text" required></textarea>
                        <span class="modal-validation-error text-error"></span>
                    </div>
                    <input type="hidden" name="lawyer_id" id="modal-lawyer-id" value=""/>
                    <div class="mb-2">
                        <label for="report-attachment" class="col-form-label">
                            Обрати файл
                            <input id="report-attachment" name="attachment" type="file" accept="application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/*,application/pdf">
                        </label>
                        <span class="modal-validation-error text-error"></span>
                    </div>
                    <div class="mb-2">
                        <p style="font-size: 14px">{{ __('modals.complaint_ps') }}</p>
                    </div>
                    <div class="text-center">
                        <button data-locale="{{ app()->getLocale() }}" id="report-lawyer-submit" type="button" class="btn site-modal-submit">@lang('modals.lawyer_complaint_submit')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
