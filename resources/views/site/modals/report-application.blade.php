<div class="modal fade site-modal" id="application-report-modal" tabindex="-1" role="dialog" aria-labelledby="application-report-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('modals.request_complaint_title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('cabinet.request.complaint', app()->getLocale()) }}">
                    @csrf
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">@lang('modals.request_complaint_comment')</label>
                        <textarea name="text" rows="3" class="form-control" id="message-text"></textarea>
                        <span class="modal-validation-error text-error"></span>
                    </div>
                    <input type="hidden" name="application_id" id="modal-application-id" value=""/>
                    <div class="text-center">
                        <button data-locale="{{app()->getLocale()}}" id="report-application-submit" type="button" class="btn site-modal-submit">@lang('modals.request_complaint_submit')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
