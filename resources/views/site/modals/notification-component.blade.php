@if($notification = session('notification'))
    <div id="notification-modal" class="modal fade site-modal modal-type-{{ $notification["type"] }}"tabindex="-1" role="dialog" aria-labelledby="report-success" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ $notification["title"] }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div>{{ $notification["content"] }}</div>
                </div>
            </div>
        </div>
    </div>
    <script>
        window.addEventListener('load', (event) => {
            $('#notification-modal').modal('show');
        });
    </script>
@endisset
