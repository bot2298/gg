<div class="modal fade site-modal" id="rate-lawyer-modal" tabindex="-1" role="dialog" aria-labelledby="rate-lawyer-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('modals.rate_title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('site.tracker.rate', app()->getLocale()) }}">
                    @csrf
                    <div>@lang('modals.rate_stars')</div>
                    <div class="rating">
                        <label>
                            <input class="stars-input" type="radio" name="stars" value="1" />
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input class="stars-input" type="radio" name="stars" value="2" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input class="stars-input" type="radio" name="stars" value="3" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input class="stars-input" type="radio" name="stars" value="4" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input class="stars-input" type="radio" name="stars" value="5" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                    </div>
                    <input name="rating" type="hidden">
                    <input id="rate-lawyer-id" name="user" value="" type="hidden">
                    <span class="modal-validation-error rating-error"></span>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">@lang('modals.rate_comment')</label>
                        <textarea name="text" rows="3" class="form-control" id="message-text"></textarea>
                        <span class="modal-validation-error text-error"></span>
                    </div>
                    <div class="text-center">
                        <button data-locale="{{app()->getLocale()}}" id="rating-submit" type="button" class="btn site-modal-submit">@lang('modals.rate_submit')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade site-modal" id="rate-lawyer-thanks" tabindex="-1" role="dialog" aria-labelledby="rate-lawyer-thanks" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Дякуємо</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>{{ __('modals.rate_success') }}</div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade site-modal" id="rate-lawyer-error" tabindex="-1" role="dialog" aria-labelledby="rate-lawyer-error" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('modals.result_error') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>{{ __('modals.rate_error') }}</div>
            </div>
        </div>
    </div>
</div>
