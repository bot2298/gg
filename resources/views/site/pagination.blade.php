<nav class="pagination">
    <div class="container">
        <div class="row">
            <div class="col-6 col-lg-3 order-1 text-left">
                <a href="#"><img src="{{ asset('img/tracker/prev.png') }}" alt=""></a>
            </div>
            <div class="col-12 col-lg-6 order-3 order-lg-2 text-center my-auto">
                <ul class="pagination-ul">
                    <li class="pagination-link mr-3"><a href="#">1</a></li>
                    <li class="pagination-link mr-3"><a href="#">2</a></li>
                    <li class="pagination-link mr-3 "><a href="#">3</a></li>
                    <li style="vertical-align: bottom; height: 100%;" class="mr-3">
                        <svg style="vertical-align: bottom" width="39" height="7" viewBox="0 0 39 7" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="7" height="7" fill="#EC9C23"/>
                            <rect x="16" width="7" height="7" fill="#EC9C23"/>
                            <rect x="32" width="7" height="7" fill="#EC9C23"/>
                        </svg>
                    </li>
                    <li class="pagination-link mr-3"><a href="#">10</a></li>
                    <li class="pagination-link"><a href="#">20</a></li>
                </ul>
            </div>
            <div class="col-6 col-lg-3 order-2 order-lg-3 text-right">
                <a href="#"><img src="{{ asset('img/tracker/next.png') }}" alt=""></a>
            </div>
        </div>
    </div>
</nav>
