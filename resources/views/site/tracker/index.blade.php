<section class="tracker-code-form__container background-fluid mb-3 footer-to-bottom need-footer-separator">
    <div class="container" id="app">
        <tracker-code-input></tracker-code-input>
    </div>
</section>
<div class="container">
    {{ Breadcrumbs::render('tracker') }}
</div>
@include('site.modals.notification-component')
