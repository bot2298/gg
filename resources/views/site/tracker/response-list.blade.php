<section class="header-info-container tracker-header-info background-fluid mb-3">
    <div class="container">
        <div class="mb-5">
            <div class="tracker-header-caption text-center">
                <span>{{ __('site.tracker.header') }}</span> <span class="request-verified-code">{{ $application->code }}</span>
            </div>
            <div class="tracker-header-date text-center">
                @if($application->created_at->eq($application->updated_at))
                    {{ __('site.tracker.created') }} {{ $application->created_at }}
                @else
                    {{ __('site.tracker.updated') }} {{ $application->updated_at }}
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-4 mb-3 mb-lg-0 text-center">
                <img class="tracker-header-img mr-3" src="{{ asset('img/dossier/01.png') }}" alt="">
                <span class="tracker-header-step active">{{ __('site.tracker.status1') }}</span>
            </div>
            <div class="col-12 col-md-4 mb-3 mb-lg-0 text-center">
                <img class="tracker-header-img mr-3" src="{{ $application->getTrackerStatus() >= \App\Models\Application::TRACKER_STATUS_BOOKMARKED ? asset('img/dossier/02.png') : asset('img/dossier/02_disabled.png') }}" alt="">
                <span class="tracker-header-step{{ $application->getTrackerStatus() >= \App\Models\Application::TRACKER_STATUS_BOOKMARKED ? ' active': null }}">{{ __('site.tracker.status2') }}</span>
            </div>
            <div class="col-12 col-md-4 mb-3 mb-lg-0 text-center">
                <img class="tracker-header-img mr-3" src="{{ $application->getTrackerStatus() === \App\Models\Application::TRACKER_STATUS_RESPONSE ? asset('img/dossier/03.png') : asset('img/dossier/03_disabled.png') }}" alt="">
                <span class="tracker-header-step{{ $application->getTrackerStatus() === \App\Models\Application::TRACKER_STATUS_RESPONSE ? ' active': null }}">{{ __('site.tracker.status3') }}</span>
            </div>
        </div>
    </div>
</section>
<section class="tracker-container">
    <div class="container" id="app">
        @if(isset($chats))
            <tracker-response-list :data="{{ json_encode($responseListData) }}"></tracker-response-list>
        @endif
            {{ $responseListData->links('cabinet.pagination') }}
            <div class="mb-5 request-info-toggle">
                <div class="tracker-page-heading d-inline-block mr-3">
                    {{ __('site.request.header') }}
                </div>
                <a href="javascript:;"><img style="width: 24px;vertical-align: sub;" src="{{ asset('img/tracker/arrow-up.png') }}" alt=""></a>
            </div>
        <div class="row mb-3 request-info-container">
            <div class="col-12 col-md-5 mb-1 mb-md-4 tracker-request-information__name">
                {{ __('forms.lawyer_country') }}
            </div>
            <div class="col-12 col-md-7 mb-4 mb-md-0 tracker-request-information__value">
                {{ __('countries.'.$application->region->country->name) }}
            </div>
            <div class="col-12 col-md-5 mb-1 mb-md-4 tracker-request-information__name">
                {{ __('forms.lawyer_region') }}
            </div>
            <div class="col-12 col-md-7 mb-4 mb-md-0 tracker-request-information__value">
                {{ __('regions.'.$application->region->name) }}
            </div>
            @if($application->city)
                <div class="col-12 col-md-5 mb-1 mb-md-4 tracker-request-information__name">
                    {{ __('forms.lawyer_city') }}
                </div>
                <div class="col-12 col-md-7 mb-4 mb-md-0 tracker-request-information__value">
                    {{ __('cities.'.$application->city->name) }}
                </div>
            @endif
            <div class="col-12 col-md-5 mb-1 mb-md-4 tracker-request-information__name">
                {{ __('site.request.content') }}
            </div>
            <div class="col-12 col-md-7 mb-4 tracker-request-information__value">
                {{ $application->content }}
            </div>
            <div class="col-12 col-md-5 mb-1 mb-md-4 tracker-request-information__name">
                {{ __('site.request.cost') }}
            </div>
            <div class="col-12 col-md-7 mb-4 mb-md-0 tracker-request-information__value">
                {{ $application->cost." ".$application->currency->getName() }}
            </div>
            <div class="col-12 col-md-5 mb-1 mb-md-4 tracker-request-information__name">
                {{ __('cabinet.request.actual_til') }}
            </div>
            <div class="col-12 col-md-7 mb-4 mb-md-0 mb-3vw tracker-request-information__value">
                {{ $application->deadline }}
            </div>
            @if($application->attachment)
                <div class="col-12 col-md-5 mb-1 mb-md-4 tracker-request-information__name">
                    {{ __('site.request.attachment') }}
                </div>
                <div class="col-12 col-md-7 mb-4 mb-md-0 tracker-request-information__value">
                    <div><a style="font-size: 20px" class="cabinet-request-attachment" target="_blank" href="{{ Storage::url($application->attachment) }}">@lang('site.request.attachment_download')</a></div>
                </div>
            @endif
        </div>
        <div class="row mb-3">
            <div class="col-12 col-sm-6 text-center text-sm-right mt-3 mb-3">
                <a href="{{ route('request.edit', app()->getLocale()) }}" class="tracker-request-action">
                    {{ __('site.tracker.change') }}
                </a>
            </div>
            <div class="col-12 col-sm-6 text-center text-sm-left mt-3 mb-3">
                <form method="POST" id="request-stop-form" action="{{ route('request.stop', app()->getLocale()) }}" class="d-none">
                    @csrf
                </form>
                <a href="javascript:;" onclick="document.querySelector('#request-stop-form').submit()" class="tracker-request-action">
                    {{ __('site.tracker.stop') }}
                </a>
            </div>
            <div class="col-12 text-center mt-3 mb-3">
                <form method="POST" id="request-exit-form" action="{{ route('request.exit', app()->getLocale()) }}" class="d-none">
                    @csrf
                </form>
                <a href="javascript:;" onclick="document.querySelector('#request-exit-form').submit()" class="tracker-request-action">
                    {{ __('site.tracker.exit') }}
                </a>
            </div>
        </div>
    </div>
</section>
@include('site.modals.notification-component')
@include('site.modals.rate')
@include('site.modals.report-lawyer')
@include('site.modals.report-result')
