<div class="mb-5">
    <div class="tracker-response">
        <div class="row">
            <div class="col-12 col-md-6 col-xl-4">
                <div class="row">
                    <div class="col-12 col-xl-7">
                        <div>
                            <img class="tracker-response-img" src="{{ asset('img/dossier/lawyer2.png') }}" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-xl-5">
                        <div class="tracker-response-name">Віктор Білозор</div>
                        <div class="tracker-response-location mb-5">Україна, Київ</div>
                        <div class="tracker-response-stats mb-3 mb-xl-0">
                            <img src="{{ asset('img/dossier/star.png') }}" alt="">
                            4,5
{{--                            <img src="{{ asset('img/tracker/quote.png') }}" alt="">--}}
{{--                            15--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-xl-5 mb-3 mb-xl-0">
                <div class="tracker-response-text">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea commodo consequat Excepteur sint occaecat cupidatat non proident, sunt in culpa qui offiia ...
                </div>
                <div class="text-right">
                    <a class="tracker-response-open mb-3 mb-xl-0" href="#">Переглянути повідомлення</a>
                </div>
            </div>
            <div class="col-12 col-xl-3">
                <div class="tracker-response-actions">
                    <a onclick="$('#rate-lawyer-modal').modal('show')" href="javascript:;" class="mb-3">Оцінити адвоката</a>
                    <a href="#" class="mb-3">Переглянути досьє</a>
                    <a onclick="$('#lawyer-report-modal').modal('show'); $('#modal-lawyer-id').val({{ auth()->id() }})" href="javascript:;">Поскаржитись</a>
                </div>
            </div>
        </div>
    </div>
</div>

