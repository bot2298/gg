<div class="col-md-5 col-12 dossier-single-field-name mb-md-3 mb-2">{{$name}}</div>
<div class="col-md-7 col-12 dossier-single-field-value mb-md-3 mb-4">
    @isset($value) {{$value}} @endisset
    @isset($img)
            <a href="{{Storage::url($img)}}" data-lightbox="image"><img style="max-width: 200px;" src="{{Storage::url($img)}}"></a>
    @endisset
    @isset($url)
            <a target="_blank" class="site-link" href="{{ $url }}">{{ $url }}</a>
    @endisset
</div>
