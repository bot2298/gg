<div class="col-lg-6 col-12 mb-2 request-form-field">
    {{$name}}
    @isset($required)
        <div class="request-form-field-required">{{ __('site.request.required_field') }}</div>
    @endisset
</div>
