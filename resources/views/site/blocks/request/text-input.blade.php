<div class="col-lg-6 col-12 mb-5">
    <input @if(isset($allowChange) && !$allowChange)
           style="background-color: #eee" @endif
           class="request-form-text @isset($style) {{$style}} @endisset @error($name) is-invalid @enderror"
           name="{{$name}}" type="{{$type}}" placeholder="{{$placeholder}}"
           value="{{ old($name, $old) }}"
           @if(isset($allowChange) && !$allowChange) readonly @endif
    >
    @error($name)
    <span class="cabinet-validation-error">{{ $message }}</span>
    @enderror
</div>

