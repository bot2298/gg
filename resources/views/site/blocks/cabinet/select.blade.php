<div class="col-md-7 col-12 mt-md-4 mt-3">
    <select @isset($idAttribute) id="{{ $idAttribute }}" @endisset @isset($disabled) disabled  @endisset @isset($data_local) data-locale="{{ request()->query('lang') ?? app()->getLocale() }}"  @endisset  @isset($data_old)  data-old="{{ $data_old }}" @endisset data-placeholder="{{ $placeholder }}" class="pl-2 cabinet-form-select @isset($style) {{ $style }} @endisset" name="{{$name}}">
        <option></option>
        @isset($data)
            @foreach($data as $item)
                <option @isset($selectedFirst) selected @endisset value="{{ $item->id ?? ''}}"> @isset($langKey)  {{ __($langKey . $item->name, [], request()->query('lang') ?? app()->getLocale()) }} @endisset</option>
            @endforeach
        @endisset
    </select>
    @isset($errorName)
        @error($errorName)
        <span class="cabinet-validation-error">{{ $message }}</span>
        @enderror
    @endisset
</div>
