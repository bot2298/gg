<div class="col-md-7 col-12 mt-md-4 mt-3">
    <div class="cabinet-container-text-socials row ml-0 justify-content-between">
        <input class="col-7 cabinet-form-text-double-input" name="{{$firstName}}" type="input" @isset($oldFirst) value="{{ $oldFirst }}" @endisset placeholder="{{$placeholder}}">
        <input class="col-4 cabinet-form-text-double-input" name="{{$secondName}}" type="input" @isset($oldSecond) value="{{ $oldSecond }}" @endisset placeholder="{{$placeholder}}">
    </div>
</div>
