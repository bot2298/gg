<div class="col-md-7 col-12 mt-md-4 mt-3">
    <input class="pl-2 cabinet-form-text @isset($errorName) @error($errorName) is-invalid @enderror @endisset @isset($style) {{$style}} @endisset"  name="{{$name}}" type="{{$type}}" @isset($old) value="{{ $old }}" @endisset placeholder="{{ __('forms.placeholder_enter', [], app('request')->input('lang')) }}">
    @isset($errorName)
        @error($errorName)
        <span class="cabinet-validation-error">{{ $message }}</span>
        @enderror
    @endisset
</div>
