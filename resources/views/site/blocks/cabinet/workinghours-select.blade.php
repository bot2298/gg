<div class="mb-3 cabinet-form-workinghours-select-container">
    <div style="margin-right: 10px">{{$day}}</div>
    <div style="margin-right: 0px;" class="row pl-2">
        <div class="col-12 col-sm-6 mb-2 mb-sm-0">
            <span class="dossier-form-from-to">{{ __('cabinet.form.from', [], $lang) }}</span>
            <select name="{{$firstName}}" class="cabinet-form-workinghours-select col-sm-9 col-8">
                <option value selected>{{ __('forms.placeholder_choose', [], $lang) }}</option>
                @for($i = 0; $i <= 23; $i++)
                    <option {{ $firstOld == ($i > 9 ? $i : '0' . $i) ? 'selected' : ''}} value="{{ $i > 9 ? $i : '0' . $i}}">{{ $i > 9 ? $i : '0' . $i}}</option>
                @endfor
            </select>
        </div>
        <div class="col-12 col-sm-6">
            <span class="dossier-form-from-to">{{ __('cabinet.form.to', [], $lang) }}</span>
            <select name="{{$secondName}}" class="cabinet-form-workinghours-select col-sm-9 col-8">
                <option value selected>{{ __('forms.placeholder_choose', [], $lang) }}</option>
                @for($i = 0; $i <= 23; $i++)
                    <option {{ $secondOld == ($i > 9 ? $i : '0' . $i) ? 'selected' : ''}} value="{{ $i > 9 ? $i : '0' . $i}}">{{ $i > 9 ? $i : '0' . $i}}</option>
                @endfor
            </select>
        </div>
    </div>
</div>

