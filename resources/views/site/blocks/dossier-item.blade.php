<div class="col-12 col-lg-6 col-xl-4 mb-4 text-center">
    <div class="dossier-item mx-auto">
        <img class="dossier-item-img" src="{{ asset($img) }}" alt="">
        <div class="dossier-item-name">{{ $name }}</div>
        <div class="dossier-item-stats">
            <div class="dossier-item-stats-a">
                {{ $starsCount }}
            </div>
            <div class="dossier-item-stats-b">
                {{ $viewsCount }}
            </div>
        </div>
        <div class="dossier-item-address">
            <div>{{ $address1 }}</div>
            <div>{{ $address2 }}</div>
        </div>
        <a href="{{ $link }}" class="dossier-item-link">
            Переглянути
        </a>
        <a href="#" class="dossier-item-bookmark">
            Додати до закладок
        </a>
    </div>
</div>
