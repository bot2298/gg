<div class="col-md-5 col-12 dossier-single-field-name mb-md-3 mb-2">{{ __('cabinet.form.doc_photo', [], $locale)}} </div>
<div class="col-md-7 col-12 dossier-single-field-value mb-md-3 mb-4">
    <image-viewer :data="{{ json_encode(['dataImages' => $value]) }}"></image-viewer>
</div>
