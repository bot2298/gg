<section class="header-info-container dossier-header-info background-fluid mb-3 need-footer-separator">
    <div class="container">
        <div class="row">
            <div class="col-12 col-xl-3 pl-5 mb-3 mb-xl-0">
                <span class="dossier-header-caption">@lang('site.dossier.header_easy')</span>
            </div>
            <div class="col-12 col-md-4 col-xl-3 mb-3 mb-lg-0 text-center text-md-left">
                <img class="dossier-header-img mr-3 mr-md-0" src="{{ asset('img/dossier/01.png') }}" alt="">
                <span class="dossier-header-step">@lang('site.dossier.header1')</span>
            </div>
            <div class="col-12 col-md-4 col-xl-3 mt-auto mb-3 mb-lg-0 text-center text-md-left">
                <img class="dossier-header-img mr-3 mr-md-0" src="{{ asset('img/dossier/02.png') }}" alt="">
                <span class="dossier-header-step">@lang('site.dossier.header2')</span>
            </div>
            <div class="col-12 col-md-4 col-xl-3 mb-3 mb-lg-0 text-center text-md-left">
                <img class="dossier-header-img mr-3 mr-md-0" src="{{ asset('img/dossier/03.png') }}" alt="">
                <span class="dossier-header-step">@lang('site.dossier.header3')</span>
            </div>
        </div>
    </div>
</section>
<div class="container">
    @if (isset($specialization_name) || isset($region_id))
        {{ Breadcrumbs::render('dossier-cat', $cat_url) }}
    @else
        {{ Breadcrumbs::render('dossier') }}
    @endif
</div>
@isset($h1)
    <div class="container">
        <h1 class="site-h1">{{ $h1 }}</h1>
    </div>
@endisset
<section class="dossier-list-container">
    <div class="container" id="app">
        <dossiers :data="{{ json_encode(['countries' => $countries, 'specializations' => $specializations, 'region_id' => $region_id ?? null, 'specialization_name' => $specialization_name ?? null]) }}"></dossiers>
        @if(isset($specialization_name) && !isset($region_id))
            <div class="dropdown">
                <div class="cat-dropdown-placeholder" id="specDropdownMenu" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
                    {{ __('layout.subcats_regions') }}
                </div>
                <div class="cat-dropdown dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach(\App\Models\Page::where('specialization_id', \App\Models\Specialization::where('name', $specialization_name)->first()->id)->whereNotNull('region_id')->get() as $cat)
                        <a class="dossier-cat-link" href="{{ route('dossier.category', ['locale' => app()->getLocale(), 'cat' => $cat->location]) }}">{{ \App\Models\Page::getH1($cat->location) }}</a>
                    @endforeach
                </div>
            </div>
        @elseif (isset($region_id) && !isset($specialization_name))
            <div class="dropdown">
                <div class="cat-dropdown-placeholder" id="specDropdownMenu" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
                    {{ __('layout.subcats_specs') }}
                </div>
                <div class="cat-dropdown dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach(\App\Models\Page::where('region_id', $region_id)->whereNotNull('specialization_id')->get() as $cat)
                        <a class="dossier-cat-link" href="{{ route('dossier.category', ['locale' => app()->getLocale(), 'cat' => $cat->location]) }}">{{ \App\Models\Page::getH1($cat->location) }}</a>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
</section>
@include('site.modals.notification-component')
