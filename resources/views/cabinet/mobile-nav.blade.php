<div class="d-block d-lg-none sidebar-navigation-mobile">
    <div class="sidebar-navigation-mobile-container">
        <button type="button" class="close close-side-navigation fade-out" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <div class="sidebar-info mb-3 fade-out">
            <img class="lawyer-avatar mr-2" src="{{ auth()->user()->getCabinetPhoto() }}" alt="">
            <span class="lawyer-name">{{ auth()->user()->getCabinetName() }}</span>
        </div>
        <div class="sidebar-navigation fade-out">
            <ul class="cabinet-navigation">
                <li class="{{ Request::segment(3) === 'dossier' ?' active':'' }}">
                    <a href="{{ route('cabinet.dossier.index', app()->getLocale()) }}" class="cabinet-navigation-link">@lang('cabinet.navigation.dossier')</a>
                    <div class="counters-container text-right">
                        @if (auth()->user()->dossiers()->where('status', \App\Models\Dossier::STATUS_NEW)->first())
                            <div class="counter-yellow">&nbsp</div>
                        @endif
                        @if (auth()->user()->dossiers()->where('status', \App\Models\Dossier::STATUS_ACTIVE)->first())
                            <div class="counter-orange">&nbsp</div>
                        @endif
                        @if (auth()->user()->dossiers()->where('status', \App\Models\Dossier::STATUS_REJECTED)->first())
                            <div class="counter-red">&nbsp</div>
                        @endif
                    </div>
                </li>
                <li class="{{ Request::segment(3)=='request'?' active':'' }}">
                    <a href="{{ route('cabinet.request', app()->getLocale()) }}" class="cabinet-navigation-link">@lang('cabinet.navigation.requests')</a>
                    <div class="counters-container text-right">
                        @if ($lessThan60 = \App\Models\Application::getCountLessThan60Mins())
                            <div class="counter-red">{{ $lessThan60 }}</div>
                        @endif

                        @if ($count = \App\Models\Application::getCountNew())
                            <div class="counter-orange">{{ $count }}</div>
                        @endif
                    </div>
                </li>
{{--                <li class="{{ Route::is('cabinet.bookmarks')?' active':'' }}">--}}
{{--                    <a href="{{ route('cabinet.bookmarks', app()->getLocale()) }}" class="cabinet-navigation-link">@lang('cabinet.navigation.bookmarks')</a>--}}
{{--                    <div class="counters-container text-right">--}}
{{--                        @if ($count = \App\Models\ApplicationBookmark::getCount())--}}
{{--                            <div class="counter-orange">{{ $count }}</div>--}}
{{--                    @endif--}}
{{--                    </div>--}}
{{--                </li>--}}
                <li class="{{ Request::segment(3)=='mail'?' active':'' }}">
                    <a href="{{ route('cabinet.mail', app()->getLocale()) }}" class="cabinet-navigation-link">@lang('cabinet.navigation.messages')</a>
                    <div class="counters-container text-right">
                        @if ($count = auth()->user()->getUnreadMessages()->count())
                            <div class="counter-orange">{{ $count }}</div>
                        @endif
                    </div>
                </li>
                <li class="{{ Request::segment(3)=='rating'?' active':'' }}">
                    <a href="{{ route('cabinet.rating', app()->getLocale()) }}" class="cabinet-navigation-link">@lang('cabinet.navigation.ratings')</a>
                    <div class="counters-container text-right">
                        @if ($count = \App\Models\Rating::getCountNew())
                            <div class="counter-orange">{{ $count }}</div>
                        @endif
                    </div>
                </li>
                <li class="{{ Request::segment(3)=='settings'?' active':'' }}">
                    <a href="{{ route('cabinet.settings', app()->getLocale()) }}" class="cabinet-navigation-link">@lang('cabinet.navigation.settings')</a>
                </li>
{{--                <li>--}}
{{--                    <form id="logout-form2" action="{{ route('logout', app()->getLocale()) }}" method="POST" style="display: none;">--}}
{{--                        @csrf--}}
{{--                    </form>--}}
{{--                    <a onclick="event.preventDefault(); document.getElementById('logout-form2').submit();"--}}
{{--                       href="{{ route('logout', app()->getLocale()) }}" class="cabinet-navigation-link">@lang('cabinet.navigation.logout')</a>--}}
{{--                </li>--}}
            </ul>
        </div>
    </div>
</div>
