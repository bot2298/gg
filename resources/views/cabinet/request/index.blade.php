<section class="cabinet-container need-footer-separator">
    <div class="container">
        <div class="row">
            @include('cabinet.sidebar')
            <div class="col-12 col-lg-8 col-xl-9 cabinet-delimiter">
                <div class="cabinet-request-container">
                    <div class="row mb-3{{ !count(auth()->user()->getActiveRegions()) ? ' d-none': null }}">
                        <div class="col-12 col-md-4 text-center text-md-left mb-3 mb-md-0">
                            <label class="checkbox-container cabinet-checkbox-select">
                                <input type="checkbox" onchange="select_all(this)">
                                <span class="checkmark"></span>
                            </label>
                            <span class="cabinet-checkbox-select"></span>
                            <div class="dropdown cabinet-select-dropdown">
                                <span class="cabinet-checkbox-expand dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false"></span>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <span class="dropdown-item" onclick="checkbox_mass(1)">@lang('cabinet.request.check_all')</span>
                                    <span class="dropdown-item" onclick="checkbox_mass(2)">@lang('cabinet.request.check_read')</span>
                                    <span class="dropdown-item" onclick="checkbox_mass(3)">@lang('cabinet.request.check_unread')</span>
                                    <span class="dropdown-item" onclick="checkbox_mass(4)">@lang('cabinet.request.check_response')</span>
                                    <span class="dropdown-item" onclick="checkbox_mass(5)">@lang('cabinet.request.check_noresponse')</span>
                                    <span class="dropdown-item" onclick="checkbox_mass(6)">@lang('cabinet.request.check_actual')</span>
                                    <span class="dropdown-item" onclick="checkbox_mass(7)">@lang('cabinet.request.check_notactual')</span>
                                </div>
                                <script>
                                    function select_all(checkbox){
                                        $('.table-row .checkbox-container input').prop('checked', checkbox.checked);
                                        if(checkbox.checked){
                                            $('.table-row').addClass('selected');
                                        } else $('.table-row').removeClass('selected');
                                    }

                                    function checkbox_mass(type){

                                        function reset(){
                                            $('.cabinet-checkbox-select input').prop('checked', false);
                                            $('.table-row .checkbox-container input').prop('checked', false);
                                            $('.table-row').removeClass('selected');
                                        }

                                        function all_checked(){
                                            return $('.table-row .checkbox-container input').length == $('.table-row .checkbox-container input:checked').length
                                        }
                                        switch(type){
                                            case 1: $('.cabinet-checkbox-select input').prop('checked', true); $('.table-row .checkbox-container input').prop('checked', true); $('.table-row').addClass('selected'); break;
                                            case 2: reset(); $('.table-row.active .checkbox-container input').prop('checked', true); $('.table-row.active').addClass('selected'); break;
                                            case 3: reset(); $('.table-row:not(.active) .checkbox-container input').prop('checked', true); $('.table-row:not(.active)').addClass('selected');break;
                                            case 4: reset(); $('.table-row.has-response .checkbox-container input').prop('checked', true); $('.table-row.has-response').addClass('selected');break;
                                            case 5: reset(); $('.table-row:not(.has-response) .checkbox-container input').prop('checked', true); $('.table-row:not(.has-response)').addClass('selected');break;
                                            case 6: reset(); $('.table-row.actual .checkbox-container input').prop('checked', true);$('.table-row.actual').addClass('selected');break;
                                            case 7: reset(); $('.table-row:not(.actual) .checkbox-container input').prop('checked', true);$('.table-row:not(.actual)').addClass('selected');break;
                                            default: break;
                                        }
                                        if(all_checked()){
                                            $('.cabinet-checkbox-select input').prop('checked', true);
                                        } else $('.cabinet-checkbox-select input').prop('checked', false);
                                    }
                                </script>
                            </div>
                            <span class="cabinet-delete-icon ml-4" id="delete-multiple" data-confirm="@lang('cabinet.request.delete_confirm')">
                                <img src="{{ asset('img/cabinet/delete.svg') }}" alt="">
                                <form method="POST" id="mass-delete-form" action="{{ route('cabinet.request.delete', app()->getLocale()) }}">
                                    @csrf
                                </form>
                            </span>
                        </div>
                        <div class="col-12 col-md-8">
                            <div class="row">
                                <div class="col-12 col-sm-6 mb-3 mb-sm-0">
                                    <select data-minimum-results-for-search="Infinity" onchange="filter()" name="" id="requests-filter"  class="dossier-menu-select filters-custom-select pl-1" data-placeholder="@lang('cabinet.request.filter_placeholder')">
                                        <option></option>
                                        <option value="default" {{ request()->query('filter') === 'default'?'selected':null }}>@lang('cabinet.request.filter_all')</option>
                                        <option value="in_bookmarks" {{ request()->query('filter') === 'in_bookmarks'?'selected':null }}>@lang('cabinet.request.filter_in_bookmarks')</option>
                                        <option value="seen" {{ request()->query('filter') === 'seen'?'selected':null }}>@lang('cabinet.request.filter_read')</option>
                                        <option value="not_seen" {{ request()->query('filter') === 'not_seen'?'selected':null }}>@lang('cabinet.request.filter_unread')</option>
                                        <option value="response" {{ request()->query('filter') === 'response'?'selected':null }}>@lang('cabinet.request.filter_response')</option>
                                        <option value="no_response" {{ request()->query('filter') === 'no_response'?'selected':null }}>@lang('cabinet.request.filter_noresponse')</option>
                                        <option value="actual" {{ request()->query('filter') === 'actual'?'selected':null }}>@lang('cabinet.request.filter_actual')</option>
                                        <option value="not_actual" {{ request()->query('filter') === 'not_actual'?'selected':null }}>@lang('cabinet.request.filter_notactual')</option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-6 mb-3 mb-sm-0">
                                    <select data-minimum-results-for-search="Infinity" onchange="filter()" name="" id="requests-sort" class="dossier-menu-select pl-1 filters-custom-select" data-placeholder="@lang('cabinet.request.sort_placeholder')">
                                        <option></option>
                                        <option value="created_desc" {{ request()->query('sort') === 'created_desc'?'selected':null }}>@lang('cabinet.request.sort_created_desc')</option>
                                        <option value="created_asc" {{ request()->query('sort') === 'created_asc'?'selected':null }}>@lang('cabinet.request.sort_created_asc')</option>
                                        <option value="cost_desc" {{ request()->query('sort') === 'cost_desc'?'selected':null }}>@lang('cabinet.request.sort_cost_desc')</option>
                                        <option value="cost_asc" {{ request()->query('sort') === 'cost_asc'?'selected':null }}>@lang('cabinet.request.sort_cost_asc')</option>
                                        <option value="deadline_desc" {{ request()->query('sort') === 'deadline_desc'?'selected':null }}>@lang('cabinet.request.sort_deadline_desc')</option>
                                        <option value="deadline_asc" {{ request()->query('sort') === 'deadline_asc'?'selected':null }}>@lang('cabinet.request.sort_deadline_asc')</option>
                                    </select>
                                </div>
                            </div>
                            <script>
                                function encodeQueryData(data) {
                                    const ret = [];
                                    for (let d in data)
                                        ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
                                    return ret.join('&');
                                }

                                function filter(){
                                    let data = encodeQueryData({
                                        'filter': document.getElementById('requests-filter').value,
                                        'sort': document.getElementById('requests-sort').value
                                    });
                                    window.location.href = '{{ route('cabinet.request', app()->getLocale()) }}' + '/?' + data;
                                }
                            </script>
                        </div>
                    </div>
                    <form method="POST" id="delete-form" action="{{ route('cabinet.request.delete', app()->getLocale()) }}">
                        @csrf
                        <input id="input-id" name="id" type="hidden">
                    </form>
                    <script>
                        function deleteSingle(element){
                            let form = $('#delete-form');
                            form.find('input#input-id').val($(element).parent().data('id'));
                            form.submit();
                        }
                        function toggleSelect(element){
                            $(element).parent().toggleClass('selected');
                            let checkbox = $(element).siblings('.table-checkmark').find('input');
                            checkbox.prop('checked', !checkbox.prop('checked'));
                        }
                    </script>
                    @if (!count(auth()->user()->getActiveRegions()))
                        <div class="text-center">@lang('cabinet.settings.access_empty', ['link' => route('cabinet.access', app()->getLocale())])</a></div>
                    @else
                        @forelse ($applications as $application)
                            @component('cabinet.request.table-row')
                                @slot('id', $application->id)
                                @slot('name', $application->name)
                                @slot('cost', $application->cost)
                                @slot('currency', $application->currency->getName())
                                @slot('country', $application->region->country->name)
                                @slot('region', $application->region->name)
                                @slot('city', $application->city)
                                @slot('text', $application->content)
                                @slot('time', $application->timeCreated())
                                @slot('date', $application->dateCreated())
                                @slot('deadline', $application->getDeadline())
                                @slot('visited', auth()->user()->isApplicationSeen($application->id))
                                @slot('in_bookmarks', $application->inBookmarks())
                                @slot('has_response', $application->hasResponse())
                                @slot('urgent', $application->lessThan60Mins())
                            @endcomponent
                        @empty
                            <div class="text-center">@lang('cabinet.request.filter_empty')</div>
                        @endforelse
                    @endif
                </div>
            </div>
            <div class="col-12 col-lg-9 offset-0 offset-lg-3 mt-5 text-center">
                {{ $applications->appends(request()->except('page'))->links('cabinet.pagination') }}
            </div>
        </div>
    </div>
</section>
@include('site.modals.report-application')
@include('site.modals.report-result')
