<section class="cabinet-container need-footer-separator">
    <div class="container">
        <div class="row">
            @include('cabinet.sidebar')
            <div class="col-12 col-lg-8 col-xl-9 cabinet-delimiter">
                <div class="cabinet-request-single">
                    <div class="mt-3 mb-3">
                        <a class="dossier-navigation" href="{{ route('cabinet.request', app()->getLocale()) }}">
                            <img style="transform: rotate(180deg); width: 36px;" src="/img/tracker/next.png" alt="">
                        </a>
                    </div>
                    <div class="cabinet-request-header">
                        <div class="row">
                            <div class="col-12 col-xl-7 text-center text-xl-left my-auto">
                                <div class="cabinet-request-user mb-2">
                                    {{ $application->name }},
                                    <span class="cabinet-request-cost">{{ $application->cost }} {{ $application->currency->getName() }},</span>
                                    @lang('cabinet.request.seeking_lawyer'): {{ __('countries.'.$application->region->country->name) }}, {{ __('regions.'.$application->region->name) }}{{ is_null($application->city)?'':', '.__('cities.'.$application->city->name)}}
                                    @if($mins = $application->lessThan60Mins())
                                        <span class="table-urgent">@lang('cabinet.request.30mins', ['mins' => $mins])</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-12 col-xl-5 text-center text-xl-right my-auto">
                                <div class="cabinet-request-info">
                                    @if (($deadline = $application->getDeadline()) !== false)
                                        @if($deadline < Carbon\Carbon::now())
                                            <span class="cabinet-request-deadline deadline-past mr-4">@lang('cabinet.request.not_actual')</span>
                                        @else
                                            <span class="cabinet-request-deadline{{ $application->lessThan60Mins() ? ' deadline-past': null }} mr-4">@lang('cabinet.request.actual_til') {{ $deadline->format('H:i d.m.Y') }}</span>
                                        @endif
                                    @endif
                                    <span class="cabinet-request-time mr-2">{{ $application->timeCreated() }}</span>
                                    <span class="cabinet-request-date">{{ $application->dateCreated() }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cabinet-request-header">
                        <div class="row">
                            <div class="col-12 col-xl-6 text-center text-xl-left my-auto">
                                <div class="cabinet-request-contacts">
{{--                                    <span>{{ $application->email }}</span>--}}
                                    @if ($application->phone)
                                        <span>{{ $application->phone }}</span>
                                    @endif
                                    @if ($application->specialization)
                                        <span>{{ __('specializations.'.$application->specialization->name) }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-12 col-xl-6 my-auto cabinet-request-bookmark text-center text-xl-right {{ $application->inBookmarks()?'in-bookmarks':null}}" data-locale="{{ app()->getLocale() }}" data-id="{{ $application->id }}">
                                @if ($application->phone)
                                    <span class="cabinet-request-callme">@lang('cabinet.request.call')</span>
                                @endif

                                <a class="application-toggle-bookmark" href="javascript:;">
                                    <img style="width: 22px;" class="add-to-bookmarks" src="{{ asset('img/cabinet/bookmark.png') }}" alt="@lang('cabinet.request.add_to_bookmarks')">
                                    <img style="width: 22px;" class="remove-from-bookmarks" src="{{ asset('img/cabinet/bookmark_fill.png') }}" alt="@lang('cabinet.request.remove_from_bookmarks')">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="cabinet-request-text">
                        {{ $application->content }}
                    </div>
                    @if($application->attachment)
                        <div class="mb-2"><a class="cabinet-request-attachment" target="_blank" href="{{ Storage::url($application->attachment) }}">@lang('cabinet.request.download_attachment')</a></div>
                    @endif
                    <a class="cabinet-request-report" onclick="$('#application-report-modal').modal('show'); $('#modal-application-id').val({{$application->id}})" href="javascript:;">@lang('cabinet.request.complaint')</a>
                    <div class="text-center mt-3 mt-sm-0">
                        @if($chat == null)
                            <div class="text-center mt-3 mt-sm-0">
                                <button class="cabinet-request-reply">
                                    @lang('cabinet.request.respond')
                                </button>
                            </div>
                        @else
                            <div class="text-center mt-3 mt-sm-0">
                                <a href="../mail?chat={{ $chat->id }}" style="color: black;">
                                    @lang('cabinet.request.go_to_chat')
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            @if($application->status === \App\Models\Application::STATUS_ACTIVE)
            <div id="cabinet-reply-block" class="col-12 col-lg-9 offset-0 offset-lg-3">
                <div class="cabinet-reply-form">
                    <form action="{{ route('start.chat', ['application' => $application->id, 'user' => auth()->id()] )}}" method="post">
                        @csrf
                        <div class="cabinet-textarea-container">
                            <img class="textarea-attach d-none" src="http://gg/img/request/attach.png" alt="">
{{--                            <span class="textarea-replyto">{{ $application->email }}</span>--}}
                            <textarea class="cabinet-reply-textarea" rows="5" name="reply-message" placeholder="@lang('cabinet.request.form_placeholder')"></textarea>
                            <button class="cabinet-reply-submit" type="submit">@lang('cabinet.request.form_submit')</button>
                        </div>
                    </form>
                </div>
            </div>
            @endif
        </div>
    </div>
</section>
@include('site.modals.report-application')
@include('site.modals.report-result')
