@if ($paginator->hasPages())
    <nav class="pagination">
        <div class="container">
            <div class="row">
                <div class="col-6 col-md-2 order-1 text-left">
                    @if (!$paginator->onFirstPage())
                        <a class="pagination-navigation-link" href="{{ $paginator->previousPageUrl() }}"><img style="transform: rotate(180deg);" src="{{ asset('img/tracker/next.png') }}" alt=""></a>
                    @endif
                </div>

                <div class="col-12 col-md-8 order-3 order-md-2 text-center my-auto">
                    <ul class="pagination-ul">
                        <?php
                        $start = $paginator->currentPage() - 1; // show 2 pagination links before current
                        $end = $paginator->currentPage() + 1; // show2 pagination links after current
                        if($start < 1) {
                            $start = 1; // reset start to 1
                            $end += 1;
                        }
                        if($end >= $paginator->lastPage() ) $end = $paginator->lastPage(); // reset end to last page
                        ?>

                        @if($start > 1)
                            <li class="pagination-link mr-2 mr-sm-3"><a href="{{ $paginator->url(1) }}">{{1}}</a></li>
                            @if($paginator->currentPage() != 3)
                                {{-- "Three Dots" Separator --}}
                                <li class="pagination-3-dots mr-2 mr-sm-3" style="vertical-align: bottom; height: 100%;">
                                    <svg style="vertical-align: bottom" width="39" height="7" viewBox="0 0 39 7" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect width="7" height="7" fill="#EC9C23"/>
                                        <rect x="16" width="7" height="7" fill="#EC9C23"/>
                                        <rect x="32" width="7" height="7" fill="#EC9C23"/>
                                    </svg>
                                </li>
                            @endif
                        @endif
                        @for ($i = $start; $i <= $end; $i++)
                            <li class="pagination-link{{$i != $paginator->lastPage()?' mr-2 mr-sm-3':null}}{{ ($paginator->currentPage() == $i) ? ' active' : '' }}"><a href="{{ $paginator->url($i) }}">{{$i}}</a></li>
                        @endfor
                        @if($end < $paginator->lastPage())
                            @if($paginator->currentPage() + 2 != $paginator->lastPage())
                                {{-- "Three Dots" Separator --}}
                                <li class="pagination-3-dots mr-2 mr-sm-3" style="vertical-align: bottom; height: 100%;">
                                    <svg style="vertical-align: bottom" width="39" height="7" viewBox="0 0 39 7" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect width="7" height="7" fill="#EC9C23"/>
                                        <rect x="16" width="7" height="7" fill="#EC9C23"/>
                                        <rect x="32" width="7" height="7" fill="#EC9C23"/>
                                    </svg>
                                </li>
                            @endif
                            <li class="pagination-link mr-2 mr-sm-3"><a href="{{ $paginator->url($paginator->lastPage()) }}">{{$paginator->lastPage()}}</a></li>
                        @endif
                    </ul>
                </div>
                <div class="col-6 col-md-2 order-2 order-md-3 text-right">
                    @if ($paginator->hasMorePages())
                        <a class="pagination-navigation-link" href="{{ $paginator->nextPageUrl() }}"><img src="{{ asset('img/tracker/next.png') }}" alt=""></a>
                    @endif
                </div>
            </div>
        </div>
    </nav>
@endif
