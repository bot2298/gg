<section class="cabinet-container need-footer-separator">
    <div class="container">
        <div class="row" id="app">
            @include('cabinet.sidebar')
            <div class="col-12 col-lg-8 col-xl-9 cabinet-delimiter">
                <div class="cabinet-settings-container">
                    @include('cabinet.settings.navigation')
                    <div class="cabinet-access-about">
                        <p class="mb-3">
                            @lang('cabinet.settings.access_about1')
                        </p>
{{--                        <p>--}}
{{--                            @lang('cabinet.settings.access_about2')--}}
{{--                        </p>--}}
{{--                        <p class="mb-3">--}}
{{--                            @lang('cabinet.settings.access_about3')--}}
{{--                        </p>--}}
{{--                        <p class="mb-3">--}}
{{--                            @lang('cabinet.settings.access_about4')--}}
{{--                        </p>--}}
{{--                        <ul class="access-about-list mb-3">--}}
{{--                            <li>--}}
{{--                                @lang('cabinet.settings.access_discount1')--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                @lang('cabinet.settings.access_discount2')--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                @lang('cabinet.settings.access_discount3')--}}
{{--                            </li>--}}
{{--                        </ul>--}}
                        <p>
                            {{ __('cabinet.settings.access_about_temp1') }}
                        </p>
                    </div>
                    <cabinet-settings-access :data="{{ json_encode(['countries' => $countries->where('active', 1)]) }}"></cabinet-settings-access>
                </div>
            </div>
        </div>
    </div>
</section>
@include('site.modals.notification-component')
