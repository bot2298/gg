<section class="cabinet-container need-footer-separator">
    <div class="container">
        <div class="row">
            @include('cabinet.sidebar')
            <div class="col-12 col-lg-8 col-xl-9 cabinet-delimiter">
                <div class="cabinet-settings-container">
                    @include('cabinet.settings.navigation')
                    <div class="col-12 col-lg-6 col-xl-4 text-center text-lg-left cabinet-settings-form">
                        <form method="POST" action="{{ route('cabinet.settings', app()->getLocale()) }}">
                            @csrf
                            <div class="settings-email-input mx-auto mx-xl-0 mb-3">
                                <label for="settings-email">@lang('cabinet.settings.login')</label>
                                <input readonly="readonly" class="@error('email') is-invalid @enderror" name="email" id="settings-email" type="email" value="{{ Auth::user()->email }}">
                                <img src="{{ asset('img/cabinet/edit.png') }}" alt="">
                                @error('email')
                                    <span class="cabinet-validation-error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="settings-password-input mb-4">
                                <label for="settings-password">@lang('cabinet.settings.password')</label>
                                <div class="position-relative">
                                    <input style="padding-right: 30px;" class="@error('password') is-invalid @enderror" name="password" id="settings-password" type="password" placeholder="@lang('cabinet.settings.placeholder_old_password')">
                                    <span style="top: 50%; transform: translateY(-50%)" class="password-toggle"></span>
                                </div>
                                @error('password')
                                    <span class="cabinet-validation-error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="mb-4">
                                <div class="position-relative">
                                    <input style="padding-right: 30px;" class="@error('newpassword') is-invalid @enderror" name="newpassword" id="settings-password-new" type="password" placeholder="@lang('cabinet.settings.placeholder_new_password')">
                                    <span style="top: 50%; transform: translateY(-50%)" class="password-toggle"></span>
                                </div>
                                @error('newpassword')
                                    <span class="cabinet-validation-error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="position-relative mb-5">
                                <input style="padding-right: 30px;" class="@error('newpassword') is-invalid @enderror" name="newpassword_confirmation" id="settings-password-new_repeat" type="password" placeholder="@lang('cabinet.settings.placeholder_new_password_confirmation')">
                                <span style="top: 50%; transform: translateY(-50%)" class="password-toggle"></span>
                            </div>
                            <div class="text-center">
                                <button type="submit">@lang('cabinet.settings.save')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
