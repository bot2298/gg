<div class="row access-row mb-2">
    <div class="col-1 d-none d-xl-block">
        {{ $num }}
    </div>
    <div class="col-12 col-xl-4 mb-2 mb-xl-0">
        {{ __('countries.'.$country) }}, {{ __('regions.'.$region) }}
{{--        ({{ $cities ?? __('cabinet.settings.access_cities_all') }})--}}
    </div>
    <div class="col-12 col-sm-6 col-xl-4 mb-2 mb-xl-0 text-center text-sm-left access-status-name{{ $active ? ' active':null }}" data-id="{{ $id }}">
        @if ($active->gt(\Illuminate\Support\Carbon::now()))
            <span class="access-status-active">@lang('cabinet.settings.access_status_active') {{ $active->format('d.m.Y') }}</span>
        @else
            <span class="access-status-notactive">@lang('cabinet.settings.access_status_notactive')</span>
        @endif
    </div>
    <div class="col-12 col-sm-6 col-xl-3 mb-2 mb-xl-0 text-center text-sm-right">
{{--        <form class="access-actions-form" method="POST" action="{{ route('cabinet.access.delete', app()->getLocale()) }}">--}}
{{--            @csrf--}}
{{--            <input name="id" type="hidden" value="{{ $id }}">--}}
{{--            <a class="mr-2 mr-md-0 mr-xl-1" href="{{ route('cabinet.access.update', ['locale'=> app()->getLocale(), 'access' => $id]) }}">--}}
{{--                <img class="access-history-action" src="{{ asset('img/cabinet/edit.png') }}" alt="@lang('cabinet.settings.access_edit')">--}}
{{--            </a>--}}
            <a class="access-status mr-2 mr-md-0{{ $active ? ' active':null }}" href="javascript:;">
                @if ($active->gt(\Illuminate\Support\Carbon::now()))
                    <span onclick="$('#access-id').val({{$id}}); $('#access-status-toggle-form').submit()" class="access-status-notactive">{{ __('cabinet.settings.access_history_stop') }}</span>
                @else
                    <span onclick="$('#access-id').val({{$id}}); $('#access-status-toggle-form').submit()" class="access-status-active">{{ __('cabinet.settings.access_history_renew') }}</span>
                @endif
            </a>
{{--            <a class="mr-2 mr-md-0" onclick="if(confirm('@lang('cabinet.settings.access_delete_confirm')')) this.parentElement.submit()" href="javascript:;">--}}
{{--                <img style="height: 28px;" class="access-history-action" src="{{ asset('img/cabinet/delete.png') }}" alt="@lang('cabinet.settings.access_delete')">--}}
{{--            </a>--}}
{{--        </form>--}}
    </div>
</div>

