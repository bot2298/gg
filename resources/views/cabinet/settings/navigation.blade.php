<nav>
    <ul class="cabinet-settings-navigation text-center text-lg-left">
        <li class="cabinet-settings-link d-block d-sm-inline-block text-center text-sm-left mr-0 mr-sm-3{{ Route::is('cabinet.settings')?' active':null }}"><a href="{{ route('cabinet.settings', app()->getLocale()) }}">@lang('cabinet.settings.nav_password')</a></li>
        <li class="cabinet-settings-link d-block d-sm-inline-block text-center text-sm-left mr-0 mr-sm-3{{ Route::is('cabinet.access') || Route::is('cabinet.access.edit')?' active':null }}"><a href="{{ route('cabinet.access', app()->getLocale()) }}">@lang('cabinet.settings.nav_access')</a></li>
        <li class="cabinet-settings-link d-block d-sm-inline-block text-center text-sm-left mr-0 mr-sm-3{{ Route::is('cabinet.access-history')?' active':null }}"><a href="{{ route('cabinet.access-history', app()->getLocale()) }}">@lang('cabinet.settings.nav_access_history')</a></li>
    </ul>
</nav>
