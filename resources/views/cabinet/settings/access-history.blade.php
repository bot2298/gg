<section class="cabinet-container need-footer-separator">
    <div class="container">
        <div class="row">
            @include('cabinet.sidebar')
            <div class="col-12 col-lg-8 col-xl-9 cabinet-delimiter">
                <div class="cabinet-settings-container">
                    @include('cabinet.settings.navigation')
                    <div class="cabinet-access-selected mt-3">
                        @if(count($access))
                            <div class="row mb-3">
                                <div class="d-none d-xl-block col-xl-1 cabinet-access-header">#</div>
                                <div class="d-none d-xl-block col-xl-4 cabinet-access-header">@lang('cabinet.settings.access_history_region')</div>
                                <div class="d-none d-xl-block col-xl-4 cabinet-access-header">@lang('cabinet.settings.access_history_status')</div>
                                <div class="d-none d-xl-block col-xl-3"></div>
                            </div>
                            <script>
                                function accessToggle(id, element) {
                                    $.post( "/{{ app()->getLocale() }}/cabinet/settings/access/toggle", { id: id})
                                        .done(function( data ) {
                                            element.classList.toggle('active');
                                            $('.access-status-name[data-id="'+id+'"]').toggleClass('active');
                                        });
                                }
                            </script>
                            <form style="display: none" id="access-status-toggle-form" method="POST" action="{{ route('cabinet.access.toggle_new', app()->getLocale()) }}">
                                @csrf
                                <input id="access-id" name="id" type="hidden" value="">
                            </form>
                            @for ($i = 0; $i < count($access); $i++)
                                @component('cabinet.settings.access-history-single')
                                    @slot('id', $access[$i]->id)
                                    @slot('num', $i+1)
                                    @slot('country', $access[$i]->region->country->name)
                                    @slot('region', $access[$i]->region->name)
                                    @slot('cities', $access[$i]->getCities())
                                    @slot('active', $access[$i]->active_until)
                                @endcomponent
                            @endfor
                        @else
                            <div class="text-center">@lang('cabinet.settings.access_empty', ['link' => route('cabinet.access', app()->getLocale())])</a></div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('site.modals.notification-component')
