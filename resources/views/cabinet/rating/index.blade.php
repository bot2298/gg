<section class="cabinet-container need-footer-separator">
    <div class="container">
        <div class="row">
            @include('cabinet.sidebar')
            <div class="col-12 col-lg-8 col-xl-9 cabinet-delimiter">
                <div class="cabinet-request-container">
                    @forelse ($ratings as $rating)
                        @component('cabinet.rating.table-row')
                            @slot('id', $rating->id)
                            @slot('email', $rating->client_email)
                            @slot('stars', $rating->stars)
                            @slot('text', $rating->comment)
                            @slot('time', $rating->timeCreated())
                            @slot('date', $rating->dateCreated())
                            @slot('visited', auth()->user()->isRatingSeen($rating->id))
                        @endcomponent
                    @empty
                        <div class="text-center">@lang('cabinet.request.rating_empty')</div>
                    @endforelse
                </div>
            </div>
            <div class="col-12 col-lg-9 offset-0 offset-lg-3 mt-5 text-center">
                {{ $ratings->appends(request()->except('page'))->links('cabinet.pagination') }}
            </div>
        </div>
    </div>
</section>
@include('site.modals.report-rating')
@include('site.modals.report-result')
