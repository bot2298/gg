<div style="margin-bottom: 10px" class="row table-row{{$visited ? ' active': null }}">
    <div data-locale="{{ app()->getLocale() }}" data-id="{{ $id }}" class="col-12 col-sm-10 pbt-10 rating-main-content">
        <div class="table-user-info mb-2">
            <span>
                @for($i=0; $i < $stars; $i++)
                    <span style="font-size:18px;" class="icon">★</span>
                @endfor
            </span>
        </div>
        <div class="table-text">
            {{ $text }}
        </div>
    </div>
    <div class="col-12 col-sm-2 pbt-10 my-auto">
        <div class="table-request-info text-right">
            <div class="cabinet-request-time">
                {{ $time }}
            </div>
            <div>
                {{ $date }}
            </div>
        </div>
        <div class="table-request-actions text-right" data-id="{{ $id }}">
            <a onclick="$('#rating-report-modal').modal('show'); $('#modal-rating-id').val({{$id}})" href="javascript:;"><img src="{{ asset('img/cabinet/report.png') }}" alt="Поскаржитись"></a>
        </div>
    </div>
</div>
