<section class="cabinet-container need-footer-separator">
    <div class="container">
        <div class="row">
            @include('cabinet.sidebar')
            <div class="col-12 col-lg-8 col-xl-9 cabinet-delimiter">
                <div class="cabinet-request-single">
                    <div class="mt-3 mb-3">
                        <a class="dossier-navigation" href="{{ url()->route('cabinet.rating', app()->getLocale()) }}">
                            <img style="transform: rotate(180deg); width: 36px;" src="/img/tracker/next.png" alt="">
                        </a>
                    </div>
                    <div class="cabinet-request-header">
                        <div class="row">
                            <div class="col-12 col-xl-7 text-center text-xl-left my-auto">
                                <div class="cabinet-request-user">
                                    @for($i=0; $i < $rating->stars; $i++)
                                        <span style="color: #EC9C23; font-size:22px;" class="icon">★</span>
                                    @endfor
                                </div>
                            </div>
                            <div class="col-12 col-xl-5 text-center text-xl-right my-auto">
                                <div class="cabinet-request-info">
                                    <span class="cabinet-request-time mr-2">{{ $rating->timeCreated() }}</span>
                                    <span class="cabinet-request-date">{{ $rating->dateCreated() }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cabinet-request-text">
                        {{ $rating->comment }}
                    </div>
                    <a onclick="$('#rating-report-modal').modal('show'); $('#modal-rating-id').val({{$rating->id}})" href="javascript:;" class="cabinet-request-report">Поскаржитись</a
                </div>
            </div>
        </div>
    </div>
</section>
@include('site.modals.report-rating')
@include('site.modals.report-result')
