<div class="row table-row{{$visited ? ' active': null }}{{$deadline ? ' actual':null}}{{$has_response ? ' has-response':null}}">
    <a type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="pl-1 request-item-more-actions d-xl-none"></a>
    <div style="min-width: unset; max-width: 150px" class="select-dropdown dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
        <a onclick="toggleSelect(this.parentElement)" style="padding: 2px 8px" class="dropdown-item" href="javascript:;">@lang('cabinet.request.select')</a>
        <a onclick="toggleSelect(this.parentElement)" style="padding: 2px 8px" class="dropdown-item" href="javascript:;">@lang('cabinet.request.unselect')</a>
    </div>
    <div class="d-none d-xl-block col-xl-1 order-2 order-xl-1 table-checkmark text-center">
        <label class="checkbox-container">
            <input type="checkbox">
            <span class="checkmark"></span>
        </label>
    </div>
    <div data-id="{{ $id }}" data-locale="{{app()->getLocale()}}" class="col-12 col-xl-9 order-1 order-xl-2 pbt-10 request-main-content">
        <div class="table-user-info mb-2">
            {{ $name }},
            <span>{{ $cost }} {{ $currency }},</span>
            @lang('cabinet.request.seeking_lawyer'): {{ __('countries.'.$country) }}, {{ __('regions.'.$region) }}{{ is_null($city)?'':', '.__('cities.'.$city->name) }}
            @if($urgent)
                <span class="table-urgent">@lang('cabinet.request.30mins', ['mins' => $urgent])</span>
            @endif
            @if ($has_response)
                <img class="d-inline-block d-sm-none" style="vertical-align: top; width: 24px;" src="{{ asset('img/cabinet/reply.png') }}" alt="">
            @endif
            <img style="width: 20px" src="{{ asset('img/cabinet/bookmark_fill.png') }}" alt="">
        </div>
        <div class="table-text">
            {{ $text }}
        </div>
    </div>
    <div class="col-12 col-xl-2 order-3 order-xl-3 pbt-10">
        <div class="table-request-info text-right flex-row flex-xl-column justify-content-around justify-content-xl-end">
            @if ($has_response)
                <span class="d-none d-sm-block request-has-reply"></span>
            @endif
            <div class="cabinet-request-time">
                {{ $time }}
            </div>
            <div>
                {{ $date }}
            </div>
            <div>
                @if ($deadline !== false)
                    <span class="{{ $deadline < Carbon\Carbon::now() ?'deadline-past':null }}">{{ $deadline < Carbon\Carbon::now() ? __('cabinet.request.not_actual') : __('cabinet.request.actual_til')." ".$deadline->format('H:i d.m.Y') }}</span>
                @endif
            </div>
        </div>
        <div data-id="{{ $id }}" data-locale="{{app()->getLocale()}}" class="table-request-actions justify-content-center justify-content-xl-end text-right {{ $in_bookmarks ? 'in-bookmarks' : null }}">
            <a onclick="$('#application-report-modal').modal('show'); $('#modal-application-id').val({{$id}})" href="javascript:;"><img src="{{ asset('img/cabinet/report.png') }}" alt="@lang('cabinet.request.complaint')"></a>
            <a onclick="event.preventDefault(); deleteSingle(this)" href="javascript:;"><img src="{{ asset('img/cabinet/delete.png') }}" alt="@lang('cabinet.request.delete')"></a>
        </div>
    </div>
</div>
