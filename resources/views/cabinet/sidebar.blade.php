<div class="col-12 col-lg-4 col-xl-3 cabinet-footer-to-bottom">
    <div class="d-none d-lg-block sidebar-info mb-3">
        <img class="lawyer-avatar mr-2" src="{{ auth()->user()->getCabinetPhoto() }}" alt="">
        <span class="lawyer-name">{{ auth()->user()->getCabinetName()}}</span>
    </div>
    <div class="d-none d-lg-block sidebar-navigation text-center text-lg-left">
        <ul class="cabinet-navigation">
            <li class="{{ Request::segment(3) === 'dossier' ? ' active':'' }}">
                <a href="{{ route('cabinet.dossier.index', app()->getLocale()) }}" class="cabinet-navigation-link">@lang('cabinet.navigation.dossier')</a>
                <div class="counters-container text-right">
                    @if (auth()->user()->dossiers()->where('status', \App\Models\Dossier::STATUS_NEW)->first())
                        <div class="counter-yellow">&nbsp</div>
                    @endif
                    @if (auth()->user()->dossiers()->where('status', \App\Models\Dossier::STATUS_ACTIVE)->first())
                        <div class="counter-orange">&nbsp</div>
                    @endif
                    @if (auth()->user()->dossiers()->where('status', \App\Models\Dossier::STATUS_REJECTED)->first())
                        <div class="counter-red">&nbsp</div>
                    @endif
                </div>
            </li>
            <li class="{{ Request::segment(3)=='request'?' active':'' }}">
                <a href="{{ route('cabinet.request', app()->getLocale()) }}" class="cabinet-navigation-link">@lang('cabinet.navigation.requests')</a>
                <div class="counters-container text-right">
                    @if ($lessThan60 = \App\Models\Application::getCountLessThan60Mins())
                        <div class="counter-red">{{ $lessThan60 }}</div>
                    @endif

                    @if ($count = \App\Models\Application::getCountNew())
                        <div class="counter-orange">{{ $count }}</div>
                    @endif
                </div>
            </li>
{{--            <li class="{{ Route::is('cabinet.bookmarks')?' active':'' }}">--}}
{{--                <a href="{{ route('cabinet.bookmarks', app()->getLocale()) }}" class="cabinet-navigation-link">@lang('cabinet.navigation.bookmarks')</a>--}}
{{--                <div class="counters-container text-right">--}}
{{--                    @if ($count = \App\Models\ApplicationBookmark::getCount())--}}
{{--                        <div class="counter-orange">{{ $count }}</div>--}}
{{--                @endif--}}
{{--                </div>--}}
{{--            </li>--}}
            <li class="{{ Request::segment(3)=='mail'?' active':'' }}">
                <a href="{{ route('cabinet.mail', app()->getLocale()) }}" class="cabinet-navigation-link">@lang('cabinet.navigation.messages')</a>
                <div class="counters-container text-right">
                    @if ($count = auth()->user()->getUnreadMessages()->count())
                        <div class="counter-orange">{{ $count }}</div>
                    @endif
                </div>
            </li>
            <li class="{{ Request::segment(3)=='rating'?' active':'' }}">
                <a href="{{ route('cabinet.rating', app()->getLocale()) }}" class="cabinet-navigation-link">@lang('cabinet.navigation.ratings')</a>
                <div class="counters-container text-right">
                    @if ($count = \App\Models\Rating::getCountNew())
                        <div class="counter-orange">{{ $count }}</div>
                    @endif
                </div>
            </li>
            <li class="{{ Request::segment(3)=='settings'?' active':'' }}">
                <a href="{{ route('cabinet.settings', app()->getLocale()) }}" class="cabinet-navigation-link">@lang('cabinet.navigation.settings')</a>
            </li>
{{--            <li>--}}
{{--                <form id="logout-form" action="{{ route('logout', app()->getLocale()) }}" method="POST" style="display: none;">--}}
{{--                    @csrf--}}
{{--                </form>--}}
{{--                <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();"--}}
{{--                   href="{{ route('logout', app()->getLocale()) }}" class="cabinet-navigation-link">@lang('cabinet.navigation.logout')</a>--}}
{{--            </li>--}}
        </ul>
    </div>
</div>
