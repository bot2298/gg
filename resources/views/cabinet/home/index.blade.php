<section class="cabinet-container need-footer-separator">
    <div class="container">
        <div class="row">
            @include('cabinet.sidebar')
            <div class="col-12 col-lg-8 col-xl-9 cabinet-delimiter">
                <div class="cabinet-index-container">
                    @php($lang = \App\Models\Dossier::firstFreeLang())
                    @if($lang || count(\App\Models\Dossier::all()) < 1)
                        <div class="col-12 text-center mb-3">
                            <a href="{{ route('cabinet.dossier.create', ['locale' => app()->getLocale(), 'lang' => $lang ? $lang : app()->getLocale()]) }}">
                                <button class="dossier-cabinet-add-dossier">
                                    {{ auth()->user()->dossiers->count() ? __('cabinet.form_index.create_another_lang'): __('cabinet.form_index.create_new') }}
                                </button>
                            </a>
                        </div>
                    @endif
                    <ul class="cabinet-index-about">
                        <li class="cabinet-index-bigstep cabinet-index-bigstep-1">
                            @lang('cabinet.index.marker1_1')
                        </li>
                        <li class="cabinet-index-step">
                            @lang('cabinet.index.marker1_2')
                        </li>
                        <li class="cabinet-index-bigstep cabinet-index-bigstep-2">
                            @lang('cabinet.index.marker2_1')
                        </li>
                        <li class="cabinet-index-step">
                            @lang('cabinet.index.marker2_2')
                        </li>
                        <li class="cabinet-index-step">
                            @lang('cabinet.index.marker2_3')
                        </li>
                        <li class="cabinet-index-bigstep cabinet-index-bigstep-3">
                            @lang('cabinet.index.marker3')
                        </li>
                        <li class="cabinet-index-bigstep cabinet-index-bigstep-4">
                            @lang('cabinet.index.marker4_1')
                        </li>
                        <li class="cabinet-index-step">
                            @lang('cabinet.index.marker4_2')
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@include('site.modals.notification-component')
