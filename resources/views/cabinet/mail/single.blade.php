<section class="cabinet-container need-footer-separator">
    <div class="container">
        <div class="row">
            @include('cabinet.sidebar')
            <div class="col-12 col-lg-8 col-xl-9 cabinet-delimiter">
                <div class="cabinet-request-single">
                    <div class="mt-3 mb-3">
                        <a class="dossier-navigation" href="{{ route('cabinet.mail', app()->getLocale()) }}">
                            <img style="transform: rotate(180deg); width: 36px;" src="/img/tracker/next.png" alt="">
                        </a>
                    </div>
                    <div class="cabinet-request-header">
                        <div class="row">
                            <div class="col-12 col-xl-4 text-center my-auto">
                                <span class="cabinet-mail-contact">nyra77@gmail.com</span>
                            </div>
                            <div class="col-12 col-xl-4 text-center my-auto">
                                <span class="cabinet-mail-contact">+380 50 698 78 52</span>
                            </div>
                            <div class="col-12 col-xl-4 text-center my-auto">
                                <div class="cabinet-request-info">
                                    <span class="cabinet-request-time mr-2">18:05</span>
                                    <span class="cabinet-request-date">21.01.2020</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cabinet-request-text">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla euismod auctor ex suscipit auctor. Sed scelerisque, odio eu laoreet semper, enim nulla tincidunt tortor, a egestas diam quam tempus quam. Aliquam auctor, ligula vitae venenatis dignissim, magna arcu pellentesque purus, eget ornare tellus nulla at lectus. Nunc ut mi suscipit, tempus est nec, tempus quam. Etiam ac augue vel lorem eleifend tincidunt. Vivamus semper convallis mattis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                        </p>
                        <p>
                            Mauris auctor eros a ex tristique mollis. Vestibulum ut tortor et diam aliquam bibendum eget sed ante. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse faucibus sem et massa dapibus, non elementum felis auctor. Nunc vel dui in mi vestibulum ornare ut ac elit. Etiam sit amet odio ac dui porttitor tempus. Phasellus bibendum sapien felis, nec pulvinar arcu fermentum quis. Nulla fringilla nibh tincidunt, ultricies sapien a, finibus elit.
                        </p>
                    </div>
                    <a class="cabinet-request-report" href="#">Поскаржитись</a>
                    <div class="text-center mt-3 mt-sm-0">
                        <button class="cabinet-request-reply active">
                            Відповісти
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-9 offset-0 offset-lg-3">
                <div class="cabinet-reply-form">
                    <form action="" method="post">
                        @csrf
                        <div class="cabinet-textarea-container">
                            <img class="textarea-attach" src="http://gg/img/request/attach.png" alt="">
                            <span class="textarea-replyto">nyra77@gmail.com</span>
                            <textarea class="cabinet-reply-textarea" rows="5" name="reply-message" placeholder="Відповісти..."></textarea>
                            <button class="cabinet-reply-submit" type="submit">Відправити</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
