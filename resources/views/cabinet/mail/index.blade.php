<section class="cabinet-container need-footer-separator">
    <div class="container" id="app">
        <div class="row">
            @include('cabinet.sidebar')
            <div class="col-12 col-lg-8 col-xl-9 cabinet-delimiter">
                <div class="cabinet-request-container pl-0">
                    @if($responseListData->all())
                        <cabinet-chat-item-container :data="{{ json_encode($responseListData) }}"></cabinet-chat-item-container>
                    @else
                        <div class="text-center">@lang('cabinet.request.chats_empty')</div>
                    @endif
                </div>
            </div>
            <div class="col-12 col-lg-9 offset-0 offset-lg-3 mt-5">
                {{ $responseListData->links('cabinet.pagination') }}
            </div>
        </div>
        @include('site.modals.report-application')
    </div>
</section>
