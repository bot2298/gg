<script>var approveDossier;</script>
@if ($crud->hasAccess('create') && $entry->status !== \App\Models\Dossier::STATUS_ACTIVE)
    <a href="javascript:void(0)"
       onclick="approveDossier(this)"
       data-route="{{ url($crud->route.'/'.$entry->getKey().'/approve') }}"
       class="btn btn-xs btn-default"
       data-button-type="approve-dossier">
        <i class="fa fa-check"></i>
        Схвалити
    </a>
@endif

<script>
    function approveClosure() {
        if (typeof approveDossier != 'function') {
            $("[data-button-type=approve-dossier]").unbind('click');

            approveDossier = function(button) {
                // ask for confirmation before deleting an item
                // e.preventDefault();
                var button = $(button);
                var route = button.attr('data-route');

                $.ajax({
                    url: route,
                    type: 'POST',
                    success: function(result) {
                        // Show an alert with the result
                        new PNotify({
                            title: "Done",
                            text: "",
                            type: "success"
                        });

                        // Hide the modal, if any
                        $('.modal').modal('hide');
                        if(typeof(crud) !== 'undefined'){
                            crud.table.ajax.reload();
                        } else location.reload();

                    },
                    error: function(result) {
                        // Show an alert with the result
                        new PNotify({
                            title: "Operation failed",
                            text: "",
                            type: "warning"
                        });
                    }
                });
            }
        }
    }
    if(typeof jQuery !== 'undefined'){
        $(function(){
            approveClosure()
        });
    } else {
        window.addEventListener('load', function() {
            approveClosure();
        })
    }


    // make it so that the function above is run after each DataTable draw event
    // crud.addFunctionToDataTablesDrawEventQueue('cloneEntry');
</script>
