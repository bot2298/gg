@if ($crud->hasAccess('create'))

    <a href="javascript:void(0)"
       onclick="blockEntry(this)"
       data-route="{{ url('/admin/rating/'.($entry->rating ? $entry->rating->id : $entry->getKey()).'/toggle-block') }}"
       class="btn btn-xs btn-default"
       data-button-type="block">
        <i class="fa fa-ban"></i>
            @if($entry->rating)
                {{ $entry->rating->blocked_at ? ' Розблокувати': ' Заблокувати' }}
            @else
                {{ $entry->blocked_at ? ' Розблокувати': ' Заблокувати' }}
            @endif
    </a>
@endif

<script>
    if (typeof blockEntry != 'function') {
        $("[data-button-type=block]").unbind('click');

        function blockEntry(button) {
            // ask for confirmation before deleting an item
            // e.preventDefault();
            var button = $(button);
            var route = button.attr('data-route');

            $.ajax({
                url: route,
                type: 'POST',
                success: function(result) {
                    // Show an alert with the result
                    new PNotify({
                        title: "Done",
                        text: "",
                        type: "success"
                    });

                    // Hide the modal, if any
                    $('.modal').modal('hide');

                    crud.table.ajax.reload();
                },
                error: function(result) {
                    // Show an alert with the result
                    new PNotify({
                        title: "Operation failed",
                        text: "",
                        type: "warning"
                    });
                }
            });
        }
    }
</script>
