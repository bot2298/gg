@if ($crud->hasAccess('create'))

    <a href="javascript:void(0)"
       onclick="toggleStatus(this)"
       data-route="{{ url('/admin/country/'.$entry->getKey().'/toggle-status') }}"
       class="btn btn-xs btn-default"
       data-button-type="block">
        <i class="fa fa-ban"></i>
        Змінити статус
    </a>
@endif

<script>

    if (typeof toggleStatus != 'function') {
        $("[data-button-type=block]").unbind('click');

        function toggleStatus(button) {
            // ask for confirmation before deleting an item
            // e.preventDefault();
            var button = $(button);
            var route = button.attr('data-route');

            $.ajax({
                url: route,
                type: 'POST',
                success: function(result) {
                    // Show an alert with the result
                    new PNotify({
                        title: "Done",
                        text: "",
                        type: "success"
                    });

                    // Hide the modal, if any
                    $('.modal').modal('hide');

                    crud.table.ajax.reload();
                },
                error: function(result) {
                    // Show an alert with the result
                    new PNotify({
                        title: "Operation failed",
                        text: "",
                        type: "warning"
                    });
                }
            });
        }
    }

    // make it so that the function above is run after each DataTable draw event
    // crud.addFunctionToDataTablesDrawEventQueue('cloneEntry');
</script>
