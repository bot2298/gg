@if ($crud->hasAccess('create'))

        <a href="javascript:void(0)"
           onclick="blockEntry(this)"
           data-route="{{ url('/admin/user/'. ($entry->user ? $entry->user->id : $entry->getKey()).'/toggle-block') }}"
           class="btn btn-xs btn-default"
           data-button-type="block">
            <i class="fa fa-ban"></i>
            @if($entry->user)
                {{ $entry->user->blocked_at ? ' Розблокувати (адвокат і всі досьє)': ' Заблокувати (адвокат і всі досьє)' }}
            @else
                {{ $entry->blocked_at ? ' Розблокувати (адвокат і всі досьє)': ' Заблокувати (адвокат і всі досьє)' }}
            @endif
        </a>
@endif

<script>

    if (typeof blockEntry != 'function') {
        $("[data-button-type=block]").unbind('click');

        function blockEntry(button) {
            // ask for confirmation before deleting an item
            // e.preventDefault();
            var button = $(button);
            var route = button.attr('data-route');

            $.ajax({
                url: route,
                type: 'POST',
                success: function(result) {
                    // Show an alert with the result
                    new PNotify({
                        title: "Done",
                        text: "",
                        type: "success"
                    });

                    // Hide the modal, if any
                    $('.modal').modal('hide');

                    crud.table.ajax.reload();
                },
                error: function(result) {
                    // Show an alert with the result
                    new PNotify({
                        title: "Operation failed",
                        text: "",
                        type: "warning"
                    });
                }
            });
        }
    }

    // make it so that the function above is run after each DataTable draw event
    // crud.addFunctionToDataTablesDrawEventQueue('cloneEntry');
</script>
