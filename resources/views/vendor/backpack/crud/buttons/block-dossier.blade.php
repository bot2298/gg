<script>var blockDossier;</script>
@if ($crud->hasAccess('create') && $entry->status !== \App\Models\Dossier::STATUS_REJECTED)
    <a href="javascript:void(0)"
       onclick="blockDossier(this)"
       data-route="{{ url($crud->route.'/'.$entry->getKey().'/block') }}"
       class="btn btn-xs btn-default"
       data-button-type="block-dossier">
        <i class="fa fa-ban"></i>
        Відхилити
    </a>
@endif

<script>
    function blockClosure(){
        if (typeof blockDossier != 'function') {
            $("[data-button-type=block-dossier]").unbind('click');

            blockDossier = function(button) {
                // ask for confirmation before deleting an item
                // e.preventDefault();
                var button = $(button);
                var route = button.attr('data-route');

                $.ajax({
                    url: route,
                    type: 'POST',
                    success: function(result) {
                        // Show an alert with the result
                        new PNotify({
                            title: "Done",
                            text: "",
                            type: "success"
                        });

                        // Hide the modal, if any
                        $('.modal').modal('hide');
                        if(typeof(crud) !== 'undefined'){
                            crud.table.ajax.reload();
                        } else location.reload();
                    },
                    error: function(result) {
                        // Show an alert with the result
                        new PNotify({
                            title: "Operation failed",
                            text: "",
                            type: "warning"
                        });
                    }
                });
            }
        }
    }
    if(typeof jQuery !== 'undefined'){
        $(function(){
            blockClosure();
        });
    } else window.addEventListener('load', function() {
        blockClosure();
    });

    // make it so that the function above is run after each DataTable draw event
    // crud.addFunctionToDataTablesDrawEventQueue('cloneEntry');
</script>
