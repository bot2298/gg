<div class="m-t-10 m-b-10 p-l-10 p-r-10 p-t-10 p-b-10">
	<div class="row">
		<div class="col-md-12">
            @if($entry->specialization)
                <div><b>Спеціалізація: </b> {{ __('specializations.'.$entry->specialization->name) }}</div><br />
            @endif
            <div style="max-width: 500px; white-space: break-spaces;">{{ $entry->content }}</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
