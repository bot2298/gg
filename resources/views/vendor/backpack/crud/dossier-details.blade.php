<div class="m-t-10 m-b-10 p-l-10 p-r-10 p-t-10 p-b-10">
    <div class="row">
        <div class="col-md-12">
            <style>
                .admin-dossier-img{
                    width: 200px;
                    margin-bottom: 10px;
                }
                .admin-dossier-img img{
                    width: 100%;
                }
                .admin-dossier-field{
                    margin-bottom: 10px;
                }
                .admin-dossier-field__name{
                    font-weight: bold;
                }
                .admin-dossier-socials img{
                    width: 40px;
                }
            </style>
            <div class="admin-dossier-img mb-3">
                <img class="w-100" src="{{ url($entry->general->photo) }}" alt="">
            </div>
            <div class="admin-dossier-field">
                <div class="admin-dossier-field__name">Ім'я</div>
                <div>{{ $entry->general->fullname }}</div>
            </div>
            @if ($entry->overview)
                @if($entry->overview['languages'])
                    <div class="admin-dossier-field">
                        <div class="admin-dossier-field__name">Вільне володіння мовами</div>
                        <div class="">{{ $entry->overview['languages'] }}</div>
                    </div>
                @endif
                @if($entry->overview['strong_sides'])
                    <div class="admin-dossier-field">
                        <div class="admin-dossier-field__name">Сильні якості</div>
                        <div class="">{{ $entry->overview['strong_sides'] }}</div>
                    </div>
                @endif
                @if($entry->overview['hobbies'])
                    <div class="admin-dossier-field">
                        <div class="admin-dossier-field__name">Хоббі</div>
                        <div class="">{{ $entry->overview['hobbies'] }}</div>
                    </div>
                @endif
                @if($entry->overview['motto'])
                    <div class="admin-dossier-field">
                        <div class="admin-dossier-field__name">Девіз</div>
                        <div class="">{{ $entry->overview['motto'] }}</div>
                    </div>
                @endif
            @endif

            <div class="admin-dossier-field">
                <div class="admin-dossier-field__name">Адреса офісу</div>
                <div class="">{{ $entry->general->office['country'] }}</div>
                <div class="">{{ $entry->general->office['city'] }}</div>
                @if($entry->general->office['address'])
                    <div class="admin-dossier-field__name">Офіси</div>
                    @foreach($entry->general->office['address'] as $address)
                        <div class="law-text-gray mb-3">{{ $address }}</div>
                    @endforeach
                @endif
            </div>

            @if($entry->contacts)
                @if($entry->contacts['working_hours'])
                    <div class="admin-dossier-field">
                        <div class="admin-dossier-field__name">Робочий час</div>
                        @if($entry->contacts['working_hours']['mo']['start'])
                            <div class="">пн {{ $entry->contacts['working_hours']['mo']['start'] }}
                                - {{ $entry->contacts['working_hours']['mo']['end'] }}</div>
                        @endif
                        @if($entry->contacts['working_hours']['tu']['start'])
                            <div class="">вт {{ $entry->contacts['working_hours']['tu']['start'] }}
                                - {{ $entry->contacts['working_hours']['tu']['end'] }}</div>
                        @endif
                        @if($entry->contacts['working_hours']['we']['start'])
                            <div class="">ср {{ $entry->contacts['working_hours']['we']['start'] }}
                                - {{ $entry->contacts['working_hours']['we']['end'] }}</div>
                        @endif
                        @if($entry->contacts['working_hours']['th']['start'])
                            <div class="">чт {{ $entry->contacts['working_hours']['th']['start'] }}
                                - {{ $entry->contacts['working_hours']['th']['end'] }}</div>
                        @endif
                        @if($entry->contacts['working_hours']['fr']['start'])
                            <div class="">пт {{ $entry->contacts['working_hours']['fr']['start'] }}
                                - {{ $entry->contacts['working_hours']['fr']['end'] }}</div>
                        @endif
                        @if($entry->contacts['working_hours']['sa']['start'])
                            <div class="">сб {{ $entry->contacts['working_hours']['sa']['start'] }}
                                - {{ $entry->contacts['working_hours']['sa']['end'] }}</div>
                        @endif
                        @if($entry->contacts['working_hours']['su']['start'])
                            <div class="">нд {{ $entry->contacts['working_hours']['su']['start'] }}
                                - {{ $entry->contacts['working_hours']['su']['end'] }}</div>
                        @endif
                    </div>
                @endif
            @endif
            @if($entry->general->office['phones'][0])
                <div class="admin-dossier-field">
                    <div class="admin-dossier-field__name">Телефон</div>
                    @foreach($entry->general->office['phones'] as $phone)
                        <div>{{ $phone['number'] }}</div>
                        <div class="dossier-single-socials mb-3">
                            <a href="#"><img class="mr-1" src="/img/socials/viber.png" alt=""></a>
                            <a href="#"><img class="mr-1" src="/img/socials/whatsapp.png" alt=""></a>
                            <a href="#"><img class="mr-1" src="/img/socials/telegram.png" alt=""></a>
                            <a href="#"><img src="/img/socials/skype.png" alt=""></a>
                        </div>
                    @endforeach
                </div>
            @endif

            @if($entry->contacts)
                @if($entry->contacts['emails'][0])
                    <div class="admin-dossier-field">
                        <div class="admin-dossier-field__name">Email</div>
                        @foreach($entry->contacts['emails'] as $email)
                            <div class="mb-3">{{ $email }}</div>
                        @endforeach
                    </div>
                @endif
            @endif

            @if($entry->overview)
                @if($entry->overview['site'])
                    <div class="admin-dossier-field">
                        <div class="admin-dossier-field__name">Web-сайт</div>
                        <div>{{ $entry->overview['site'] }}</div>
                    </div>
                @endif
                    <div class="admin-dossier-field">
                        <div class="admin-dossier-field__name">Соціальні мережі</div>
                        @if($entry->overview['facebook'])
                            <div class="admin-dossier-socials">
                                <img src="/img/socials/facebook.png" alt="">
                                <span>{{ $entry->overview['facebook'] }}</span>
                            </div>
                        @endif
                        @if($entry->overview['instagram'])
                            <div class="admin-dossier-socials">
                                <img src="/img/socials/youtube.png" alt="">
                                <span>{{ $entry->overview['instagram'] }}</span>
                            </div>
                        @endif
                        @if($entry->overview['linkedin'])
                            <div class="admin-dossier-socials">
                                <img src="/img/socials/youtube.png" alt="">
                                <span>{{ $entry->overview['linkedin'] }}</span>
                            </div>
                        @endif
                        @if ($entry->overview['youtube'])
                            <div class="admin-dossier-socials">
                                <img src="/img/socials/youtube.png" alt="">
                                <span>{{ $entry->overview['youtube'] }}</span>
                            </div>
                        @endif
                        @if ($entry->overview['twitter'])
                            <div class="admin-dossier-socials">
                                <img src="/img/socials/youtube.png" alt="">
                                <span>{{ $entry->overview['twitter'] }}</span>
                            </div>
                        @endif
                    </div>
            @endif

            <div class="admin-dossier-field">
                <div class="admin-dossier-field__name">Свідоцтво про право на адвокатську діяльність</div>
                <div class="admin-dossier-field__name">Країна отримання</div>
                <div>{{ $entry->license['country'] }}</div>
                <div class="admin-dossier-field__name">Орган, що видав</div>
                <div>{{ $entry->license['institution'] }}</div>
                <div class="admin-dossier-field__name">Рік отримання</div>
                <div>{{ $entry->license['year'] }}</div>
                <div class="admin-dossier-field__name">Номер свідоцтва</div>
                <div>{{ $entry->license['number'] }}</div>
                <div class="admin-dossier-field__name">Фото документа</div>
                <img src="{{ url($entry->license['photo']) }}" alt="">
            </div>

            <div class="admin-dossier-actions">
                @include('vendor.backpack.crud.buttons.block-dossier')
            </div>

        </div>
    </div>
</div>
<div class="clearfix"></div>
