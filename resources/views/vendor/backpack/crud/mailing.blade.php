@extends('backpack::layout')

@section('before_styles')
    <link rel="stylesheet" href="/vendor/adminlte/bower_components/select2/dist/css/select2.min.css">
@endsection

@section('after_scripts')
    <script src="/vendor/adminlte/bower_components/select2/dist/js/select2.full.js"></script>
    <script src="{{ asset('js/main.js') }}"></script>
@endsection

@section('header')
    <section class="content-header">
        <h1>
            <span class="text-capitalize">Розсилка</span>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
            <li><a href="{{ url(config('backpack.base.route_prefix'), 'chat') }}" class="text-capitalize">Повідомлення</a></li>
            <li class="active">Створення розсилки</li>
        </ol>
    </section>
@endsection

@section('content')
        <a href="{{ url(config('backpack.base.route_prefix'), 'chat') }}" class="hidden-print"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>Повідомлення</span></a>

    <div class="row m-t-20">
        <div class="">
            <!-- Default box -->

{{--            @include('crud::inc.grouped_errors')--}}

            <form method="post"
                  action="{{ route('mailing.store') }}"
{{--                  @if ($crud->hasUploadFields('create'))--}}
{{--                  enctype="multipart/form-data"--}}
{{--                @endif--}}
            >
                {!! csrf_field() !!}
                <div class="col-md-12 col-lg-8">

                    <div class="row">
                        <div class="col-xs-4" style="margin-bottom: 10px;">
                            <label>Країна</label>
                            <select id="country-select" data-locale="{{app()->getLocale()}}" class="request-form-select custom-select w-100 input-outlined @error('country_id') is-invalid @enderror" name="country_id" data-placeholder="@lang('forms.placeholder_choose')" data-old="{{request()->query('country')}}">
                                <option></option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}">{{ __('countries.'.$country->name) }}</option>
                                @endforeach
                            </select>
                            @error('country_id')
                                <span class="cabinet-validation-error">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="col-xs-4" style="margin-bottom: 10px;">
                            <label>Регіон</label>
                            <select id="region-select" data-locale="{{app()->getLocale()}}" class="request-form-select custom-select w-100 input-outlined @error('region_id') is-invalid @enderror" name="region_id" data-placeholder="@lang('forms.placeholder_choose')" data-old="{{request()->query('region')}}" disabled>
                                <option></option>
                            </select>
                        </div>
                        <div class="col-xs-4" style="margin-bottom: 10px;">
                            <label>Місто</label>
                            <select id="city-select" data-locale="{{app()->getLocale()}}" class="request-form-select custom-select w-100 @error('city') is-invalid @enderror" name="city_id" data-placeholder="@lang('forms.placeholder_enter')" data-old="{{request()->query('city')}}" disabled>
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group col-xs-12">
                            <select name="locale" id="">
                                @foreach (config('app.locales') as $locale => $name)
                                    <option value="{{ $locale }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Повідомлення</label>
                            <textarea rows="8" name="message" class="form-control"></textarea>
                            @error('message')
                            <span class="cabinet-validation-error">{{ $message }}</span>
                            @enderror
                        </div>
                    </div><!-- /.box-body -->
                    <div class="">
                        <button type="submit" class="btn btn-success">
                            <span>Розіслати</span>
                        </button>
                    </div><!-- /.box-footer-->

                </div><!-- /.box -->
            </form>
        </div>
    </div>

@endsection
