<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<!-- Users, Roles, Permissions -->
@role('Адміністратор')
<li class="treeview">
    <a href="#">
        <i class="fa fa-key"></i>
        <span>Дозволи</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('manager') }}"><i class="fa fa-users"></i> <span>Користувачі</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="fa fa-user-o"></i> <span>Ролі</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="fa fa-key"></i> <span>Дозволи</span></a></li>
    </ul>
</li>
<!-- Regions/Countries -->
<li class="treeview">
    <a href="#">
        <i class="fa fa-map-o"></i>
        <span>Регіони</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href='{{ backpack_url('country') }}'><i class=''></i> <span>Країни</span></a></li>
        <li><a href='{{ backpack_url('region') }}'><i class=''></i> <span>Регіони</span></a></li>
        <li><a href='{{ backpack_url('city') }}'><i class=''></i> <span>Міста</span></a></li>
    </ul>
</li>
@endrole
<!-- Lawyers -->
@canany(['Адвокати.Статистика', 'Адвокати.Заявки', 'Адвокати.Профілі', 'Адвокати.Розсилка-повідомлень', 'Адвокати.Скарги-на-адвокатів'])
<li class="treeview">
    <a href="#">
        <i class="fa fa-user-circle"></i>
        <span>Адвокати</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        @can('Адвокати.Статистика')
            <li><a href='{{ backpack_url('dossiers/stats') }}'><i class=''></i> <span>Статистика</span></a></li>
        @endcan
        @can('Адвокати.Заявки')
            <li>
                <a href='{{ backpack_url('dossier') }}'><i class=''></i>
                    <span>Заявки</span>
                    @if ($count = \App\Models\Dossier::getNewCount())
                        <span class="pull-right-container">
                        <small class="label pull-right bg-yellow">{{ $count }}</small>
                    </span>
                    @endif
                </a>
            </li>
        @endcan
        @can('Адвокати.Профілі')
            <li><a href='{{ backpack_url('user') }}'><i class=''></i> <span>Профілі</span></a></li>
            <li><a href='{{ backpack_url('card') }}'><i class=''></i> <span>Візитки</span></a></li>
        @endcan
        @can('Адвокати.Розсилка-повідомлень')
            <li><a href='{{ backpack_url('chat') }}'><i class=''></i> <span>Розсилка повідомлень</span></a></li>
        @endcan
        @can('Адвокати.Скарги-на-адвокатів')
            <li>
                <a href='{{ backpack_url('lawyer-complaint') }}'><i class=''></i>
                    <span>Скарги на адвокатів</span>
                    @if ($count = \App\Models\Complaint::getNewCount(\App\Models\Complaint::TYPE_LAWYER))
                        <span class="pull-right-container">
                        <small class="label pull-right bg-yellow">{{ $count }}</small>
                    </span>
                    @endif
                </a>
            </li>
        @endcan
    </ul>
</li>
@endcanany
<!-- Clients -->
@canany(['Клієнти.Статистика', 'Клієнти.Запити', 'Клієнти.Скарги-на-запити', 'Клієнти.Оцінки-та-коментарі', 'Клієнти.Скарги-на-оцінки-та-коментарі'])
    <li class="treeview">
        <a href="#">
            <i class="fa fa-users"></i>
            <span>Клієнти</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            @can('Клієнти.Статистика')
                <li><a href='{{ backpack_url('application/stats') }}'><i class=''></i> <span>Статистика</span></a></li>
            @endcan
            @can('Клієнти.Запити')
                <li><a href='{{ backpack_url('application') }}'><i class=''></i> <span>Запити {{-- \App\Models\Application::getActiveCount() ? '('.\App\Models\Application::getActiveCount().')' : '' --}}</span></a></li>
            @endcan
            @can('Клієнти.Скарги-на-запити')
                <li>
                    <a href='{{ backpack_url('application-complaint') }}'><i class=''></i>
                        <span>Скарги на запити</span>
                        @if ($count = \App\Models\Complaint::getNewCount(\App\Models\Complaint::TYPE_APPLICATION))
                            <span class="pull-right-container">
                        <small class="label pull-right bg-yellow">{{ $count }}</small>
                    </span>
                        @endif
                    </a>
                </li>
            @endcan
            @can('Клієнти.Оцінки-та-коментарі')
                <li><a href='{{ backpack_url('rating') }}'><i class=''></i> <span>Оцінки та коментарі</span></a></li>
            @endcan
            @can('Клієнти.Скарги-на-оцінки-та-коментарі')
                <li>
                    <a href='{{ backpack_url('rating-complaint') }}'><i class=''></i>
                        <span>Скарги на оцінки та коментарі</span>
                        @if ($count = \App\Models\Complaint::getNewCount(\App\Models\Complaint::TYPE_RATING))
                            <span class="pull-right-container">
                        <small class="label pull-right bg-yellow">{{ $count }}</small>
                    </span>
                        @endif
                    </a>
                </li>
            @endcan
        </ul>
    </li>
@endcanany

@can('SEO')
    <li class="treeview">
        <a href="#">
            <i class="fa fa-google"></i>
            <span>SEO</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
            <li><a href='{{ backpack_url('page') }}'><i class=''></i> <span>Сторінки</span></a></li>
            <li><a href='{{ backpack_url('pageinfo') }}'><i class=''></i> <span>Мета теги</span></a></li>
        </ul>
    </li>
@endcan

@can('Переклади')
    <li><a href="{{ backpack_url('translations') }}"><i class="fa fa-language"></i> <span>Переклади</span></a></li>
@endcan

@can('Фідбек')
    <li>
        <a href="{{ backpack_url('feedback') }}"><i class="fa fa-comments"></i>
            <span>Фідбек</span>
            @if ($count = \App\Models\Feedback::getNewCount())
                <span class="pull-right-container">
                        <small class="label pull-right bg-yellow">{{ $count }}</small>
                    </span>
            @endif
        </a>
    </li>
@endcan

@can('Валюти')
    <li><a href="{{ backpack_url('currency') }}"><i class="fa fa-money"></i> <span>Валюти</span></a></li>
@endcan




