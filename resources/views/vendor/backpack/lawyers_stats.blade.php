@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            Статистика заявок
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
            <li class="active">Статистика досьє</li>
        </ol>
    </section>
@endsection

@section('before_styles')
    <link rel="stylesheet" href="/vendor/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="/vendor/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="/vendor/adminlte/bower_components/select2/dist/css/select2.min.css">
@endsection

@section('after_scripts')
    <script src="/vendor/adminlte/bower_components/moment/min/moment.min.js"></script>
    <script src="/vendor/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="/vendor/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="/vendor/adminlte/bower_components/select2/dist/js/select2.full.js"></script>
{{--    <script src="/vendor/adminlte/bower_components/chart.js/Chart.js"></script>--}}
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="{{ asset('js/main.js') }}"></script>
@endsection

@section('content')

    <script>
        var monthgroup = @json($monthgroup, JSON_PRETTY_PRINT);
        var daygroup = @json($daygroup, JSON_PRETTY_PRINT);
        var count = Object.values(monthgroup).reduce((a, b) => a + b, 0);
        console.log(count);
        console.log(monthgroup);
        console.log(daygroup);
    </script>

    <div style="margin-bottom: 20px;" class="row justify-content-center">
        <div class="col-12 col-md-offset-3 col-md-4">
            <div class="form-group">
                <label>Інтервал:</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="date-range">
                </div>
                <!-- /.input group -->
            </div>
            <script>
                function encodeQueryData(data) {
                    const ret = [];
                    for (let d in data)
                        ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
                    return ret.join('&');
                }

                function filter(){
                    let date_start = $('#date-range').data('daterangepicker').startDate.format("DD/MM/YYYY"),
                        date_end = $('#date-range').data('daterangepicker').endDate.format("DD/MM/YYYY"),
                        country = document.getElementById('country-select').value,
                        region = document.getElementById('region-select').value,
                        city = document.getElementById('city-select').value,
                        statuses = $('#status-multi-select').val(),
                        data = encodeQueryData({
                            'from': date_start,
                            'to': date_end,
                            'country': country,
                            'region': region,
                            'city': city,
                            'statuses': statuses.join()
                        });
                        window.location.href = '{{ route('dossier-stats') }}' + '/?' + data;
                }
            </script>
            <div style="margin-bottom: 10px;">
                <label>Країна</label>
                <select id="country-select" data-locale="{{app()->getLocale()}}" class="request-form-select custom-select w-100 input-outlined @error('country_id') is-invalid @enderror" name="country_id" data-placeholder="@lang('forms.placeholder_choose')" data-old="{{request()->query('country')}}">
                    <option></option>
                    @foreach ($countries as $country)
                        <option value="{{ $country->id }}">{{ __('countries.'.$country->name) }}</option>
                    @endforeach
                </select>
            </div>
            <div style="margin-bottom: 10px;">
                <label>Регіон</label>
                <select id="region-select" data-locale="{{app()->getLocale()}}" class="request-form-select custom-select w-100 input-outlined @error('region_id') is-invalid @enderror" name="region_id" data-placeholder="@lang('forms.placeholder_choose')" data-old="{{request()->query('region')}}" disabled>
                    <option></option>
                </select>
            </div>
            <div style="margin-bottom: 10px;">
                <label>Місто</label>
                <select id="city-select" data-locale="{{app()->getLocale()}}" class="request-form-select custom-select w-100 @error('city') is-invalid @enderror" name="city_id" data-placeholder="@lang('forms.placeholder_enter')" data-old="{{request()->query('city')}}" disabled>
                    <option></option>
                </select>
            </div>
            <div class="form-group">
                <label>Статуси</label>
                <select id="status-multi-select" multiple="" class="form-control">
                    <option value="{{ \App\Models\Dossier::STATUS_SAVED }}">Чернетка</option>
                    <option value="{{ \App\Models\Dossier::STATUS_NEW }}">Нова заявка</option>
                    <option value="{{ \App\Models\Dossier::STATUS_ACTIVE }}">Прийнята заявка</option>
                    <option value="{{ \App\Models\Dossier::STATUS_NOT_ACTIVE }}">Не активне досьє</option>
                    <option value="{{ \App\Models\Dossier::STATUS_REJECTED }}">Відхилена заявка</option>
                </select>
            </div>
            <div style="margin-bottom: 10px;" class="text-center">
                <button style="margin-right: 15px" onclick="filter()" type="button" class="btn btn-primary">Оновити графіки</button>
                <button onclick="window.location.href='{{ route('dossier-stats') }}'" type="button" class="btn btn-primary">Скинути фільтри</button>
            </div>
            <div class="text-center result-count">
                Кількість за вказаним фільтром: <span>0</span>
            </div>
        </div>
    </div>
    <div style="padding: 20px" class="row">
        <div class="col-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">По місяцях</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="monthChart" width="800" height="400"></canvas>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <div style="padding: 20px" class="row">
        <div class="col-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">По днях</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="dayChart" width="800" height="400"></canvas>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
        <script>
            window.addEventListener('load', (event) => {
                $('#country-select').select2({
                    width: '100%',
                    dropdownPosition: 'below',
                    minimumResultsForSearch: Infinity,
                });
                $('#region-select').select2({
                    width: '100%',
                    dropdownPosition: 'below',
                    minimumResultsForSearch: Infinity,
                });
                $('#date-range').daterangepicker({
                    opens: 'center',
                    startDate: '{{$from}}',
                    endDate: '{{$to}}',
                    locale: {
                        format: 'DD/MM/YYYY'
                    }
                })
                document.querySelector('.result-count span').innerHTML = count;

                var monthCtx = document.getElementById('monthChart').getContext('2d');
                var monthChart = new Chart(monthCtx, {
                    type: 'line',
                    data: {
                        labels: Object.keys(monthgroup),
                        datasets: [{
                            label: 'Досьє по місяцях',
                            data: Object.values(monthgroup),
                            lineTension: 0.2,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        responsive: true,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });

                var dayCtx = document.getElementById('dayChart').getContext('2d');
                var dayChart = new Chart(dayCtx, {
                    type: 'line',
                    data: {
                        labels: Object.keys(daygroup),
                        datasets: [{
                            label: 'Досьє по днях',
                            lineTension: 0.2,
                            data: Object.values(daygroup),
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        responsive: true,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
            });

        </script>
@endsection
