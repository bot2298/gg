require('lightbox2');
window.jsPDF = require('./libs/jspdf.min')
window.html2canvas = require('html2canvas');

window.Vue = require('vue')

import VueLazyload from 'vue-lazyload';

Vue.use(VueLazyload)


Vue.component('image-viewer', require('./components/dossier-single/ImageViewerComponent.vue').default);

const app = new Vue({
    el: "#app",
    data: {

    },
    mounted() {

    }
})
