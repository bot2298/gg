window.Vue = require('vue')

import VueInternationalization from 'vue-i18n';
import Locale from './vue-i18n-locales.generated';

Vue.use(VueInternationalization);

const lang = document.documentElement.lang.substr(0, 2);
// or however you determine your current app locale

const i18n = new VueInternationalization({
    locale: lang,
    messages: Locale
});

let uri = window.location.search.substring(1);
let params = new URLSearchParams(uri);
Vue.prototype.locale = lang;
Vue.prototype.lang = params.get("lang");

Vue.component('dossiers-bookmark', require('./components/dossier/BookmarkComponent.vue').default);

const app = new Vue({
    el: "#app",
    i18n,
})
