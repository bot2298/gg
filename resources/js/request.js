import intlTelInput from 'intl-tel-input';

const input = document.querySelector("#intl-phone");
intlTelInput(input, {
    initialCountry: "ua",
    nationalMode: false,
    separateDialCode: true,
    hiddenInput: 'phone',
    utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.5/js/utils.min.js",
    preferredCountries: [
        'ua', 'pl', 'md'
    ]
});
