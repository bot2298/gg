window.Vue = require('vue')

import VueInternationalization from 'vue-i18n';
import Locale from './vue-i18n-locales.generated';

import VueChatScroll from 'vue-chat-scroll';
Vue.use(VueChatScroll)
Vue.use(VueInternationalization);

const lang = document.documentElement.lang.substr(0, 2);
// or however you determine your current app locale

const i18n = new VueInternationalization({
    locale: lang,
    messages: Locale
});

let uri = window.location.search.substring(1);
let params = new URLSearchParams(uri);
Vue.prototype.locale = lang;
Vue.prototype.lang = params.get("lang");

import TextareaAutosize from 'vue-textarea-autosize'

Vue.use(TextareaAutosize);


Vue.component('cabinet-chat-item-container', require('./components/cabinet/chat/ItemContainerComponent.vue').default);
Vue.component('cabinet-chat-message-list-container', require('./components/cabinet/chat/MessageListComponent.vue').default);

const app = new Vue({
    el: "#app",
    i18n,
})
