window.Vue = require('vue')

import VueInternationalization from 'vue-i18n';
import Locale from './vue-i18n-locales.generated';
import VueMask from 'v-mask'
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';
import VueLazyload from 'vue-lazyload';

Vue.use(VueLazyload)

Vue.component('v-select', vSelect);

Vue.use(VueMask);
Vue.use(VueInternationalization);

const lang = document.documentElement.lang.substr(0, 2);
// or however you determine your current app locale

const i18n = new VueInternationalization({
    locale: lang,
    messages: Locale
});

let uri = window.location.search.substring(1);
let params = new URLSearchParams(uri);
Vue.prototype.locale = lang;
Vue.prototype.lang = params.get("lang");

Vue.component('cabinet-text-input', require('./components/cabinet/InputTextComponent.vue').default);
Vue.component('cabinet-select', require('./components/cabinet/SelectComponent.vue').default);
Vue.component('cabinet-textarea', require('./components/cabinet/TextareaComponent.vue').default);
Vue.component('cabinet-academic-status', require('./components/cabinet/AcademicStatusComponent.vue').default);
Vue.component('cabinet-education-higher', require('./components/cabinet/EducationHigherComponent.vue').default);
Vue.component('cabinet-teaching', require('./components/cabinet/TeachingComponent.vue').default);
Vue.component('cabinet-experience-other', require('./components/cabinet/ExperienceOtherComponent.vue').default);
Vue.component('cabinet-training', require('./components/cabinet/TrainingComponent.vue').default);
Vue.component('cabinet-publications', require('./components/cabinet/PublicationsComponent.vue').default);
Vue.component('cabinet-recommendations', require('./components/cabinet/RecommendationsComponent.vue').default);
Vue.component('cabinet-honors', require('./components/cabinet/HonorsComponent.vue').default);
Vue.component('cabinet-document', require('./components/cabinet/DocumentPhotoComponent.vue').default);
Vue.component('cabinet-services', require('./components/cabinet/ServicesComponent.vue').default);
Vue.component('cabinet-avatar', require('./components/cabinet/AvatarComponent.vue').default);
Vue.component('cabinet-phone', require('./components/cabinet/PhoneComponent.vue').default);
Vue.component('cabinet-input-site', require('./components/cabinet/InputSiteComponent.vue').default);
Vue.component('cabinet-input-soclink', require('./components/cabinet/InputSoclinkComponent.vue').default);
Vue.component('cabinet-redirect-preview', require('./components/cabinet/RedirectPreviewComponent.vue').default);
Vue.component('cabinet-license', require('./components/cabinet/LicenseComponent.vue').default);
Vue.component('cabinet-spec-exp', require('./components/cabinet/SpecExpComponent.vue').default);
Vue.component('cabinet-single-select', require('./components/cabinet/SelectSingleComponent').default);


Vue.component('select-custom', require('./components/Select2Component.vue').default);
Vue.component('dossiers', require('./components/dossier/DossiersComponent.vue').default);
Vue.component('dossiers-bookmark', require('./components/dossier/BookmarkComponent.vue').default);

const app = new Vue({
    el: "#app",
    i18n,
    data: {
        showOverview: false,
        showContacts: false,
        showEducation: false,
        showExperience: false,
        showExperienceOther: false,
        showTraining: false,
        showPublications: false,
        showRecommendations: false,
        showHonors: false,
        showServices: false,
        expOptions: [],
        appeal_exp: false,
        cassation_exp: false,
        intl_exp: false
    },
    methods: {
        setAppealExpCaseShow(bool) {
            this.appeal_exp = bool
        },
        setCassationExpCaseShow(bool) {
            this.cassation_exp = bool
        },
        setIntlExpCaseShow(bool) {
            this.intl_exp = bool
        },
    }
})
