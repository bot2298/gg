<?php

return array (
  'index' =>
  array (
    'marker1_1' => 'The lawyer\'s office is a section of Googllaw, the functionality of which can provide communication with clients according to your individual needs.',
    'marker1_2' => 'In this section, you can access a database of customer inquiries to resolve legal issues and publish dossiers at <a target="_blank" href="https://googllaw.com" class="site-link"> googllaw.com </a>',
    'marker2_1' => 'It is not possible to access requests without creating and publishing a dossier or vice versa (creating a dossier and publishing a dossier and not accessing requests).',
    'marker2_2' => 'You can create a dossier in the "Dossier" section.',
    'marker2_3' => 'The dossier will be published on the request to find a lawyer in your chosen regions in the section "Lawyer\'s dossier" <a target="_blank" href="https://googllaw.com" class="site-link"> googllaw.com </a >. In the same regions, you will have access to customer inquiries to resolve legal issues.',
    'marker3' => 'You can access the services of the service and select the regions for professional activities in "Settings" → "Access to the service" or by clicking on the link "Publish dossiers" in the section "Dossiers".',
    'marker4_1' => 'This section is created exclusively for persons who have a licensed right to practice law.',
    'marker4_2' => 'The work in this section will be useful for lawyers who are confident in their competence and who are not afraid to work according to the rules of modern services.',
  ),
  'navigation' =>
  array (
    'dossier' => 'Dossier',
    'dossier_pending' => 'Pending',
    'dossier_active' => 'Active',
      'dossier_saved' => 'Draft',
    'dossier_not_active' => 'Not active',
      'dossier_rejected' => 'Rejected',
    'requests' => 'Requests',
    'bookmarks' => 'Bookmarks',
    'messages' => 'Messages',
    'ratings' => 'Comments',
    'settings' => 'Settings',
    'logout' => 'Logout',
  ),
    'form_index' =>
        array (
            'create_new' => 'Create a dossier',
            'create_another_lang' => 'Create a dossier on another language',
            'edit' => 'Edit',
            'delete' => 'Remove',
            'delete_confirm' => 'Do you really want to remove this dossier?',
            'activate' => 'Activate',
            'view' => 'View',
            'stop' => 'Disable'
        ),
    'form' =>
        array (
            'required_field' => 'Required field',
            'lang_confirm' => 'Completed data will not be saved',
            'dossier_lang' => 'The language of the dossier',
            'fullname' => 'Full name',
            'fullname_placeholder' => 'Surname, first name',
            'title_general' => 'General Information',
            'photo' => 'Photo',
            'title_sub_content' => 'Contacts',
            'sub_title_office' => "Lawyer's office",
            'country' => 'Country',
            'region' => 'Region',
            'city' => 'City (district)',
            'office_address' => 'Office address',
            'phone_number' => 'Phone number',
            'textarea_enter_text' => 'Enter text',
            'chars_left' => ':chars characters left',
            'title_licence' => 'Advocacy',
            'sub_title_licence' => 'Certificate of the right to practice law',
            'licence_country' => 'Country of receipt',
            'licence_year' => 'Year of receipt',
            'licence_institution' => 'Authority that issued',
            'licence_number' => 'Certificate number',
            'doc_photo' => 'Photo of the document',
            'drag_image' => 'Drag image',
            'or_choose_doc' => '(or) choose',
            'by_default_doc' => 'By default',
            'select_by_default' => 'Set as default',
            'image_doc_by_default' => 'This image will be displayed by default',
            'title_recommended' => 'Recommended (optional) to fill fields',
            'title_recommended_sub' => 'Fields that are not fill will NOT be published',
            'title_overview' => 'General information',
            'overview_languages' => 'Fluency in languages',
            'overview_motto' => 'Motto',
            'overview_strong_sides' => 'Strong qualities',
            'overview_hobbies' => 'Hobbies',
            'overview_site' => 'Website',
            'overview_facebook_followers' => 'Facebook / subscribers',
            'overview_instagram_followers' => 'Instagram / subscribers',
            'overview_linkedin_followers' => 'Linkedin / subscribers',
            'overview_twitter_followers' => 'Twitter / subscribers',
            'overview_youtube_followers' => 'YouTube / subscribers',
            'title_contact' => 'Contacts',
            'none' => 'missing',
            'from' => 'From',
            'to' => 'To',
            'day_mo' => 'Monday',
            'day_tu' => 'Tuesday',
            'day_we' => 'Wednesday',
            'day_th' => 'Thursday',
            'day_fr' => 'Friday',
            'day_sa' => 'Saturday',
            'day_su' => 'Sunday',
            'title_education' => 'Education',
            'sub_title_education_higher' => 'Higher Education',
            'sub_title_education_academic_status' => 'Academic status',
            'sub_title_education_teaching' => 'Teaching activities',
            'title_experience' => 'Advocacy',
            'experience_year' => 'Experience',
            'textarea_placeholder' => 'Less than 3,000 characters',
            'experience_country' => 'Country of implementation',
            'experience_region' => 'Region of implementation',
            'experience_city' => 'City (district)',
            'experience_specializations' => 'Specialization',
            'experience_activities' => 'Activity',
            'experience_form' => 'Form of activity',
            'experience_appeal_exp' => 'Experience of representation in the court of appeal',
            'experience_appeal_cases' => 'An important case in which you took part in the appellate instance and which can characterize your professional competence (name of the court, period of consideration of the case, summary of the case)',
            'experience_cassation_exp' => 'Experience of representation in the court of cassation',
            'experience_cassation_cases' => 'An important case in which you took part in the cassation instance and which can characterize your professional competence (name of the court, period of consideration of the case, summary of the case)',
            'experience_intl_exp' => 'Experience in representing international institutions (bodies, organizations, etc.) in dispute resolution',
            'experience_intl_cases' => 'An important case in which you have participated in an international institution and which can characterize your professional competence (name of the court, period of consideration of the case, summary of the case)',
            'title_other_experience' => 'Other professional activities',
            'sub_title_patent_license' => 'Patent Attorney\'s Certificate',
            'patent_license_institution' => "Patent Attorney's Certificate",
            'patent_license_number' => 'Certificate number',
            'title_training' => 'Certification training',
            'title_publications' => 'Publications',
            'title_recommendations' => 'Recommendations',
            'title_honors' => 'Distinctions',
            'title_services' => 'Cost of services',
            'preview_link' => 'Dossier preview',
            'workinghours' => 'Working hours',
            'save_submit' => 'Save',
            'publicate_submit' => 'Publish',
            'add_photo' => 'Add a photo',
            'junior_bachelor' => 'Junior bachelor',
            'bachelor' => 'Bachelor',
            'master' => 'Master',
            'specialist' => 'Specialist',
            'doctor_of_philosophy' => 'Doctor of Philosophy',
            'doctor_of_science' => 'Doctor of Science',
            'other' => 'other',
            'legal' => 'Legal',
            'economic' => 'Economic',
            'historical' => 'Historical',
            'biological' => 'Біологічна',
            'veterinary' => 'Veterinary',
            'military' => 'Military',
            'geographic' => 'Geographically',
            'public_administration' => 'Governance',
            'culturology' => 'Culturology',
            'medical' => 'Medical',
            'art_history' => 'Art history',
            'pedagogical' => 'Pedagogically',
            'political' => 'Political',
            'psychological' => 'Psychological',
            'agricultural' => 'Agricultural',
            'social_communications' => 'Social communications',
            'technical' => 'Technical',
            'pharmaceutical' => 'Pharmaceutical',
            'physics_and_mathematics' => 'Physical and mathematical',
            'physical_education_and_sports' => 'Physical education and sports',
            'philological' => 'Philological',
            'philosophical' => 'Philosophical',
            'chemical' => 'Chemically',
            'professor' => 'Professor',
            'associate_professor' => 'Docent',
            'senior_researcher' => 'Senior Research Fellow',
            'consultations' => 'Consultations',
            'accompaniment' => 'Accompaniment',
            'preparation_doc' => 'Preparation of documents',
            'representation' => 'Representation',
            'individually' => 'Individually',
            'bureau' => 'Bureau',
            'association' => "Association",
            'available' => 'Available',
            'missing' => 'Missing',
            'period' => 'Period',
            'institution_higher-education' => 'Institution of higher education',
            'degree' => 'Degree',
            'field' => 'Branch',
            'specialty' => 'Specialty',
            'degree_rank' => 'Rank',
            'subject' => 'Subject',
            'cities' => 'City',
            'activities' => 'Activity',
            'form' => 'Form of activity',
            'topic' => 'Topic',
            'position' => 'Position',
            'place_of_work' => 'Place of work',
            'year' => 'Year of receipt',
            'university' => 'School',
            'edition' => 'Edition',
            'link_to_public' => 'Link to the publication',
            'recomm_doc' => 'Document of a recommendatory nature',
            'name' => 'Name',
            'add_education' => 'Add education',
            'add_license' => 'Add license',
            'add_academic' => 'Add rank',
            'add_teaching' => 'Add activity',
            'add_training' => 'Add an event',
            'add_publication' => 'Add a publication',
            'add_recommendation' => 'Add a recommendation',
            'add_honor' => 'Add a distinction',
            'year_honors' => 'Year',
            'add_cost' => 'Add a service',
            'delete_license' => 'Delete license',
            'delete_education' => 'Delete education',
            'delete_academic' => 'Delete rank',
            'delete_teaching' => 'Delete activity',
            'delete_training' => 'Delete an event',
            'delete_publication' => 'Delete a publication',
            'delete_recommendation' => 'Delete a recommendation',
            'delete_honor' => 'Delete a distinction',
            'delete_cost' => 'Delete a service',
            'name_cost' => 'Name of the service',
            'price' => 'Price',
            'currency' => 'Currency',
            'all_filled' => 'Make sure all required fields are filled!',
            'main_address' => 'Primary address that will appear in the profile business card',
        ),
  'request' =>
  array (
    'check_all' => 'Select all',
    'check_read' => 'Select all read',
    'check_unread' => 'Select all unread',
    'check_response' => 'Select all with response',
    'check_noresponse' => 'Select all without response',
    'check_actual' => 'Select actual',
    'check_notactual' => 'Select not actual',
    'filter_placeholder' => 'Filter',
    'filter_all' => 'All',
    'filter_read' => 'Read',
    'filter_unread' => 'Unread',
    'filter_response' => 'Responded',
    'filter_noresponse' => 'Didn\'t respond',
    'filter_actual' => 'Actual',
    'filter_notactual' => 'Not actual',
      'filter_in_bookmarks' => 'In bookmarks',
    'filter_empty' => 'No results for the filter',
    'sort_placeholder' => 'Sort',
    'sort_created_desc' => 'Newer first',
    'sort_created_asc' => 'Older first',
    'sort_cost_desc' => 'Higher cost',
    'sort_cost_asc' => 'Lower cost',
    'sort_deadline_desc' => 'Actual first',
    'sort_deadline_asc' => 'Not actual first',
    'seeking_lawyer' => 'seeking for a lawyer',
    '30mins' => 'Actual :mins mins',
    'not_actual' => 'Not actual',
    'actual_til' => 'Actual til',
    'call' => 'The client asks to call him',
    'add_to_bookmarks' => 'Add to bookmarks',
    'remove_from_bookmarks' => 'Remove from bookmarks',
    'download_attachment' => 'Download attachment',
    'complaint' => 'Report',
    'respond' => 'Respond',
    'delete' => 'Remove',
    'delete_confirm' => 'Do you really want to remove these requests?',
    'form_placeholder' => 'Respond...',
    'form_submit' => 'Send',
    'bookmarks_empty' => 'You dont have bookmarks yet',
    'bookmark_delete' => 'Do you really want to remove these bookmarks?',
    'bookmarks_filters_empty' => 'No results for the filter',
    'rating_empty' => 'You dont have ratings yet',
    'go_to_chat' => 'Go to chat',
      'chats_empty' => 'You dont have messages yet',
      'select' => 'Select',
      'unselect' => 'Unselect',
  ),
  'settings' =>
  array (
    'nav_password' => 'Login and password',
    'nav_access' => 'Service access',
    'nav_access_history' => 'Access history',
    'login' => 'Login',
    'password' => 'Change password',
    'placeholder_old_password' => 'Enter old password',
    'placeholder_new_password' => 'Enter new password',
    'placeholder_new_password_confirmation' => 'Repeat new password',
    'save' => 'Save',
    'add' => 'Add',
    'access_about1' => 'By using our service, you can place your own file on it and at the same time get access to the customer database of your chosen region.',
    'access_about2' => 'When you select the appropriate region (s) of the country, your file will be displayed.',
    'access_about3' => 'The cost of the service depends on the country and region in which you intend to work.',
    'access_about4' => 'When choosing the right to access two or more regions at the same time, you get a discount:',
      'access_about_temp1' => 'Currently, access to the service is free and you can work in no more than two regions of Ukraine.',
    'access_discount1' => 'from 2 to 4 regions - 5%;',
    'access_discount2' => 'from 5 to 9 regions - 10%;',
    'access_discount3' => 'from 10 to 14 regions - 15%;',
      'access_pay' => 'Pay',
      'access_gain' => 'Get access',
      'access_add_region' => 'Add region',
      'access_region' => 'Region',
      'access_period' => 'Period',
      'access_period_to' => 'til',
      'access_to_pay' => 'TO PAY: :sum uah ',
      'access_cost' => '1 month cost',
      'access_selected_count' => 'Regions selected',
      'access_uah' => 'uah',
      'access_sum' => 'Sum :sum uah',
      'access_discount' => 'Discount',
    'access_select_country' => 'Select the country in which you intend to place the dossier and access customer requests:',
    'access_select_region' => 'Select the region in which you intend to place the dossier:',
    'access_select_cities' => 'Select the cities (optional) whose queries you want to see',
    'access_history_country' => 'Country',
    'access_history_region' => 'Region',
    'access_history_cities' => 'Cities',
    'access_history_status' => 'Work in region status',
    'access_history_stop' => 'Stop access',
    'access_history_renew' => 'Renew access',
    'access_edit' => 'Change',
    'access_toggle_status' => 'Toggle status',
    'access_delete' => 'Delete',
    'access_delete_confirm' => 'Delete access?',
    'access_status_active' => 'Active until',
    'access_status_notactive' => 'Not active',
    'access_empty' => 'Choose regions in <a class="site-link" href=":link">settings</a>',
    'access_cities_all' => 'All cities',
  ),
    'mail' =>
        array (
            'show_request' => 'Go to request',
            'admin' => 'Administration',
            'request_notactive' => 'Request is not active'
        ),
);
