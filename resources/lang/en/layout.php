<?php

return array (
  'home' => 'Home',
  'bookmarks' => 'Bookmarks',
  'faq' => 'FAQ/Write to us',
  'about' => 'About',
  'dossier' => 'Dossier',
    'all_dossier' => 'All dossier',
    'subcats_regions' => 'Regions',
    'subcats_specs' => 'Specializations',
  'request' => 'Request',
  'tracker' => 'Googllaw-tracker',
  'cabinet' => 'Cabinet',
  'faq' => 'FAQ / Write us',
  'policy' => 'Privacy policy',
  'offer' => 'Public offer agreement',
  'share' => 'Share',

    'title' => [
        'cabinet' => [
            'home' => [
                'index'=> 'Cabinet',
            ],
            'bookmarks' => [
                'index' => 'Cabinet | Bookmarks'
            ],
            'mail' => [
                'index' => 'Cabinet | Messages',
                'single' => 'Cabinet | Message',
            ],
            'settings' => [
                'index' => 'Cabinet | Settings',
                'access' => 'Cabinet | Access',
                'access-history' => 'Cabinet | Access history',
            ],
            'rating' => [
                'index' => 'Cabinet | Ratings and comments',
                'single' => 'Cabinet | Ratings and comments',
            ],
            'request' => [
                'index' => 'Cabinet | Requests',
                'single' => 'Cabinet | Request',
            ]
        ],
        'site' => [
            'home' => [
                'index' => 'Main'
            ],
            'faq' => [
                'index' => 'FAQ'
            ],
            'about' => [
                'index' => 'About'
            ],
            'bookmarks' => [
                'index' => 'Bookmarks'
            ],
            'dossier' => [
                'index' => 'Dossier',
                'single' => 'Dossier'
            ],
            'dossier-single' => [
                'index' => 'Dossier single'
            ],
            'cabinet' => [
                'dossier-index' => 'Cabinet | Dossier',
                'dossier-form' => 'Cabinet | Dossier',
            ],
            'request' => [
                'index' => 'Request',
                'verification' => 'Request verification',
                'verified' => 'Request verified',
            ],
            'tracker' => [
                'index' => 'Tracker',
                'remind-code' => 'Tracker',
                'response-list' => 'Tracker',
            ],
            'policy' => [
                'index' => 'Privacy policy'
            ],
            'auth' => [
                'forgot-password' => 'Forgot password',
                'login' => 'Login',
                'register' => 'Register',
                'reset-password' => 'Password restore',
                'verify' => 'E-mail verification'
            ]
        ]
    ]
);
