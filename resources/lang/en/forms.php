<?php

return array (
  'lawyer_country' => 'Lawyer\'s country',
  'lawyer_region' => 'Lawyer\'s region',
  'lawyer_city' => 'Lawyer\'s city (district)',
  'lawyer_specialization' => 'Lawyer\'s specialization',
  'placeholder_choose' => 'Choose',
    'placeholder_choose_country' => 'Choose country',
    'placeholder_choose_region' => 'Choose region',
  'placeholder_enter' => 'Enter',
    'placeholder_enter_text' => 'Enter text',
  'placeholder_password' => 'Password',
    'placeholder_newpassword' => 'Новий пароль',
  'placeholder_password_confirmation' => 'Password repeat',
    'placeholder_newpassword_confirmation' => 'Password repeat',
);
