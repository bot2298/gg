<?php

return array (
  'about' =>
  array (
    'header_goal' => '<span> Our goal </span>is to create a modern platform for communication between lawyers and clients',
    'goal' => 'Project objectives',
    'goal1' => 'Bring together the best lawyers',
    'goal2' => 'Promote information about lawyers',
    'goal3' => 'Create a database of requests for legal assistance',
    'goal4' => 'Automate the process of finding lawyers',
    'goal5' => 'Establish communication between lawyers and clients',
    'sections' => 'Service sections',
    'section1' => 'Dossiers',
    'section2' => 'Requests',
    'section3' => 'Googllaw-tracker',
    'section4' => 'Cabinet',
    'sections_caption' => 'Work on the service of persons seeking a legal solution is free and does not require registration.',
    'dossier' => 'Dossier',
    'dossier_about' => 'The Googllaw dossier is a collection of information about a lawyer\'s professional, personal and social competence, as well as his or her professional ethics and integrity.',
    'dossier_list' => 'The lawyer\'s dossier contains the following information:',
    'dossier_list1' => 'Contacts',
    'dossier_list2' => 'Education',
    'dossier_list3' => 'Professional activity',
    'dossier_list4' => 'Achievements',
    'dossier_list5' => 'Rating and reviews',
    'dossier_marker1' => 'The lawyer\'s dossier is fixed according to the territory of his professional activity.',
    'dossier_marker2' => 'For the convenience of finding the necessary lawyers in the section it is possible to use filters.',
    'dossier_marker3' => 'This section will be useful for those who prefer to independently select the best from the results of the study and evaluation of objective information.',
    'request' => 'Request',
    'request_marker1' => 'Googllaw\'s request is a formulation of a legal question to professional users of our service for the purpose of obtaining information about lawyers who are able to solve it according to their level of workload, specialization and regional affiliation.',
    'request_marker2' => 'So, to find the right lawyer, all you have to do is send a request, and Googllaw will do the work of selecting the lawyers you need on your own.',
    'request_marker3' => 'To form a request, it is necessary to provide answers to simple standard questions, in particular, about the region in which you need to find a lawyer and summarize the question.',
    'request_marker4' => 'If you need to get immediate legal assistance (involved in an accident, search, arrest, etc.), you can indicate this in the request by filling in the field on the duration of its consideration.',
    'request_marker5' => 'Request generation is free and does not require registration.',
    'request_marker6' => 'The request after sending is placed in the Googllaw database of the region specified by you and is available to the users of the service registered in it.',
    'request_marker7' => 'Information on the results of the request can be obtained in the section "Googllaw-tracker", according to the individual code that will be generated after sending the request.',
    'request_marker8' => 'The section will be useful for people who do not want to spend time looking for a lawyer, but want to get information about specific lawyers who can solve the problem.',
    'tracker' => 'Googllaw-tracker',
    'tracker_about' => 'Googllaw-tracker - a feature that provides communication between clients and lawyers according to the individual needs of each.',
    'tracker_list' => 'In this section the client has an opportunity:',
    'tracker_list1' => 'Track information about the status of your request',
    'tracker_list2' => 'Choose a lawyer from among those who can decide',
    'tracker_list3' => 'Evaluate the attorneys who responded to the request',
    'tracker_list4' => 'Edit request',
    'tracker_list5' => 'Stop processing the request',
    'tracker_marker1' => 'Access to the section can be provided only by individual code, which is generated after sending the request.',
    'tracker_marker2' => 'Lawyers decide to respond or not to respond to the request, taking into account the level of their workload, specialization and regional affiliation.',
    'tracker_marker3' => 'The service in the "Googllaw-tracker" section will inform you about a lawyer who can resolve your issue immediately after the lawyer\'s response to this request.',
    'cabinet' => 'Lawyer\'s cabinet',
    'cabinet_about' => 'The lawyer\'s cabinet is a functional that provides communication of lawyers with clients in accordance with the individual needs of lawyers.',
    'cabinet_list' => 'In this section, the lawyer has the opportunity to:',
    'cabinet_list1' => 'Bring together the best lawyers',
    'cabinet_list2' => 'Promote information about lawyers',
    'cabinet_list3' => 'Create a database of requests for legal assistance',
    'cabinet_list4' => 'Automate the process of finding lawyers',
    'cabinet_list5' => 'Establish communication between lawyers and clients',
    'cabinet_services' => 'Services:',
    'cabinet_service1' => 'Access to the database of client requests for legal issues',
    'cabinet_service2' => 'Publish the dossier at <a target="_blank" class="site-link" href="#"> googllaw.com </a>.',
    'cabinet_marker1' => 'Use of the services of a lawyer is paid and in accordance with the tariffs of the region (regions) in which you intend to work.',
    'cabinet_marker2' => 'You can access the services of the service and select the regions for professional activity in the "Lawyer\'s Cabinet" → "Settings" → "Access to the service".',
    'cabinet_marker3' => 'Using the services of the service, the lawyer gets access to client requests in his chosen region (regions), and his file is simultaneously attached to this region (regions) and is publicly available by search in the "Lawyer\'s dossier" googllaw.com.',
    'cabinet_marker4' => 'It is possible to review customer inquiries only if the dossier is created and consent is given for its publication in the respective region.',
    'cabinet_marker5' => 'It is not possible to publish a dossier and not access requests, or vice versa - not to publish a dossier and access requests.',
    'cabinet_marker6' => 'The section is created exclusively for persons who have a licensed right to practice law.',
    'cabinet_marker7' => 'The work in this section will be useful for lawyers who are confident in their competence and who are not afraid to work according to the rules of modern services.',
    'self' => 'If you need to resolve a legal issue and want to find a lawyer yourself, in the section "Lawyers\' Dossier"',
    'self_step1' => 'Adjust filters',
    'self_step2' => 'Review the dossier',
    'self_step3' => 'Choose the best',
    'help' => 'If you need help finding a lawyer, it\'s free and without registration',
    'help_step1' => 'Form a request to lawyers',
    'help_step2' => 'See the files of the lawyers who responded to the request',
    'help_step3' => 'Choose the best',
    'lawyer' => 'If you are a lawyer and would like to post your file on Googllaw, as well as access the database of lawyers, in the section "Lawyer\'s cabinet"',
    'lawyer_step1' => 'Register and create your own dossier',
    'lawyer_step2' => 'Select regions to access the service',
    'lawyer_step3' => 'Choose customers according to your needs',
    'footer1' => 'The Googllaw team makes every effort to make the service attractive.',
    'footer2' => 'We look forward to receiving your suggestions and suggestions at <a class="site-link" href="mailto:googllaw@gmail.com"> googllaw@gmail.com </a>.',
    'footer3' => 'Please also support us by sharing this product!',
  ),
  'dossier' =>
  array (
    'header_easy' => 'Choosing a lawyer is easy!',
    'header1' => 'Adjust filters',
    'header2' => 'Review the dossier',
    'header3' => 'Choose the best one',
      'to_bookmark' => 'Add to bookmark',
      'delete_from_bookmark' => 'Delete from bookmark',
      'show_more' => 'Show more',
      'loading' => 'Loading...',
      'empty_results' => 'No results found',
      'adve_search' => 'Advanced search',
      'rating' => 'Rating',
      'find_law' => 'Find a lawyer',
      'from' => 'from',
      'fullname_placeholder' => 'Enter name',
      'sort_year_1_5' => 'from 1 to 5',
      'sort_year_5_10' => 'from 5 to 10',
      'sort_year_10_20' => 'from 10 to 20',
      'sort_year_20_30' => 'from 20 to 30',
      'sort_year_30_40' => 'from 30 to 40',
      'sort_year_40_50' => 'from 40 to 50',
      'filter_fullname_desc' => 'Name from Z to A',
      'filter_fullname_asc' => 'Name from А to Z',
      'filter_rating_desc' => 'Rating descending',
      'filter_rating_asc' => 'Rating ascending',
      'filter_years_desc' => 'Experience descending',
      'filter_years_asc' => 'Experience ascending',
      'title_lawyers' => 'Lawyers',
      'title_lawyers_region' => ' in ',
      'title_lawyers_spec' => ' of specialization '
  ),
    'dossier_single' =>
        array (
            'to_bookmark' => 'Add to bookmark',
            'from_bookmark' => 'Remove from bookmarks',
            'back' => 'Back',
            'report' => 'Report',
            'download' => 'Dossier PDF',
            'followers' => 'Followers'
        ),
  'home' =>
  array (
    'caption' => 'Quick search for lawyers!',
    'caption_more' => 'Choose! Communicate! Decide',
    'about' => '<span> Googllaw </span> is a service that brings together clients and lawyers according to the individual needs of each and based on the use of the <span> Google </span> advertising service.',
    'need_help' => 'Request to lawyers',
    'need_help_action' => 'Make a request',
    'no_need_help' => 'Dossiers',
    'no_need_help_action' => 'See the dossiers',
    'sent' => 'Requests sent',
    'lawyers_count' => 'Lawyers count',
  ),
  'request' =>
  array (
      'main_caption' => 'Make a request and choose a lawyer!',
      'caption1' => 'Easy',
      'caption2' => 'Free',
      'caption3' => 'No registration needed',
    'header' => 'Request information',
      'required_field' => 'Required field',
      'content' => 'Content',
    'chars_left' => '<span>5000</span> characters left',
    'cost' => 'Estimated cost',
    'deadline' => 'Deadline',
      'deadline1hour' => '1 hour',
      'deadline3hours' => '3 hours',
      'deadline12hours' => '12 hours',
    'deadline24hours' => '24 hours',
    'deadline3days' => '3 days',
    'deadline7days' => '7 days',
    'deadline30days' => '30 days',
    'name' => 'Your name',
    'email' => 'E-mail',
    'phone' => 'Phone',
    'attachment' => 'Attachment',
    'attachment_download' => 'Download',
    'callme' => 'If you want to be called, leave your phone number',
    'attach_alt' => 'Attach the document',
    'info' => 'The request after sending will be placed in the Googllaw database of the region specified by you and is available to Googllaw registered users (lawyers).',
    'agreement' => 'I consent to the processing of my data, agree to the <a target="_blank" class="site-link" href=":link">rules and privacy policies of Googllaw</a>',
    'send' => 'Send',
      'update' => 'Update',
      'verify_header' => 'Verify your request!',
      'verify_caption' => 'To verify your request follow the link which was sent to the e-mail you specified',
      'change_email' => 'If you made a mistake in E-mail, you can change it.',
      'change_email_action' => 'Change e-mail',
      'check_spam' => 'If you don\'t see it in your inbox, check spam and promotions folders',
      'your_code' => 'Your code',
      'send_email' => 'Do you want to receive code by email?',
      'yes' => 'Yes',
      'no' => 'No',
  ),
    'tracker' => [
        'info' => 'You can get information about the request and the lawyers interested in its consideration in the section Googllaw-tracker only using the specified code',
        'header' => 'Request',
        'created' => 'created',
        'updated' => 'updated',
        'status1' => 'Pending',
        'status2' => 'Lawyers interested',
        'status3' => 'Lawyers responded',
        'change' => 'Edit',
        'stop' => 'Stop',
        'exit' => 'Exit tracker',
        'stopped' => 'The request has been suspended. You can create a new request in the "Request for lawyers" section."',
        'protection' => 'This action is performed to prevent unauthorized automated access to your personal data.',
        'responses_header' => 'Lawyers who responded to your request',
        'access_header' => 'Tracker access',
        'restore_header' => 'Restore code',
        'response' => [
            'rate' => 'Rate lawyer',
            'dossier' => 'See the dossier',
            'complaint' => 'Report',
            'message' => 'Go to message',
            'send' => 'Send',
            'send_placeholder' => 'Write a message...'
        ],
        'form' => [
            'enter_code' => 'Enter your request code',
            'track' => 'Track',
            'follow' => 'To access information about your request, please follow the link to the email address used for it: ',
            'enter_email' => 'Enter your e-mail',
            'get_code' => 'Get the code',
            'email_sent' => 'We sent you the code to',
            'press' => 'press',
            'email_another' => 'to send again',
            'forgot_code' => 'Forgot the code?'
        ]

    ],
    'faq' => [
        'heading' => 'Faq page',
        'item1' => 'What should i do if i forgot request code?',
        'item1_content' => ' <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
        'item2' => 'Gather the best lawyers',
        'feedback_header' => 'Do you have any other questions? Contact us',
        'placeholders' => [
            'name' => 'First name',
            'lastname' => 'Last name',
            'email' => 'E-mail',
            'textarea' => 'Your message',
            'place_of_living' => 'Place of living',
            'postal_address' => 'Postal address',
            'number' => 'Contact number'
        ],
        'submit' => 'Send'

    ],
    'policy' => [
        'heading' => 'Googllaw privacy policy',
        'content1' => [
            'header' => 'I. What information do we collect?',
            'subcontent1' => [
                'header' => 'Information and content provided',
                'content_p1' => 'We, Googllaw Limited Liability Company, process the content, messages and other information you provide when using the relevant product, in particular when you register an account, create or distribute information about yourself, request legal assistance or notice when communicate with other users.',
                'content_p2' => 'You may choose to provide or not provide additional additional information about yourself when filling in the relevant fields of the dossier profile or when making a request to lawyers.',
                'content_p3' => 'Users who intend to make a request to lawyers form a request to the extent they deem sufficient and necessary, understanding that it will be available to users who have access to such requests in the region concerned.',
            ],
            'subcontent2' => [
                'header' => 'Networks and connections',
                'content_p1' => 'We collect information about the accounts, the people you connect with, and how you interact with them on Googllaw, such as how often you respond to inquiries.',
            ],
            'subcontent3' => [
                'header' => 'How you use Googllaw',
                'content_p1' => 'We collect information about how you use Googllaw, such as the dossier fields you fill out; time, frequency and duration of your activity.',
            ],
            'subcontent4' => [
                'header' => 'Information on operations performed in Googllaw',
                'content_p1' => 'If you use Googllaw\'s paid ability to publish files and access attorneys\' inquiries, we receive your payment information.',
            ],
            'subcontent5' => [
                'header' => 'Data about the actions of other users and information about you that they provide',
                'content_p1' => 'We also receive and analyze content, messages and information that other people provide when using our Googllaw. For example, information when other people share or comment on your file.',
            ],
            'subcontent6' => [
                'header' => 'Device information',
                'content_p1' => 'We collect information from computers, phones, TVs, and other devices connected to the Internet that you use when visiting Googllaw.',
                'list' => [
                    'header' => 'Here is the information we get from the above devices:',
                    'item1' => 'device attributes: information about the operating system, hardware and software versions, battery level, signal power, free storage space, browser type, names and types of applications and files, as well as plug-ins;',
                    'item2' => 'device operation: information about the operation of the device and the style of behavior during its use. For example, whether the window is in the foreground or background, mouse movements (this helps to distinguish a person from bots);',
                    'item3' => 'IDs: Individual IDs, device IDs, and other IDs from games, applications, or accounts that you use.',
                    'item4' => 'Device alerts: Bluetooth alerts, as well as information about nearby Wi-Fi hotspots, beacons, and mobile towers',
                    'item5' => 'device settings data: information that you allow us to receive through the device settings you have activated, such as access to the camera or pictures.',
                    'item6' => 'network and connection: information about your mobile operator or internet service provider, language, time zone, mobile phone number, IP address, connection speed;',
                    'item7' => 'Cookie data: Cookie data stored on your device, including cookie IDs and cookie settings..',
                ],
            ],
            'subcontent7' => [
                'p' => 'By using Googllaw services, you consent to the processing of your personal data, including the collection, recording, systematization, accumulation, storage, deletion, clarification (update, change), use, depersonalization, blocking, deletion, destruction, and other actions in accordance with of this Policy, product terms of use and legislation.'
            ]
        ],
        'content2' => [
            'header' => 'II. How do we use this information?',
            'p1' => 'We use the information obtained to improve Googllaw.',
            'subcontent1' => [
                'header' => 'Promoting protection, security and integrity',
                'p1' => 'We use the information at our disposal to verify accounts and activity, to combat harmful behavior, to detect and combat spam and other adverse events, to ensure the integrity of Googllaw, and to promote the protection and security of Googllaw. For example, we use the information in our possession to investigate suspicious activity or violations of our terms or policies, or to determine that someone needs help.'
            ],
            'subcontent2' => [
                'header' => 'Communicating with you',
                'p1' => 'We use the information at our disposal to send you messages, communicate with you about Googllaw, and inform you of our policies and conditions. We also use your information to respond to you when you contact us.'
            ],
            'subcontent3' => [
                'header' => 'Research and innovation for social well-being',
                'p1' => 'We use the information at our disposal (including information from research partners) to conduct and support research and innovation in the areas of general social well-being, technological progress, public interest, health and well-being.'
            ],
        ],
        'content3' => [
            'header' => 'III. How is this information disseminated?',
            'subcontent1' => [
                'header' => 'People and accounts with whom you share information and communicate',
                'p1' => 'Anyone can see the open information. Open information includes data that you fill in when creating a lawyer\'s file. Open information can be shared or uploaded using Googllaw. We also allow you to see who and what left comments and assessments on the lawyers\' files, as well as information on the number of their views.',
                'p2' => 'The lawyer\'s file, his assessments and feedback are open within the period of publication of the file, unless such access has been terminated by Googllaw or by the user himself. The request for lawyers is available to users registered in the respective region. For example, when you create a request for assistance in Kyiv, such a request will be available to users who have access to requests for lawyers in Kyiv.',
                'p3' => 'When you intend to contact a client or a lawyer at Googllaw, you choose who you want to communicate with. For example, when you have access to a lawyer\'s request in a particular region, you choose which request to respond to. You also choose the lawyer who responded to your request (if any). The request for lawyers will be open in a certain region within the period of its publication, if such access has not been terminated by Googllaw or by the user himself.'
            ],
            'subcontent2' => [
                'header' => 'New owner',
                'p1' => 'If or part of Googllaw becomes the property or control of a new owner, or the components of Googllaw change, we may transfer information about you to the new owner.'
            ],
            'subcontent3' => [
                'header' => 'Transfer of information to third parties',
                'p1' => 'We work with third-party partners to help us provide and improve Googllaw, so we can conduct business and provide services to users around the world. We do not sell any information about you to anyone and will never do so. We also set strict limits on how our partners can use and disclose the information we provide to them.',
                'list' => [
                    'header' => 'The following are third-party partners to whom we provide information:',
                    'item1' => 'partners who use our analytics services. We provide generalized statistics and analytical results that help improve our work;',
                    'item2' => 'researchers and scientists. We also provide information to research partners and researchers to conduct research aimed at developing scientific knowledge and innovations that contribute to research in the areas of general social well-being, technological development, public interest, health and well-being;',
                    'item3' => 'police. We provide information to law enforcement agencies or in response to inquiries required by law.'
                ]
            ]
        ],
        'content4' => [
            'header' => 'IV. How can I manage and delete information about me?',
            'p1' => 'We provide you with access to your data and the ability to change or delete it in the lawyer\'s office and in the Googllaw tracker.',
            'p2' => 'We store the data until it is no longer needed to provide our services or until the account is deleted.',
            'p3' => 'When you delete your account, you also delete what you have posted (for example, photos, status updates). Your information shared by other users is not part of your account and cannot be deleted. If you do not wish to delete your account, but would like to discontinue using Googllaw, you may discontinue access to the product or not renew it.'
        ],
        'content5' => [
            'header' => 'V. How do we respond to legal requests or prevent harm?',
            'list' => [
                'header' => 'We have access to your information, store it and pass it on to control, law enforcement and other bodies:',
                'item1' => 'in response to a formal request (for example, a court decision). Among other things, we may respond to formal inquiries received from the authorities of other countries or territories if we have reasonable grounds to consider such a response to be a legitimate requirement of that country or territory, if it affects users living there and in the absence of inconsistencies. with international standards;',
                'item2' => 'if we have reason to believe that it is necessary to detect, prevent and prevent offenses;',
                'item3' => 'for our protection (including our rights, property);',
                'item4' => 'for your protection and the protection of other users, in particular as part of investigations or inquiries by regulatory, supervisory and regulatory authorities.'
            ],
            'p1' => 'We take all necessary measures to protect user data from unauthorized access.',
            'p2' => 'We seek to protect personal information, but warn of the potential adverse effects of cyberattacks, illegal actions and other force majeure.'
        ],
        'content6' => [
            'header' => 'VI. How do we manage and transmit data as part of our global services?',
            'p1' => 'We use information inside Googllaw.',
            'p2' => 'We may provide certain information to our partners on a contractual basis and solely for the purpose of improving Googllaw\'s performance.'
        ],
        'content7' => [
            'header' => 'VII. Where do we store data?',
            'p1' => 'Googllaw is hosted on servers in Montreal, Canada'
        ],
        'content8' => [
            'header' => 'VIІI. How do we notify you of changes to this policy?',
            'p1' => 'Policy changes are posted immediately after they are made in the appropriate section on the main and other pages of Googllaw.',
        ],
        'content9' => [
            'header' => 'IХ. How can I ask Googllaw?',
            'p1' => 'You can contact us online by using the FAQ on the main and other pages of Googllaw or by mail: Googllaw, Ukraine, Kiev, st. Sichovykh Striltsiv, 77, of. 707.'
        ]
    ],
    'dossier_pdf_header' => 'LAWYER\'S DOSSIER',
    'password_8_chars' => 'The password should contain at least 8 characters'
);
