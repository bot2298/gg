<?php

return array (
  'failed' => 'These credentials do not match our records.',
  'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
  'login_header' => 'Cabinet login',
  'forgot_password' => 'Forgot password?',
  'login_action' => 'Login',
  'login_register' => 'Register',
  'login_google' => 'Login using Google',
  'login_facebook' => 'Login using Facebook',
  'restore_password' => 'Restore password',
  'change_password' => 'Change password',
  'restore_password_action' => 'Restore',
    'restore_password_action_final' => 'Change',
  'register_header' => 'Registration',
  'email_confirm_header' => 'Verify your E-mail!',
  'email_confirm_resent' => 'You have been sent a new link to confirm your email',
  'email_confirm_info' => 'To verify registration on Googllaw follow the link, that we sent to you email <a href="mailto::email" class="site-link">:email</a>',
    'email_check_spam' => 'If you don\'t see it in your inbox, check spam and promotions folders',
  'email_confirm_resend' => 'If you didn\'t receive the email, <a class="site-link" href=":link"> click here to get a new one </a>.',
);
