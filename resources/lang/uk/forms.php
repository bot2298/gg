<?php

return array (
  'lawyer_country' => 'Країна адвоката',
  'lawyer_region' => 'Регіон адвоката',
  'lawyer_city' => 'Місто (район) адвоката',
  'lawyer_specialization' => 'Спеціалізація адвоката',
  'placeholder_choose' => 'Оберіть',
  'placeholder_choose_country' => 'Оберіть країну',
   'placeholder_choose_region' => 'Оберіть регіон',
  'placeholder_enter' => 'Зазначте',
  'placeholder_enter_text' => 'Введіть текст',
  'placeholder_password' => 'Пароль',
    'placeholder_newpassword' => 'Новий пароль',
  'placeholder_password_confirmation' => 'Повторіть пароль',
    'placeholder_newpassword_confirmation' => 'Повторіть пароль',
);
