<?php

return array (
  'home' => 'Головна',
  'bookmarks' => 'Закладки',
  'faq' => 'FAQ/Напишіть нам',
  'about' => 'Про проєкт',
  'dossier' => 'Досьє адвокатів',
  'all_dossier' => 'Всі досьє',
    'subcats_regions' => 'Регіони',
    'subcats_specs' => 'Спеціалізації',
  'request' => 'Запит адвокатам',
  'tracker' => 'Googllaw-трекер',
  'cabinet' => 'Кабінет адвоката',
  'faq' => 'FAQ / Напишіть нам',
  'policy' => 'Політика приватності',
  'offer' => 'Договір про публічну оферту',
  'share' => 'Поділитись продуктом',

    'title' => [
        'cabinet' => [
            'bookmarks' => [
                'index' => 'Кабінет | Закладки',
            ],
            'home' => [
                'index'=> 'Кабінет',
            ],
            'mail' => [
                'index' => 'Кабінет | Повідомлення',
                'single' => 'Кабінет | Повідомлення',
            ],
            'settings' => [
                'index' => 'Кабінет | Налаштування',
                'access' => 'Кабінет | Доступ',
                'access-history' => 'Кабінет | Історія доступів',
            ],
            'rating' => [
                'index' => 'Кабінет | Оцінки та коментарі',
                'single' => 'Кабінет | Оцінки та коментарі',
            ],
            'request' => [
                'index' => 'Кабінет | Запити',
                'single' => 'Кабінет | Запит',
            ]
        ],
        'site' => [
            'home' => [
                'index' => 'Головна'
            ],
            'faq' => [
                'index' => 'FAQ'
            ],
            'about' => [
                'index' => 'Про проєкт'
            ],
            'bookmarks' => [
                'index' => 'Закладки'
            ],
            'dossier' => [
                'index' => 'Досьє адвокатів',
                'single' => 'Досьє'
            ],
            'dossier-single' => [
                'index' => 'Досьє адвоката'
            ],
            'cabinet' => [
                'dossier-index' => 'Кабінет | Досьє',
                'dossier-form' => 'Кабінет | Досьє',
            ],
            'request' => [
                'index' => 'Запит адвокатам',
                'verification' => 'Підтвердження запиту',
                'verified' => 'Запит підтверджено',
            ],
            'tracker' => [
                'index' => 'Трекер',
                'remind-code' => 'Трекер',
                'response-list' => 'Трекер',
            ],
            'policy' => [
                'index' => 'Політика приватності'
            ],
            'auth' => [
                'forgot-password' => 'Забули пароль',
                'login' => 'Вхід',
                'register' => 'Реєстрація',
                'reset-password' => 'Відновлення пароля',
                'verify' => 'Підтвердження e-mail'
            ]
        ]
    ]
);
