<?php

return array (
  'failed' => 'Неправильні дані для входу',
  'throttle' => 'Забагато спроб входу. Спробуйте пізніше.',
  'login_header' => 'Вхід до кабінету',
  'forgot_password' => 'Забули пароль?',
  'login_action' => 'Вхід',
  'login_register' => 'Зарєструватись',
  'login_google' => 'Вхід через Google',
  'login_facebook' => 'Вхід через Facebook',
  'restore_password' => 'Відновити пароль',
    'change_password' => 'Змінити пароль',
  'restore_password_action' => 'Відновити',
    'restore_password_action_final' => 'Змінити',
  'register_header' => 'Реєстрація',
  'email_confirm_header' => 'Підтвердіть електронну пошту!',
  'email_confirm_resent' => 'Вам відправлено нове посилання для підтвердження електронної пошти',
  'email_confirm_info' => 'Для підтвердження реєстрації на Googllaw перейдіть за посиланням, направленим на зазначену Вами електронну адресу <a href="mailto::email" class="site-link">:email</a>',
  'email_check_spam' => 'Якщо Ви не бачите відповідного вхідного повідомлення на електронній пошті, то спробуйте знайти його у спамі або у кошику з рекламою.',
  'email_confirm_resend' => 'Якщо ви не отримали лист, <a class="site-link" href=":link">натисність тут, щоби отримати новий</a>.',
);
