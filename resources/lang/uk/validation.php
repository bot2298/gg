<?php

return array (
  'accepted' => 'Ви повинні дати згоду на обробку даних',
  'active_url' => 'The :attribute is not a valid URL.',
  'after' => 'The :attribute must be a date after :date.',
  'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
  'alpha' => 'Поле :attribute може містити тільки літери',
  'alpha_dash' => 'The :attribute may only contain letters, numbers, dashes and underscores.',
  'alpha_num' => 'The :attribute may only contain letters and numbers.',
  'array' => 'The :attribute must be an array.',
  'before' => 'The :attribute must be a date before :date.',
  'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
  'between' =>
  array (
    'numeric' => 'The :attribute must be between :min and :max.',
    'file' => 'The :attribute must be between :min and :max kilobytes.',
    'string' => 'The :attribute must be between :min and :max characters.',
    'array' => 'The :attribute must have between :min and :max items.',
  ),
  'boolean' => 'The :attribute field must be true or false.',
  'confirmed' => 'Підтвердження поля не співпадає',
  'date' => 'The :attribute is not a valid date.',
  'date_equals' => 'The :attribute must be a date equal to :date.',
  'date_format' => 'The :attribute does not match the format :format.',
  'different' => 'The :attribute and :other must be different.',
  'digits' => 'The :attribute must be :digits digits.',
  'digits_between' => 'The :attribute must be between :min and :max digits.',
  'dimensions' => 'The :attribute has invalid image dimensions.',
  'distinct' => 'The :attribute field has a duplicate value.',
  'email' => 'Неправильний e-mail',
  'ends_with' => 'The :attribute must end with one of the following: :values',
  'exists' => 'Даний :attribute не існує.',
  'file' => 'The :attribute must be a file.',
  'filled' => 'The :attribute field must have a value.',
  'gt' =>
  array (
    'numeric' => 'The :attribute must be greater than :value.',
    'file' => 'The :attribute must be greater than :value kilobytes.',
    'string' => 'The :attribute must be greater than :value characters.',
    'array' => 'The :attribute must have more than :value items.',
  ),
  'gte' =>
  array (
    'numeric' => 'The :attribute must be greater than or equal :value.',
    'file' => 'The :attribute must be greater than or equal :value kilobytes.',
    'string' => 'The :attribute must be greater than or equal :value characters.',
    'array' => 'The :attribute must have :value items or more.',
  ),
  'image' => 'The :attribute must be an image.',
  'in' => 'The selected :attribute is invalid.',
  'in_array' => 'The :attribute field does not exist in :other.',
  'integer' => 'Введіть число',
  'ip' => 'The :attribute must be a valid IP address.',
  'ipv4' => 'The :attribute must be a valid IPv4 address.',
  'ipv6' => 'The :attribute must be a valid IPv6 address.',
  'json' => 'The :attribute must be a valid JSON string.',
  'lt' =>
  array (
    'numeric' => 'The :attribute must be less than :value.',
    'file' => 'The :attribute must be less than :value kilobytes.',
    'string' => 'The :attribute must be less than :value characters.',
    'array' => 'The :attribute must have less than :value items.',
  ),
  'lte' =>
  array (
    'numeric' => 'The :attribute must be less than or equal :value.',
    'file' => 'The :attribute must be less than or equal :value kilobytes.',
    'string' => 'The :attribute must be less than or equal :value characters.',
    'array' => 'The :attribute must not have more than :value items.',
  ),
  'max' =>
  array (
    'numeric' => ':attribute не може бути більшою ніж :max.',
    'file' => 'Розмір файлу не може бути більшим ніж :max Кбайт',
    'string' => ':attribute не може містити більше :max символів',
    'array' => 'The :attribute may not have more than :max items.',
  ),
  'mimes' => 'Файл може бути лише таких типів: :values.',
  'mimetypes' => 'Файл може бути лише таких типів: :values.',
  'min' =>
  array (
    'numeric' => 'The :attribute must be at least :min.',
    'file' => 'The :attribute must be at least :min kilobytes.',
    'string' => ':attribute має містити не менше :min символів',
    'array' => 'The :attribute must have at least :min items.',
  ),
  'not_in' => 'The selected :attribute is invalid.',
  'not_regex' => 'The :attribute format is invalid.',
  'numeric' => 'The :attribute must be a number.',
  'present' => 'The :attribute field must be present.',
  'regex' => 'The :attribute format is invalid.',
  'required' => 'Це поле обов\'язкове для заповнення',
  'required_if' => 'The :attribute field is required when :other is :value.',
  'required_unless' => 'The :attribute field is required unless :other is in :values.',
  'required_with' => 'The :attribute field is required when :values is present.',
  'required_with_all' => 'The :attribute field is required when :values are present.',
  'required_without' => 'The :attribute field is required when :values is not present.',
  'required_without_all' => 'The :attribute field is required when none of :values are present.',
  'same' => 'The :attribute and :other must match.',
  'size' =>
  array (
    'numeric' => 'The :attribute must be :size.',
    'file' => 'The :attribute must be :size kilobytes.',
    'string' => 'The :attribute must be :size characters.',
    'array' => 'The :attribute must contain :size items.',
  ),
  'starts_with' => 'The :attribute must start with one of the following: :values',
  'string' => 'The :attribute must be a string.',
  'timezone' => 'The :attribute must be a valid zone.',
  'unique' => ':attribute вже існує.',
  'uploaded' => 'The :attribute failed to upload.',
  'url' => 'The :attribute format is invalid.',
  'uuid' => 'The :attribute must be a valid UUID.',
  'custom' =>
  array (
    'attribute-name' =>
    array (
      'rule-name' => 'custom-message',
    ),
  ),
  'attributes' =>
  array (
    'cost' => 'Вартість',
    'name' => 'Ім\'я',
    'content' => 'Суть питання адвокату',
    'newpassword' => 'Новий пароль',
      'code' => 'код',
      'region_id' => 'Регіон',
      'country_id' => 'Країна',
      'city_id' => 'Місто',
      'attachment' => 'Додаток',
      'password' => 'Пароль',
      'email' => 'E-mail'
  ),
    'cabinet_form' =>
        array (
            'image_max' => 'Максимальний розмір зображення 3 мегабайта',
            'extension' => 'Неправильне розширення ',
            'not_valid_format' => 'Невірний формат запису',
        ),
    'chat' =>
        array (
            'session_expire' => 'Повідомлення не було відпраленне, ваша сессія закінчилась, будь ласка, перезавантажте сторінку та зайдіть у трекер знову.',
        ),
    'password_confirmation' => 'Паролі не співпадають'
);
