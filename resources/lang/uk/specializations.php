<?php

return array (
  'gov' => 'Адміністративні справи (діяльність органів влади)',
  'alimenty' => 'Аліменти',
  'bank' => 'Банківські та інші фінансові справи',
  'military' => 'Військові справи',
  'gospo' => 'Господарські справи (діяльність юридичних осіб)',
  'dtp' => 'ДТП',
  'emigration' => 'Еміграція',
  'house' => 'Житлові справи',
  'consumer' => 'Захист прав споживачів',
  'land' => 'Земельні справи',
  'narco' => 'Злочини у сфері обігу наркотичних засобів (наркотики)',
  'intel' => 'Інтелектуальна власність',
  'credit' => 'Кредити',
  'krimi' => 'Кримінальні справи',
  'med' => 'Медичні справи',
  'customs' => 'Митні справи',
  'intl' => 'Міжнародні справи',
  'sea' => 'Морські перевезення',
  'estate' => 'Нерухомість',
  'pens' => 'Пенсійні справи',
  'avia' => 'Повітряне (авіа) право',
  'tax' => 'Податкові справи',
  'divorce' => 'Розлучення',
  'selo' => 'Сільське господарство',
  'family' => 'Сімейні справи',
  'heritage' => 'Спадщина',
  'admin' => 'Справи про адміністративні правопорушення',
  'insurance' => 'Страхування',
  'work' => 'Трудові спори',
  'civil' => 'Цивільні справи',
);
