<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack Crud Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD interface.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    // Forms
    'save_action_save_and_new'                => 'Зберегти і створити',
    'save_action_save_and_edit'               => 'Зберегти і продовжити редагування',
    'save_action_save_and_back'               => 'Зберегти і вийти',
    'save_action_changed_notification'        => 'Дія після збереження була змінена',

    // Create form
    'add'                                     => 'Додати',
    'back_to_all'                             => 'Повернутись до списку',
    'cancel'                                  => 'Відміна',
    'add_a_new'                               => 'Додати новий',

    // Edit form
    'edit'                                    => 'Редагувати',
    'save'                                    => 'Зберегти',

    // Revisions
    'revisions'                               => 'Версії',
    'no_revisions'                            => 'Версій не знайдено',
    'created_this'                            => 'створив це',
    'changed_the'                             => 'змінив',
    'restore_this_value'                      => 'Відновити це значення',
    'from'                                    => 'з',
    'to'                                      => 'по',
    'undo'                                    => 'Крок назад',
    'revision_restored'                       => 'Версія успішно відновлена',

    // CRUD table view
    'all'                                     => 'Всі ',
    'in_the_database'                         => 'в базі даних',
    'list'                                    => 'Список',
    'actions'                                 => 'Дії',
    'preview'                                 => 'Перегляд',
    'delete'                                  => 'Видалити',
    'admin'                                   => 'Адміністратор',
    'details_row'                             => 'Это строка сведений. Измените, пожалуйста',
    'details_row_loading_error'               => 'Помилка при завантаженні',

    // Confirmation messages and bubbles
    'delete_confirm'                          => 'Ви впевнені, що хочете видалити цей запис?',
    'delete_confirmation_title'               => 'Запис видалено',
    'delete_confirmation_message'             => 'Запис було успішно видалено',
    'delete_confirmation_not_title'           => 'НЕ видалено',
    'delete_confirmation_not_message'         => 'Виникла помилка. Запит не видалено',
    'delete_confirmation_not_deleted_title'   => 'Не видалено',
    'delete_confirmation_not_deleted_message' => 'Без змін',

    // DataTables translation
    'emptyTable'                              => 'В таблиці немає доступних даних',
    'info'                                    => 'Показано _START_ до _END_ зі _TOTAL_ збігів',
    'infoEmpty'                               => 'Показано 0 до 0 зі 0 збігів',
    'infoFiltered'                            => '(відфільтровано зі _MAX_ збігів)',
    'infoPostFix'                             => '',
    'thousands'                               => ',',
    'lengthMenu'                              => '_MENU_ записів на сторінці',
    'loadingRecords'                          => 'Завантаження...',
    'processing'                              => 'Обробка...',
    'search'                                  => 'Пошук: ',
    'zeroRecords'                             => 'Збігів не знайдено',
    'paginate'                                => [
        'first'    => 'Перша',
        'last'     => 'Остання',
        'next'     => 'Наступна',
        'previous' => 'Попередня',
    ],
    'aria'                                    => [
        'sortAscending'  => ': натисність для сортування за зростанням',
        'sortDescending' => ': натисність для сортування за спаданням',
    ],

    // global crud - errors
    'unauthorized_access'                     => 'У вас недостатньо прав для перегляду цієї сторінки',
    'please_fix'                              => 'Виправте дані помилки:',

    // global crud - success / error notification bubbles
    'insert_success'                          => 'Запис успішно додано',
    'update_success'                          => 'Запис успішно змінено',

    // CRUD reorder view
    'reorder'                                 => 'Змінити порядок',
    'reorder_text'                            => 'Використовуйте drag&drop для зміни порядку.',
    'reorder_success_title'                   => 'Готово',
    'reorder_success_message'                 => 'Порядок було збережено.',
    'reorder_error_title'                     => 'Помилка',
    'reorder_error_message'                   => 'Порядок не було збережно',

    // CRUD yes/no
    'yes'                                     => 'Так',
    'no'                                      => 'Ні',

    // CRUD filters navbar view
    'filters'                                 => 'Фільтри',
    'toggle_filters'                          => 'Перемкнути фільтри',
    'remove_filters'                          => 'Очистити фільтри',

    // Fields
    'browse_uploads'                          => 'Завантажити файли',
    'clear'                                   => 'Очистити',
    'page_link'                               => 'Посилання на сторінку',
    'page_link_placeholder'                   => 'http://example.com/your-desired-page',
    'internal_link'                           => 'Внутрішнє посилання',
    'internal_link_placeholder'               => 'Внутрішнє посилання. Наприклад: \'admin/page\' (без лапок) для \':url\'',
    'external_link'                           => 'Зовнішнє посилання',
    'choose_file'                             => 'Обрати файл',

    //Table field
    'table_cant_add'                          => 'Не вдалося додати новий :entity',
    'table_max_reached'                       => 'Максимальну кількість з :max досягнуто',

    // File manager
    'file_manager'                            => 'Файловий менеджер',
];
