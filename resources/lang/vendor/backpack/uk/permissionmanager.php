<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Permission Manager Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Laravel Backpack - Permission Manager
    | Author: Lúdio Oliveira <ludio.ao@gmail.com>, translating to Russian: Nikita K. <exotickg1@gmail.com>
    |
    */
    'name'                  => 'Ім\'я',
    'role'                  => 'Роль',
    'roles'                 => 'Ролі',
    'roles_have_permission' => 'Ролі, які мають даний дозвіл',
    'permission_singular'   => 'дозвіл',
    'permission_plural'     => 'дозволи',
    'user_singular'         => 'Користувач',
    'user_plural'           => 'Користувачі',
    'email'                 => 'E-mail',
    'extra_permissions'     => 'Додаткові дозволи',
    'password'              => 'Пароль',
    'password_confirmation' => 'Повторіть пароль',
    'user_role_permission'  => 'Дозволи ролі користувача',
    'user'                  => 'Користувач',
    'users'                 => 'Користувачі',
];
