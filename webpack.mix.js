const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/app.scss', 'public/css')
    .styles(['public/css/main.css'], 'public/css/main.min.css')
    .scripts(['public/js/main.js'], 'public/js/main.min.js')
    .js('resources/js/vendor.js', 'public/js')
    .js('resources/js/bookmarks.js', 'public/js')
    .js('resources/js/tracker.js', 'public/js')
    .js('resources/js/dossier', 'public/js')
    .js('resources/js/dossier-single', 'public/js')
    .js('resources/js/cabinet-chat', 'public/js')
    .js('resources/js/access.js', 'public/js')
    .js('resources/js/request.js', 'public/js');
