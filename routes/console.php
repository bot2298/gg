<?php

use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use Illuminate\Foundation\Inspiring;
use Spatie\Sitemap\SitemapGenerator;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('js:trans', function () {
    Artisan::call('vue-i18n:generate');
})->describe('generate js translations');

Artisan::command('regions:ukraine', function() {
    function translit($string){
        return str_replace(' ','_', transliterator_transliterate('Ukrainian-Latin/BGN; [\u0080-\u7fff] remove; NFD; [:Nonspacing Mark:] Remove; NFC; [:Punctuation:] Remove; Lower();', $string));
    }

    $this->info('Deleting country Ukraine');
    Country::where('name','ukraine')->delete();
    $this->info('Adding country Ukraine');

    $country = new Country();
    $country->name = 'ukraine';
    $country->active = 1;
    $country->save();

    $url = 'http://api.novaposhta.ua/v2.0/json/';

    $regions = [
        'ukraine_vinnytska' => 'dcaad5a7-4b33-11e4-ab6d-005056801329',
        'ukraine_volynska' => 'dcaad676-4b33-11e4-ab6d-005056801329',
        'ukraine_dnipropetrovska' => 'dcaad735-4b33-11e4-ab6d-005056801329',
        'ukraine_donetska' => 'dcaad81c-4b33-11e4-ab6d-005056801329',
        'ukraine_zhytomyrska' => 'dcaad8fb-4b33-11e4-ab6d-005056801329',
        'ukraine_zakarpatska' => 'dcaad993-4b33-11e4-ab6d-005056801329',
        'ukraine_zaporizka' => 'dcaada26-4b33-11e4-ab6d-005056801329',
        'ukraine_ivanofrankivska' => 'dcaadac6-4b33-11e4-ab6d-005056801329',
        'ukraine_kyivska' => 'dcaadb64-4b33-11e4-ab6d-005056801329',
        'ukraine_kirovohradska' => 'dcaadbf9-4b33-11e4-ab6d-005056801329',
        'ukraine_luhanska' => 'dcaadc91-4b33-11e4-ab6d-005056801329',
        'ukraine_lvivska' => 'dcaadd3a-4b33-11e4-ab6d-005056801329',
        'ukraine_mykolaivska' => 'dcaaddd7-4b33-11e4-ab6d-005056801329',
        'ukraine_odeska' => 'dcaade6d-4b33-11e4-ab6d-005056801329',
        'ukraine_poltavska' => 'dcaadf02-4b33-11e4-ab6d-005056801329',
        'ukraine_rivnenska' => 'dcaadfa0-4b33-11e4-ab6d-005056801329',
        'ukraine_sumska' => 'dcaae036-4b33-11e4-ab6d-005056801329',
        'ukraine_ternopilska' => 'dcaae303-4b33-11e4-ab6d-005056801329',
        'ukraine_kharkivska' => 'dcaae3a1-4b33-11e4-ab6d-005056801329',
        'ukraine_khersonska' => 'dcaae44b-4b33-11e4-ab6d-005056801329',
        'ukraine_khmelnytska' => 'dcaae4e5-4b33-11e4-ab6d-005056801329',
        'ukraine_cherkaska' => 'dcaae57c-4b33-11e4-ab6d-005056801329',
        'ukraine_chernivetska' => 'dcaae60e-4b33-11e4-ab6d-005056801329',
        'ukraine_chernihivska' => 'dcaae6a8-4b33-11e4-ab6d-005056801329',
    ];
    $bar = $this->output->createProgressBar(count($regions));
    $bar->start();

    $kyiv = new Region();
    $kyiv->name = 'ukraine_kyiv';
    $kyiv->country_id = $country->id;
    $kyiv->save();

    $kyiv_districts = [
        'ukraine_kyiv_golosiivskyi',
        'ukraine_kyiv_darnytskyi',
        'ukraine_kyiv_desnyanskyi',
        'ukraine_kyiv_dniprovskyi',
        'ukraine_kyiv_obolonskyi',
        'ukraine_kyiv_pecherskyi',
        'ukraine_kyiv_podilskyi',
        'ukraine_kyiv_svyatoshynskyi',
        'ukraine_kyiv_solomyanskyi',
        'ukraine_kyiv_shevchenkivskyi',
    ];

    $this->info(PHP_EOL.'Creating Kyiv');
    foreach ($kyiv_districts as $kyiv_district){
        $district = new City();
        $district->name = $kyiv_district;
        $district->region_id = $kyiv->id;
        $district->save();
    }


    foreach($regions as $name => $id){
        $this->info(PHP_EOL.'Parsing region: '.__('regions.'.$name));
        $counter = 0;
        $body = json_encode([
            'apiKey' => 'fab19be7f0886b1f3ad41fba2039c559',
            'modelName' => 'AddressGeneral',
            'calledMethod' => 'getSettlements',
            'methodProperties' => [
                'AreaRef' => $id,
                "Page" => 1,
            ],
        ]);

        $client = new \GuzzleHttp\Client();

        $response = $client->post($url, [
            'body' => $body
        ]);

        $data = $response->getBody()->getContents();
        $data = json_decode($data);

        $pages = (int)ceil((int)$data->info->totalCount / 150);

        $region = new Region();
        $region->name = $name;
        $region->country_id = $country->id;
        $region->save();
        for ($i = 1; $i <= $pages; $i++){
            $body = json_encode([
                'apiKey' => 'fab19be7f0886b1f3ad41fba2039c559',
                'modelName' => 'AddressGeneral',
                'calledMethod' => 'getSettlements',
                'methodProperties' => [
                    'AreaRef' => $id,
                    "Page" => $i
                ],
            ]);

            $client = new \GuzzleHttp\Client();

            $response = $client->post($url, [
                'body' => $body
            ]);

            $data = $response->getBody()->getContents();
            $data = json_decode($data);

            foreach ($data->data as $item) {
                if ($item->SettlementTypeDescription === 'місто') {
                    $city = new City();
                    $city->name = $name."_".translit($item->Description);
                    $city->region_id = $region->id;
                    $city->save();
                    $counter++;
                }
            }
        }
        $this->info('Parsed '.$counter." cities");
        $bar->advance();
    }
    $this->info(PHP_EOL.'Regions (Ukraine) parsed');

    $this->info('Crimea');
    $crimea = new Region();
    $crimea->name = 'ukraine_crimea';
    $crimea->country_id = $country->id;
    $crimea->save();

    $sevas = new Region();
    $sevas->name = 'ukraine_sevastopol';
    $sevas->country_id = $country->id;
    $sevas->save();

})->describe('parse regions (Ukraine)');

Artisan::command('install:fresh', function() {
    $this->info('Migrating fresh');
    Artisan::call('migrate:fresh');
    $this->info('Parsing regions (Ukraine)');
    Artisan::call('regions:ukraine');
    $this->info('Seeding');
    Artisan::call('db:seed');
})->describe('fresh db, parse regions and seed');

Artisan::command('currency:update', function() {
    $client = new \GuzzleHttp\Client();
    $date = \Illuminate\Support\Carbon::yesterday()->format('d.m.Y');
    $endpoint =  "https://api.privatbank.ua/p24api/exchange_rates?json&date=$date";

    $this->info("Currency update: $date");
    $response = $client->get($endpoint);
    if($response->getStatusCode() == 200) {
        $data = $response->getBody()->getContents();
        $data = json_decode($data);
        foreach($data->exchangeRate as $currency_rate){
            if($currency = \App\Models\Currency::where('name', $currency_rate->currency ?? '')->first()){
                $currency->rate = $currency_rate->saleRateNB;
                $currency->save();
                $this->info($currency->name." updated");
            }
        }
    }
})->describe('update currencies using Privatbank API');

Artisan::command('access:notify {days}', function($days) {
    $this->info("Date ".\Carbon\Carbon::now()->addDays((int)$days)->format('Y-m-d'));
    $access_to_notify = \App\Models\Access
        ::whereDate('active_until', '=', \Carbon\Carbon::now()->addDays((int)$days)->format('Y-m-d'))
        ->get();
    foreach($access_to_notify as $access) {
        $access->user->notify(new \App\Notifications\AccessExpiring($access));
        $access->save();
    }
    $this->info($access_to_notify->count()." users notified");
})->describe('notify users whose access expires in {days}');

Artisan::command('sitemap:generate', function() {
    SitemapGenerator::create(config('app.url'))->writeToFile(public_path('sitemap.xml'));
    $this->info('Sitemap generated');
})->describe('generate sitemap');
