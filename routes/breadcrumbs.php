<?php

Breadcrumbs::for('home', function ($trail) {
    $trail->push(__('layout.home'), route('index', app()->getLocale()));
});

Breadcrumbs::for('about', function ($trail) {
    $trail->parent('home');
    $trail->push(__('layout.about'), route('about', app()->getLocale()));
});

Breadcrumbs::for('bookmarks', function ($trail) {
    $trail->parent('home');
    $trail->push(__('layout.bookmarks'), route('bookmarks', app()->getLocale()));
});

Breadcrumbs::for('dossier', function ($trail) {
    $trail->parent('home');
    $trail->push(__('layout.dossier'), route('dossier', app()->getLocale()));
});
Breadcrumbs::for('dossier-cat', function ($trail, $url) {
    $page = \App\Models\Page::findByLocation($url);
    $trail->parent('dossier');
    if($page->specialization){
        $trail->push(__('specializations.'.$page->specialization->name), route('dossier.category', ['locale' => app()->getLocale(), 'cat' => \App\Models\Page::where('specialization_id', $page->specialization->id)->firstOrFail()->location]));
    }
    if($page->region){
        $trail->push(__('regions.'.$page->region->name), route('dossier.category', ['locale' => app()->getLocale(), 'cat' => \App\Models\Page::where('region_id', $page->region_id)->firstOrFail()->location]));
    }
});
Breadcrumbs::for('dossier-single', function ($trail) {
    $trail->parent('dossier');
    $trail->push(__('layout.title.site.dossier-single.index'), route('dossier', app()->getLocale()));
});

Breadcrumbs::for('request', function ($trail) {
    $trail->parent('home');
    $trail->push(__('layout.request'), route('request', app()->getLocale()));
});

Breadcrumbs::for('tracker', function ($trail) {
    $trail->parent('home');
    $trail->push(__('layout.title.site.tracker.index'), route('tracker.index', app()->getLocale()));
});

Breadcrumbs::for('faq', function ($trail) {
    $trail->parent('home');
    $trail->push(__('layout.title.site.faq.index'), route('faq', app()->getLocale()));
});

Breadcrumbs::for('policy', function ($trail) {
    $trail->parent('home');
    $trail->push(__('layout.title.site.policy.index'), route('policy', app()->getLocale()));
});


