<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\CheckApplicationAccess;
use App\Models\Region;
use Illuminate\Http\Request;

Route::group(['middleware' => 'language', 'prefix' => '{locale}', 'where' => ['locale' => '[a-zA-Z]{2}']], function () {
    Route::get('/', 'IndexController@index')->name('index');
    Route::get('about', 'IndexController@about')->name('about');
    Route::get('bookmarks', 'IndexController@bookmarks')->name('bookmarks');
    Route::get('faq', 'IndexController@faq')->name('faq');
    Route::post('feedback', 'FeedbackController@store')->name('feedback');
    Route::get('policy', 'IndexController@policy')->name('policy');
    Route::get('dossier', 'IndexController@dossier')->name('dossier');
    Route::get('dossier/{cat}', 'DossierCategoriesController@index')->name('dossier.category');
    Route::get('dossier/view/{dossier}', 'IndexController@dossiersingle')->name('dossier.single');

    Route::get('request', 'ApplicationController@requestForm')->name('request');
    Route::post('request/regions', 'ApplicationController@regions');
    Route::post('request/cities', 'ApplicationController@cities');
    Route::post('request', 'ApplicationController@create')->name('request.create');

    Route::get('request/update', 'ApplicationController@showEditForm')->name('request.edit');
    Route::get('request/change-email', 'ApplicationController@changeEmail')->name('request.change-email');
    Route::post('request/update', 'ApplicationController@update')->name('request.update');
    Route::post('request/stop', 'ApplicationController@stop')->name('request.stop');
    Route::post('request/exit', 'ApplicationController@exit')->name('request.exit');

    Route::get('request/verify', 'ApplicationController@verify')->name('request.verify');
    Route::get('request/verification', 'ApplicationController@verification')->name('request.verification');
    Route::get('request/verified', 'ApplicationController@verified')->name('request.verified');
    Route::post('request/verified', 'ApplicationController@sendCode')->name('request.send-code');

    Route::get('/tracker', 'TrackerController@index')->name('tracker.index');
    Route::get('tracker/remind-code', 'TrackerController@remindCode')->name('tracker.remind.code');
    Route::get('tracker/request', 'TrackerController@application')->middleware(CheckApplicationAccess::class)->name('tracker.application');


    Route::post('tracker/rate', 'RatingController@store')->name('site.tracker.rate');
    Route::post('tracker/complaint', 'ComplaintController@lawyerTypeStore')->name('tracker.complaint');

    Route::prefix('cabinet')->middleware(['verified'])->group(function () {
        Route::get('/', 'CabinetController@index')->name('cabinet.index');

        Route::get('request', 'ApplicationController@index')->name('cabinet.request');
        Route::get('request/{application}', 'ApplicationController@show')->name('cabinet.request-single');
        Route::post('request/delete', 'ApplicationController@delete')->name('cabinet.request.delete');

        Route::post('request/complaint', 'ComplaintController@applicationTypeStore')->name('cabinet.request.complaint');
        Route::post('rating/complaint', 'ComplaintController@ratingTypeStore')->name('cabinet.rating.complaint');

//        Route::get('bookmarks', 'ApplicationBookmarkController@index')->name('cabinet.bookmarks');
        Route::post('bookmarks/toggle', 'ApplicationBookmarkController@toggle')->name('cabinet.bookmarks.toggle');
        Route::post('bookmarks/remove', 'ApplicationBookmarkController@remove')->name('cabinet.bookmarks.remove');

        Route::get('mail', 'CabinetController@mail')->name('cabinet.mail');

        Route::get('rating', 'RatingController@index')->name('cabinet.rating');
        Route::get('rating/{rating}', 'RatingController@show')->name('cabinet.rating-single');

        Route::get('settings/index', 'ChangePasswordController@index')->name('cabinet.settings');
        Route::post('settings/index', 'ChangePasswordController@store')->name('cabinet.change-password');

        Route::post('settings/access/regions', 'AccessController@regions');
        Route::post('settings/access/toggle', 'AccessController@toggleActive')->name('cabinet.access.toggle');
        Route::post('settings/access/toggle_new', 'AccessController@toggleActiveNew')->name('cabinet.access.toggle_new');
//        Route::post('settings/access/delete', 'AccessController@delete')->name('cabinet.access.delete');
        Route::get('settings/access', 'AccessController@showForm')->name('cabinet.access');
        Route::post('settings/access', 'AccessController@create')->name('cabinet.access.create');
        Route::get('settings/access/{access}', 'AccessController@showEditForm')->name('cabinet.access.edit');
//        Route::post('settings/access/{access}', 'AccessController@update')->name('cabinet.access.update');
        Route::get('settings/access-history', 'AccessController@history')->name('cabinet.access-history');

        Route::get('dossier', 'DossierController@index')->name('cabinet.dossier.index');
        Route::get('dossier/create', 'DossierController@create')->name('cabinet.dossier.create');
        Route::post('dossier/store', 'DossierController@store')->name('cabinet.dossier.store');
        Route::get('dossier/{id}/edit', 'DossierController@edit')->name('cabinet.dossier.edit');
        Route::put('dossier/{id}/update', 'DossierController@update')->name('cabinet.dossier.update');
        Route::delete('dossier/{id}/destroy', 'DossierController@destroy')->name('cabinet.dossier.destroy');
        Route::patch('dossier/{id}/stop', 'DossierController@stop')->name('cabinet.dossier.stop');
        Route::patch('dossier/{id}/activate', 'DossierController@activate')->name('cabinet.dossier.activate');
});

    Auth::routes(['verify' => true]);
});
Route::post('register/clean', 'IndexController@cleanReg')->name('register.clean');
Route::post('login/verify-check', 'IndexController@isUserVerified')->name('verify.check');
Route::post('request/verify-check', 'IndexController@isApplicationVerified')->name('application.verify.check');

Route::post('dossier/{id}/bookmark/delete', 'DossierController@deleteFromBookmark');
Route::post('dossier/{id}/bookmark/add', 'DossierController@addToBookmark');

Route::get('login/facebook', 'Auth\LoginController@redirectToFacebook')->name('login.facebook');
Route::get('login/facebook/callback', 'Auth\LoginController@handleFacebookCallback');
Route::get('login/google', 'Auth\LoginController@redirectToGoogle')->name('login.google');
Route::get('login/google/callback', 'Auth\LoginController@handleGoogleCallback');

Route::get('/', 'IndexController@home');
