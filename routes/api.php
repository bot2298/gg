<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('dossier', 'API\DossierController@getAll');

Route::post('tracker/request/email', 'API\TrackerController@getApplication');
Route::post('tracker/request/remind', 'API\TrackerController@remindCodeMail');
Route::post('tracker/request/sendmail', 'API\TrackerController@sendAccessMail');

Route::post('chat/start', [
    'uses' => 'API\ChatsController@startChat', 'as' => 'start.chat'
]);

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('messages/{id}', 'API\ChatsController@fetchMessages');
    Route::post('message/store', 'API\ChatsController@sendMessage');
    Route::post('chat/{id}/viewed', 'API\ChatsController@updateSeenStatus');
});
