<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::group(['middleware' => ['role:Адміністратор']], function () {
        CRUD::resource('region', 'RegionCrudController');
        CRUD::resource('country', 'CountryCrudController');
        Route::post('country/{country}/toggle-status', 'CountryCrudController@toggleStatus');
        CRUD::resource('city', 'CityCrudController');
    });

    Route::group(['middleware' => ['permission:Адвокати.Статистика']], function () {
        Route::get('dossiers/stats', 'LawyersStatsController@index')->name('dossier-stats');
    });

    Route::group(['middleware' => ['permission:Адвокати.Заявки']], function () {
        CRUD::resource('dossier', 'DossierCrudController');
        Route::post('dossier/{dossier}/block', 'DossierCrudController@block');
        Route::post('dossier/{dossier}/approve', 'DossierCrudController@approve');
    });

    Route::group(['middleware' => ['permission:Адвокати.Профілі']], function () {
        CRUD::resource('user', 'UserCrudController');
        CRUD::resource('card', 'CardCrudController');
    });

    Route::group(['middleware' => ['permission:Клієнти.Статистика']], function () {
        Route::get('application/stats', 'ApplicationStatsController@index')->name('application-stats');
    });

    Route::group(['middleware' => ['permission:Клієнти.Запити']], function () {
        CRUD::resource('application', 'ApplicationCrudController');
    });

    Route::group(['middleware' => ['permission:Клієнти.Оцінки-та-коментарі']], function () {
        CRUD::resource('rating', 'RatingCrudController');
    });

    Route::group(['middleware' => ['permission:Клієнти.Скарги-на-запити']], function () {
        CRUD::resource('application-complaint', 'ApplicationComplaintCrudController');
    });

    Route::group(['middleware' => ['permission:Клієнти.Скарги-на-оцінки-та-коментарі']], function () {
        CRUD::resource('rating-complaint', 'RatingComplaintCrudController');
    });

    Route::group(['middleware' => ['permission:Адвокати.Розсилка-повідомлень']], function () {
        CRUD::resource('chat', 'ChatCrudController');
        Route::get('mailing', 'ChatCrudController@mailingForm')->name('mailing.form');
        Route::post('mailing', 'ChatCrudController@mailing')->name('mailing.store');
    });
    Route::group(['middleware' => ['permission:Адвокати.Скарги-на-адвокатів']], function () {
        CRUD::resource('lawyer-complaint', 'LawyerComplaintCrudController');
    });

    Route::group(['middleware' => ['permission:SEO']], function () {
        CRUD::resource('page', 'PageCrudController');
        CRUD::resource('pageinfo', 'PageInfoCrudController');
    });

    Route::group(['middleware' => ['permission:Фідбек']], function () {
        CRUD::resource('feedback', 'FeedbackCrudController');
    });

    Route::group(['middleware' => ['permission:Валюти']], function () {
        CRUD::resource('currency', 'CurrencyCrudController');
    });

    Route::group(['middleware' => ['permission:Адвокати.Профілі|Клієнти.Оцінки-та-коментарі']], function () {
        Route::post('user/{user}/toggle-block', 'UserCrudController@toggleBlock');
    });
    Route::group(['middleware' => ['permission:Клієнти.Запити|Клієнти.Скарги-на-запити']], function () {
        Route::post('application/{application}/toggle-block', 'ApplicationCrudController@toggleBlock');
    });
    Route::group(['middleware' => ['permission:Клієнти.Скарги-на-оцінки-та-коментарі']], function () {
        Route::post('rating/{rating}/toggle-block', 'RatingCrudController@toggleBlock');
    });

}); // this should be the absolute last line of this file
