<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Dossier;
use App\Models\General;
use Faker\Generator as Faker;

$factory->define(General::class, function (Faker $faker) {
    return [
        'fullname' => $faker->name,
        "photo" => '/img/dossier-single/lawyer_big.png',
        "office" => [
            "country" =>\App\Models\Country::all()->random()->id,
            "region" => \App\Models\Region::all()->random()->id,
            "city" => \App\Models\City::all()->random()->id,
            "address" => [
                $faker->address, $faker->address
            ],
            "phones" => [
                [
                    "number" => "380931234567",
                    "messengers" => ["viber" => "viber", "telegram" => "telegram"]
                ],
                [
                    "number" => "380975431231",
                    "messengers" => ["whatsapp" => "whatsapp", "skype" => "skype", "telegram" => "telegram"]
                ]
            ]
        ],
        'dossier_id' => Dossier::all()->random()
    ];
});
