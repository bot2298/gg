<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Dossier;
use App\Models\Experience;
use App\Models\General;
use Faker\Generator as Faker;

$factory->define(Dossier::class, function (Faker $faker) {
    $created = \Illuminate\Support\Carbon::now()->subDays(rand(1,100))->addMinutes(rand(0,1440));
    return [
        'region_id' => \App\Models\Region::all()->random(),
        'status' => [Dossier::STATUS_SAVED, Dossier::STATUS_NEW, Dossier::STATUS_ACTIVE, Dossier::STATUS_REJECTED, DOSSIER::STATUS_NOT_ACTIVE][rand(0,4)],
        'locale' => array_rand(config('app.locales')),
        'created_at' => $created,
        'updated_at' => $created,
        'license' => [
            "country" => 1,
            "institution" => "Міністерство адвокатських справ",
            "year" => 2020,
            "number" => "52343112",
            "photo" => '/img/dossier-single/photo.png'
        ],
        'overview' => [
            "languages" => "Українська, російська, англійська",
            "motto" => "Двічі в одну ріку не ввійдеш",
            "strong_sides" => "пунктуальність, кмітливість",
            "hobbies" => "гольф, велосипед",
            "site" => "https://googllaw.com",
            "facebook" => "https://facebook.com/googllaw",
            "facebook_followers" => 312,
            "instagram" => "https://instagram.com/googllaw",
            "instagram_followers" => 53443,
            "linkedin" => "https://linkedin.com/googllaw",
            "linkedin_followers" => 1000,
            "twitter" => "https://twitter.com/googllaw",
            "twitter_followers" => 3217,
            "youtube" => "https://youtube.com/googllaw",
            "youtube_followers" => 8766
        ],
        'contacts' => [
            "emails" => [$faker->freeEmail(), $faker->freeEmail()],
            "working_hours" => [
                "mo" => [
                    "start" => 10,
                    "end" => 18
                ],
                "tu" => [
                    "start" => 10,
                    "end" => 18
                ],
                "we" => [
                    "start" => 10,
                    "end" => 18
                ],
                "th" => [
                    "start" => 10,
                    "end" => 18
                ],
                "fr" => [
                    "start" => 10,
                    "end" => 18
                ],
                "sa" => [
                    "start" => 10,
                    "end" => 16
                ],
                "su" => [
                    "start" => 10,
                    "end" => 16
                ]
            ]
        ],
        'education' => [
            "higher" => [
                [
                    "from" => 2011,
                    "to" => 2016,
                    "country" => "Україна",
                    "university" => "Вінницький національний технічний університет",
                    "degree" => "магістр",
                    "field" => "Право",
                    "specialty" => "Міжнародне право",
                    "photo" => '/img/dossier-single/photo.png'
                ],
                [
                    "from" => 2016,
                    "to" => 2020,
                    "country" => "Україна",
                    "university" => "Київська юридична академія",
                    "degree" => "магістр",
                    "field" => "Право",
                    "specialty" => "Кримінальне право",
                    "photo" => "/img/dossier-single/photo.png"
                ]
            ],
            "academic_status" => [
                [
                    "from" => 2018,
                    "to" => 2019,
                    "university" => "Київська юридична академія",
                    "degree" => "Доктор юридичних наук",
                    "photo" => '/img/dossier-single/photo.png'
                ]
            ],
            "teaching" => [
                [
                    "from" => 2019,
                    "to" => 2020,
                    "university" => "Київська юридична академія",
                    "topic" => "Криміналістика"
                ]
            ]
        ],
        'experience_other' => [
            "experience" => [
                [
                    "from" => 2018,
                    "to" => 2019,
                    "position" => "помічник",
                    "place_of_work" => "Юридична компанія ABC"
                ],
                [
                    "from" => 2019,
                    "to" => 2020,
                    "position" => "босс",
                    "place_of_work" => "Юридична компанія Googllaw"
                ]
            ],
            "patent_license" => [
                "country" => "Україна",
                "institution" => "Міністерство адвокатських справ",
                "year" => 2020,
                "number" => "812974",
                "photo" => '/img/dossier-single/photo.png'
            ]
        ],
        'training' => [
            [
                "from" => "04-11-2011",
                "to" => "04-12-2011",
                "country" => "Україна",
                "university" => "Вінницький національний технічний університет",
                "topic" => "Криміналістика від А до Я",
                "photo" => '/img/dossier-single/photo.png'
            ],
            [
                "from" => "08-11-2016",
                "to" => "18-12-2016",
                "country" => "Україна",
                "university" => "Вінницький національний технічний університет",
                "topic" => "Сімейне право від А до Я",
                "photo" => '/img/dossier-single/photo.png'
            ]
        ],
        'publications' => [
            [
                "edition" => "Козятинські вісті",
                "topic" => "Мій шлях",
                "url" => "https://google.com",
                "photo" => '/img/dossier-single/photo.png'
            ],
            [
                "edition" => "Вінниця юридична",
                "topic" => "Моя боротьба",
                "url" => "https://google.com",
                "photo" => '/img/dossier-single/photo.png'
            ]
        ],
        'recommendations' => [
            [
                "fullname" => "Василенко Василь",
                "position" => "тракторист",
                "place_of_work" => "поле",
                "document" => '/img/dossier-single/photo.png'
            ],
            [
                "fullname" => "Сінченко Валерій",
                "position" => "підприємець",
                "place_of_work" => "ТОВ Вазон",
                "document" => '/img/dossier-single/photo.png'
            ]
        ],
        'honors' => [
            [
                "year" => 2018,
                "name" => "Кращий працівник року 2018"
            ],
            [
                "year" => 2019,
                "name" => "Кращий працівник року 2019"
            ]
        ],
        'services' => [
            [
                "name" => "Захист в суді",
                "cost" => "200",
                "currency" => "uah"
            ],
            [
                "name" => "Нотаріус",
                "cost" => "100",
                "currency" => "uah"
            ]
        ],
    ];
});
