<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Experience;
use App\Models\Spec;
use Faker\Generator as Faker;

$factory->define(Spec::class, function (Faker $faker) {
    $activities = ['consultations', 'accompaniment', 'preparation_doc', 'representation'];
    $specializations = [
        'gov', 'alimenty', 'bank', 'military', 'gospo', 'dtp', 'emigration', 'house', 'consumer', 'land', 'narco',
        'intel', 'credit', 'krimi', 'med', 'customs', 'intl', 'sea', 'estate', 'pens', 'avia', 'tax', 'divorce', 'selo',
        'family', 'heritage', 'admin', 'insurance', 'work', 'civil',
    ];
    return [
        'dossier_id' => \App\Models\Dossier::all()->random(),
        "specializations" => [$specializations[$faker->numberBetween(0, 28)], $specializations[$faker->numberBetween(0, 28)]],
        "activities" => [$activities[$faker->numberBetween(0, 3)], $activities[$faker->numberBetween(0, 3)]],
        "form" => "Натуральна",
        "appeal_exp" => $faker->numberBetween(0, 5),
        "appeal_cases" => [
            $faker->realText()
        ],
        "cassation_exp" => $faker->numberBetween(0, 5),
        "cassation_cases" => [
            $faker->realText()
        ],
        "intl_exp" => $faker->numberBetween(0, 5),
        "intl_cases" => [
            $faker->realText()
        ],
    ];
});
