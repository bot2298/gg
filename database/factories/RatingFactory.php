<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Rating;
use Faker\Generator as Faker;

$factory->define(Rating::class, function (Faker $faker) {
    $created = \Carbon\Carbon::now()->subDays(rand(1,100))->addMinutes(rand(0,1440));
    return [
        'client_email' => \App\Models\Application::all()->pluck('email')->random(),
        'user_id' => \App\Models\User::all()->random(),
        'stars' => rand(1,5),
        'comment' => $faker->text($maxNbChars = 500),
        'created_at' => $created,
        'updated_at' => $created
    ];
});
