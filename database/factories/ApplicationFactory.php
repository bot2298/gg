<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Application;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;use Illuminate\Support\Str;

$factory->define(Application::class, function (Faker $faker) {
    $created_at = Carbon::now()->subDays(rand(1,100))->addMinutes(rand(0,1440));
    $deadline = $created_at->copy()->addDays(rand(0,100));
    return [
        'code' => substr(str_shuffle(str_repeat($x='0123456789', ceil(12/strlen($x)) )),1,12),
        'token' => Str::random(60),
        'region_id' => \App\Models\Region::all()->random(),
        'city_id' => null,
        'specialization_id' => \App\Models\Specialization::all()->random(),
        'content' => $faker->sentences(20, true),
        'attachment' => '',
        'cost' => rand(50, 10000),
        'currency_id' => \App\Models\Currency::all()->random(),
        'deadline' => $deadline,
        'name' => $faker->name,
        'email' => $faker->email,
        'phone' => '+380 (93) 11-22-333',
        'phone_locale' => 1,
        'status' => [
            Application::STATUS_UNVERIFIED,
            Application::STATUS_ACTIVE,
            Application::STATUS_STOPPED,
            Application::STATUS_BLOCKED
        ][rand(0,2)],
        'created_at' => $created_at
    ];
});
