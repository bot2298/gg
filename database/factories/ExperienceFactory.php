<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Dossier;
use App\Models\Experience;
use App\Models\Spec;
use Faker\Generator as Faker;

$factory->define(Experience::class, function (Faker $faker) {
    return [
        'dossier_id' => \App\Models\Dossier::all()->random(),
        'years' => $faker->numberBetween(0, 5),
        'countries' => ["Україна", "Африка"],
        'regions' => ["Вінницька область", "Хмельницька область"],
        'cities' => ["Вінниця", "Хмельницький"],
    ];
});
