<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->json('specializations')->nullable();
            $table->json('activities')->nullable();
            $table->string('form')->nullable();
            $table->string('appeal_exp')->nullable();
            $table->json('appeal_cases')->nullable();
            $table->string('cassation_exp')->nullable();
            $table->json('cassation_cases')->nullable();
            $table->string('intl_exp')->nullable();
            $table->json('intl_cases')->nullable();
            $table->bigInteger('dossier_id')->unsigned()->nullable();
            $table->foreign('dossier_id')
                ->references('id')->on('dossiers')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specs');
    }
}
