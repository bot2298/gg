<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDossiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dossiers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('locale');
            $table->tinyInteger('status')->default(0);
            $table->bigInteger('region_id')->unsigned();
            $table->bigInteger('city_id')->unsigned()->nullable();
            $table->json('license');
            $table->json('overview')->nullable();
            $table->json('contacts')->nullable();
            $table->json('education')->nullable();
            $table->json('experience_other')->nullable();
            $table->json('training')->nullable();
            $table->json('publications')->nullable();
            $table->json('recommendations')->nullable();
            $table->json('honors')->nullable();
            $table->json('services')->nullable();
            $table->string('qr')->nullable();
            $table->timestamp('admin_seen_at')->nullable();
            $table->bigInteger('seen_counter')->unsigned()->default(0);
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('region_id')
                ->references('id')->on('regions')
                ->onDelete('cascade');
            $table->foreign('city_id')
                ->references('id')->on('cities')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resumes');
    }
}
