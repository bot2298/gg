<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToComplaints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('complaints', function (Blueprint $table) {
            $table->string('fullname')->nullable()->after('response');
            $table->string('address')->nullable()->after('fullname');
            $table->string('contact_email')->nullable()->after('address');
            $table->string('contact_number')->nullable()->after('contact_email');
            $table->string('attachment')->nullable()->after('contact_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('complaints', function (Blueprint $table) {
            $table->dropColumn('fullname');
            $table->dropColumn('address');
            $table->dropColumn('contact_email');
            $table->dropColumn('contact_number');
            $table->dropColumn('attachment');
        });
    }
}
