<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToFeedback extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('feedback', function (Blueprint $table) {
            $table->string('place_of_living')->nullable()->after('message');
            $table->string('postal_address')->nullable()->after('place_of_living');
            $table->string('contact_number')->nullable()->after('postal_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feedback', function (Blueprint $table) {
            $table->dropColumn('place_of_living');
            $table->dropColumn('postal_address');
            $table->dropColumn('contact_number');
        });
    }
}
