<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->nullable()->unique();
            $table->string('token')->unique();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->bigInteger('region_id')->unsigned();
            $table->bigInteger('city_id')->unsigned()->nullable();
            $table->bigInteger('specialization_id')->unsigned()->nullable();
            $table->text('content');
            $table->string('attachment')->nullable();
            $table->bigInteger('cost')->unsigned()->default(0);
            $table->bigInteger('currency_id')->unsigned();
            $table->timestamp('deadline');
            $table->string('name');
            $table->string('email');
            $table->string('phone')->nullable();
            $table->tinyInteger('phone_locale')->unsigned()->nullable();
            $table->tinyInteger('status')->unsigned()->default(0);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('region_id')
                ->references('id')->on('regions')
                ->onDelete('cascade');

            $table->foreign('city_id')
                ->references('id')->on('cities')
                ->onDelete('cascade');

            $table->foreign('currency_id')
                ->references('id')->on('currencies')
                ->onDelete('cascade');

            $table->foreign('specialization_id')
                ->references('id')->on('specializations')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
