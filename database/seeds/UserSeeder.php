<?php

use App\Models\General;
use App\Models\Region;
use Illuminate\Database\Seeder;
use SimpleSoftwareIO\QrCode;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lawyer_role = Role::create(['name' => 'Адвокат', 'guard_name' => 'web']);
        $client_role = Role::create(['name' => 'Клієнт', 'guard_name' => 'web']);

        if(config('app.debug')){
            //lawyers
            factory(\App\Models\User::class, 100)->create()->each(function ($user) {
                $user->assignRole('Адвокат');
                $rand_region = \App\Models\Region::all()->random();
                \App\Models\Access::create([
                    'user_id' => $user->id,
                    'region_id' => $rand_region->id,
                    'active_until' => \Carbon\Carbon::now()->addDays(30)
                ]);
                $user->dossiers()->saveMany(factory(\App\Models\Dossier::class, 2)->create()->each(function ($dossier) use ($rand_region) {
                    $dossier->region_id = $rand_region->id;
                    if(\App\Models\City::where('region_id', $rand_region->id)->get()->isNotEmpty()){
                        $dossier->city_id = \App\Models\City::where('region_id', $rand_region->id)->get()->random()->id;
                    }
                    $url = config('app.url').route('dossier.single', ['locale' => app()->getLocale(), 'dossier' => $dossier->id], false);
                    $path = "qr_codes/$dossier->id.png";
                    \QrCode::size(500)->format('png')->generate($url, public_path("storage/$path"));
                    $dossier->qr = $path;
                    $dossier->save();
                    $dossier->general()->save(factory(General::class)->make());
                }));
            });

            //clients
            factory(\App\Models\User::class, 250)->create()->each(function ($user) {
                $user->assignRole('Клієнт');
                $user->applications()->saveMany(factory(\App\Models\Application::class, 1)->create());
            });
        }

        $test_user = \App\Models\User::create([
            'email' => 'test@mail.com',
            'password' => Hash::make('qwerty123'),
            'api_token' => \Illuminate\Support\Str::random(60),
            'email_verified_at' => now(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $test_user->assignRole('Адвокат');
    }
}
