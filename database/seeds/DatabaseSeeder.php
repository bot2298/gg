<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SpecializationSeeder::class);
        $this->call(CurrencySeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ExperienceSeeder::class);
        $this->call(SpecSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(RatingSeeder::class);
        $this->call(PageSeeder::class);
        $this->call(PrivacyPageSeeder::class);
    }
}
