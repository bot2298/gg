<?php

use Illuminate\Database\Seeder;

class SpecSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(config('app.debug')){
            factory(\App\Models\Spec::class, 25)->create();
        }
    }
}
