<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Illuminate\Support\Carbon;
use Laracasts\TestDummy\Factory as TestDummy;

class PrivacyPageSeeder extends Seeder
{
    public function run()
    {
        DB::table('pages')->insert([
            [
                'location' => 'site.policy.index',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }
}
