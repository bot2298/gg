<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class RatingSeeder extends Seeder
{
    public function run()
    {
        if(config('app.debug')){
            factory(\App\Models\Rating::class, 500)->create();
        }
    }
}
