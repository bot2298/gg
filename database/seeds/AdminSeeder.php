<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Illuminate\Support\Carbon;
use Laracasts\TestDummy\Factory as TestDummy;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdminSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'Адвокати.Статистика', 'guard_name' => 'backpack']);
        Permission::create(['name' => 'Адвокати.Заявки', 'guard_name' => 'backpack']);
        Permission::create(['name' => 'Адвокати.Профілі', 'guard_name' => 'backpack']);
        Permission::create(['name' => 'Адвокати.Розсилка-повідомлень', 'guard_name' => 'backpack']);
        Permission::create(['name' => 'Адвокати.Скарги-на-адвокатів', 'guard_name' => 'backpack']);

        Permission::create(['name' => 'Клієнти.Статистика', 'guard_name' => 'backpack']);
        Permission::create(['name' => 'Клієнти.Запити', 'guard_name' => 'backpack']);
        Permission::create(['name' => 'Клієнти.Скарги-на-запити', 'guard_name' => 'backpack']);
        Permission::create(['name' => 'Клієнти.Оцінки-та-коментарі', 'guard_name' => 'backpack']);
        Permission::create(['name' => 'Клієнти.Скарги-на-оцінки-та-коментарі', 'guard_name' => 'backpack']);

        Permission::create(['name' => 'Переклади', 'guard_name' => 'backpack']);
        Permission::create(['name' => 'SEO', 'guard_name' => 'backpack']);
        Permission::create(['name' => 'Фідбек', 'guard_name' => 'backpack']);
        Permission::create(['name' => 'Валюти', 'guard_name' => 'backpack']);

        $role = Role::create(['name' => 'Адміністратор', 'guard_name' => 'backpack']);
        $role->givePermissionTo(Permission::all());

        $super_admin = \App\Models\Manager::create([
            'email' => 'admin@mail.com',
            'password' => Hash::make('123456wW!'),
            'created_at' => Carbon::now(),
            'api_token' => \Illuminate\Support\Str::random(60),
            'email_verified_at' => Carbon::now(),
        ]);
        $super_admin->assignRole('Адміністратор');
    }
}
