<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class PageSeeder extends Seeder
{
    private $specs = [
        // url => spec key
        'administratyvni-spravy' => 'gov',
        'alimenty' => 'alimenty',
        'finansovi-spravy' => 'bank',
        'viyskovi-spravy' => 'military',
        'gosporadski-spravy' => 'gospo',
        'dtp' => 'dtp',
        'emigraciya' => 'emigration',
        'zhytlovi-spravi' => 'house',
        'zakhyst-prav-spozhyvachiv' => 'consumer',
        'zemelni-spravy' => 'land',
        'narkotyky' => 'narco',
        'intelektualna-vlasnist' => 'intel',
        'kredyty' => 'credit',
        'kryminalni-srpavy' => 'krimi',
        'medychni-spravy' => 'med',
        'mytni-spravy' => 'customs',
        'mizhnarodni-spravy' => 'intl',
        'morski-perevezennya' => 'sea',
        'nerukhomist' => 'estate',
        'pensiyni-spravy' => 'pens',
        'povitryane-pravo' => 'avia',
        'podatkovi-spravy' => 'tax',
        'rozluchennia' => 'divorce',
        'silske-gospodarstvo' => 'selo',
        'simeyni-spravy' => 'family',
        'spadshyna' => 'heritage',
        'admin-pravoporushennia' => 'admin',
        'srakhuvannia' => 'insurance',
        'trudovi-spory' => 'work',
        'tsyvilni-spravy' => 'civil',
    ];

    private $regions = [
        // url => region key
        'kyiv' => 'ukraine_kyiv',
        'kharkiv' => 'ukraine_kharkivska',
        'dnipro' => 'ukraine_dnipropetrovska',
        'odesa' => 'ukraine_odeska',
        'zaporizhzhia' => 'ukraine_zaporizka',
        'lviv' => 'ukraine_lvivska',
        'mykolaiv' => 'ukraine_mykolaivska',
        'vinnytsia' => 'ukraine_vinnytska',
        'kherson' => 'ukraine_khersonska',
        'poltava' => 'ukraine_poltavska',
        'chernihiv' => 'ukraine_chernihivska',
        'cherkasy' => 'ukraine_cherkaska',
        'sumy' => 'ukraine_sumska',
        'zhytomyr' => 'ukraine_zhytomyrska',
        'khmelnystkyi' => 'ukraine_khmelnytska',
        'kropyvnytskyi' => 'ukraine_kirovohradska',
        'rivne' => 'ukraine_rivnenska',
        'chernivtsi' => 'ukraine_chernivetska',
        'ternopil' => 'ukraine_ternopilska',
        'ivano-frankivsk' => 'ukraine_ivanofrankivska',
        'lutsk' => 'ukraine_volynska',
        'uzhhorod' => 'ukraine_zakarpatska',
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            [
                'location' => 'site.home.index',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'location' => 'site.about.index',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'location' => 'site.bookmarks.index',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'location' => 'site.dossier.index',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'location' => 'site.request.index',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'location' => 'site.tracker.index',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'location' => 'site.faq.index',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);

        //dossier categories
        foreach ($this->regions as $region_url => $region_key){
            // regions
            $region_page = new \App\Models\Page();
            $region_page->location = $region_url;
            $region = \App\Models\Region::where('name', $region_key)->first();
            $region_page->region_id = $region->id;
            $region_page->save();
        }

        foreach ($this->specs as $spec_url => $spec_key){
            // specs
            $spec_page = new \App\Models\Page();
            $spec_page->location = $spec_url;
            $spec = \App\Models\Specialization::where('name', $spec_key)->first();
            $spec_page->specialization_id = $spec->id;
            $spec_page->save();

            foreach ($this->regions as $region_url => $region_key){
                // specs + regions
                $region_spec_page = new \App\Models\Page();
                $region_spec_page->location = $spec_url."-".$region_url;
                $region = \App\Models\Region::where('name', $region_key)->first();
                $region_spec_page->region_id = $region->id;
                $region_spec_page->specialization_id = $spec->id;
                $region_spec_page->save();
            }
        }
    }
}
